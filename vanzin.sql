-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 24-Ago-2020 às 15:08
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `vanzin`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresas`
--

CREATE TABLE `empresas` (
  `id` int(11) NOT NULL,
  `razao_social` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `fantasia` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnpj` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'CNJP OU CPF',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `nr_bombas` int(255) DEFAULT NULL,
  `marca_bombas` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `bicos` int(255) DEFAULT NULL,
  `bandeira` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `software` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `postos_rede` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `nr_postos` int(11) DEFAULT NULL,
  `mecanico_atende` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `observacoes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `cidade` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `regiao_atua` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credenciamento_inmetro` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_constituicao` date NOT NULL,
  `nr_funcionarios` int(255) DEFAULT NULL,
  `credenciamento_crea` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filiais` int(255) NOT NULL,
  `nr_tecnicos` int(255) NOT NULL,
  `representante_legal` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insc_estadual` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `produtos_representa` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo_cadastro_id` int(11) DEFAULT NULL,
  `dthr_cadastro` timestamp NULL DEFAULT current_timestamp(),
  `cep` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `venc_inmetro` date NOT NULL,
  `dt_treinamento` date DEFAULT NULL,
  `tp_cadastro` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'PJ',
  `contrato_social` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `cartao_cnpj` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo_ibge_cidade` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fl_interesse_curso` int(11) NOT NULL DEFAULT 0,
  `fl_inadimplencia` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `empresas`
--

INSERT INTO `empresas` (`id`, `razao_social`, `fantasia`, `cnpj`, `email`, `telefone`, `endereco`, `nr_bombas`, `marca_bombas`, `bicos`, `bandeira`, `software`, `postos_rede`, `nr_postos`, `mecanico_atende`, `observacoes`, `cidade`, `estado`, `pais`, `bairro`, `regiao_atua`, `credenciamento_inmetro`, `data_constituicao`, `nr_funcionarios`, `credenciamento_crea`, `filiais`, `nr_tecnicos`, `representante_legal`, `insc_estadual`, `produtos_representa`, `tipo_cadastro_id`, `dthr_cadastro`, `cep`, `venc_inmetro`, `dt_treinamento`, `tp_cadastro`, `contrato_social`, `cartao_cnpj`, `codigo_ibge_cidade`, `latitude`, `longitude`, `fl_interesse_curso`, `fl_inadimplencia`) VALUES
(2, 'WERTCO IND. COM. E SERVIÇOS DE MANUTENÇÃO EM BOMBAS LTDA.', 'WERTCO', '27.314.980/0001-53', 'comercial@wertco.com.br', '(11) 3900 - 2565', 'Avenida Getúlio Vargas, 280 - Cj. 415', NULL, '', NULL, '', '', '', NULL, '', NULL, 'Arujá', 'SP', 'Brasil', 'jardim angêlo', 'País inteiro', '1231', '0000-00-00', NULL, '123', 0, 0, '123', '188103734119', NULL, 1, NULL, '07400-230', '0000-00-00', '0000-00-00', 'PJ', '', NULL, '', '-24.3384622', '-47.644051', 0, 0),
(3461, 'MIL CRIACAO E DESENVOLVIMENTO WEB LTDA', 'MIL CRIACAO E DESENVOLVIMENTO WEB LTDA', '12.481.170/0001-14', 'email@email.com', '(53) 3028-3044', 'AV SAO FRANCISCO DE PAULA, 2722 ', NULL, '', NULL, '', '', '', NULL, '', NULL, 'PELOTAS', 'RS', 'Brasil', 'AREAL', NULL, NULL, '0000-00-00', NULL, NULL, 0, 0, NULL, 'insc', NULL, 1, '2020-08-17 22:57:25', '96080730', '0000-00-00', NULL, 'PJ', NULL, 'C:\\xampp2\\htdocs\\vanzin/Consultas/Receita_Federal/Receita_12481170000114_18-08-2020.html', NULL, NULL, NULL, 0, 0),
(3462, 'MINASUL TRANSPORTES LTDA', 'MINASUL TRANSPORTES LTDA', '93.380.624/0001-62', 'email@email.com.br', '53 984073868', 'R NACOES UNIDAS, 143 ', NULL, '', NULL, '', '', '', NULL, '', NULL, 'PELOTAS', 'RS', 'Brasil', 'TRES VENDAS', NULL, NULL, '0000-00-00', NULL, NULL, 0, 0, NULL, 'insc', NULL, 1, '2020-08-17 23:03:43', '96065100', '0000-00-00', NULL, 'PJ', NULL, 'C:\\xampp2\\htdocs\\vanzin/Consultas/Receita_Federal/Receita_93380624000162_18-08-2020.html', NULL, NULL, NULL, 0, 0),
(3463, 'ARROZEIRA PELOTAS INDUSTRIA E COMERCIO DE CEREAIS LTDA', 'ARROZEIRA PELOTAS', '97.371.843/0001-36', 'email@email.com.br', '(53) 3273-7488', 'AV FERNANDO OSORIO, 5552 ', NULL, '', NULL, '', '', '', NULL, '', NULL, 'PELOTAS', 'RS', 'Brasil', 'TRES VENDAS', NULL, NULL, '0000-00-00', NULL, NULL, 0, 0, NULL, 'teste', NULL, 1, '2020-08-17 23:07:25', '96070740', '0000-00-00', NULL, 'PJ', NULL, 'C:\\xampp2\\htdocs\\vanzin/Consultas/Receita_Federal/Receita_97371843000136_18-08-2020.html', NULL, NULL, NULL, 0, 0),
(3464, 'SAGRES OPERACOES PORTUARIAS LTDA', 'SAGRES', '05.291.903/0001-92', 'email@eamil.com.br', '(53) 3233-1133', 'AV MAJOR CARLOS PINTO, 530 532', NULL, '', NULL, '', '', '', NULL, '', NULL, 'RIO GRANDE', 'RS', 'Brasil', 'CIDADE NOVA', NULL, NULL, '0000-00-00', NULL, NULL, 0, 0, NULL, 'insc.', NULL, 1, '2020-08-18 00:08:35', '96211020', '0000-00-00', NULL, 'PJ', NULL, 'C:\\xampp2\\htdocs\\vanzin/Consultas/Receita_Federal/Receita_05291903000192_18-08-2020.html', NULL, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `estados`
--

CREATE TABLE `estados` (
  `id` int(11) NOT NULL,
  `codigo_uf` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `uf` char(2) NOT NULL,
  `regiao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `estados`
--

INSERT INTO `estados` (`id`, `codigo_uf`, `nome`, `uf`, `regiao`) VALUES
(1, 12, 'Acre', 'AC', 1),
(2, 27, 'Alagoas', 'AL', 2),
(3, 16, 'AmapÃ¡', 'AP', 1),
(4, 13, 'Amazonas', 'AM', 1),
(5, 29, 'Bahia', 'BA', 2),
(6, 23, 'CearÃ¡', 'CE', 2),
(7, 53, 'Distrito Federal', 'DF', 5),
(8, 32, 'EspÃ­rito Santo', 'ES', 3),
(9, 52, 'GoiÃ¡s', 'GO', 5),
(10, 21, 'MaranhÃ£o', 'MA', 2),
(11, 51, 'Mato Grosso', 'MT', 5),
(12, 50, 'Mato Grosso do Sul', 'MS', 5),
(13, 31, 'Minas Gerais', 'MG', 3),
(14, 15, 'ParÃ¡', 'PA', 1),
(15, 25, 'ParaÃ­ba', 'PB', 2),
(16, 41, 'ParanÃ¡', 'PR', 4),
(17, 26, 'Pernambuco', 'PE', 2),
(18, 22, 'PiauÃ­', 'PI', 2),
(19, 33, 'Rio de Janeiro', 'RJ', 3),
(20, 24, 'Rio Grande do Norte', 'RN', 2),
(21, 43, 'Rio Grande do Sul', 'RS', 4),
(22, 11, 'RondÃ´nia', 'RO', 1),
(23, 14, 'Roraima', 'RR', 1),
(24, 42, 'Santa Catarina', 'SC', 4),
(25, 35, 'SÃ£o Paulo', 'SP', 3),
(26, 28, 'Sergipe', 'SE', 2),
(27, 17, 'Tocantins', 'TO', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `acao` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dthr_alteracao` timestamp NOT NULL DEFAULT current_timestamp(),
  `ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `logs`
--

INSERT INTO `logs` (`id`, `controller`, `model`, `acao`, `user_id`, `dthr_alteracao`, `ip`) VALUES
(1, 'Área Administrador | editar empresa', 'empresas', 'EDIÇÃO', 2, '2020-08-17 22:46:57', '::1'),
(2, 'Área Administrador | cadastro empresa', 'empresas', 'INSERÇÃO', 2, '2020-08-17 22:57:25', '::1'),
(3, 'Área Administrador | cadastro empresa', 'empresas', 'INSERÇÃO', 2, '2020-08-17 23:03:43', '::1'),
(4, 'Área Administrador | cadastro empresa', 'empresas', 'INSERÇÃO', 2, '2020-08-17 23:07:25', '::1'),
(5, 'Área Administrador | atualiza usuário', 'usuarios', 'EDIÇÂO', 2, '2020-08-17 23:31:33', '::1'),
(6, 'Área Administrador | atualiza usuário', 'usuarios', 'EDIÇÂO', 2, '2020-08-17 23:31:40', '::1'),
(7, 'Área Administrador | atualiza usuário', 'usuarios', 'EDIÇÂO', 2, '2020-08-17 23:34:06', '::1'),
(8, 'Área Administrador | cadastro empresa', 'empresas', 'INSERÇÃO', 2, '2020-08-18 00:08:35', '::1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `log_campos`
--

CREATE TABLE `log_campos` (
  `id` int(11) NOT NULL,
  `campo` varchar(255) DEFAULT NULL,
  `novo_valor` varchar(255) DEFAULT NULL,
  `log_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `log_campos`
--

INSERT INTO `log_campos` (`id`, `campo`, `novo_valor`, `log_id`) VALUES
(1, 'razao_social', 'WERTCO IND. COM. E SERVIÇOS DE MANUTENÇÃO EM BOMBAS LTDA.', 1),
(2, 'id', '2', 1),
(3, 'fantasia', 'WERTCO', 1),
(4, 'cnpj', '27.314.980/0001-53', 1),
(5, 'insc_estadual', '188103734119', 1),
(6, 'email', 'comercial@wertco.com.br', 1),
(7, 'telefone', '(11) 3900 - 2565', 1),
(8, 'endereco', 'Avenida Getúlio Vargas, 280 - Cj. 415', 1),
(9, 'bairro', 'jardim angêlo', 1),
(10, 'cep', '07400-230', 1),
(11, 'cidade', 'Arujá', 1),
(12, 'estado', 'SP', 1),
(13, 'pais', 'Brasil', 1),
(14, 'credenciamento_inmetro', '1231', 1),
(15, 'venc_inmetro', '202020-11-30', 1),
(16, 'tipo_cadastro_id', '1', 1),
(17, 'razao_social', 'MIL CRIACAO E DESENVOLVIMENTO WEB LTDA', 2),
(18, 'fantasia', 'MIL CRIACAO E DESENVOLVIMENTO WEB LTDA', 2),
(19, 'cnpj', '12.481.170/0001-14', 2),
(20, 'telefone', '(53) 3028-3044', 2),
(21, 'endereco', 'AV SAO FRANCISCO DE PAULA, 2722 ', 2),
(22, 'email', 'email@email.com', 2),
(23, 'cidade', 'PELOTAS', 2),
(24, 'estado', 'RS', 2),
(25, 'tipo_cadastro_id', '1', 2),
(26, 'insc_estadual', 'insc', 2),
(27, 'pais', 'Brasil', 2),
(28, 'bairro', 'AREAL', 2),
(29, 'cep', '96080730', 2),
(30, 'cartao_cnpj', 'C:\\xampp2\\htdocs\\vanzin/Consultas/Receita_Federal/Receita_12481170000114_18-08-2020.html', 2),
(31, 'razao_social', 'MINASUL TRANSPORTES LTDA', 3),
(32, 'fantasia', 'MINASUL TRANSPORTES LTDA', 3),
(33, 'cnpj', '93.380.624/0001-62', 3),
(34, 'telefone', '53 984073868', 3),
(35, 'endereco', 'R NACOES UNIDAS, 143 ', 3),
(36, 'email', 'email@email.com.br', 3),
(37, 'cidade', 'PELOTAS', 3),
(38, 'estado', 'RS', 3),
(39, 'tipo_cadastro_id', '1', 3),
(40, 'insc_estadual', 'insc', 3),
(41, 'pais', 'Brasil', 3),
(42, 'bairro', 'TRES VENDAS', 3),
(43, 'cep', '96065100', 3),
(44, 'cartao_cnpj', 'C:\\xampp2\\htdocs\\vanzin/Consultas/Receita_Federal/Receita_93380624000162_18-08-2020.html', 3),
(45, 'razao_social', 'ARROZEIRA PELOTAS INDUSTRIA E COMERCIO DE CEREAIS LTDA', 4),
(46, 'fantasia', 'ARROZEIRA PELOTAS', 4),
(47, 'cnpj', '97.371.843/0001-36', 4),
(48, 'telefone', '(53) 3273-7488', 4),
(49, 'endereco', 'AV FERNANDO OSORIO, 5552 ', 4),
(50, 'email', 'email@email.com.br', 4),
(51, 'cidade', 'PELOTAS', 4),
(52, 'estado', 'RS', 4),
(53, 'tipo_cadastro_id', '1', 4),
(54, 'insc_estadual', 'teste', 4),
(55, 'pais', 'Brasil', 4),
(56, 'bairro', 'TRES VENDAS', 4),
(57, 'cep', '96070740', 4),
(58, 'cartao_cnpj', 'C:\\xampp2\\htdocs\\vanzin/Consultas/Receita_Federal/Receita_97371843000136_18-08-2020.html', 4),
(59, 'nome', 'Luciano', 5),
(60, 'id', '3925', 5),
(61, 'email', 'email2@teste.com.br', 5),
(62, 'cpf', '944.667.290-26', 5),
(63, 'telefone', '(99) 8989 - 89898', 5),
(64, 'celular', '(94) 4667 - 29026', 5),
(65, 'tipo_cadastro_id', '1', 5),
(66, 'empresa_id', '3461', 5),
(67, 'subtipo_cadastro_id', '1', 5),
(68, 'ativo', '1', 5),
(69, 'senha', '14e1b600b1fd579f47433b88e8d85291', 5),
(70, 'nome', 'Luciano', 6),
(71, 'id', '3925', 6),
(72, 'email', 'email2@teste.com.br', 6),
(73, 'cpf', '944.667.290-26', 6),
(74, 'telefone', '(99) 8989 - 89898', 6),
(75, 'celular', '(94) 4667 - 29026', 6),
(76, 'tipo_cadastro_id', '1', 6),
(77, 'empresa_id', '3461', 6),
(78, 'subtipo_cadastro_id', '1', 6),
(79, 'ativo', '1', 6),
(80, 'senha', '14e1b600b1fd579f47433b88e8d85291', 6),
(81, 'nome', 'Luciano', 7),
(82, 'id', '3925', 7),
(83, 'email', 'email2@teste.com.br', 7),
(84, 'cpf', '944.667.290-26', 7),
(85, 'telefone', '(99) 8989 - 89898', 7),
(86, 'celular', '(94) 4667 - 29026', 7),
(87, 'tipo_cadastro_id', '1', 7),
(88, 'empresa_id', '3461', 7),
(89, 'subtipo_cadastro_id', '1', 7),
(90, 'ativo', '1', 7),
(91, 'senha', '14e1b600b1fd579f47433b88e8d85291', 7),
(92, 'razao_social', 'SAGRES OPERACOES PORTUARIAS LTDA', 8),
(93, 'fantasia', 'SAGRES', 8),
(94, 'cnpj', '05.291.903/0001-92', 8),
(95, 'telefone', '(53) 3233-1133', 8),
(96, 'endereco', 'AV MAJOR CARLOS PINTO, 530 532', 8),
(97, 'email', 'email@eamil.com.br', 8),
(98, 'cidade', 'RIO GRANDE', 8),
(99, 'estado', 'RS', 8),
(100, 'tipo_cadastro_id', '1', 8),
(101, 'insc_estadual', 'insc.', 8),
(102, 'pais', 'Brasil', 8),
(103, 'bairro', 'CIDADE NOVA', 8),
(104, 'cep', '96211020', 8),
(105, 'cartao_cnpj', 'C:\\xampp2\\htdocs\\vanzin/Consultas/Receita_Federal/Receita_05291903000192_18-08-2020.html', 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `subtipo_cadastros`
--

CREATE TABLE `subtipo_cadastros` (
  `id` int(11) NOT NULL,
  `tipo_cadastro_pai` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `nvl` int(11) NOT NULL,
  `obs` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `subtipo_cadastros`
--

INSERT INTO `subtipo_cadastros` (`id`, `tipo_cadastro_pai`, `descricao`, `nvl`, `obs`) VALUES
(1, 4, 'Responsável Legal', 1, 'Possui acesso ao contrato de serviço, realiza a gestão dos funcionários que possuirão acesso ao sistema da wertco, acesso a solicitação e atualização do cartão técnico, downloads e gerador json.'),
(2, 4, 'Gerente Técnico', 2, 'Realiza a gestão dos funcionários que possuirão acesso ao sistema da wertco, acesso a solicitação e atualização do cartão técnico,  downloads e gerador json'),
(3, 4, 'Funcionário', 3, 'Possuirá acesso a solicitação e atualização do cartão técnico,  downloads e gerador json.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_cadastros`
--

CREATE TABLE `tipo_cadastros` (
  `id` int(11) NOT NULL,
  `descricao` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipo_cadastros`
--

INSERT INTO `tipo_cadastros` (`id`, `descricao`) VALUES
(1, 'clientes'),
(2, 'representantes'),
(3, 'mecanicos'),
(4, 'tecnicos'),
(5, 'administrador comercial'),
(6, 'indicadores'),
(7, 'administrador tecnico'),
(8, 'administrador geral'),
(9, 'assistencia tecnica'),
(10, 'indicadores2'),
(11, 'feira'),
(12, 'representantes2'),
(13, 'financeiro'),
(14, 'almoxarifado'),
(15, 'qualidade'),
(16, 'suprimentos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `cpf` varchar(20) DEFAULT NULL,
  `telefone` varchar(50) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `endereco` text DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `pais` varchar(30) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `tipo_cadastro_id` int(11) NOT NULL,
  `dthr_cadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `ativo` int(11) NOT NULL DEFAULT 0,
  `foto` varchar(255) NOT NULL,
  `dt_treinamento` date DEFAULT NULL,
  `local_treinamento` varchar(250) NOT NULL,
  `fl_termos` int(11) NOT NULL DEFAULT 0,
  `dthr_aceite_termos` datetime NOT NULL DEFAULT current_timestamp(),
  `subtipo_cadastro_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `cpf`, `telefone`, `celular`, `email`, `endereco`, `cidade`, `estado`, `pais`, `empresa_id`, `senha`, `tipo_cadastro_id`, `dthr_cadastro`, `ativo`, `foto`, `dt_treinamento`, `local_treinamento`, `fl_termos`, `dthr_aceite_termos`, `subtipo_cadastro_id`) VALUES
(2, 'Mateus Santos', '008.713.100-58', '(53) 9840-73868', '(53) 9840-73868', 'webmaster@wertco.com.br', 'Avenida ferreira viana, 2962,m casa 260', NULL, NULL, '', 2, 'f36363b4ec88a8be2baf199aa07110db', 8, '2018-02-09 16:38:15', 1, 'img_perfil/20cacf38352f88b3f92df41b224c1153.jpg', '0000-00-00', '', 1, '2018-12-12 08:14:20', 0),
(3924, 'mateus', '944.667.290-26', '(23) 1312 - 31231', '(99) 9999 - 99999', 'email@teste.com.br', NULL, NULL, NULL, '', 3461, '74be16979710d4c4e7c6647856088456', 1, '2020-08-17 23:21:07', 1, '', NULL, '', 0, '2020-08-17 20:21:07', 0),
(3925, 'Luciano', '944.667.290-26', '(99) 8989 - 89898', '(94) 4667 - 29026', 'email2@teste.com.br', NULL, NULL, NULL, '', 3461, '14e1b600b1fd579f47433b88e8d85291', 1, '2020-08-17 23:30:50', 1, '', NULL, '', 0, '2020-08-17 20:30:50', 1),
(3926, 'wagner', '151.981.000-80', '(11) 1111 - 11111', '(11) 1111 - 11111', 'email3@teste.com.br', NULL, NULL, NULL, '', 3461, '74be16979710d4c4e7c6647856088456', 1, '2020-08-17 23:36:52', 1, '', NULL, '', 0, '2020-08-17 20:36:52', 0),
(3927, 'Roberto', '392.779.580-14', '(39) 2779 - 58014', '(39) 2779 - 58014', 'email4@teste.com.br', NULL, NULL, NULL, '', 3461, '224cf2b695a5e8ecaecfb9015161fa4b', 1, '2020-08-17 23:38:15', 1, '', NULL, '', 0, '2020-08-17 20:38:15', 0),
(3928, 'tste', '001.011.940-05', '(12) 3891 - 73981', '(12) 3123 - 12312', 'doidovarrido@gmail.com', NULL, NULL, NULL, '', 3464, '14e1b600b1fd579f47433b88e8d85291', 1, '2020-08-18 00:10:41', 1, '', NULL, '', 0, '2020-08-17 21:10:41', 0);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_empresas_x_tipo_cadastros` (`tipo_cadastro_id`);

--
-- Índices para tabela `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `log_campos`
--
ALTER TABLE `log_campos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `subtipo_cadastros`
--
ALTER TABLE `subtipo_cadastros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subtipo_cadastro_x_tipo_cadastro` (`tipo_cadastro_pai`);

--
-- Índices para tabela `tipo_cadastros`
--
ALTER TABLE `tipo_cadastros`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_usuario_X_empresa` (`empresa_id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3465;

--
-- AUTO_INCREMENT de tabela `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de tabela `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `log_campos`
--
ALTER TABLE `log_campos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT de tabela `subtipo_cadastros`
--
ALTER TABLE `subtipo_cadastros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `tipo_cadastros`
--
ALTER TABLE `tipo_cadastros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3929;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `subtipo_cadastros`
--
ALTER TABLE `subtipo_cadastros`
  ADD CONSTRAINT `fk_subtipo_cadastro_x_tipo_cadastro` FOREIGN KEY (`tipo_cadastro_pai`) REFERENCES `tipo_cadastros` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
