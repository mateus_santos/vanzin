<?php

class SubAtividadeModel extends CI_Model {
	
	public function insert($subatividade)
	{
        $this->db->insert('chamado_subatividade', $subatividade);
		return $this->db->insert_id();
    }
	
	public function find($id)
    {
		$this->db->select('cs.*');
		$this->db->select('e.razao_social tecnico');
		$this->db->select('e.endereco endereco');
		$this->db->select('UPPER(u.nome) contato');
		$this->db->select("CONCAT(e.cidade, '/',e.estado) cidade");
		$this->db->select("DATE_FORMAT(DATE_ADD(ct.ultima_atualizacao, INTERVAL ct.periodo_renovacao MONTH), '%d/%m/%Y') validade");
		$this->db->join('empresas e', 'cs.parceiro_id = e.id');
		$this->db->join('usuarios u', 'cs.contato_id = u.id');
		$this->db->join('cartao_tecnicos ct', 'ct.usuario_id = u.id', 'left');
		$this->db->where('cs.id', $id);
		return $this->db->get('chamado_subatividade cs')->row_array();
    }
	
	public function update($subatividade)
    {
		$this->db->where('id', $subatividade['id']);
        $this->db->update('chamado_subatividade', $subatividade);        
        return $this->db->affected_rows();
	}
	
	public function select($atividade_id)
    {
		$this->db->select('*');
        $this->db->select('DATE_FORMAT(inicio, "%d/%m/%Y %H:%i:%s") inicio');
		$this->db->select('DATE_FORMAT(fim, "%d/%m/%Y %H:%i:%s") fim');
		$this->db->where('atividade_id', $atividade_id);
		$this->db->order_by('id');
		return $this->db->get('chamado_subatividade')->result_array();
    }

}
?>