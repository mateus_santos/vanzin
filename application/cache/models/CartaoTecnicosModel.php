<?php

class CartaoTecnicosModel extends CI_Model {

	public function add ($data) {
        
	    return $this->db->insert('cartao_tecnicos', $data);		

	}

    public function insereSolicitacaoCartao($data) {
        
        return $this->db->insert('cartao_tecnico_solicita', $data);     

    }

    public function selectCartoes(){

        $sql =  "SELECT ct.*,u.nome FROM cartao_tecnicos ct, usuarios u WHERE ct.usuario_id = u.id";
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function getCartao($id){

        $sql =  "SELECT ct.*,u.nome FROM cartao_tecnicos ct, usuarios u WHERE ct.usuario_id = u.id and ct.id=".$id;
        $query = $this->db->query($sql);
        return $query->row();   
    }
   
    public function atualizaStatus($dados)
    {
        $update = array(
            'ativo' => $dados['ativo']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('cartao_tecnicos', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function excluirCartao($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('cartao_tecnicos')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaCartao($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('cartao_tecnicos', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function verificaTag($tag){

        $sql =  "SELECT count(*) as total FROM cartao_tecnicos WHERE tag = '".$tag['tag']."'";
        $query = $this->db->query($sql);
        
        return $query->row();   
    }

    public function getDadosCartao($usuario_id){

        $sql =  "SELECT * FROM cartao_tecnicos WHERE usuario_id = '".$usuario_id."'";
        $query = $this->db->query($sql);
        
        return $query->row();      
    }

    public function insereLogCartao($data) {
        
        $this->db->insert('cartao_tecnico_log', $data);     

    }

    public function verificaSolicitacaoCartao($usuario_id){

        $sql =  "SELECT count(*) as total FROM cartao_tecnico_solicita WHERE solicitante_id = '".$usuario_id."'";
        $query = $this->db->query($sql);
        
        return $query->row();   
    }

    public function getSolicitacoes(){
        $sql =  "SELECT concat(e.cnpj,' - ',e.razao_social) as empresa,u.*,c.* FROM cartao_tecnico_solicita c, empresas e, usuarios u
                    WHERE u.empresa_id = e.id and
                          c.solicitante_id = u.id and                          
                          c.usuario_aceite_id = 0";

        $query = $this->db->query($sql);        
        return $query->result();
    }

    public function confirmarEnvio($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('cartao_tecnico_solicita', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function confirmarExclusaoSolicitacao($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('cartao_tecnico_solicita')){
            return true;
        }else{
            return false;
        }
    }
       
}
?>