<?php

class ChamadoSolucaoModel extends CI_Model {
	
	public function select($causa_id)
    {
		$this->db->select('cs.*');
		$this->db->select('COUNT(cacs.id) total');
		$this->db->where('cs.causa_id', $causa_id);
		$this->db->join('chamado_atividade_causa_solucao cacs', 'cs.id = cacs.solucao_id', 'left');
		$this->db->order_by('total', 'DESC');
		$this->db->group_by('cs.id');
        return $this->db->get('chamado_solucao cs')->result_array();
    }
	
	public function insert($solucao)
	{
        $this->db->insert('chamado_solucao', $solucao);
		return $this->db->insert_id();
    }
	
	public function findByDescricao($causa_id, $descricao)
	{
		$this->db->where('descricao', $descricao);
		$this->db->where('causa_id', $causa_id);
        return $this->db->get('chamado_solucao')->row_array();
	}

}
?>