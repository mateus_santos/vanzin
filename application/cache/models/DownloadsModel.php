<?php

class DownloadsModel extends CI_Model {

	public function insereDownloads ($data) {
        
		return $this->db->insert('downloads', $data);
	}

    public function insereDownloadCadastros ($data) {
        
        return $this->db->insert('download_cadastros', $data);
    }
    
    public function getDownloads($id = null)
    {
        if( $id != NULL ){
            $where = ' and d.id='. $id ;
        }else{
            $where = ' and 1=1';
        }

    	$sql =  "SELECT d.*, group_concat( t.descricao ) as tipo, group_concat( t.id ) as tipo_id  FROM downloads d, download_cadastros dc, tipo_cadastros t where dc.tipo_cadastro_id = t.id and d.id = dc.download_id ".$where.' group by d.id';
    	$query = $this->db->query($sql);
    	return $query->result();

    }   

    public function getDownloadsPorArea($area)
    {
        if( $area == 'tecnicos' ){
            $where = ' and t.id=4';
        }elseif('clientes'){
            $where = ' and t.id=1';
        }

        $sql =  "SELECT d.*, group_concat( t.descricao ) as tipo, group_concat( t.id ) as tipo_id  FROM downloads d, download_cadastros dc, tipo_cadastros t where dc.tipo_cadastro_id = t.id and d.id = dc.download_id ".$where.' group by d.id';
        $query = $this->db->query($sql);
        return $query->result();

    }   

    public function getEstados()
    {
    	
    	$sql =  "SELECT * FROM estados";
    	$query = $this->db->query($sql);
    	return $query->result();
    	
    }   

    public function excluirDownloads($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('downloads')){
            return true;
        }else{
            return false;
        }
    }

    public function getDownloadsPorEstado($estado_id)
    {

    	$sql =  "SELECT count(*) as total from downloads WHERE estado_id =".$estado_id;
    	$query = $this->db->query($sql);
    	return $query->result_array();

    }

    public function getDownloadsById($id)
    {

    	$sql =  "SELECT i.id, i.valor_tributo,i.estado_id FROM downloads i WHERE i.id=".$id;
    	$query = $this->db->query($sql);
    	return $query->result_array();

    } 

    public function atualizaDownloads($dados)
    {
                
        $this->db->where('id', $dados['id']);

        if($this->db->update('downloads', $dados)){
            return true;
        }else{
            return false;
        }

    }

      public function excluirTipoCadastro($id){
        
        $this->db->where('download_id', $id);
        if($this->db->delete('download_cadastros')){
            return true;
        }else{
            return false;
        }
    }

}
?>