<?php

class ZonaAtuacaoModel extends CI_Model {
	
	public function adicionar($data){
		
		$this->db->insert('zona_atuacao', $data);
		return $this->db->insert_id();
	}

	public function buscaEstadosDisponiveis(){
        $sql = "	SELECT e.* FROM estados e 
        			WHERE e.uf not in (select estado from zona_atuacao)";
        
        $query  =   $this->db->query($sql);       

        return $query->result_array();
    }

    public function select(){
    	$sql = "	SELECT 	z.id, z.usuario_id, u.nome, group_concat(z.estado) as estados 
    				FROM 	zona_atuacao z, estados e, usuarios u 
    				WHERE 	z.usuario_id = u.id and 
    						z.estado = e.uf 
    				GROUP BY z.usuario_id ";

    	$query  =   $this->db->query($sql);       

        return $query->result_array();
    }

    public function buscaEstadosUsuarios($usuario_id){
        $sql = "	SELECT 	z.estado, e.nome 	FROM zona_atuacao z, estados e
        			WHERE 	z.estado = e.uf 	and  
        					z.usuario_id = ".$usuario_id;
        
        $query  =   $this->db->query($sql);       

        return $query->result_array();
    }

    public function delete($usuario_id){ 
        
        $this->db->where('usuario_id', $usuario_id);
        if(    $this->db->delete('zona_atuacao') ){
            return true;
        }else{
            return false;
        }
    }

    public function buscarEmailResp($estado){
    	$sql = "	SELECT u.email, u.nome FROM orcamentos o, empresas e, zona_atuacao z, usuarios u
    				WHERE 	o.empresa_id = e.id and 
    						z.estado = e.estado and 
    						z.usuario_id = u.id and 
    						z.estado = '".$estado."'";
    	$query  =   $this->db->query($sql);       

        return $query->row_array();

    }

    public function buscaConsultorWertco(){
        
        $sql = "SELECT  DISTINCT z.usuario_id, u.nome 
                FROM    usuarios u, zona_atuacao z 
                WHERE   u.id = z.usuario_id ";
        
        $query  =   $this->db->query($sql);  
        
        return $query->result_array();

    }
}

?>