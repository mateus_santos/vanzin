<?php

class ConfiguracaoModel extends CI_Model {
	
	public function save($configuracao)
	{
		$this->db->where('empresa_id',$configuracao['empresa_id']);
		$cfg = $this->db->get('configuracao')->row_array();
		if(count($cfg) > 0){
			$this->db->where('id', $cfg['id']);
			$this->db->update('configuracao', $configuracao);
			$retorno = $this->db->affected_rows();
		} else {
			$this->db->insert('configuracao', $configuracao);
			$retorno = $this->db->insert_id();
		}
		return $retorno;
    }
	
	public function findByEmpresa($empresa_id)
    {
		$this->db->where('c.empresa_id', $empresa_id);
		return $this->db->get('configuracao c')->row_array();
    }
	
	
}
?>