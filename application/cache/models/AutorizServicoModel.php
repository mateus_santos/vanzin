<?php

class AutorizServicoModel extends CI_Model {	
	
	public function insereAutorizacao($autorizacao)
	{
        $this->db->insert('chamado_autoriz_servico', $autorizacao);
		return $this->db->insert_id();
    }
	
	public function buscaTecnico($chamado_id, $tecnico_id)
    {
		
		$sql = "SELECT 	t.*, e.razao_social, e.cnpj, p.razao_social as posto, p.cnpj as posto_cnpj
				FROM 	usuarios as t,  chamado_autoriz_servico as cas, empresas e, empresas p
				WHERE 	t.id = cas.tecnico_id and
						t.empresa_id = e.id and
						cas.cliente_id = p.id and
						cas.chamado_id = ".$chamado_id." and
						cas.tecnico_id = ".$tecnico_id;

		return $this->db->query($sql)->row_array();
    }

    public function buscaSolicitacaoServico($chamado_id){
    	$sql = "SELECT 	t.*, e.razao_social, e.cnpj, p.razao_social as posto, p.cnpj as posto_cnpj, cas.dthr_solicitacao, cas.dthr_aceite, cas.id as solicitacao_id, cas.email_tecnico, cas.tipo, cas.endereco_cliente, cas.bairro_cliente, cas.cep_cliente, cas.cidade_cliente, cas.uf_cliente, cas.solicitacao, cas.fl_aceite
				FROM 	usuarios as t,  chamado_autoriz_servico as cas, empresas e, empresas p
				WHERE 	t.id = cas.tecnico_id and
						t.empresa_id = e.id and
						cas.cliente_id = p.id and
						cas.chamado_id = ".$chamado_id;

		return $this->db->query($sql)->result_array();
    }
	
    public function buscaTotalAceite($chamado_id){

    	$sql = "SELECT count(*) as total FROM chamado_autoriz_servico WHERE chamado_id =".$chamado_id." and dthr_aceite != '0000-00-00 00:00:00'";
    	return $this->db->query($sql)->row_array();
    }

	public function update($update)
    {
    	
		$this->db->where('chamado_id', $update['chamado_id']);            
		$this->db->where('tecnico_id', $update['tecnico_id']);            
		return  $this->db->update('chamado_autoriz_servico', $update);   
    }
	
	
}
?>