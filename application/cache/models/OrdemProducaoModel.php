<?php

class OrdemProducaoModel extends CI_Model {
	
	public function find($id)
	{
		$this->db->select('op.*');
		$this->db->select('itens.descricao item');
		$this->db->where('op.id', $id);
		$this->db->join('itens', 'op.item_id = itens.id');
		return $this->db->get("ordem_producao op")->row_array();
	}
		
	
	public function selectOps()
    {
        $sql = "SELECT o.*, s.descricao as status FROM ordem_producao o, status_op s, itens i
				where i.id = o.item_id and o.status_op_id = s.id";
        $query = $this->db->query($sql);
        return $query->result_array();

    }

	
	public function insereOp ($data) {
        
		return $this->db->insert('ordem_producao', $data);
	}

   
    public function verificaOp($sql)
    {
    	
    	$query = $this->db->query($sql);
    	return $query->row_array();

    }

    public function verificaQtd($id)
    {
        $sql = "SELECT qtd,observacao,combustivel FROM ordem_producao WHERE id = ".$id;
        $query = $this->db->query($sql);
        return $query->row_array();

    }
    
    public function excluirOrdemProducao($id,$motivo){
        
        $this->db->where('id', $id);

        $update = array(    'status_op_id'          =>  4,
                            'motivo_cancelamento'   =>  $motivo    ) ;

        if($this->db->update('ordem_producao',  $update)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizaQtdObs($dados)
    {
        $update = array('qtd' 	        =>	$dados['qtd'],
                        'observacao'    =>  $dados['observacao'],
                        'combustivel'   =>  $dados['combustivel']);
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('ordem_producao', $update)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizarOrdemProducao($dados)
    { 
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('ordem_producao', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function verificaOpFechadaPorPedido($pedido_id){
        
        $sql = "SELECT count(*) AS total FROM ordem_producao WHERE status_op_id != 3 and pedido_id=".$pedido_id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getOpsByPedido($pedido_id){
        
        $sql = "SELECT o.*, p.arquivo as upload_bb FROM ordem_producao o, pedidos p WHERE o.pedido_id = p.id and o.pedido_id=".$pedido_id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }


  
}
?>