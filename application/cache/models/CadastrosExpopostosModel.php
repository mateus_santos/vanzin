<?php 

class CadastrosExpopostosModel extends CI_Model {

	public function insere($data){

        if($this->db->insert('cadastros_expopostos', $data)){
            
            return true;
        }else{
            return false;
        }

	}

	public function insereLogCampos($data){
		if($this->db->insert('log_campos', $data)){

            return true;
        }else{
        	
            return false;
        }		
	}
    
}