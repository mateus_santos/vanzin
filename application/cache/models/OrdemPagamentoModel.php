<?php

class OrdemPagamentoModel extends CI_Model {
	
	public function find($id)
	{
		$this->db->select('op.*');
        $this->db->select('c.cliente_id, c.contato_id, c.tipo_id,c.prioridade_id, c.status_id,c.inicio,c.fim,c.usuario_id,c.ordem_pagamento_id');
		$this->db->select('d.descricao despesa');
        $this->db->select('itens.*');
        $this->db->select('concat(e.cnpj," - ",e.razao_social) cliente');
        $this->db->select('e.cnpj,e.razao_social');
		$this->db->where('op.id', $id);
		$this->db->join('ordem_pagamento_itens itens', 'op.id = itens.ordem_pagamento_id');
        $this->db->join('despesas d', 'itens.despesa_id = d.id');
        $this->db->join('chamado c', 'op.chamado_id = c.id', 'left');
        $this->db->join('empresas e', 'c.cliente_id = e.id', 'left');
		return $this->db->get("ordem_pagamento op")->result_array();
	}
		
	
	public function getOpsByChamado($id)
    {
       
        $sql = "SELECT  o.*, t.descricao as tipo, i.qtd, d.descricao as despesa, i.valor, i.id as item_id, i.dt_servico, s.descricao as status, e.email as email_tecnico    
                FROM    ordem_pagamento o, ordem_pagamento_status s, ordem_pagamento_itens i, tipo_despesas t, despesas d, empresas e
				WHERE   e.id = o.favorecido_id and
                        o.id = i.ordem_pagamento_id and 
                        i.despesa_id = d.id and
                        o.status_id = s.id and
                        d.tipo_despesa_id = t.id and 
                        o.chamado_id = ".$id."
                ORDER BY   i.dt_servico ASC";

        $query = $this->db->query($sql);
        return $query->result_array();

    }

	
	public function insereOp ($data) {
        
		return $this->db->insert('ordem_pagamento', $data);
	}

    public function insertItemOp ($data) {
        
        return $this->db->insert('ordem_pagamento_itens', $data);
    }

   
    public function verificaOp($sql)
    {
    	
    	$query = $this->db->query($sql);
    	return $query->row_array();

    }

    public function verificaQtd($id)
    {
        $sql = "SELECT qtd,observacao FROM ordem_pagamento WHERE id = ".$id;
        $query = $this->db->query($sql);
        return $query->row_array();

    }

    
    public function excluirOrdemProducao($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('ordem_pagamento')){
            return true;
        }else{
            return false;
        }
    }

    public function excluirItem($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('ordem_pagamento_itens')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaQtdObs($dados)
    {
        $update = array('qtd' 	        =>	$dados['qtd'],
                        'observacao'    =>  $dados['observacao']);
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('ordem_pagamento', $update)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizarOrdemPagamento($dados)
    { 
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('ordem_pagamento', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function insereOrdemPagtoHistorico($dados){

        $sql    =   "SELECT * FROM ordem_pagamento WHERE id=".$dados['id'];
        $query  =   $this->db->query($sql);
        $row    =   $query->row_array();

        $salvar = array(    "favorecido_id"         =>  $row['favorecido_id'], 
                            "valor_op"              =>  $row['valor_op'],
                            "status_id"             =>  2,
                            "descricao"             =>  $row['descricao'], 
                            "usuario_id"            =>  $dados['usuario_id'],
                            "dt_emissao"            =>  date('Y-m-d H:i:s'),
                            "chamado_id"            =>  $row['chamado_id'],
                            "ordem_pagamento_id"    =>  $dados['id'],
                            "email_emissao"         =>  $dados['email_emissao'] );

        if(  $this->db->insert('ordem_pagamento_historico', $salvar)  ){
            return true;
        }else{
            return false;
        }
    
    }

    public function verificaOpFechadaPorPedido($pedido_id){
        
        $sql = "SELECT count(*) AS total FROM ordem_pagamento WHERE status_op_id != 3 and pedido_id=".$pedido_id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getOpsByPedido($pedido_id){

        
        $sql = "SELECT  o.*, p.arquivo as upload_bb 
                FROM    ordem_pagamento o, pedidos p 
                WHERE   o.pedido_id = p.id and o.pedido_id=".$pedido_id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function listaHistoricoOrdemPagto($chamado_id){        
        $sql = "SELECT  o.*, date_format(o.dt_emissao,'%d/%m/%Y %H:%i:%s') as data_emis, u.nome, f.razao_social as favorecido
                FROM    ordem_pagamento_historico o, usuarios u, empresas f
                WHERE   o.usuario_id    =   u.id    and 
                        o.favorecido_id =   f.id    and
                        o.chamado_id    =   ".$chamado_id." GROUP BY o.id ";
                 
        $query = $this->db->query($sql);
        return $query->result_array();

    }

    public function buscaValorTotal($ordem_pagamento_id){
        $sql = "SELECT  o.id, sum(i.valor*i.qtd) as valor_total
                FROM    ordem_pagamento o, ordem_pagamento_itens i
                WHERE   o.id = i.ordem_pagamento_id and o.id = ".$ordem_pagamento_id." GROUP BY o.id";
        $query = $this->db->query($sql);
        return $query->row_array();
    
    }

}
?>