<?php

class PedidosModel extends CI_Model {

	public function add ($data) {
        
	    return $this->db->insert('pedidos', $data);		

	}

    public function selectPedidos(){

        $sql =  "SELECT     p.*, concat(e.id,' | ',e.cnpj, ' | ', e.razao_social) as cliente, s.descricao as status, e.email, o.contato_posto, o.empresa_id, o.id as orcamento_id   
                    FROM    pedidos p, empresas e, orcamentos o, status_pedidos s 
                    WHERE   o.empresa_id = e.id     and 
                            p.orcamento_id = o.id   and 
                            p.status_pedido_id = s.id";
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function selectPedidosPorEmpresa($empresa_id){

        $sql =  "SELECT     p.*, concat(e.id,' | ',e.cnpj, ' | ', e.razao_social) as cliente, s.descricao as status, e.email, o.contato_posto, o.empresa_id, o.id as orcamento_id   
                    FROM    pedidos p, empresas e, orcamentos o, status_pedidos s 
                    WHERE   o.empresa_id = e.id     and 
                            p.orcamento_id = o.id   and 
                            p.status_pedido_id = s.id and
                            o.empresa_id = e.id and 
                            e.id = ".$empresa_id;
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function getPedidosById($id){

        $sql =  "SELECT     p.*, concat(e.cnpj,' | ',e.razao_social) as cliente, concat(u.nome,' | ',t.cnpj,' | ', t.razao_social) as tecnico, e.email 
                    FROM    pedidos p 
                INNER JOIN  orcamentos o ON p.orcamento_id = o.id 
                INNER JOIN  empresas e ON o.empresa_id = e.id 
                LEFT JOIN   usuarios u ON u.id = p.tecnico_id 
                LEFT JOIN   empresas t ON u.empresa_id = t.id 
                WHERE       p.id = ".$id;
                           
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function excluirPedidos($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('pedidos')){
            return true;
        }else{
            return false;
        }
    }

    public function excluirPedidoItens($id){
        
        $this->db->where('pedido_id', $id);
        if($this->db->delete('pedido_itens')){
            return true;
        }else{
            return false;
        }
    }

    public function excluirPedidoFormPagto($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('pedido_form_pag')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaPedidos($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('pedidos', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizaStatusPedido($dados)
    {
                        
        $this->db->where('id', $dados['id']);

        if($this->db->update('pedidos', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function pedidosEmpresasPdf($id){

        $sql =  "SELECT     p.*, e.cnpj,e.razao_social, e.insc_estadual, e.telefone, e.endereco,e.bairro,e.cep, e.email,e.cidade, e.estado,
							concat(u.nome,' | ',t.cnpj,' | ', t.razao_social) as tecnico,
							f.descricao as frete, concat(i.nome,' | ',indi_emp.cnpj,' | ', indi_emp.razao_social,' | ',indi_emp.email,' | ',indi_emp.telefone) as indicador, o.cel_contato_posto, contato_p.celular as celular_contato, e.tp_cadastro
                    FROM    pedidos p
                INNER JOIN  orcamentos o        ON  p.orcamento_id  = o.id
                LEFT JOIN   fretes f            ON  o.frete_id      = f.id
                INNER JOIN  empresas e          ON  o.empresa_id    = e.id
                LEFT JOIN   usuarios u          ON  u.id = p.tecnico_id
                LEFT JOIN   usuarios i          ON  (i.id = o.indicador_id or i.id = o.indicador)
                LEFT JOIN   empresas indi_emp   ON  i.empresa_id = indi_emp.id
                LEFT JOIN   usuarios contato_p  ON  contato_p.id = o.contato_id
                LEFT JOIN   empresas t          ON  u.empresa_id = t.id 
                WHERE       p.id = ".$id;
                             
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function buscaIcmsOrcamentoPedido($id) 
    {
         
        $sql =  "SELECT i.valor_tributo
                    FROM        pedidos p
                    INNER JOIN  orcamentos o    ON o.id = p.orcamento_id                
                    INNER JOIN  empresas e      on o.empresa_id = e.id 
                    INNER JOIN  estados es      ON es.uf = e.estado  
                    INNER JOIN  icms i          on i.estado_id = es.id
                    WHERE p.id = ".$id;

        $query = $this->db->query($sql);
        return $query->row();
    }

    public function pedidosProdutos($id){
        $sql =  "SELECT     pro.descricao, pro.modelo, pro.codigo, pi.qtd, pi.produtos, pi.valor, pro.tipo_produto_id, pi.produto_id, pro.modelo_tecnico
                    FROM    pedidos p 
                INNER JOIN  pedido_itens pi ON pi.pedido_id = p.id                                 
                INNER JOIN  produtos pro    ON pro.id = pi.produto_id               
                WHERE p.id = ".$id;
                           
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function pedidosFormaPagto($id){
        $sql =  "SELECT     pfp.*
                    FROM    pedidos p 
                INNER JOIN  pedido_form_pag pfp ON pfp.pedido_id = p.id                                                 
                WHERE p.id = ".$id;
                           
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function insereFormaPagamento ($data) {
        
        return $this->db->insert('pedido_form_pag', $data);     

    }

    public function inserePedidoItens ($data) {
        
        return $this->db->insert('pedido_itens', $data);     

    }
	
	public function retornaClientePorPedido($id){
		$sql = "SELECT concat(e.cnpj,' - ', e.razao_social) as cliente, e.id as cliente_id, e.cidade, e.estado as uf, e.pais FROM empresas e
				INNER JOIN orcamentos o ON o.empresa_id = e.id
				INNER JOIN pedidos p ON o.id = p.orcamento_id
				where p.id=".$id;
				
		$query = $this->db->query($sql);
        return $query->row();
	}
	
	public function buscaOpsPorPedido($pedido_id){
		$sql = "SELECT * FROM ordem_producao WHERE pedido_id = ".$pedido_id." and status_op_id != 4";
		$query = $this->db->query($sql);
         
        return $query->result_array();
	}

    public function selectPedidosFeira(){

        $sql =  "SELECT p.*, concat(e.id,' | ',e.cnpj, ' | ', e.razao_social) as cliente, s.descricao as status, e.email, o.contato_posto, o.empresa_id, o.id as orcamento_id       
                FROM    pedidos p, empresas e, orcamentos o, status_pedidos s, usuarios u
                WHERE   o.empresa_id = e.id and
                         p.orcamento_id = o.id and 
                         p.status_pedido_id = s.id and 
                         u.id = p.usuario_geracao and
                         u.tipo_cadastro_id = 11 and 
                         date(p.dthr_geracao) between '2019-08-13' and '2019-08-15'
                         
                ORDER BY p.id DESC";
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function buscaStatus(){
        $sql =  "SELECT * FROM status_pedidos";
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getRelatorioPedidos($filtros){
        $where="1=1";
        
        if( $filtros['status_id'] != '' ){
            $where.=" and p.status_pedido_id = ".$filtros['status_id'];
            
        }

        if( $filtros['dt_ini'] != '' && $filtros['dt_fim'] != ''){
            $where.=" and date(p.dthr_geracao) between '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."'";
            
        }

        if( $filtros['indicador_id'] != '' ){
            $where.=" and i.id =".$filtros['indicador_id']." or indic.id=".$filtros['indicador_id'];
            
        }

        $sql = "SELECT  p.id, s.descricao as status, concat(e.cnpj, '|',e.razao_social) as cliente, concat(i.cnpj, '|',i.razao_social) as indicador, COALESCE(sum( op.qtd * op.valor ), p.valor_total) as valor_total, i.id as indicador_id, sum(if(pro.tipo_produto_id in (1,2,3,6),op.qtd,0) ) as total_bombas
                  FROM  orcamentos o
                  INNER JOIN    pedidos p               ON  p.orcamento_id      =   o.id   
                  INNER JOIN    pedido_itens     op     ON  p.id                =   op.pedido_id                  
                  LEFT  JOIN    empresas e              ON  o.empresa_id        =   e.id 
                  LEFT  JOIN    usuarios u              ON  o.solicitante_id    =   u.id
                  LEFT  JOIN    empresas i              ON  u.empresa_id        =   i.id 
                  LEFT  JOIN    usuarios ind            ON  o.indicador_id      =   ind.id
                  INNER JOIN    status_pedidos s        ON  s.id                =   p.status_pedido_id 
                  LEFT  JOIN    empresas indic          ON  ind.empresa_id      =   indic.id
                  INNER JOIN    produtos pro            ON op.produto_id        =   pro.id
                  WHERE ".$where."
                  group by p.id  
                ORDER BY p.id DESC";
        
        $query = $this->db->query($sql);
        $retorno['orcamento'] = $query->result_array();    

        return $retorno;
    }

    public function sincronismoStx(){
        $sql = 'SELECT COALESCE(max(sincronismo_id),0)+1 as sincronismo_id FROM stx_pedido';
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function insereStxPedido ($data) {
        
        return $this->db->insert('stx_pedido', $data);     

    }  

    public function numeroItem($pedido_id){
        $sql    =   "SELECT COALESCE(count(*),0)+1 as total from stx_pedido_item where pedido_internet = ".$pedido_id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getSincronismoStx($pedido_id){
        $sql = 'SELECT sincronismo_id FROM stx_pedido WHERE pedido_internet = '.$pedido_id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function sincronismoStxItem(){
        $sql = 'SELECT COALESCE(max(sincronismo_id),0)+1 as sincronismo_id FROM stx_pedido_item';
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function insereStxPedidoItem ($data) {
        
        return $this->db->insert('stx_pedido_item', $data);     

    }

    public function buscaFormaEntrega($pedido_id){
        
        $sql = "SELECT e.* 
                FROM    entregas e, pedidos p, orcamentos o 
                WHERE   e.id = o.entrega_id and 
                        o.id = p.orcamento_id and 
                        p.id = ".$pedido_id;

        $query = $this->db->query($sql);

        return $query->row_array();
    }



}
?>