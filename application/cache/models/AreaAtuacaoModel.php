<?php
class AreaAtuacaoModel extends CI_Model {

	public function add($data) {
        return $this->db->insert('area_atuacao', $data);		
    }

    public function select( $empresa_id ){
        $sql =  "SELECT * FROM area_atuacao where empresa_id = ".$empresa_id;
        $query = $this->db->query($sql);
        return $query->result_array();   
    }

    public function excluir($id){
        
        $this->db->where('id', $id);
        
        if($this->db->delete('area_atuacao')){
            return true;
        }else{
            return false;
        }

    }

    public function update($dados)
    {

        $this->db->where('id', $dados['id']);

        if($this->db->update('area_atuacao', $dados)){
            return true;
        }else{
            return false;
        }

    }

}

?>