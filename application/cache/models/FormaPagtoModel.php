<?php

class FormaPagtoModel extends CI_Model {

	public function add ($data) {
        
	    return $this->db->insert('forma_pagto', $data);		

	}

    public function selectFormaPagto(){

        $sql =  "SELECT * FROM forma_pagto";
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function getFormaPagtoById($id){

        $sql =  "SELECT * FROM forma_pagto where id =".$id;
        $query = $this->db->query($sql);
        return $query->row();
    }   

    public function excluirFormaPagto($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('forma_pagto')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaFormaPagto($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('forma_pagto', $dados)){
            return true;
        }else{
            return false;
        }

    }

}
?>