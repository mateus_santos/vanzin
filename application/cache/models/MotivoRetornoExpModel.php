<?php

class MotivoRetornoExpModel extends CI_Model {

	public function add ($data) {
        
	    return $this->db->insert('motivo_retorno_exp', $data);		

	}

    public function selectMotivoExclusao(){

        $sql =  "SELECT * FROM motivo_retorno_exp";
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function get_motivo_retorno_exp_ById($id){

        $sql =  "SELECT * FROM motivo_retorno_exp where id =".$id;
        $query = $this->db->query($sql);
        return $query->row();
    }   

    public function excluir($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('motivo_retorno_exp')){
            return true;
        }else{
            return false;
        }
    }

    public function atualiza($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('motivo_retorno_exp', $dados)){
            return true;
        }else{
            return false;
        }

    }

}
?>