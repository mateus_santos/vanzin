<?php

class MY_Controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('LogModel','logM');
		
	}
	
	public function _load_view($page,$data = null) {
	    $data['view'] 		= 	$page;
	    	    
	    //header
	    $this->load->view('inc/header',$data);
	    //menu
	    $this->load->view('inc/navigation',$data);
	    //view
	    $this->load->view($page,$data);
	    //footer
		$this->load->view('inc/footer',$data);
	
	}

	//Carregar template expopostos
	public function _load_view_feira($page,$data = null) {
	    $data['view'] = $page; //css
	    //header
	    $this->load->view('inc/header_feira',$data); 
	    //menu
	    $this->load->view('inc/navigation',$data); 
	    //view
	    $this->load->view($page,$data); 
	    //footer
	    $this->load->view('inc/footer_feira',$data); 
	}

	public function _is_logged()
    {
        $user = $this->session->userdata('logged_in');
        return isset($user);
    }

    public function makeThumbnail($sourcefile,$max_width, $max_height, $endfile, $type){
		// Takes the sourcefile (path/to/image.jpg) and makes a thumbnail from it
		// and places it at endfile (path/to/thumb.jpg).
		// Load image and get image size.		   		   
		switch($type){
			case'image/png':
				$img = imagecreatefrompng($sourcefile);
				break;
			case'image/jpeg':
				$img = imagecreatefromjpeg($sourcefile);
				break;
			case'image/gif':
				$img = imagecreatefromgif($sourcefile);
				break;
			default : 
			return 'Un supported format';
		}

		$width = imagesx( $img );
		$height = imagesy( $img );

		if ($width > $height) {
		    if($width < $max_width)
				$newwidth = $width;
			
			else
			
		    $newwidth = $max_width;	
			
			
		    $divisor = $width / $newwidth;
		    $newheight = floor( $height / $divisor);
		}
		else {
			
			 if($height < $max_height)
		         $newheight = $height;
		     else
				 $newheight =  $max_height;
			 
		    $divisor = $height / $newheight;
		    $newwidth = floor( $width / $divisor );
		}

		// Create a new temporary image.
		$tmpimg = imagecreatetruecolor( $newwidth, $newheight );

		imagealphablending($tmpimg, false);
		imagesavealpha($tmpimg, true);
			
		// Copy and resize old image into new image.
		$return = imagecopyresampled( $tmpimg, $img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height );

		// Save thumbnail into a file.
		//compressing the file
		switch($type){
			case'image/png':
				imagepng($tmpimg, $endfile, 0);
				break;
			case'image/jpeg':
				imagejpeg($tmpimg, $endfile, 100);
				break;
			case'image/gif':
				imagegif($tmpimg, $endfile, 0);
				break;
		}

		// release the memory
		imagedestroy($tmpimg);
		imagedestroy($img);

		return $return;
	}

	public function log($controller, $model, $action, $usuario_alteracao, $campos_alterados, $valores_alterados, $ip){
		$insertLog = array( 'controller'=>	$controller,
							'model' 	=>	$model,
							'acao'		=>	$action,
							'user_id'	=> 	$usuario_alteracao,
							'ip'		=> 	$ip);

		if($this->logM->insereLog($insertLog)){
			if( is_array($campos_alterados) && is_array($valores_alterados) )
			{
				$log_id  = $this->db->insert_id();
				foreach ($campos_alterados as $key => $value) {
					$insert = array('campo'			=> 	$key,
									'novo_valor' 	=> 	$value,
									'log_id' 		=>	$log_id);

					$this->logM->insereLogCampos($insert);
				}
			}
		}

	}
	
	public function ConsultaCNPJRF($cnpj){
	    
	    $url = 'http://webservice.keyconsultas.net/receita/cnpj/?cnpj='.$cnpj.'&token=BRK018cb-5943-467b-86cc-3fac8033578c';
	            
	    $consulta_str = $this->Executa_URL($url);
	    	    
	    $consulta = json_decode($consulta_str);	    
	    
        if($consulta->{'code'} == '1'){
            $dir = $this->SaveConsultaRF($consulta,$cnpj);
    
            $consulta->{'Save_Receita'} = $dir;
        } else {
            //sleep(3);
        }

        return $consulta;
	    
	}                                                                                                                                                                                                                                                                                                                           

	public function SaveConsultaRF($consulta,$cnpj)
	{		
	    $dir = getcwd()."/Consultas/Receita_Federal/Receita_" . $cnpj . "_" . date("d-m-Y").".html";	    
	    $file = fopen($dir, "w");
	    fwrite($file, utf8_decode($consulta->{'html'}));
	    fclose($file);
	    return $dir;
	}

	public function ConsultaCNPJSintegra($cnpj,$estado){
	    $url = 'http://webservice.keyconsultas.net/sintegra_'.strtolower($estado).'/cnpj/?cnpj='.$cnpj.'&token=BRK018cb-5943-467b-86cc-3fac8033578c';
	            
	    $consulta_str = $this->Executa_URL($url);
	    $consulta = json_decode($consulta_str);
	    
	    $dir = $this->SaveConsultaSintegra($consulta,$cnpj);
	    
	    $consulta->{'Save_Sintegra'} = $dir;
	    
	    return $consulta;
	}

	public function SaveConsultaSintegra($consulta,$cnpj)
	{
	    $dir = base_url("Consultas\Sintegra\Sintegra_" . $cnpj . "_" . date("d-m-Y").".html");
		//$dir = "C:/xampp/htdocs/wertco-site/Consultas/Sintegra/Sintegra_" . $cnpj . "_" . date("d-m-Y");
	    if (isset($consulta->{'data'}->{'certificado_url'}))
	    {
	        $dir = $dir . ".pdf";
	        copy($consulta->{'data'}->{'certificado_url'}, $dir);
	    }
	    else
	    {
	        if (isset($consulta->{'html'})){
	            $dir = $dir . ".html";
	            $file = fopen($dir, "w");
	            fwrite($file, $consulta->{'html'});
	            fclose($file);
	        }
	    }
	    
	    return $dir;
	}

	private function Executa_URL($url)
	{
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $ret_str = curl_exec($ch);
	    curl_close($ch);
	    
	    return $ret_str;
	}


	public function Executa_URL_https($url)
	{
	    $ch = curl_init();
	    curl_reset($ch);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false); // adiciona https
	    $ret_str = curl_exec($ch);	    
	    curl_close($ch);	    
	    return json_decode($ret_str);
	}
	/* 
		****************
		* API RECINTOS *
		****************
	*/
	public function autenticar()
    {
        $dados_token = $this->getDadosToken();

        return array(
            strval($dados_token["set-token"]),
            strval($dados_token["x-csrf-token"]),
        );
    }

    public function enviarDados($api, $dados, $token, $x_csrf_token)
    {	
    	
		//URL_BASE_API_PRO=https://portalunico.siscomex.gov.br/recintos-ext/api/ext
        $url = "https://val.portalunico.siscomex.gov.br/recintos-ext/api".$api;

        // echo $dados; die;
        // inicia comunicação
        $ch = curl_init($url);
        try {
            // retorna para uma variável
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // tempo de espera
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            // mostrar mais texto
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            // enviar via post
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dados);
            // mostrar o cabeçalho no retorno
            curl_setopt($ch, CURLOPT_HEADER, true);
            // cabeçalhos necessários para o webservice
            $headers = array();
            $headers[] = "Content-Type: application/json";
            $headers[] = "Authorization: $token";
            $headers[] = "X-CSRF-Token: $x_csrf_token";
            $headers[] = "Role-Type: DEPOSIT";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // retorna a comunicação
            $resultRaw = $this->enviarRequisicao($ch);

            
			$info = curl_getinfo($ch);
            file_put_contents(getcwd()  . "/certificados/log.txt", "$dados\n\n", FILE_APPEND);
            chmod(getcwd()  . "/certificados/log.txt", 0777);
            

            list($cabecalhos, $corpo) = $this->lerResposta($resultRaw);
			//var_dump($cabecalhos);
            $this->atualizarValidadeToken($cabecalhos);

            // verifica se deu erro
            if (curl_errno($ch)) {
                throw new Exception(curl_errno($ch) . ' - ' . curl_error($ch));
            }

            $corpoIso = strrpos($cabecalhos["content-type"], 'ISO-8859-1');
            $corpoDecodificado = ($corpoIso === false) ? utf8_encode($corpo) : $corpo;

            $info = curl_getinfo($ch);
            if ($info["http_code"] != '200' && $info["http_code"] != '201' && $info["http_code"] != '204') {
                throw new Exception($corpoDecodificado);
            }
            
            // fecha
            curl_close($ch);
            unset($ch);
            // transforma retorno em um array
            return  json_encode($info);
        } catch (Exception $e) {
            curl_close($ch);
            throw $e;
        }
    }

    private function enviarRequisicao($ch)
    {
        $res = curl_exec($ch);
        
        $info = curl_getinfo($ch);
        $url = $info["url"];
        $sysdate = date("d/m/Y H:m:s");
        file_put_contents(getcwd()  . "/certificados/log.txt", "$sysdate - $url\n\n$res\n\n", FILE_APPEND);
        chmod(getcwd()  . "/certificados/log.txt", 0777);        

        return $res;
    }

    private function getDadosToken()
    {
        $json_token = file_get_contents(getcwd()  . "/certificados/token/token.json");

        if ($json_token) {
            $dados_token = (array) json_decode($json_token);

            if ($dados_token) {
                $validade = intval($dados_token['x-csrf-expiration']);
                $agora =  date_create()->format('Uv');

                if ($validade > $agora)
                    return $dados_token;
            }
        }

        return $this->revalidarToken();
    }

    private function revalidarToken()
    {
        // inicia comunicação
        
		//URL_BASE_AUTENTICACAO_PRO=https://portalunico.siscomex.gov.br/portal/api/autenticar
        $ch = curl_init("https://val.portalunico.siscomex.gov.br/portal/api/autenticar");
        try {

            // retorna para uma variável
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // tempo de espera
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            // mostrar mais texto
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            // enviar via post
            curl_setopt($ch, CURLOPT_POST, true);
            // não interessa o corpo da resposta, apenas o cabeçalho
            // curl_setopt($ch, CURLOPT_NOBODY, 1);
            // não mostrar o cabeçalho no retorno
            curl_setopt($ch, CURLOPT_HEADER, true);
            // cabeçalhos necessários para o webservice
            $headers = array();
            $headers[] = "content-type: application/json";
            $headers[] = "role-type: DEPOSIT";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            // local com o certificado do site a ser acessado
            //curl_setopt($ch, CURLOPT_CAINFO, getcwd()  . "/certificados/certified.pem");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // skip certificate check
			//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true); // Check whether the SSL encryption algorithm exists from the certificate
			// local com o certificado do site a ser acessado
			curl_setopt($ch, CURLOPT_CAPATH, "/home/wwmils/ssl/certs/milsistemas_com_br_cdca5_d1a01_1656658346_2fb77c7e21eb928fdf2ca595ef211902.crt");
            curl_setopt($ch, CURLOPT_SSLCERT, getcwd()  . "/certificados/certified.pem");
            curl_setopt($ch, CURLOPT_SSLCERTPASSWD, "senha123@");

            // retorna a comunicação
            $resultRaw = $this->enviarRequisicao($ch);
            list($cabecalhos, $corpo) = $this->lerResposta($resultRaw);

            // verifica se deu erro
            if (curl_errno($ch)) {
                throw new Exception(curl_errno($ch) . ' - ' . curl_error($ch));
            }

            $info = curl_getinfo($ch);
            $result = json_decode($corpo);			
			
            if ($info["http_code"] != '200' && $info["http_code"] != '201' && $info["http_code"] != '204') {
				var_dump($result);
				
				if ($result && $result->message)
                    throw new Exception($result->message);
				
                throw new Exception($corpo);
            }

            // fecha
            curl_close($ch);
            unset($ch);

            file_put_contents(getcwd()  . "/certificados/token/token.json", json_encode($cabecalhos));
            chmod(getcwd()  . "/certificados/token/token.json", 0777);

            return $cabecalhos;
        } catch (Exception $e) {
            curl_close($ch);
            throw $e;
        }
    }

    private function atualizarValidadeToken($cabecalhos)
    {
        $json_token = file_get_contents(getcwd()  . "/certificados/token/token.json");
        if (!$json_token) return;
		
        $dados_token = (array) json_decode($json_token);
        
        if (!array_key_exists('x-csrf-token', $cabecalhos) || !array_key_exists('x-csrf-expiration',$cabecalhos))
            return;

        $dados_token['x-csrf-token'] = $cabecalhos['x-csrf-token'];
        $dados_token['x-csrf-expiration'] = $cabecalhos['x-csrf-expiration'];

        file_put_contents(getcwd()  . "/certificados/token/token.json", json_encode($dados_token));
        chmod(getcwd()  . "/certificados/token/token.json", 0777);
    }

    private function lerResposta($texto)
    {
        $texto = trim($texto);
        $linhas = explode("\r\n", $texto);

        array_shift($linhas);

        $leuCabecalhos = false;
        $cabecalhos = array();
        $corpo = "";
        foreach ($linhas as $linha) {
            if (!$linha) {
                $leuCabecalhos = true;
                continue;
            };

            if (!$leuCabecalhos) {
                list($chave, $valor) = explode(": ", $linha);
                $cabecalhos[strtolower($chave)] = $valor;
            } else {
                $corpo .= $linha;
            }
        }

        return array($cabecalhos, $corpo);
    }

}