<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class AreaTransito extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() ||  (($this->session->userdata('tipo_acesso') != 'administrador empresas') && ($this->session->userdata('tipo_acesso') != 'administrador geral') && ($this->session->userdata('tipo_acesso') != 'gate terminal') ) ) {
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('AgendamentoModel', 'agendaM');		
		$this->load->model('MotoristaModel', 'motoristaM');
		$this->load->model('NotaFiscalModel', 'notaM');
        $this->load->model('AcessoModel', 'acessoM');
        $this->load->model('TransitoModel', 'transitoM');
        $this->load->model('TerminalModel', 'terminalM');
        $this->load->model('LogModel', 'logM');
        $this->load->model('TransitoMovimentacaoModel', 'transitoMM');        
		$this->load->helper('form');
		$this->load->helper('url');
	}
 
	public function index()
	{
       
		$parametros						=	$this->session->userdata();
        $parametros['title']			=	"Área da Trânsito Simplificado";	
        $parametros['dados']			=	array();
		$this->_load_view('area-transito/index-transito',$parametros);

	}
	

    public function atualizarTransito($id=null){

        if($this->input->post()){

            $dt_ini = $this->input->post('dthr_atracacao');
            $dt_hr = explode(" ",trim($dt_ini));
            $dt = explode("/",$dt_hr[0]);
            $dt_ini = $dt[2].'-'.$dt[1].'-'.$dt[0].' '.$dt_hr[1];

            if(!empty($this->input->post('dthr_desatracacao'))){
                $dt_fim = $this->input->post('dthr_desatracacao');
                $dt_hr_f = explode(" ",trim($dt_fim));
                $dtf = explode("/",$dt_hr_f[0]);
                $dt_fim = $dtf[2].'-'.$dtf[1].'-'.$dtf[0].' '.$dt_hr_f[1];
                //$dt_fim = date_format(new DateTime($dt_fim),'Y-m-d H:i:s');
            }else{
                $dt_fim = null;
            }

            //Operações
            if(!empty($this->input->post('dthr_ini_operacao'))){
                $dt_ini_oper = $this->input->post('dthr_ini_operacao');
                $dt_hr_ini = explode(" ",trim($dt_ini_oper));
                $dtf = explode("/",$dt_hr_ini[0]);
                $dt_ini_oper = $dtf[2].'-'.$dtf[1].'-'.$dtf[0]. ' '.$dt_hr_ini[1];
            }else{
                $dt_ini_oper = null;
            }

            //Operações
            if(!empty($this->input->post('dthr_fim_operacao'))){
                $dt_fim_oper = $this->input->post('dthr_fim_operacao');
                $dt_hr_fim = explode(" ",trim($dt_fim_oper));
                $dtf = explode("/",$dt_hr_fim[0]);
                $dt_fim_oper = $dtf[2].'-'.$dtf[1].'-'.$dtf[0]. ' '.$dt_hr_fim[1];
            }else{
                $dt_fim_oper = null;
            }

            $altera = array(
                'id' 		    =>	$this->input->post('id'),
                'dthr_atracacao' 	=>	date_format(new DateTime($dt_ini),'Y-m-d H:i:s'),
				'dthr_desatracacao' =>	date_format(new DateTime($dt_fim),'Y-m-d H:i:s'),
                'dthr_ini_operacao'	=>	date_format(new DateTime($dt_ini_oper),'Y-m-d H:i:s'),
				'dthr_fim_operacao' =>	date_format(new DateTime($dt_fim_oper),'Y-m-d H:i:s'),
                'nr_fundeio'		=>	$this->input->post('nr_fundeio'),
				'navio' 		    =>	$this->input->post('navio'),
				'imo' 			    =>	$this->input->post('nr_imo'),
				'nr_escala' 	    =>	$this->input->post('nr_escala'),
				'id_terminal' 	    =>	$this->input->post('id_terminal'),
                'fl_encerrado'      =>  $this->input->post('fl_encerrado')
				
            );
/*echo "<pre>";
var_dump($altera);
echo "</pre>"; die;*/
            $salva      = 0;
            $salva_due  = 0;
            $erro_exclui_due = 0;
            if($this->transitoM->atualizaTransito($altera)){
                $salva = 1;
            }else{
                $salva = 2; 
            }

            if($this->input->post('id_empresa')){//Verifica se existe
                //Consistencias antes de salvar a due
                foreach($this->input->post('id_empresa') as $y => $verifica){

                    $agrupa[$y]  = array(
                        'id_empresa'    => $verifica,
                        'nr_ordem'      => $this->input->post('ordem['.$y.']'),
                        'fl_aberta'     => $this->input->post('status['.$y.']')

                    );
                }

                //Faz a verificação se esta aberta a due
                for($z=0; $z< count($agrupa); $z++){
                    
                    foreach($agrupa[$z] as $w => $item){
                        
                        
                        if($w == 'id_empresa'){
                            
                            $existe = 0;
                            $guarda_fl_aberta[] = '';
                            //$guarda_ordem[]     = '';
                            $conta_aberta = 0;//contador para verificar se mesmo cliente tem alguma due em aberto
                            for($p=0; $p < count($agrupa); $p++){
                                //echo 'dentro '.$agrupa[$p]['id_empresa'].' - '; 
                                
                                if($item == $agrupa[$p]['id_empresa']){
                                    $guarda_fl_aberta[$existe]  = $agrupa[$p]['fl_aberta'];
                                // $guarda_ordem[$existe]      = $agrupa[$p]['nr_ordem'];
                                    $existe++;
                                // echo 'existe '.$item. ' existe '.$existe.'<br>';
                                    if($existe > 1 && $conta_aberta == 0){
                                        
                                        for($g=0; $g < $existe; $g++){
                                        // echo 'aberto: '.$guarda_fl_aberta[$g]."<br>";
                                            //Verifica se tem mais uma due aberta para o mesmo cliente
                                            if($guarda_fl_aberta[$g] == 'S'){
                                            // echo  'fl_aberta SIM '.$guarda_fl_aberta[$g].'<br><br>';
                                                $conta_aberta++;
                                            }
                                        
                                        if($conta_aberta > 1){
                                            $this->session->set_flashdata('erro', 'erro_aberto');
                                            redirect('/areaTransito/atualizarTransito/'.$this->input->post('id'));  
                                        }
                                        // echo 't '.$conta_aberta."<br><br>";
                                            //Verifica a ordem da due por cliente
                                            //echo 'Ordem '. $guarda_ordem[$g].' ------ '.$g;
                                        }
                                    //  echo 'Aberta '.$agrupa[$p]['fl_aberta'].' ------ ';
                                    }    
                                }
                            }
                            
                        
                        }
                    }
                }

            }

            
            //die;

            $verifica_movimento = $this->transitoM->existeMovimento($this->input->post('id'));

            if($verifica_movimento[0]->existe > 0){
                
                /*echo 'id '.$this->input->post('id');
               
                echo '<pre>'.var_dump($conta_movimento[0]->total).'</pre></br>';
                die;*/
                $conta_movimento = $this->transitoM->buscaMovimentacao($this->input->post('id'), 0);
               // echo $conta_movimento[0]->total;
                $erro_exclui_due = 0;
                if($conta_movimento[0]->total == 0){
                    //Se for zerado o sistema exclui as dues
                    $this->transitoM->excluirTodasDue($this->input->post('id'));
                }else{

                    foreach($this->transitoM->buscaDues($this->input->post('id')) as $x => $valor){
                      // echo count($this->transitoM->buscaMovimentacao($this->input->post('id'),$this->input->post('nr_due['.$x.']'))).'</br>';
                        $existe_due = $this->transitoM->buscaMovimentacao($this->input->post('id'),$this->input->post('nr_due['.$x.']'));
                       // echo $existe_due[0]->total.'</br>'; 
                        if (empty($this->input->post('nr_due['.$x.']'))){
                            $existe_due[0]->total = 0;
                        }
                        if($existe_due[0]->total == 0){
                            if (empty($this->input->post('nr_due['.$x.']'))){
                                //echo 'aq '.$valor->nr_due; 
                                $this->transitoM->excluirDue($this->input->post('id'), $valor->id_empresa, $valor->nr_due);
                            }
                           // echo $this->input->post('nr_due['.$x.']').'</br>'; die;
                            
                        }

                      //  echo $this->input->post('nr_due['.$x.']').'</br>';
                    }
                    //die;
                 //   $conta_movimento = $this->transitoM->buscaMovimentacao($this->input->post('id'), $this->input->post('nr_due[]'));
//echo $this->input->post('nr_due[1]');
                  
                  //  $erro_exclui_due     = 1;
                  //  $this->session->set_flashdata('erro', 'erro'); 
                   
                }
                

                foreach($this->input->post('id_empresa') as $x => $valor){

                    $id_transito   = $this->input->post('id');
                    $nr_due        = $this->input->post('nr_due['.$x.']');
                    
                    $salva_due     = 0;
                    $atualiza_due  = array(
                        'id_empresa'    => $valor,
                        'ncm'           => $this->input->post('ncm['.$x.']'),
                        'ce_mercante'   => $this->input->post('ce_mercante['.$x.']'),
                        'peso_manif'    => str_replace(',', '.', str_replace('.','',$this->input->post('peso['.$x.']'))),
                        'id_tipo'       => $this->input->post('tipo['.$x.']'),
                        'nr_ordem'      => $this->input->post('ordem['.$x.']'),
                        'fl_aberta'     => $this->input->post('status['.$x.']')

                    );
//Faz a verificação pois pode ter um acrescimo de due quando ja existirem dues cadastradas 
                    $verifica_due = $this->transitoM->verificaDue($nr_due);
//echo $nr_due .' - '.var_dump($verifica_due);
                    if($verifica_due[0]->total > 0){
                        $id_empresa = $valor;

                        if($this->transitoM->atualizaDue($atualiza_due,$nr_due,$id_transito,$id_empresa )){
                        
                            $salva_due = 1;
                        }
                    }else{
                        $inclui_due = array(
                            'id_transito'   => $this->input->post('id'),
                            'nr_due'        => $this->input->post('nr_due['.$x.']'),
                            'id_empresa'    => $valor,
                            'ncm'           => $this->input->post('ncm['.$x.']'),
                            'ce_mercante'   => $this->input->post('ce_mercante['.$x.']'),
                            'peso_manif'    => str_replace(',', '.', str_replace('.','',$this->input->post('peso['.$x.']'))),
                            'id_tipo'       => $this->input->post('tipo['.$x.']'),
                            'nr_ordem'      => $this->input->post('ordem['.$x.']'),
                            'fl_aberta'     => $this->input->post('status['.$x.']')

                        );
                        
                        if($this->transitoM->addDue($inclui_due)){
                            $salva_due = 1;
                        }
                    }
                }
               
               // $this->session->set_flashdata('erro', 'erro');
               // redirect('/areaTransito/atualizarTransito/'.$this->input->post('id'));
            }else{
                
                if(!empty($this->input->post('exportador'))){
                    if(count($this->transitoM->buscaDues($this->input->post('id'))) > 0){
                        $this->transitoM->excluirTodasDue($this->input->post('id'));
                    }
                    foreach($this->input->post('id_empresa') as $x => $valor){
                        $salva_due  = 0;
                        $inclui_due = array(
                            'id_transito'   => $this->input->post('id'),
                            'nr_due'        => $this->input->post('nr_due['.$x.']'),
                            'id_empresa'    => $valor,
                            'ncm'           => $this->input->post('ncm['.$x.']'),
                            'ce_mercante'   => $this->input->post('ce_mercante['.$x.']'),
                            'peso_manif'    => str_replace(',', '.', str_replace('.','',$this->input->post('peso['.$x.']'))),
                            'id_tipo'       => $this->input->post('tipo['.$x.']'),
                            'nr_ordem'      => $this->input->post('ordem['.$x.']'),
                            'fl_aberta'     => $this->input->post('status['.$x.']')

                        );

                        if($this->transitoM->addDue($inclui_due)){
                            $salva_due = 1;
                        }
                        
                    }
        
                }else{
                    //Se for zerado o sistema exclui as dues
                    $this->transitoM->excluirTodasDue($this->input->post('id'));
                }
            }

            if($salva == 1){
                if($salva_due == 1){
                    if($erro_exclui_due > 0){
                        $this->session->set_flashdata('erro', 'erro'); 
                    }else{
                        $this->session->set_flashdata('sucesso', 'ok'); 
                    } 
                }else{
                    if($erro_exclui_due > 0){
                        $this->session->set_flashdata('erro', 'erro'); 
                    }else{
                        $this->session->set_flashdata('sucesso', 'ok'); 
                    }
                    
                }
            }
           

            redirect('/areaTransito/atualizarTransito/'.$this->input->post('id'));
            
        }else{
           
            $parametros					=	$this->session->userdata();
            $parametros['title']		=	"Área da Trânsito Simplificado";	
            $parametros['dados']	    =	$this->transitoM->buscaTransito($id);
            $parametros['terminais']	= 	$this->terminalM->getTerminais(); 
            $parametros['tipo_carga']   = 	$this->transitoM->getTipo(); 
          
            $this->_load_view('area-transito/atualizar-transito',$parametros);
        }
    }

    public function cadastrarTransito(){

        if($this->input->post()){

            $dt_ini = $this->input->post('dthr_atracacao');
            $dt_hr = explode(" ",trim($dt_ini));
            $dt = explode("/",$dt_hr[0]);
            $dt_ini = $dt[2].'-'.$dt[1].'-'.$dt[0]. ' '.$dt_hr[1];
            

            if(!empty($this->input->post('dthr_desatracacao'))){
                $dt_fim = $this->input->post('dthr_desatracacao');
                $dt_hr_f = explode(" ",trim($dt_fim));
                $dtf = explode("/",$dt_hr_f[0]);
                $dt_fim = $dtf[2].'-'.$dtf[1].'-'.$dtf[0]. ' '.$dt_hr[1];
            }else{
                $dt_fim = null;
            }

            //Operações
            if(!empty($this->input->post('dthr_ini_operacao'))){
                $dt_ini_oper = $this->input->post('dthr_ini_operacao');
                $dt_hr_f = explode(" ",trim($dt_ini_oper));
                $dtf = explode("/",$dt_hr_f[0]);
                $dt_ini_oper = $dtf[2].'-'.$dtf[1].'-'.$dtf[0]. ' '.$dt_hr[1];
            }else{
                $dt_ini_oper = null;
            }

            //Operações
            if(!empty($this->input->post('dthr_fim_operacao'))){
                $dt_fim_oper = $this->input->post('dthr_fim_operacao');
                $dt_hr_f = explode(" ",trim($dt_fim_oper));
                $dtf = explode("/",$dt_hr_f[0]);
                $dt_fim_oper = $dtf[2].'-'.$dtf[1].'-'.$dtf[0]. ' '.$dt_hr[1];
            }else{
                $dt_fim_oper = null;
            }
            
            
            $dadosTransito = array(
		        'dthr_atracacao' 	=>	date_format(new DateTime($dt_ini),'Y-m-d H:i:s'),
				'dthr_desatracacao' =>	date_format(new DateTime($dt_fim),'Y-m-d H:i:s'),
                'dthr_ini_operacao'	=>	date_format(new DateTime($dt_ini_oper),'Y-m-d H:i:s'),
				'dthr_fim_operacao' =>	date_format(new DateTime($dt_fim_oper),'Y-m-d H:i:s'),
                'nr_fundeio' 	    =>	$this->input->post('nr_fundeio'),
				'navio' 		    =>	$this->input->post('navio'),
				'imo' 			    =>	$this->input->post('nr_imo'),
				'nr_escala' 	    =>	$this->input->post('nr_escala'),
				'id_terminal' 	    =>	$this->input->post('id_terminal')
				
            );
           
            
            $id = $this->transitoM->add($dadosTransito);
            if($id)
			{
				$this->session->set_flashdata('sucesso', 'ok');	
				redirect('/areaTransito/atualizarTransito/'.$id);			
			}else{
				$this->session->set_flashdata('erro', 'erro');
				redirect('/areaTransito/cadastrarTransito');
			}	
        }else{
            $parametros			 		=	$this->session->userdata();		
            $parametros['title']		=	"Cadastrar Novo Trânsito Simplificado";		
            $parametros['empresa'] 		= 	$this->session->userdata('razao_social');		
            $parametros['id_empresa'] 	= 	$this->session->userdata('empresa_id');
            $parametros['terminais']	= 	$this->terminalM->getTerminais();
            $parametros['tipo_carga']	= 	$this->transitoM->getTipo(); 
           /* if( $this->session->userdata('tipo_acesso') == 'administrador geral'){
                $parametros['id_empresa'] 	= 	'';
                $parametros['terminais']		= 	$this->transitoM->getTerminais();
            }
    */
            if( $this->session->userdata('tipo_acesso') == 'administrador empresas' || $this->session->userdata('tipo_acesso') == 'gate terminal'){
                
                $parametros['empresas']		= 	$this->empresasM->getClientesEmpresas($parametros['id_empresa']);
            }
            $this->_load_view('area-transito/cadastra-transito',$parametros);


        }
		
    }

    public function pesquisaTransito(){
        $parametros			 		=	$this->session->userdata();		
        $parametros['title']		=	"Pesquisa Trânsito Simplificado";
        $parametros['dados']		=	array();
       if($this->input->post()){
            if($this->input->post('dt_ini') !=''){
            
                $dt_ini = $this->input->post('dt_ini');
              /*  $dt_hr = explode(" ",$dt_ini);
                $dt = explode("/",$dt_hr[0]);
                $dt_ini = $dt[2].'-'.$dt[1].'-'.$dt[0].' '.$dt_hr[1];
                */
                $dt = explode("/",$dt_ini);
                $dt_ini = $dt[2].'-'.$dt[1].'-'.$dt[0];

                if(!empty($this->input->post('dt_fim'))){
                    $dt_fim = $this->input->post('dt_fim');
                   /* $dt_hr_f = explode(" ",$dt_fim);
                    $dtf    = explode("/",$dt_hr_f[0]);
                    $dt_fim = $dtf[2].'-'.$dtf[1].'-'.$dtf[0].' '.$dt_hr_f[1];*/
                    $dtf    = explode("/",$dt_fim);
                    $dt_fim = $dtf[2].'-'.$dtf[1].'-'.$dtf[0];

                    $dt_fim = date_format(new DateTime($dt_fim),'Y-m-d');
                }else{
                    $dt_fim = date_format(new DateTime(),'Y-m-d');
                }

                $dt_ini 	=	date_format(new DateTime($dt_ini),'Y-m-d');
                //$dt_fim 	=	date_format(new DateTime($dt_fim),'Y-m-d');
        
            }else{
                $dt_ini 	=	null;
                $dt_fim     =	null;
            }
            $nr_fundeio	=	$this->input->post('nr_fundeio');
            $navio   	=	$this->input->post('navio');
            $due		=	$this->input->post('nr_imo');
            $nr_escala 	=	$this->input->post('nr_escala');
                
            $parametros['dados']	    =	$this->transitoM->getTransito($dt_ini, $dt_fim, $navio, $due, $nr_escala);
       }
       
        $this->_load_view('area-transito/index-transito',$parametros);
    }

    public function ajusteTransito(){
        $parametros			 		=	$this->session->userdata();		
        $parametros['title']		=	"Ajuste de Saldo do Trânsito Simplificado";
        $parametros['dados']		=	array();
       if($this->input->post()){

            $id_trans	=	$this->input->post('id_transito');
            $nr_due	    =	$this->input->post('nr_due');
            $entrega	=	$this->input->post('entrega');
            $placa 	    =	'AAA-0000';
            $cpf        =   '00000000000';
            //Verifica o tipo
            if($entrega == 1){
                //Entrega
                $peso   	=	$this->input->post('peso');
            }else{
                //Recepção
                $peso   	=	- $this->input->post('peso');
            }

            $dadosTransito = array(
                'id_transito' 	    =>	$id_trans,
                'dthr_movimento'    =>	date_format(new DateTime(),'Y-m-d H:i:s'),
                'nr_due'	        =>	$nr_due,
                'placa'             =>	$placa,
                'cpf_motorista' 	=>	$cpf,
                'peso_aferido' 		=>	$peso,
                'fl_envio_siscomex' =>	'S',
                'fl_impresso' 	    =>	'S'
            );

           /* echo '<pre>';
            var_dump($dadosTransito);
            echo '</pre>';
            die;*/

            $id = $this->transitoMM->add($dadosTransito);
            if($id)
            {
                $placa 	    =	'AAA0000';
                $dadosLog = array(
                    'nr_due'	        =>	$nr_due,
                    'placa'             =>	$placa,
                    'cpf_motorista' 	=>	$cpf,
                    'dthr_envio'        =>	date_format(new DateTime(),'Y-m-d H:i:s'),
                    'cd_retorno' 		=>	'CCTR-IN0002',
                    'descricao'         =>	'Ajuste de Saldo',
                    'peso' 		        =>	$peso,
                    'operacao' 	        =>	'E',
                    'id_transito_mov'   =>	$id_trans
                );
 
                $this->logM->insereLogSiscomex($dadosLog);

                $this->session->set_flashdata('sucesso', 'ok');	
                redirect('/areaTransito/ajusteTransito/');			
            }else{
                $this->session->set_flashdata('erro', 'erro');
                redirect('/areaTransito/ajusteTransito');
            }	

            //$parametros['dados']	    =	$this->transitoM->getTransito($dt_ini, $dt_fim, $navio, $due, $nr_escala);
       }
       
        $this->_load_view('area-transito/ajuste-transito',$parametros);
    }

    //Autocomplete do Exportador
    public function buscaExportadorAjax(){

        $dados = array(	'exportador'		=>	$_POST['exportador']);

        $parametros			 		=	$this->session->userdata();		

        $resultado['exportador']		= 	$this->empresasM->getExportadoresAutocomplete($_POST['exportador']);

        echo json_encode(array('retorno' => $resultado));

    }

    //Verifica se a due existe
    public function verificaDueAjax(){

        $dados = array(	'nr_due'		=>	$_POST['nr_due']);

        $resultado['existe']		= 	$this->transitoM->verificaDue($_POST['nr_due']);

        echo json_encode(array('retorno' => $resultado));

    }

    //Busca as informações da due 
    public function buscaDueAjax(){

        $dados = array(	'nr_due'		=>	$_POST['nr_due']);

        $resultado['dados']		= 	$this->transitoM->buscaNrDue($_POST['nr_due']);

        echo json_encode(array('retorno' => $resultado));

    }

    //Busca os tipos de Carga 
    public function buscaTipoAjax(){

        $resultado['tipos']		= 	$this->transitoM->getTipo();

        echo json_encode(array('retorno' => $resultado));

    }

    //Verifica se foi efetuada a pesagem pelo datamex 
    public function verificaPesagemAjax(){
        $parametros             =	$this->session->userdata();	
        
        //$placa                  =	str_replace('-','',$_POST['placa']); 
        $placa                  =	$_POST['placa']; 
        $resultado['existe']	= 	$this->transitoM->VerificaPesagem($placa);

        echo json_encode(array('retorno' => $resultado));

    }

    //Verifica se foi efetuada a pesagem e enviada ao siscomex 
    public function verificaSiscomexAjax(){
        $parametros             =	$this->session->userdata();	
        
        //$placa                  =	str_replace('-','',$_POST['placa']); 
        $placa                  =	$_POST['placa']; 
        $resultado['existe']	= 	$this->transitoM->VerificaSiscomex($placa);

        echo json_encode(array('retorno' => $resultado));

    }

    public function excluirTransitoAjax(){

        $parametros     =	$this->session->userdata();	
        
        $id             =	$_POST['id'];
        $resultado['existe_movimento'] = $this->transitoM->existeMovimento($id);
        $verifica	= 	$resultado['existe_movimento'];
        
        if($verifica[0]->existe > 0){
            echo json_encode(array('retorno' => $resultado));
        } else{
            $resultado['exclui_due']	    = 	$this->transitoM->excluirTodasDue($id);
            $resultado['exclui_transito']	= 	$this->transitoM->excluirTransito($id);
            echo json_encode(array('retorno' => $resultado));
        }

        
    }

    public function emissaotransito(){
        $parametros			 		=	$this->session->userdata();		
        $parametros['title']		=	"Emissão de Extrato de Trânsito Simplificado";
        
        $this->_load_view('area-transito/emissao-transito',$parametros);
    }

    public function emiteExtrato(){

		$parametros 				= 	$this->session->userdata();
		$parametros['title']		=	"Emite Extrato";
		$parametros['placa']		=	$_POST['placa'];
        //verifica qual metodo vai chamar se for checado
		
        if(empty($this->input->post('fl_reimprimir'))){
            //Tras todos
            $parametros['listagem']	    =	$this->transitoM->getExtrato($parametros['placa'],0);	
        }else{
            //Traz o ultimo
            $parametros['listagem']	    =	$this->transitoM->getExtrato($parametros['placa'],1);	
        }
			
        $parametros['data_hora']    =   $this->acessoM->dataHoraExata();  
        $update = array('placa' => (mb_strpos($parametros['placa'],'-') === false) ? substr_replace($parametros['placa'],'-',3, 0) : $parametros['placa'],
                        'fl_impresso'    =>  'S' );
        
        $this->transitoMM->update($update);
		
		$this->_load_view('area-transito/emite-extrato',$parametros);
				
	}
    
	
}