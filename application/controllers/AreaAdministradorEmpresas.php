<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Dompdf\Dompdf;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');


class AreaAdministradorEmpresas extends MY_Controller { 
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() ||  ($this->session->userdata('tipo_acesso') != 'administrador empresas')  ) {
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');		
		$this->load->model('CotasModel', 'cotasM');
		$this->load->model('AcessoModel', 'acessoM');
		$this->load->model('AgendamentoModel', 'agendaM');
		$this->load->helper('form');
		$this->load->helper('url');
		
	}

	public function index()
	{
		$parametros				=	$this->session->userdata();		
		$parametros['title']	=	"Área do Administrador do sistema";		
		$this->_load_view('area-administrador-empresas/index',$parametros);

	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){
			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-administrador-empresas/editar-cliente',	$parametros );	
			}

		}else{

			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-administrador-empresas/editar-cliente',	$parametros );	
		}
	
	}

	public function gestaoUsuarios()
	{

		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuariosEmpresa($this->session->userdata('empresa_id'));		
		$parametros['title']	=	"Gestão de Usuários";
		$this->_load_view('area-administrador-empresas/gestao-usuario/gestao-usuario-e',$parametros);
	}

	/****************************************************************************
	**************** Método Ajax - Alterar status do usuario ********************
	*****************************************************************************/
	public function alteraStatus()
	{
		
		$dados = array(	'id'		=>	$_POST['id'],
						'ativo'	=>	$_POST['ativo']	);

		if($this->usuariosM->atualizaStatus($dados)){
			$this->log('Área Administrador Empresas | ativa usuário','usuarios','EDIÇÂO', $this->session->userdata('usuario_id'), $dados,$dados,$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso'));
		
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}
	
	private function enviaEmailConfirmacao($email_destino){

 		$email = '	<html>
						<head></head>
						<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
							<div class="content" style="width: 600px; height: 100%;">
								<img src="http://www.wertco.com.br/bootstrap/img/confirmacaoExpopostos.png" />
							</div>
						</body>
					</html>';	
		
		$this->load->library('email');
		
		$result = $this->email
		    ->from('vendas@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to($email_destino)
		    ->subject('WERTCO - Confirmação de Acesso')
		    ->message($email)
		    ->send();

		return $result;	

 	}

	/****************************************************************************
	*****************************************************************************
	**************** Método Responsável por Editar Usuários	*********************
	*****************************************************************************
	*****************************************************************************/

	public function editarUsuario($id = null){

		if($this->input->post('salvar') == 1){
			
			$update = $this->input->post();			
			unset($update['salvar']);
			unset($update['senha2']);
			unset($update['senha']);			

			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha')));
			}
			 
			if($this->usuariosM->atualizaUsuario($update)){				
				$this->log('Área Administrador | atualiza usuário','usuarios','EDIÇÂO', $this->session->userdata('usuario_id'), $update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['title']	=	"Gestão de Usuários";
				$parametros['dados']	=	$this->usuariosM->getUsuarios();
				$this->_load_view('area-administrador-empresas/gestao-usuario/gestao-usuario',	$parametros );
			}
		}else{
			
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->usuariosM->getUsuario($id);
			$parametros['title']				=	"Editar Usuários";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->getEmpresasPorCliente($this->session->userdata('empresa_id'));
			$parametros['subtipos']				= 	$this->usuariosM->buscaSubtipoCadastro();			
			$this->_load_view('area-administrador-empresas/gestao-usuario/editar-usuario',	$parametros );
		}
	}

	public function gestaoEmpresas($pesquisa = null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->empresasM->getEmpresasPorCliente($this->session->userdata('empresa_id'));
		$parametros['title']	=	"Gestão de Clientes";
		$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-administrador-empresas/empresas/gestao-clientes',$parametros);
	}

	public function visualizarEmpresa($id,$pesquisa=null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->empresasM->getEmpresasUsuarios($id);			
		$parametros['subtipos']	=	$this->empresasM->getSubtipoByTipo($parametros['dados'][0]->tipo_cadastro_id);
		//$parametros['clientes'] = 	$this->empresasM->getClientesClassificadoras();
		$parametros['clientes_vinculados'] 	= 	$this->empresasM->getClientesEmpresas($id);
		$parametros['title']	=	"Empresa #".$id;
		$parametros['pesquisa'] = 	$pesquisa;
		
		$this->_load_view('area-administrador-empresas/empresas/visualiza-empresa',$parametros);
	}

	public function cadastrarEmpresa()
	{
		if($this->input->post()){

			$dadosEmpresa = array(
		        'razao_social' 		=>	$this->input->post('razao_social'),
				'fantasia' 			=>	$this->input->post('fantasia'),
				'cnpj' 				=>	$this->input->post('cnpj'),
				'telefone' 			=>	$this->input->post('telefone'),
				'endereco' 			=>	$this->input->post('endereco'),
				'email' 			=>	$this->input->post('email'),
				'cidade' 			=>	$this->input->post('cidade'),
				'estado' 			=>	$this->input->post('estado'),
				'tipo_cadastro_id'	=> 	1,
				'insc_estadual'		=> 	$this->input->post('inscricao_estadual'),
				'pais'				=> 	$this->input->post('pais'),
				'bairro'			=> 	$this->input->post('bairro'),
				'cep'				=> 	$this->input->post('cep'),
				'cartao_cnpj'		=>	$this->input->post('cartao_cnpj')
		    );
			
			$conteudo= 	"CNPJ: ". 	$this->input->post('cnpj')." <br/> ";
			$conteudo.= "Razão Social: ". 	$this->input->post('razao_social')." <br/> ";
			$conteudo.=	'Fantasia: '.	$this->input->post('fantasia').' <br/> ';
			$conteudo.=	'CNPJ: '.	$this->input->post('cnpj').' <br/> ';
			$conteudo.=	'Telefone: '.	$this->input->post('telefone').' <br/> ';
			$conteudo.=	'Endereço: '.	$this->input->post('endereco').' <br/> ';
			$conteudo.=	'Cidade:  '	.	$this->input->post('cidade').' <br/> ';
			$conteudo.=	'Estado:  '	.	$this->input->post('estado').' <br/> ';
			$conteudo.=	'País: 	  '.	$this->input->post('pais').' <br/> ';	
			$conteudo.=	'Usuário que cadastrou:	  '.	$this->session->userdata('nome');

			if($cliente_id = $this->empresasM->add($dadosEmpresa))
			{
				$this->log('Área Administrador Cliente| cadastro empresa','empresas','INSERÇÃO', $this->session->userdata('usuario_id'),$dadosEmpresa,$dadosEmpresa,$_SERVER['REMOTE_ADDR']);
				//$this->enviaEmail('webmaster@companytec.com.br','Empresa Cadastrada',$conteudo,$this->input->post('cartao_cnpj'));
				// insere e já vincula o cliente ao administrador				
				$vinculaCliente = 	array(	'administrador_id' 	=>	$this->session->userdata('empresa_id'),
											'cliente_id' 		=> 	$cliente_id);
				$this->empresasM->insereClienteAdm($vinculaCliente);								
				$this->session->set_flashdata('retorno', 'sucesso');	
				redirect('/AreaAdministradorEmpresas/gestaoEmpresas/');			
			}else{
				$this->session->set_flashdata('erro', 'erro');
				$this->gestaoEmpresas();
			}					 
			
		}else{


			$parametros 			= 	$this->session->userdata();		
			//$parametros['estados']	=	$this->icmsM->getEstados();
			$parametros['title']	=	"Cadastro de Empresas";

			$parametros 					= 	$this->session->userdata();		
			$parametros['estados']			=	$this->empresasM->getEstados();
			$parametros['tipo_cadastros']	=	$this->empresasM->getTiposCadastro();
			$parametros['title']			=	"Cadastro de Empresas";

			$this->_load_view('area-administrador-empresas/empresas/cadastra-empresa',$parametros);
		}
	}

	public function editarEmpresa($id = null, $pesquisa = null){

		if($this->input->post('salvar') == 1){

			$update = $this->input->post();
			
			unset($update['salvar']);
			unset($update['tipo_acesso_id']);
			$update['tipo_cadastro_id'] = $this->input->post('tipo_acesso_id');			

			if($this->empresasM->atualizaEmpresas($update)){				
				$this->log('Área Administrador | editar empresa','empresas','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('retorno', 'sucesso');	
				redirect('/AreaAdministradorEmpresas/gestaoEmpresas/');	
			}

		}else{
			$row 								= 	$this->empresasM->getEmpresa($id);		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Empresas";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();			
			$parametros['pesquisa']				= 	$pesquisa;			 
			$this->_load_view('area-administrador-empresas/empresas/editar-empresa',	$parametros );	
		}
	}

	public function vincularClienteClass()
	{
		$dados_insert = $this->input->post();
		if($this->empresasM->vincularCliente($dados_insert)){
			
			$this->session->set_flashdata('sucesso', 'ok');	
 		    redirect('/AreaAdministrador/visualizarEmpresa/'.$dados_insert['classificadora_id']);
		}
	}

}

