<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class AreaExpedicao extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() ||  (($this->session->userdata('tipo_acesso') != 'expedicao') && ($this->session->userdata('tipo_acesso') != 'administrador geral')) ) {
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('AgendamentoModel', 'agendaM');		
		$this->load->model('MotoristaModel', 'motoristaM');
		$this->load->model('NotaFiscalModel', 'notaM');
		$this->load->model('AcessoModel', 'acessoM');
		$this->load->model('TerminalModel', 'terminalM');
		$this->load->helper('form');
		$this->load->helper('url');
	}
 
	public function index()
	{
		
		$parametros						=	$this->session->userdata();
		$parametros['title']			=	"Área da Expedição";	
		// Passa o parametro 2 para identificar que é expedição
		$parametros['classificacao']	= 	$this->acessoM->buscaCaminhoesPatio($this->session->userdata('empresa_id'),2);
		$parametros['classificado']		= 	$this->acessoM->buscaCaminhoesClassificadosHoje($this->session->userdata('usuario_id'),2);
		$parametros['destinos']			= 	$this->terminalM->buscaTerminais();
		$this->_load_view('area-expedicao/index-expedicao',$parametros);

	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){
			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-expedicao/editar-cliente',	$parametros );	
			}

		}else{

			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-expedicao/editar-cliente',	$parametros );	
		}
	
	}	

	public function classificaAcesso()
	{
		$atualizaAcesso = array(	'id'					=> 	$this->input->post('acesso_id'),
									'id_classificadora' 	=>	$this->session->userdata('empresa_id'),
									'id_usuario_classific'	=> 	$this->session->userdata('usuario_id'),
									'dthr_classific' 		=> 	date('Y-m-d H:i:s'),
									'fl_atende_classific'	=> 	$this->input->post('situacao'),
									'motivo_classific'		=> 	$this->input->post('motivo'),
									'id_terminal'			=> 	$this->input->post('terminal') 	);

		if( $this->acessoM->atualizaAcesso($atualizaAcesso) ){

			$atualizaTerminal = array( 	'id' 			=> 	$this->input->post('agendamento_id'),
										'id_terminal'	=> 	$this->input->post('terminal') 	);
			if($this->agendaM->atualiza($atualizaTerminal)){
				echo json_encode(array('retorno' => 'sucesso' ));
			}else{
				echo json_encode(array('retorno' => 'erro' ));	
			}
		}else{
			echo json_encode(array('retorno' => 'erro' ));
		}
	}
	
}