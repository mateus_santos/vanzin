<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include __DIR__.'/src/SimpleXLSX.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class AreaClientes extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() ||  (($this->session->userdata('tipo_acesso') != 'clientes') && ($this->session->userdata('tipo_acesso') != 'administrador geral') && ($this->session->userdata('tipo_acesso') != 'administrador empresas') && ($this->session->userdata('tipo_acesso') != 'gate terminal')) ) {
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('AgendamentoModel', 'agendaM');		
		$this->load->model('MotoristaModel', 'motoristaM');
		$this->load->model('NotaFiscalModel', 'notaM');
		$this->load->model('MensagensModel', 'mensagensM');
		$this->load->model('CotasModel', 'cotasM');
		$this->load->model('TurnoModel', 'turnoM');
		$this->load->helper('form');
		$this->load->helper('url');

	}
 
	public function index()
	{
		$parametros				=	$this->session->userdata();		
		$parametros['title']	=	"Área do Cliente";		
		$this->_load_view('area-administrador/index',$parametros);

	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){
			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-clientes/editar-cliente',	$parametros );	
			}

		}else{

			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-clientes/editar-cliente',	$parametros );	
		}
	
	}	

	public function agendamentos()
	{
		$parametros			 			=	$this->session->userdata();		
		$parametros['title']			=	"Agendamentos";		
		$empresa_id	 					= 	$this->session->userdata('empresa_id');

		if( $parametros['tipo_acesso'] == 'administrador geral' || $parametros['tipo_acesso'] == 'gate terminal'){
			$empresa_id = '';
			$parametros['empresas']		= 	$this->empresasM->getClientes();
		}

		if( $parametros['tipo_acesso'] == 'administrador empresas'){
			$parametros['empresas']		= 	$this->empresasM->getClientesEmpresas($empresa_id);
		}

		$parametros['agendamentos'] 	= 	$this->agendaM->buscaAgendamentosDoDiaGeral($empresa_id, $parametros['tipo_acesso']);

		$this->_load_view('area-clientes/agendamentos/agendamentos',$parametros);

	}

	public function mensagens()
	{
		$parametros			 		=	$this->session->userdata();		
		$parametros['title']		=	"Mensagens";		
		$parametros['usuario_id']	= 	$this->session->userdata('usuario_id');		
		$parametros['mensagens'] 	= 	$this->mensagensM->buscaMensagens();

		$this->_load_view('area-clientes/mensagens/mensagens',$parametros);
	}

	public function cadastrarMensagem()
	{
		$parametros			 		=	$this->session->userdata();		
		$parametros['title']		=	"Cadastrar Novo Agendamento";		
		$parametros['empresa'] 		= 	$this->session->userdata('razao_social');		
		$parametros['id_empresa'] 	= 	$this->session->userdata('empresa_id');		
		if( $this->session->userdata('tipo_acesso') == 'administrador geral' ){
			
			$parametros['clientes']		= 	$this->usuariosM->getUsuarios();
		}elseif($this->session->userdata('tipo_acesso') == 'administrador empresas'){
			$parametros['clientes'] 	= 	$this->usuariosM->getUsuariosClientesEmpresas($parametros['id_empresa']); 
		}else{
			$parametros['clientes'] 	= 	$this->usuariosM->getUsuariosEmpresa(2); 
		}
		
		$this->_load_view('area-clientes/mensagens/cadastrar-mensagem',$parametros);
	}

	/*
	*
	* Função para inserção de todo agendamento
	* Method: POST
	*
	*/
	public function inserirMensagem()
	{
		
		$dadosMensagem = array(	'titulo' 			=> 	$this->input->post('titulo'),
								'descricao'			=> 	$this->input->post('descricao'),								
								'usuario_id'		=> 	$this->session->userdata('usuario_id') 	);

		if($mensagem_id = $this->mensagensM->insere($dadosMensagem)){			

			foreach( $this->input->post('destinatario_id') as $destino ){
				$dadosDestino = array('mensagem_id'	=> $mensagem_id,
										'usuario_id'	=> $destino	);

				$this->mensagensM->insereDestino($dadosDestino);
			}	
			$this->session->set_flashdata('retorno', 'sucesso');	
	 		redirect('/AreaClientes/Mensagens/');
		}else{
			$this->session->set_flashdata('retorno', 'erro');	
			$this->session->set_flashdata('msg', 'Não inseriu entre em contato com a vanzin');
		    redirect('/AreaClientes/Mensagens/');
			
 		}

	}

	public function visualizarMensagem($mensagem_id)
	{
		
		$parametros			 		=	$this->session->userdata();		
		$parametros['title']		=	"Visualizar Mensagem";		
		$parametros['dados']		= 	$this->mensagensM->buscaMensagem($mensagem_id);		
		$parametros['respostas'] 	= 	$this->mensagensM->buscaRespostas($mensagem_id);

		$this->_load_view('area-clientes/mensagens/visualizar-mensagem',$parametros);	

	}

	public function enviarResposta()
	{
		
		$dadosMensagem = array(	'mensagem_id' 		=> 	$this->input->post('mensagem_id'),
								'descricao'			=> 	$this->input->post('descricao'),		
								'destinatario_id'	=> 	$this->input->post('destinatario_id'),								
								'usuario_id'		=> 	$this->session->userdata('usuario_id') );

		if($mensagem_id = $this->mensagensM->insereResposta($dadosMensagem)){
			
			$this->session->set_flashdata('retorno', 'sucesso');	
	 		redirect('/AreaClientes/visualizarMensagem/'.$this->input->post('mensagem_id'));
		}else{
			$this->session->set_flashdata('retorno', 'erro');	
			$this->session->set_flashdata('msg', 'Não inseriu entre em contato com a vanzin');
		    redirect('/AreaClientes/Mensagens/');
			
 		}

	}

	public function retornaAgendamentosAjax(){

    	$parametros 	= 	$this->input->post();
    	$filtros = "";
    	if($parametros['dt_ini'] != '' & $parametros['dt_fim'] != ''){
    		$dt_ini = explode('/', $parametros['dt_ini']);
    		$dt_fim = explode('/', $parametros['dt_fim']);
    		$filtros.=" and agendamento.dt_agenda between '".$dt_ini[2]."-".$dt_ini[1]."-".$dt_ini[0]."' and '".$dt_fim[2]."-".$dt_fim[1]."-".$dt_fim[0]."'";
    	}	    
    	
    	if($parametros['placa'] != ''){
    		$filtros.=" and agendamento.placa = '".$parametros['placa']."'";
    	}
    	
    	if($parametros['nr_nf'] != ''){
			$filtros.=" and nota_fiscal.nr_nf = '".$parametros['nr_nf']."'";     				
    	}
    	
    	$empresa_id	 = 	$this->session->userdata('empresa_id');
		if( $this->session->userdata('tipo_acesso') == 'administrador geral'){
			$empresa_id = $parametros['empresa_id'];
		}

		$data['agendamentos']	= 	$this->agendaM->buscaAgendamentosGeral($empresa_id, $filtros);
		$retorno 				= 	$this->load->view('/area-clientes/agendamentos/agendamentos-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno ));

    }
 
	public function cadastrarAgendamento()
	{
		$parametros			 		=	$this->session->userdata();		
		$parametros['title']		=	"Cadastrar Novo Agendamento";		
		$parametros['empresa'] 		= 	$this->session->userdata('razao_social');		
		$parametros['id_empresa'] 	= 	$this->session->userdata('empresa_id');
		$parametros['fretes']		= 	$this->agendaM->buscaFretes();
		$parametros['turnos'] 		= 	$this->turnoM->buscaTurnos();
		if( $this->session->userdata('tipo_acesso') == 'administrador geral'){
			$parametros['id_empresa'] 	= 	'';
			$parametros['empresas']		= 	$this->empresasM->getClientes();
		}

		if( $this->session->userdata('tipo_acesso') == 'administrador empresas' || $parametros['tipo_acesso'] == 'gate terminal'){
			
			$parametros['empresas']		= 	$this->empresasM->getClientesEmpresas($parametros['id_empresa']);
		}
		$this->_load_view('area-clientes/agendamentos/cadastra-agendamento',$parametros);
	}

	/*
	*
	* Função upload da nota e validações de regras do agendamento
	* Method: AJAX/POST
	*
	*/
	public function uploadNfe()
	{
		
		$tipo 			= 	explode('.', $_FILES['file']['name']);
		$arquivo 		=	md5($_FILES['file']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
		
		$configuracao = array(
	        'upload_path'   	=> 	'./nfe_agendamento/',		        
	        'allowed_types' 	=> 	'xml',
	        'file_name'     	=> 	$arquivo
	    );      
		
	    $this->load->library('upload', $configuracao);
	    
	    if ($this->upload->do_upload('file')){
			//lê o xml e insere 	
			$xml=simplexml_load_file('./nfe_agendamento/'.$arquivo);
						
			if(isset($xml->protNFe->infProt->chNFe)){
				$verificaAgenda = $this->agendaM->verificaNotaAgendamento($xml->protNFe->infProt->chNFe);
			
				if( isset($verificaAgenda['id']) ){
					echo json_encode(array( 'retorno'		=> 'ja existe',
			    							'agendamento'	=> 	$verificaAgenda['id'],
			    							'placa'			=> 	$verificaAgenda['placa'] )	);	
				}else{

					$embalagens = $this->agendaM->buscaEmbalagem();
					$combo_embalagem = "";
					foreach( $embalagens as $embalagem ){
						$combo_embalagem.='<option value="'.$embalagem['id'].'">'.$embalagem['no_embalagem'].'</option>';
					}
					if(!$xml->NFe){
						echo json_encode(array('retorno' => 'padrao errado'));	
						die;
					}
					$arquivo_xml = "<input type='hidden' id='arquivo_xml_".$arquivo."' name='arquivo_xml[".$xml->NFe->infNFe->ide->nNF."][]' value='".$arquivo."' />";
					$nfe="";
					$nfe.= "<input type='hidden' name='nr_nf[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->ide->nNF."' />" ;
					$nfe.= "<input type='hidden' name='serie_nf[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->ide->serie."' />" ;
					$nfe.= "<input type='hidden' name='dt_emissao[".$xml->NFe->infNFe->ide->nNF."][]' value='".date('Y-m-d H:i:s', strtotime($xml->NFe->infNFe->ide->dhEmi))."' />" ;
					$nfe.= "<input type='hidden' name='no_emitente[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->emit->xNome."' />" ;
					$nfe.= "<input type='hidden' name='nr_doc_emitente[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->emit->CNPJ."' />" ;
					$nfe.= "<input type='hidden' name='no_destinatario[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->dest->xNome."' />" ;
					$nfe.= "<input type='hidden' name='cnpj_destinatario[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->dest->CNPJ."' />" ;
					$nfe.= "<input type='hidden' id='pesoB_".$xml->NFe->infNFe->ide->nNF."' name='peso_bruto_total[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->vol->pesoB."' />" ;
					$nfe.= "<input type='hidden' class='pesoL_nfe' id='pesoL_".$xml->NFe->infNFe->ide->nNF."' name='peso_liq_total[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->vol->pesoL."' />" ;
					$nfe.= "<input type='hidden' id='qtd_".$xml->NFe->infNFe->ide->nNF."' name='quant_total[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->vol->qVol."' />" ;
					$nfe.= "<input type='hidden' name='unid_medida[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->vol->esp."' />" ;
					$nfe.= "<input type='hidden' name='valor[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->total->ICMSTot->vNF."' />" ;
					$nfe.= "<input type='hidden' name='vl_ipi[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->total->ICMSTot->vIPI."' />" ;
					$nfe.= "<input type='hidden' name='chave_acesso[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->protNFe->infProt->chNFe."' />" ;
					$nfe.= "<input type='hidden' name='nr_doc_transp[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->transporta->CNPJ."' />" ;
					$nfe.= "<input type='hidden' name='no_transp[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->transporta->xNome."' />" ; 
					$html="";
					//teste de cfof
					$cfop = 0;
					foreach($xml->NFe->infNFe->det as $prod){
						// Se o CFOP da mercadoria iniciar diferente de 7, verifica se existe na tabela de cfop
						if(substr($prod->CFOP,0,1) != '7' ){
							
							$cfop_ = $this->agendaM->verificaCfop($prod->prod->CFOP);
							if( $cfop_['total_cfop'] == 0 ){
								$cfop++;
							}
						}	

						$html.='
								<tr>
									<td><input type="text" class="form-control" name="nr_pedido['.$xml->NFe->infNFe->ide->nNF.'][]" required /></td>
									<td><input type="text" class="form-control" name="mercadoria['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->xProd.'" />
										<input type="hidden" name="ncm['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->NCM.'" />
										<input type="hidden" name="ds_item['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->xProd.'" />
										<input type="hidden" name="qtde['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->qCom.'" class="qtd_produto_v" />
										<input type="hidden" name="valor['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->vProd.'" />
										<input type="hidden" name="cfop['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->CFOP.'" />
									</td> 
									<td>
										<select class="form-control" name="embalagem['.$xml->NFe->infNFe->ide->nNF.'][]" required="required" id="emb_'.$xml->NFe->infNFe->ide->nNF.'" onchange="verificaEmbalagem('.$xml->NFe->infNFe->ide->nNF.', this);">
											<option value="">Selecione</option>
											'.$combo_embalagem.'
										</select>
									</td>
									<td><input type="text" class="form-control qtd qtd_'.$xml->NFe->infNFe->ide->nNF.'"  name="qtd['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.intval($prod->prod->qCom).'" readonly="readonly"/></td> 
								</tr>';
					}
					
					if($cfop > 0){
						echo json_encode(array('retorno'	=> 'cfop errado'));		
									
					}else{

				    	echo json_encode(array('retorno'	=> 'sucesso',
				    							'html' 		=> 	$html,
				    							'arquivo' 	=> 	$arquivo_xml,
				    							'nfe' 		=> 	$nfe	 )	);
				    }
			    }
			}else{
				echo json_encode(array('retorno' => 'Nfe errada' ));	
			}
	    }else{
	    	echo json_encode(array('retorno' => 'Erro de upload' ));
	    }

	}

	public function verificaCpfAgendamento()
	{
		$where = array( 'cpf' 	=> 	$this->input->post('cpf'),
						'cnh' 	=> 	'' );
		echo json_encode($this->agendaM->verificaCpfCnh( $where ) ); 
	}

	public function verificaCnhAgendamento()
	{
		$where = array(	'cpf' 	=> 	'',
						'cnh' 	=> 	$this->input->post('cnh') ); 
		echo json_encode($this->agendaM->verificaCpfCnh( $where )); 
	}

	/*
	*
	* Função para inserção de todo agendamento
	* Method: POST
	*
	*/
	public function inserirAgendamento()
	{
		//verifica se motorista já está cadastrado, senão cadastra o motorista
		if( ! $this->input->post('id_motorista') ){

			$insereMotorista = array(	'cpf' 		=> 	$this->input->post('cpf'),
										'nr_cnh' 	=> 	$this->input->post('cnh'),
										'nome'		=> 	$this->input->post('motorista'),
										'fone'		=> 	$this->input->post('telefone'),
										'fl_estrangeiro' => $this->input->post('fl_motorista')	);

			if( $this->input->post('fl_estrangeiro') == 1 ){
				$insereMotorista['nr_doc'] = $this->input->post('cpf');
			}
			
			$id_motorista = $this->motoristaM->insere($insereMotorista);
			
		}else{
			$id_motorista = $this->input->post('id_motorista');
		}

		$dt_agenda = explode('/',$this->input->post('data')); 
		
		$placa = strtoupper($this->input->post('placa')); 
		
		if(strpos($this->input->post('placa'), '-') === false){

			$placa = substr_replace($this->input->post('placa'), '-', 3, 0);
			
		}

		$dadosAgenda = array(	'id_tipo_operacao' 	=> 	$this->input->post('id_tipo_operacao'),
								'dt_agenda' 		=> 	$dt_agenda[2]."-".$dt_agenda[1]."-".$dt_agenda[0],
								'id_empresa'		=> 	$this->input->post('id_empresa'),
								'placa'				=> 	$placa,
								'id_motorista'		=> 	$id_motorista,
								'id_terminal'		=> 	1,
								'id_usuario'		=> 	$this->session->userdata('usuario_id'),
								'id_frete'			=> 	$this->input->post('id_frete'),
								'id_transportadora'	=> 	$this->input->post('id_transportadora'), 
								'id_turno'			=> 	$this->input->post('id_turno') );

		$id_agenda = $this->agendaM->insere($dadosAgenda);
		$this->enviaApiCredenciamento($id_agenda);
		$this->enviaApiCredenciamentoPessoas($id_agenda);
		if($id_agenda != ''){
			//insere agendamento item			

			$i=0;
			$erro = 0;			
			//var_dump($this->input->post());
			if($this->input->post('id_tipo_operacao') == 2){
				$dadosItensAgenda = array(	'id_agenda' 	=> 	$id_agenda,
											'descricao'		=> 	$this->input->post('mercadoria'),
											'id_embalagem' 	=> 	0,
											'nr_pedido'		=> 	$this->input->post('nr_pedido'),
											'peso'			=> 	str_replace('.','',$this->input->post('qtd'))	);
										
				if(!$this->agendaM->insereItem($dadosItensAgenda)){
					$erro++;
				}
			}else{

				foreach( $this->input->post('nr_nf') as $index => $nr_nf ){
					//insere nota fiscal
					//echo $index.' - <br>';
					$i=0;
					foreach($nr_nf as $nr_nf){
						//echo $this->input->post('serie_nf')[$nr_nf][$i];die;					
						$dadosNf = array(	'id_agenda'			=> 	$id_agenda,
											'nr_nf' 			=> 	$nr_nf,
											'serie_nf'			=> 	$this->input->post('serie_nf')[$nr_nf][$i],
											'dt_emissao' 		=> 	$this->input->post('dt_emissao')[$nr_nf][$i],
											'no_emitente'		=> 	$this->input->post('no_emitente')[$nr_nf][$i],
											'nr_doc_emitente' 	=> 	$this->input->post('nr_doc_emitente')[$nr_nf][$i],	
											'no_destinatario'	=> 	$this->input->post('no_destinatario')[$nr_nf][$i],
											'cnpj_destinatario'	=> 	$this->input->post('cnpj_destinatario')[$nr_nf][$i],
											'peso_bruto_total'	=> 	$this->input->post('peso_bruto_total')[$nr_nf][$i],	
											'peso_liq_total'	=> 	$this->input->post('peso_liq_total')[$nr_nf][$i],
											'quant_total'		=> 	$this->input->post('quant_total')[$nr_nf][$i],
											'unid_medida'		=> 	$this->input->post('unid_medida')[$nr_nf][$i],
											'vl_ipi'			=> 	$this->input->post('vl_ipi')[$nr_nf][$i],
											'chave_acesso'		=> 	$this->input->post('chave_acesso')[$nr_nf][$i],
											'nr_doc_transp'		=> 	$this->input->post('nr_doc_transp')[$nr_nf][$i],
											'no_transp'			=> 	$this->input->post('no_transp')[$nr_nf][$i],
											'arquivo'			=> 	$this->input->post('arquivo_xml')[$nr_nf][$i]	 );
						
						
						$id_nf = $this->notaM->insere($dadosNf);
						if($id_nf != ''){
							$i = 0;	
							//insere itens do agendamento
							
							foreach($this->input->post('mercadoria')[$nr_nf] as $merc){

								$dadosItensAgenda = array(	'id_agenda' 	=> 	$id_agenda,
															'descricao'		=> 	$merc,
															'id_embalagem' 	=> 	$this->input->post('embalagem')[$nr_nf][$i],
															'nr_pedido'		=> 	$this->input->post('nr_pedido')[$nr_nf][$i],
															'peso'			=> 	str_replace('.','',$this->input->post('qtd')[$nr_nf][$i])	);
										
								$this->agendaM->insereItem($dadosItensAgenda);
								$i++;	

							}

							$i = 0;	
							foreach($this->input->post('ncm')[$nr_nf] as $ncm){
								$dadosItensNf = array(	'id_nota_fiscal'=> 	$id_nf,
														'ncm'			=> 	$ncm[$i],
														'ds_item' 		=> 	$this->input->post('ds_item')[$nr_nf][$i],
														'qtde'			=> 	$this->input->post('qtde')[$nr_nf][$i],
														'valor'			=> 	$this->input->post('valor')[$nr_nf][$i],
														'cfop'			=> 	$this->input->post('cfop')[$nr_nf][$i]	);

								$this->notaM->insereItem($dadosItensNf);
								$i++;	

							}
						}else{
							$erro++;
							
						}
						$i++;
					}
					$i++;
				}
				
				
			}
			if( $erro > 0){
				$this->session->set_flashdata('retorno', 'erro');	
				$this->session->set_flashdata('msg', 'Não inseriu a nota fiscal, entre em contato com o suporte técnico');
			    redirect('/AreaClientes/Agendamentos/');
			}
		}else{
			$this->session->set_flashdata('retorno', 'erro');	
			$this->session->set_flashdata('msg', 'Não inseriu o agendamento, entre em contato com o suporte técnico');	
 		    redirect('/AreaClientes/Agendamentos/');
		}		

		$this->session->set_flashdata('retorno', 'sucesso');	
 		redirect('/AreaClientes/Agendamentos/');

	}


	//Importa os Agendamentos
	public function importarAgendamentos($valores =null)
	{
		
		$parametros 					= 	$this->session->userdata();
		$parametros['dados']			=	$this->agendaM->getLogErrosAgenda();
		$parametros['dados_cliente']	=	$this->empresasM->getEmpresas();

		if( $this->session->userdata('tipo_acesso') == 'administrador empresas' ){
			$parametros['dados_cliente']	=	$this->empresasM->getEmpresasPorCliente( $this->session->userdata('empresa_id') );
		}else{
			$parametros['dados_cliente']	=	$this->empresasM->getEmpresas();
		}
		$parametros['title']			=	"Importa Agendamentos";
		$parametros['importou']			=	$valores;
			
		//$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-clientes/agendamentos/importar-agendamentos',$parametros);
	}

	//Grava erros no log
	public function insere_erros($nr_arquivo, $ds_erro, $arquivo, $id_user, $dthr_atual){

		$dados_erro = array(
			'nr_agenda_numero'  => 	$nr_arquivo,						
			'ds_erro'			=>	$ds_erro,
			'arquivo' 			=> 	$arquivo,
			'id_usuario' 		=>	$id_user,
			'dthr_importado'	=> $dthr_atual
		);
		
		if($this->agendaM->insere_log_erro($dados_erro)){
			return 1;//Sucesso	
		}else{
			return 0;//Erro	
		}

	}

	/* Esta sendo usa no Sistema devido a versão do PHP ser 7.028 */
	public function importaArquivoAgendamentos(){

		if(isset($_FILES['arquivo'])){
			$ext = strtolower(substr($_FILES['arquivo']['name'],-4)); //Pegando extensão do arquivo
			$new_name ="planilha_".date('Y-m-d-H-i') .".". $ext; //Definindo um novo nome para o arquivo
			$dir = './../vanzin/uploads/'; //Diretório para uploads
			$nome_completo = $dir . $new_name;
			move_uploaded_file($_FILES['arquivo']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
			//echo $nome_completo;

//$xlsx->sheetNames() = Nome da aba da Planilha

			//if ( $xlsx = SimpleXLSX::parse('./../vanzin/uploads/teste.xlsm') ) {
			if ( $xlsx = SimpleXLSX::parse($dir.$new_name) ) {
				//echo '<pre>'.print_r( $xlsx->sheetNames(), true ).'</pre>';
			
				// output worsheet 1
				$records = $xlsx->rows();
				//echo '<pre>'.print_r( $records, true ).'</pre>';
			
				$parametros 					= 	$this->session->userdata();

				$id_usuario 		= $parametros['usuario_id'];
				$dthr_atual 		= date('Y-m-d H:i:s');
				//Expedição é o tipo
				$tipo_operacao 		= 2;
				$numero 			= null;
				$id_empresa 		= 35;
				$id_motorista 		= null;
				$motorista 			= '';
				$peso 				= '';
				$conta_linhas 		= 0;
				$erros 				= '';//Variavel utilizada para registro dos erros na importação.
				$id_agenda_anterior = null;
				$cliente_final		= null;

				$dim = $xlsx->dimension();
				$num_cols = $dim[0];
				$num_rows = $dim[1];
				
				//verifica e realizar a exclusão dos Logs
				if($num_rows > 0){
					$this->agendaM->excluirLogsAgenda();
				}
				
				foreach ( $records as $valor ) {
					$conta_linhas++;
					//Trata o valor do número
					if(!empty($valor[0])){

						if(trim(strToUpper($valor[0])) != 'NUMERO'){
							$numero = $valor[0];
						
						}else{
							$numero = null;
						}
					}else{
						$numero = null;
					//	$ds_erro= " Linha Excel: ".$conta_linhas." Não importada - Coluna número Vazia ou inexistente. Por Favor Verifique.";
					//	$log 	= $this->insere_erros($numero, $ds_erro, $nome_completo, $id_usuario, $dthr_atual);
							
					}

					 
					//Trata o valor do frete
					if(!empty($valor[1])){

						if(trim(strToUpper($valor[1])) != 'FRETE'){
							$frete = $valor[1];
							
							$buscaFrete = $this->agendaM->verificaFrete($frete);
							
							if(!empty($buscaFrete)){
								$total_frete	= $buscaFrete[0]->total;

								if($total_frete == 0){
									$ds_erro = " Linha Excel: ".$conta_linhas." Não importada -  Frete Inexistente. Por Favor Verifique.";
									$log = $this->insere_erros($numero, $ds_erro, $nome_completo, $id_usuario, $dthr_atual);
								}else{
									$id_frete	= $buscaFrete[0]->id;
								}
							}else{
								$ds_erro = " Linha Excel: ".$conta_linhas." Não importada -  Frete Inexistente. Por Favor Verifique.";
								$log = $this->insere_erros($numero, $ds_erro, $nome_completo, $id_usuario, $dthr_atual);
								
							}

						}
					}
					
					//Trata o valor do data
					if(!empty($valor[2])){

						if(trim(strToUpper($valor[2])) != 'DATA'){
						
							$dt_agenda = substr($valor[2],0,10); 
							
						}
					}

					//Trata o valor do CNPJ
					if(!empty($valor[3])){

						/*if(trim(strToUpper($valor[3])) != 'CNPJ'){
							$cnpj = $valor[3];
							
							//Retorna a empresa pelo cnpj
							$dadosEmpresa = $this->empresasM->getCnpj($cnpj);
							if(count($dadosEmpresa) == 0){
								$ds_erro = " Linha Excel: ".$conta_linhas." Não importada - Empresa não cadastrada. Por Favor Verifique.";
								$id_empresa = null;

								$log = $this->insere_erros($numero, $ds_erro, $nome_completo, $id_usuario, $dthr_atual);
						
							}else{
								$id_empresa	= $dadosEmpresa[0]->id;
								
							}
							
						}*/

						if(trim(strToUpper($valor[3])) != 'CLIENTE'){
							$cliente_final = $valor[3];

						}

						
					}

					//Trata o valor do Placa
					if(!empty($valor[4])){

						if(trim(strToUpper($valor[4])) != 'PLACA'){
							$placa = $valor[4];
							
							if (!strstr($placa)){
								$placa = substr($placa,0,3).'-'.substr($placa,-4);
							}
							
						}
					}
				

					//Trata o valor do CPF
					if(!empty($valor[5])){

						if(trim(strToUpper($valor[5])) != 'CPF'){
							$cpf = $valor[5];
							
							//Retorna do motorista
							$dadosMotorista = $this->motoristaM->verificaCPF($cpf);
							if(count($dadosMotorista) == 0){

								$id_motorista = null;
								$ds_erro = " Linha Excel: ".$conta_linhas." - Motorista não cadastrado. Por Favor Verifique.";
								$log = $this->insere_erros($numero, $ds_erro, $nome_completo, $id_usuario, $dthr_atual);
						
							}else{
								$id_motorista = $dadosMotorista[0]->id;
								
							}
							
						}
					}
					
					
					//Trata o valor do CNH
					if(!empty($valor[6])){

						if(trim(strToUpper($valor[6])) != 'CNH'){
							$cnh = $valor[6];
						}
					}
					
					
					//Trata o valor do Motorista
					if(!empty($valor[7])){

						if(trim(strToUpper($valor[7])) != 'MOTORISTA'){
							$motorista = $valor[7];
						}
					}
					
					//Trata o valor do Transportadora
					if(!empty($valor[8])){
						$id_transportadora = null;
						if(trim(strToUpper($valor[8])) != 'CNPJ'){
							$transportadora = $valor[8];

							//Retorna a empresa pelo cnpj
							$dadosTransp = $this->empresasM->getCnpjNaoFormatado($transportadora);
							if(count($dadosTransp) == 0){
								$ds_erro = " Linha Excel: ".$conta_linhas." Não importada - Transportadora não cadastrada. Por Favor Verifique.";
								$id_empresa = null;

								$log = $this->insere_erros($numero, $ds_erro, $nome_completo, $id_usuario, $dthr_atual);
						
							}else{
								$id_transportadora	= $dadosTransp[0]->id;
								
							}
						}
					}

					
					//Trata o valor da Transportadora
					if(!empty($valor[9])){

						if(trim(strToUpper($valor[9])) != 'TRANSPORTADORA'){
							
							$nome_transportadora = $valor[9];
							
						}
					}
					

					//Trata o valor do Telefone
					
					if(!empty($valor[10])){

						if(trim(strToUpper($valor[10])) != 'TELEFONE'){
							$telefone = $valor[10];
						}
					}
					//Caso não tenha o motorista realiza o cadastro
					if(($id_motorista == null and $motorista != '') and $cpf != ''){
						
						//Insere o motorista quando não existe
						$insereMotorista = array(	
							'cpf' 		=> 	$cpf,
							'nr_cnh' 	=> 	$cnh,
							'nome'		=> 	$motorista,
							'fone'		=> 	$telefone);
							
						$id_motorista = $this->motoristaM->insere($insereMotorista);
					}
				

					//Trata o valor do Telefone
					if(!empty($valor[10])){

						if(trim(strToUpper($valor[10])) != 'TELEFONE'){
							$telefone = $valor[10];
						}
					}
					

					//Id do terminal 
					$id_terminal = 1;
					$dados = array();
					
					if(!empty($valor[1])){
						
						if(trim(strToUpper($valor[1])) != 'FRETE'){

							//Verifica se o frete é 1 = CIF, caso for aplica as validações, quando for tipos 2 ou 3 não aplica algumas validações
							if($id_frete == 1){
								echo 'Registro:d '.$numero.' - '.trim(strToUpper($valor[1])).'</br>';
									$dados = array(
										'id_tipo_operacao'	=>	$tipo_operacao,
										'dt_agenda' 		=> 	$dt_agenda,
										'id_terminal' 		=>	$id_terminal,
										'id_empresa'		=> 35,//Código da Empresa TIMAC, única empresa a fazer a importação
										'placa'				=> $placa,
										'id_motorista'		=>	$id_motorista,
										'id_usuario'		=> $id_usuario,
										'dthr_insercao'		=> $dthr_atual,
										'fl_email_enviado'	=>	0,
										'id_frete'			=> $id_frete,
										'id_transportadora'	=> $id_transportadora
										
									);	
									
							}else{
								//Entra aqui quando for os outros tipos de frete
								$dados = array(
									'id_tipo_operacao'	=>	$tipo_operacao,
									'dt_agenda' 		=> 	$dt_agenda,
									'id_terminal' 		=>	$id_terminal,
									'id_empresa'		=> 35,//Código da Empresa TIMAC, única empresa a fazer a importação
									'placa'				=> $placa,
									'id_motorista'		=>	1,//Códido de motorista não informado
									'id_usuario'		=> $id_usuario,
									'dthr_insercao'		=> $dthr_atual,
									'fl_email_enviado'	=>	0,
									'id_frete'			=> $id_frete,
									'id_transportadora'	=> 0,
									'cliente_final'		=> $cliente_final,
									'transportadora'	=> $nome_transportadora,
									'motorista'			=> $motorista
									
								);	
							}
							/*echo " insert - <pre>";
							print_r($dados);
							echo "</pre></br></br>";
*/
//echo $numero.' - '.$id_empresa.' - '.$id_agenda_anterior.' motorista - '.$id_motorista.'</br></br>';
							if(count($dados) > 0){
								echo 'Aqui '.$id_empresa.' - '.$id_agenda_anterior.' - '. $cliente_final.'</br>';
								if($id_empresa != null || $id_agenda_anterior != null || $cliente_final != null){
									
									//Cria o agendamento
									if($id_empresa != null || $cliente_final != null){
										if(($numero == null || $numero == '' ) && $id_agenda_anterior != null){
										
											$id_agenda 			= $id_agenda_anterior;

										}else{
											$id_agenda_anterior = null;
											$id_agenda 			= $this->agendaM->insere($dados);
											$id_agenda_anterior = $id_agenda;
											echo 'Inseriuuuu'.$id_agenda.'</br>';
										}
									}
									if($id_agenda){
										//Trata o valor do Pedido
										if(!empty($valor[11])){

											if(trim(strToUpper($valor[11])) != 'PEDIDO'){
												$pedido = $valor[11];
											}
										}

										//Trata o valor do Produto
										if(!empty($valor[12])){

											if(trim(strToUpper($valor[12])) != 'PRODUTO'){
												$produto = $valor[12];
											}
										}

										//Trata o valor do Embalagem
										if(!empty($valor[13])){

											if(trim(strToUpper($valor[13])) != 'EMBALAGEM'){
												$embalagem = $valor[13];
											}
										}

										//Trata o valor do Peso
										if(!empty($valor[14])){

											if(trim(strToUpper($valor[14])) != 'PESO'){
												$peso = $valor[14];
											}
										}

										if($id_frete == 1){

											//Insere o item do agendamento
											$dadosItensAgenda = array(	'id_agenda' 	=> 	$id_agenda,
											'descricao'		=> 	$produto,
											'id_embalagem' 	=> 	$embalagem,
											'nr_pedido'		=> 	$pedido,
											'peso'			=> 	str_replace('.','',$peso)	);
										}else{

											//Insere o item do agendamento
											$dadosItensAgenda = array(	'id_agenda' 	=> 	$id_agenda,
											'descricao'		=> 	'Não Informado',
											'id_embalagem' 	=> 	0,
											'nr_pedido'		=> 	$pedido,
											'peso'			=> 	str_replace('.','',$peso)	);
										}

										$this->agendaM->insereItem($dadosItensAgenda);
		
										//$this->session->set_flashdata('sucesso', 'ok');	
									}else{
										$this->session->set_flashdata($erros, 'erro');	
									}
								}else{
									$this->session->set_flashdata($erros, 'erro');	
								}
							}

							

						}
					}

				}//fim do foreach
			//	die;
				$this->session->set_flashdata('retorno', 'sucesso');	
				redirect('/AreaClientes/importarAgendamentos/importado');

			} else {
				echo SimpleXLSX::parseError();
			}
		
			
		}
	}
	

	public function verificaHoraLimiteAgendamento()
	{
		//Configura o timezone a ser utilizado
   		date_default_timezone_set('America/Sao_Paulo');

		$dados = $this->agendaM->verificaCotasAgendamento($this->input->post('empresa_id'), $this->input->post('data'));
		if($dados['peso'] > 0){
			if( $dados=='' ){
				echo json_encode(array('retorno' => 'sucesso'));
			}else{
				$hora_limite = substr_replace($dados['hr_limite_agenda'], ':', 2, 0);
	
				if( strtotime($hora_limite) <= strtotime(date('H:i')) ){
					echo json_encode(array(	'retorno' 		=> 'erro',
											'hora_limite' 	=> $hora_limite	,
											'peso' 			=> null));
				}else{
					echo json_encode(array('retorno' => 'sucesso'));
				}
			}
		}else{
			if(!empty($this->input->post('id_turno'))){
				/*Habilita o campo turno */
				$dados = $this->agendaM->verificaCotasTurnoAgendamento($this->input->post('empresa_id'), $this->input->post('id_turno'));
				if(!empty($dados['peso'])){
					if( $dados=='' ){
						echo json_encode(array('retorno' => 'erro'));
					}else{
						$hora_limite = substr_replace($dados['hr_limite_agenda'], ':', 2, 0);
			
						if( strtotime($hora_limite) <= strtotime(date('H:i')) ){
							echo json_encode(array(	'retorno' 		=> 'erro',
													'hora_limite' 	=> $hora_limite,	
													'peso' 			=> null));
						}else{
							echo json_encode(array('retorno' => 'sucesso'));
						}
					}
				}else{
					echo json_encode(array(	'retorno' 		=> 'erro',
											'hora_limite' 	=> null,
											'peso' 			=> '0'	));
				}
			}else{
				echo json_encode(array(	'retorno' 		=> 'erro',
										'hora_limite' 	=> null	,
										'peso' 			=> '0'));
			}
		}
		

	}

	public function verificaCotasAgendamento()
	{
		
		$dados = $this->agendaM->verificaCotasAgendamento($this->input->post('empresa_id'), $this->input->post('dt_agendamento'));		
		//busca total agendandado		
		//$periodo_ini 	= 	$this->descobreIniPeriodo($this->input->post('dt_agendamento'));		
		//$periodo_fim 	= 	$this->input->post('dt_agendamento');
		$peso_agendado 	= 	$this->agendaM->buscaTotalAgendadoPeriodo($this->input->post('empresa_id'),$this->input->post('dt_agendamento') );		
		$total_			= 	$this->input->post('total') / 1000;
		// Soma o total agendado com o solicitado para agendar		
		$total = 0;	
		
		//echo isset($peso_agendado['total'])."</br>";
		if(isset($peso_agendado['total'])){
			$total = $peso_agendado['total'] + $total_;
		}		
		//echo $total_.'<br>';
		
		//echo $peso_agendado['total'] .'+'. $total_.' - '.str_replace('.','',$dados['peso']). '<' . intval($total);
		if(!isset($dados['peso'])){
			echo json_encode(array(	'retorno' 	=> 'erro',				
		            				'mensagem' 	=> 'Não foram inserido as cotas para você, entre em contato com o setor administrativo da Vanzin.'));
		}elseif( $dados['fl_sem_cota'] == 1 ){
			echo json_encode(array( 'retorno' 	=> 'sucesso'));

		}elseif(  str_replace('.','',$dados['peso']) <= intval($total) ){
			//var_dump($dados['peso'] ); die;
			if($dados['peso'] > 0){
				echo json_encode(array( 'retorno' 	=> 'erro',				
		            				'mensagem' 	=> 'Peso agendado maior que o permitido para a empresa.') );
			}else{
				$dados_turno = $this->agendaM->verificaCotasTurnoAgendamento($this->input->post('empresa_id'), $this->input->post('id_turno'));			
				//echo 'Turno '.str_replace('.','',$dados_turno['peso']) .' total '. intval($total); die;
				if(str_replace('.','',$dados_turno['peso']) <= intval($total) ){
					echo json_encode(array( 'retorno' 	=> 'erro',				
		            				'mensagem' 	=> 'Peso agendado maior que o peso do turno permitido para a empresa.') );
				}else{
					echo json_encode(array( 'retorno' 	=> 'sucesso'));
				}
			}
			/*echo json_encode(array( 'retorno' 	=> 'erro',				
		            				'mensagem' 	=> 'Peso agendado maior que o permitido para a empresa.') );
*/
		}else{
			echo json_encode(array( 'retorno' 	=> 'sucesso'));
		}
	}


	
		
	private function descobreIniPeriodo($dt_agenda)
	{
		
		//Configura o timezone a ser utilizado
   		date_default_timezone_set('America/Sao_Paulo');
   		$data = date('Y-m-d');
		switch ( date('w', strtotime($dt_agenda)) ) {
			case 1:
				$data = $data;
				break;
			case 2:
				
				$data = date('Y-m-d', strtotime('-1 day', strtotime($data)));
				break;
			case 3:
				$data = date('Y-m-d', strtotime('-2 days', strtotime($data)));
				break;
			case 4:
				$data = date('Y-m-d', strtotime('-3 days', strtotime($data)));
				break;
			case 5:
				$data = date('Y-m-d', strtotime('-4 days', strtotime($data)));
				break;
			case 6:
				$data = date('Y-m-d', strtotime('-5 days', strtotime($data)));
				break;							
		}

		return $data;

	}

	public function editarAgendamento($id_agenda)
	{
		$parametros			 				=	$this->session->userdata();
		$parametros['title']				=	"Editar Agendamento";
		$parametros['empresa'] 				= 	$this->session->userdata('razao_social');
		$parametros['id_empresa'] 			= 	$this->session->userdata('empresa_id');
		if( $this->session->userdata('tipo_acesso') == 'administrador geral'){
			$parametros['id_empresa'] 	= 	'';
			$parametros['empresas']		= 	$this->empresasM->select();
		}
		if( $parametros['tipo_acesso'] == 'administrador empresas'){			
			$parametros['empresas']		= 	$this->empresasM->getClientesEmpresas($parametros['id_empresa']);
		}
		$parametros['dados']				= 	$this->agendaM->buscaAgendamentoPorId($id_agenda);		
		$parametros['agendamento_itens'] 	= 	$this->agendaM->buscaItensAgendamento($id_agenda);
		$parametros['embalagens'] 			= 	$this->agendaM->buscaEmbalagem();
		$parametros['fretes']				= 	$this->agendaM->buscaFretes();
		$parametros['turnos'] 				= 	$this->turnoM->buscaTurnos();
		$verifica 	= 	$this->agendaM->verificaAcesso($id_agenda);
		if( $verifica > 0 && ($this->session->userdata('usuario_id') != 1 && $this->session->userdata('usuario_id') != 2) ){
			$this->session->set_flashdata('retorno', 'erro');	
			$this->session->set_flashdata('msg', 'Impossível alterar o agendamento, pois o caminhão já acessou o pátio');
 		    redirect('/AreaClientes/Agendamentos/');				
		}

		/*echo "<pre>";
		var_dump($parametros);
		echo "</pre>"; die;*/
		$this->_load_view('area-clientes/agendamentos/editar-agendamento',$parametros);
	}

	public function atualizaAgendamento()
	{
		
		//verifica se motorista já está cadastrado, senão cadastra o motorista
		if( ! $this->input->post('id_motorista') ){

			$insereMotorista = array(	'cpf' 		=> 	$this->input->post('cpf'),
										'nr_cnh' 	=> 	$this->input->post('cnh'),
										'nome'		=> 	$this->input->post('motorista'),
										'fone'		=> 	$this->input->post('telefone'),
										'fl_estrangeiro'	=> 	$this->input->post('fl_motorista')	);

			if($this->input->post('fl_motorista') == 1){
				$insereMotorista['nr_doc']	= $this->input->post('cpf');	
			}

			$id_motorista = $this->motoristaM->insere($insereMotorista);
		}else{
			$id_motorista = $this->input->post('id_motorista');
		}		
		//atualiza o agendamento
		$dt_agenda = explode('/',$this->input->post('data'));
		$dadosAgenda = array(	'id'			=> 	$this->input->post('id_agenda'),
								'dt_agenda' 	=> 	$dt_agenda[2]."-".$dt_agenda[1]."-".$dt_agenda[0],
								'id_empresa'	=> 	$this->input->post('id_empresa'),
								'placa'			=> 	strtoupper($this->input->post('placa')),
								'id_motorista'	=> 	$id_motorista,
								'id_terminal'	=> 	1,
								'id_usuario'	=> 	$this->session->userdata('usuario_id'),
								'id_frete' 		=> 	$this->input->post('id_frete'),
								'id_transportadora' => $this->input->post('id_transportadora'),
								'id_turno' 		=> $this->input->post('id_turno')	 );
/*echo "<pre>";
var_dump($dadosAgenda);
echo "</pre>"; die;*/
		$this->agendaM->atualiza($dadosAgenda);
		// veerifica se foi inserido uma nova nota
		if( $this->input->post('id_agenda_item')[0] != ''  ){
			$i = 0;
			foreach($this->input->post('mercadoria') as $merc){
				$dadosItensAgenda = array(	'id' 			=> 	$this->input->post('id_agenda_item')[$i],
											'id_agenda' 	=> 	$this->input->post('id_agenda'),
											'descricao'		=> 	$merc,
											'id_embalagem' 	=> 	$this->input->post('embalagem')[$i],
											'nr_pedido'		=> 	$this->input->post('nr_pedido')[$i],
											'peso'			=> 	str_replace('.','',$this->input->post('qtd')[$i])		);

				$this->agendaM->atualizaItem($dadosItensAgenda);
				$i++;	

			}
		}else{
			//excluir itens
			if($this->agendaM->excluirItens($this->input->post('id_agenda'))){
			//insere agendamento item
				$i = 0;
				foreach($this->input->post('mercadoria') as $merc){
					$dadosItensAgenda = array(	'id_agenda' 	=> 	$this->input->post('id_agenda'),
												'descricao'		=> 	$merc,
												'id_embalagem' 	=> 	$this->input->post('embalagem')[$i],
												'nr_pedido'		=> 	$this->input->post('nr_pedido')[$i],
												'peso'			=> 	$this->input->post('qtd')[$i]	);

					if(!$this->agendaM->insereItem($dadosItensAgenda)){
						echo '<pre>Erro!';
						die;
					}
					$i++;	

				}

			}

			if($this->notaM->excluir($this->input->post('id_agenda'))){
				//insere nota fiscal
				$dadosNf = array(	'id_agenda'			=> 	$this->input->post('id_agenda'),
									'nr_nf' 			=> 	$this->input->post('nr_nf'),
									'serie_nf'			=> 	$this->input->post('serie_nf'),
									'dt_emissao' 		=> 	$this->input->post('dt_emissao'),
									'no_emitente'		=> 	$this->input->post('no_emitente'),
									'nr_doc_emitente' 	=> 	$this->input->post('nr_doc_emitente'),	
									'no_destinatario'	=> 	$this->input->post('no_destinatario'),
									'cnpj_destinatario'	=> 	$this->input->post('cnpj_destinatario'),
									'peso_bruto_total'	=> 	$this->input->post('peso_bruto_total'),	
									'peso_liq_total'	=> 	$this->input->post('peso_liq_total'),
									'quant_total'		=> 	$this->input->post('quant_total'),
									'unid_medida'		=> 	$this->input->post('unid_medida'),
									'vl_ipi'			=> 	$this->input->post('vl_ipi'),
									'chave_acesso'		=> 	$this->input->post('chave_acesso'),
									'nr_doc_transp'		=> 	$this->input->post('nr_doc_transp'),
									'no_transp'			=> 	$this->input->post('no_transp'),
									'arquivo'			=> 	$this->input->post('arquivo_xml')	 );

				$id_nf = $this->notaM->insere($dadosNf);
				if($id_nf != ''){
					$i = 0;
					foreach($this->input->post('ncm') as $ncm){
						$dadosItensNf = array(	'id_nota_fiscal'=> 	$id_nf,
												'ncm'			=> 	$ncm,
												'ds_item' 		=> 	$this->input->post('ds_item')[$i],
												'qtde'			=> 	$this->input->post('qtde')[$i],
												'valor'			=> 	$this->input->post('valor')[$i],
												'cfop'			=> 	$this->input->post('cfop')[$i]	);

						$this->notaM->insereItem($dadosItensNf);
						$i++;	

					}
				}else{
					$this->session->set_flashdata('retorno', 'erro');	
					$this->session->set_flashdata('msg', 'Não inseriu a nota fiscal, entre em contato com o suporte técnico');
		 		    redirect('/AreaClientes/Agendamentos/');
				}
			}
					
		}

		$this->session->set_flashdata('retorno', 'sucesso');	
 		redirect('/AreaClientes/Agendamentos/');


	}

	public function excluirAgendamento()
	{
		
		$verificaEntrada = $this->agendaM->verificaEntradaPatio($this->input->post('id'));
		//Caso não tenha acesso ao patio, pode excluir
		if( $verificaEntrada['total'] > 0  ){
			echo json_encode(array('retorno' => false,
									'titulo' => "Ops!",
									'texto'	=> 	"Este agendamento já deu entrada no pátio, impossível realizar a exclusão.",
									'tipo' 	=> 	"warning"	 	 ));
		}else{
			if( $this->agendaM->excluir($this->input->post('id')) ){
				unset($_SESSION['sucesso']);
				echo json_encode(array(	'retorno' 	=> 	true,
										'titulo' 	=> 	"Ok!",
										'texto'		=> 	"Agendamento excluído com sucesso.",
										'tipo' 		=> 	"success"	 	 ));
			}else{
				unset($_SESSION['erro']);
				echo json_encode(array(	'retorno' 	=> 	false,
										'titulo' 	=> 	"Atenção!",
										'texto'		=> 	"Problema ao excluir, entre em contato com o suporte técnico.",
										'tipo' 		=> 	"danger"	 	 ));
			}	

		}

	}

	public function atualizaMotorista()
	{
		$update = array(	'id' 	=> 	$this->input->post('id_motorista'),
							'fone' 	=> 	$this->input->post('fone'),
							'nome' 	=> 	$this->input->post('nome')	 );

		if( $this->motoristaM->atualiza($update) ){
			echo json_encode(array('retorno' => 'sucesso'));	
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}
							
	}

	public function verificaVeiculo()
	{
		
		echo json_encode($this->agendaM->verificaVeiculo( $this->input->post('placa') ) ); 
	}

	public function cadastrarTransportadora()
	{
				
		foreach ($this->input->post('dados') as $dado) {
			$dados[] = array($dado['name'] => $dado['value']);
		}		

		$dadosEmpresa = array(
	        'razao_social' 		=>	$dados[5]['razao_social'],
			'fantasia' 			=>	$dados[7]['fantasia'],
			'cnpj' 				=>	$dados[0]['cnpj'],
			'telefone' 			=>	$dados[10]['telefone'],
			'endereco' 			=>	$dados[12]['endereco'],
			'email' 			=>	$dados[9]['email'],
			'cidade' 			=>	$dados[14]['cidade'],
			'estado' 			=>	$dados[15]['estado'],
			'tipo_cadastro_id'	=> 	23,
			'insc_estadual'		=> 	$dados[8]['inscricao_estadual'],
			'pais'				=> 	$dados[16]['pais'],
			'bairro'			=> 	$dados[13]['bairro'],
			'cep'				=> 	$dados[11]['cep'],
			'cartao_cnpj'		=>	$dados[2]['cartao_cnpj'],
			'id_usuario_cadastro' => $this->session->userdata('usuario_id')	
	    );	

		if($empresa_id = $this->empresasM->add($dadosEmpresa))
		{
			$this->log('Área Administrador | cadastro empresa','empresas','INSERÇÃO', $this->session->userdata('usuario_id'),$dadosEmpresa,$dadosEmpresa,$_SERVER['REMOTE_ADDR']);
			echo json_encode(array(	'retorno' => 'sucesso',
									'empresa'	=> 	$dados[0]['cnpj'].'-'.$dados[5]['razao_social'],
									'empresa_id'	=> 	 $empresa_id) );
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}					 
	
	}

	public function retornaTransportadoras()
	{
		$rows = $this->empresasM->buscaTransportadora($_GET['term']);

		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label' 		=>	$row['cnpj'].'-'.$row['razao_social'],
								 	'id'			=>	$row['id'],
								 	'value'			=>	$row['cnpj'].'-'.$row['razao_social'],
								 	'empresa_id' 	=> 	$row['id'] 	);
				
			}

			echo json_encode($dados);
		}
	}

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('vanzin@milsistemas.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->send();
		}else{
			$result = $this->email
			    ->from('vanzin@milsistemas.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)
			    ->send();
		}

		return $result;		
	}

	private function enviaApiCredenciamento($id){ 
		$tabela = 'api_credenciamento_veiculo';			 
		$verifica = $this->agendaM->verificaEnvioCredenciamento($tabela, $id);
		if($verifica['total'] == 0){
			//Insere na tabela api_controle_credenciamento_veiculo
			if($id_api = $this->agendaM->insereApiCredenciamentoVeiculo($id)){ 
				$dados = $this->agendaM->buscaDadosCredencimantoEnvioApi($id);
				//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_ocorrencia']))
				//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_registro']))
				$data_envio = date('Y-m-d\TH:i:s.150O',strtotime(date('Y-m-d H:i:s')));
				$dadosEnvio = array( 	'tipoOperacao' 	=> 	$dados['tp_operacao'],
										'idEvento'		=> 	$dados['id_evento'],
										'dataHoraOcorrencia'	=>	$data_envio,
										'dataHoraRegistro'		=> 	$data_envio,									
										'contingencia'			=> 	false,
										'codigoRecinto' 		=> 	'0302701',
										'placa'					=> 	$dados['placa'],
										'cpfResponsavel'		=> 	$dados['cpf_usuario'],
										'dataInicioValidade'	=> 	$dados['dt_validade_ini'],
										'dataFimValidade'		=> 	$dados['dt_validade_fim'] );
				//Autenticação e envio para o siscomex						
				list($token, $x_csrf_token) = $this->autenticar();
				$retorno_envio = $this->enviarDados('/ext/credenciamento-veiculos', json_encode($dadosEnvio), $token, $x_csrf_token);
				$code = json_decode($retorno_envio);
				$atualizaControle = array(	'id' 				=> $id_api,
											'fl_envio' 			=> 'S',
											'retorno_envio' 	=> 	$retorno_envio,
											'id_retorno'		=> 	$code->http_code	);
				$this->agendaM->atualizaApiCredencimantoVeiculos($atualizaControle);
				
			}
		}
	}
	
	private function enviaApiCredenciamentoPessoas($id){ 
		$tabela = 'api_credenciamento_pessoa';			 
		$verifica = $this->agendaM->verificaEnvioCredenciamento($tabela, $id);
			if($verifica['total'] == 0){
			//Insere na tabela api_controle_credenciamento_veiculo
			if($id_api = $this->agendaM->insereApiCredenciamentoPessoas($id)){ 
				$dados = $this->agendaM->buscaDadosCredencimantoPessoasEnvioApi($id);
				//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_ocorrencia']))
				//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_registro']))
				$cpf = str_replace('-','',str_replace('.','',$dados['cpf']));
				$data_envio = date('Y-m-d\TH:i:s.150O',strtotime(date('Y-m-d H:i:s')));
				$dadosEnvio = array( 	'tipoOperacao' 	=> 	$dados['tp_operacao'],
										'idEvento'		=> 	$dados['id_evento'],
										'dataHoraOcorrencia'	=>	$data_envio,
										'dataHoraRegistro'		=> 	$data_envio,									
										'contingencia'			=> 	false,
										'codigoRecinto' 		=> 	'0302701',
										'cnh'					=> 	$dados['cnh'],
										'nome'					=> 	$dados['nome'],
										'cpf'					=> 	$cpf,
										'dataInicioValidade'	=> 	$dados['dt_validade_ini'], 
										'motivacao'				=> 	$dados['motivo'] 	);
				
				//Autenticação e envio para o siscomex						
				list($token, $x_csrf_token) = $this->autenticar();
				$retorno_envio = $this->enviarDados('/ext/credenciamento-pessoas', json_encode($dadosEnvio), $token, $x_csrf_token);
				$code = json_decode($retorno_envio);
				$atualizaControle = array(	'id' 				=> $id_api,
											'fl_envio' 			=> 'S',
											'retorno_envio' 	=> 	$retorno_envio,
											'id_retorno'		=> 	$code->http_code);
				$this->agendaM->atualizaApiCredencimantoPessoas($atualizaControle);
				
			}
		}
	}


}
