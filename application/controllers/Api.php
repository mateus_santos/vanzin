<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Api extends MY_Controller {
	public function __construct() {
		parent::__construct();
		// carrega a model a ser utilizada neste controller
		$this->load->model('AcessoModel', 'acessoM');
		$this->load->model('TransitoMovimentacaoModel', 'transitoMM');
		$this->load->model('LogApiModel', 'logApiM');
		$this->load->model('TransitoModel', 'transitoM');
		
	}

	public function recepcaoCarga(){
		
		if( $this->input->post('hash') == '937a19a6c487c1e7fafd3c233020e234' ){
			
			$info_erro = "";
			if( $this->input->post('placa') != ''){
				if( strlen($this->input->post('placa')) != 7 ){
					
					$info_erro.= '0 - Erro: Campo Placa do Cavalo (placa) com numero de caracteres errado :'.strlen($this->input->post('placa')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Placa do Cavalo (placa) Vazio. ';
			}

			if( $this->input->post('dthr_pesagem') != ''){
				if( strlen($this->input->post('dthr_pesagem')) != 19 ){
					
					$info_erro.= '0 - Erro: Campo Data-Hora da 2 Pesagem (dthr_pesagem) com numero de caracteres errado :'.strlen($this->input->post('dthr_pesagem')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Data-Hora da 2 Pesagem (dthr_pesagem) Vazio. ';
			}

			if( $this->input->post('peso_liquido') != ''){
				if (strpos($this->input->post('peso_liquido'), ',') !== false) {
					$peso = explode(',', $this->input->post('peso_liquido'));
					if( strlen($peso[0]) > 13 || strlen($peso[1]) > 3 ){
						$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) com formato errado, verifique o manual. ';
					}
				}else{
					$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) com formato errado, verifique o manual. ';		
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) Vazio. ';
			}
			
			if( $this->input->post('peso_tara') != ''){
				if (strpos($this->input->post('peso_tara'), ',') !== false) {
					$peso_t = explode(',', $this->input->post('peso_tara'));
					if( strlen($peso_t[0]) > 13 || strlen($peso_t[1]) > 3 ){
						$info_erro.= '0 - Erro: Campo Peso Tara (Kg) (peso_tara) com formato errado, verifique o manual. ';
					}
				}else{
					$info_erro.= '0 - Erro: Campo Peso Tara (Kg) (peso_tara) com formato errado, verifique o manual. ';		
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Peso Tara (Kg) (peso_tara) Vazio. ';
			}
			
			if( $this->input->post('peso_bruto') != ''){
				if (strpos($this->input->post('peso_bruto'), ',') !== false) {
					$peso_b = explode(',', $this->input->post('peso_bruto'));
					if( strlen($peso_b[0]) > 13 || strlen($peso_b[1]) > 3 ){
						$info_erro.= '0 - Erro: Campo Peso Bruto (Kg) (peso_bruto) com formato errado, verifique o manual. ';
					}
				}else{
					$info_erro.= '0 - Erro: Campo Peso Bruto (Kg) (peso_bruto) com formato errado, verifique o manual. ';		
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Peso Bruto (Kg) (peso_bruto) Vazio. ';
			}
			
			$protocolo = ' Protocolo: '.md5(date('d-m-Y H:i:s:u'));
			$placa = (mb_strpos($this->input->post('placa'),'-') === false) ? substr_replace($this->input->post('placa'),'-',3, 0) : $this->input->post('placa');
			$peso_liquido 	= str_replace(',', '.', $this->input->post('peso_liquido'));
			$peso_tara 		= str_replace(',', '.', $this->input->post('peso_tara'));
			$peso_bruto		= str_replace(',', '.', $this->input->post('peso_bruto'));
			
			$insereLog = array(	'protocolo' 		=> $protocolo,
								'metodo' 			=> 	'recepcao',
								'placa' 			=> 	$placa,
								'dthr_movimentacao' => $this->input->post('dthr_pesagem'),
								'peso'				=> 	$peso_liquido,
								'peso_tara'			=> 	$peso_tara,
								'peso_bruto'		=> 	$peso_bruto );
			
			if( $info_erro == "" ){  				
				$dadosUpdate = array( 	'peso_aferido' 	=> 	$peso_liquido,
										'dthr_pesagem' 	=> 	$this->input->post('dthr_pesagem'),
										'placa'			=> 	$placa,
										'peso_tara'		=> 	$peso_tara,
										'peso_bruto'	=> 	$peso_bruto		);

				$acessos = $this->acessoM->buscaAcessoPlaca($placa);

				//if( $acessos['total'] > 0 ){

					if( $this->acessoM->atualizaRecepcaoCarga( $dadosUpdate ) ){
						$insereLog['retorno'] = '1 - Enviado-Registrado com sucesso.';
						$this->logApiM->insere($insereLog);
						$this->enviaApiPesagemVeiculoCargaR($placa,'ACESSO-PATIO-RECEPCAO-');
						echo json_encode(array('retorno' => '1 - Enviado-Registrado com sucesso.'.$protocolo ));
					}else{
						$insereLog['retorno'] = '0 - Erro na atualização de dados, entre em contato com a MilSistemas.';
						$this->logApiM->insere($insereLog);
						echo json_encode(array('retorno' => '0 - Erro na atualização de dados, entre em contato com a MilSistemas.'.$protocolo ));
					}
				/*}else{
					$insereLog['retorno'] = '0 - Erro a placa não realizou agendamento, entre em contato com a MilSistemas.';
					$this->logApiM->insere($insereLog);
					echo json_encode(array('retorno' => '0 - Erro a placa não realizou agendamento, entre em contato com a MilSistemas.' .$protocolo));
				}*/

			}else{
				$insereLog['retorno'] = $info_erro;
				$this->logApiM->insere($insereLog);
				echo json_encode(array('retorno' => $info_erro));	
			}

		}else{
			$insereLog['retorno'] = '0 - Erro: Chave Incorreta.';
			$this->logApiM->insere($insereLog);
			echo json_encode(array('retorno' => '0 - Erro: Chave Incorreta.'));
		}

	}

	public function embarqueCarga(){
		
		if( $this->input->post('hash') == '937a19a6c487c1e7fafd3c233020e234' ){
			
			$info_erro = "";
			if( $this->input->post('placa') != ''){
				if( strlen($this->input->post('placa')) != 7 ){
					
					$info_erro.= '0 - Erro: Campo Placa do Cavalo (placa) com numero de caracteres errado :'.strlen($this->input->post('placa')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Placa do Cavalo (placa) Vazio. ';
			}

			if( $this->input->post('cpf_motorista') != ''){
				if( strlen($this->input->post('cpf_motorista')) != 11 ){
					
					$info_erro.= '0 - Erro: Campo CPF Motorista (cpf_motorista) com numero de caracteres errado :'.strlen($this->input->post('cpf_motorista')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Cpf Motorista Vazio. ';
			}

			if( $this->input->post('dthr_pesagem') != ''){
				if( strlen($this->input->post('dthr_pesagem')) != 19 ){
					
					$info_erro.= '0 - Erro: Campo Data-Hora da 2 Pesagem (dthr_pesagem) com numero de caracteres errado :'.strlen($this->input->post('dthr_pesagem')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Data-Hora da 2 Pesagem (dthr_pesagem) Vazio. ';
			}

			if( $this->input->post('cnpj_dono') != ''){
				if( strlen($this->input->post('cnpj_dono')) != 14 ){
					
					$info_erro.= '0 - Erro: Campo Dono da Carga (CNPJ) (cnpj_dono) com numero de caracteres errado :'.strlen($this->input->post('dthr_pesagem')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Dono da Carga (CNPJ) (cnpj_dono) Vazio. ';
			}

			if( $this->input->post('peso_liquido') != ''){
				if (strpos($this->input->post('peso_liquido'), ',') !== false) {
					$peso = explode(',', $this->input->post('peso_liquido'));
					if( strlen($peso[0]) > 13 || strlen($peso[1]) > 3 ){
						$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) com formato errado, verifique o manual. ';
					}
				}else{
					$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) com formato errado, verifique o manual. ';		
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) Vazio. ';
			}
			
			if( $this->input->post('peso_tara') != ''){
				if (strpos($this->input->post('peso_tara'), ',') !== false) {
					$peso_t = explode(',', $this->input->post('peso_tara'));
					if( strlen($peso_t[0]) > 13 || strlen($peso_t[1]) > 3 ){
						$info_erro.= '0 - Erro: Campo Peso Tara (Kg) (peso_tara) com formato errado, verifique o manual. ';
					}
				}else{
					$info_erro.= '0 - Erro: Campo Peso Tara (Kg) (peso_tara) com formato errado, verifique o manual. ';		
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Peso Tara (Kg) (peso_tara) Vazio. ';
			}
			
			if( $this->input->post('peso_bruto') != ''){
				if (strpos($this->input->post('peso_bruto'), ',') !== false) {
					$peso_b = explode(',', $this->input->post('peso_bruto'));
					if( strlen($peso_b[0]) > 13 || strlen($peso_b[1]) > 3 ){
						$info_erro.= '0 - Erro: Campo Peso Bruto (Kg) (peso_bruto) com formato errado, verifique o manual. ';
					}
				}else{
					$info_erro.= '0 - Erro: Campo Peso Bruto (Kg) (peso_bruto) com formato errado, verifique o manual. ';		
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Peso Bruto (Kg) (peso_bruto) Vazio. ';
			}
			
			$protocolo = ' Protocolo: '.md5(date('d-m-Y H:i:s:u'));
			$placa 			= 	substr_replace($this->input->post('placa'),'-',3, 0);
			$peso_liquido 	= 	str_replace(',', '.', $this->input->post('peso_liquido'));
			$peso_tara 		= str_replace(',', '.', $this->input->post('peso_tara'));
			$peso_bruto		= str_replace(',', '.', $this->input->post('peso_bruto'));
			$insereLog = array(	'protocolo' 		=> $protocolo,
								'metodo' 			=> 'embarque',
								'placa'				=> $placa,
								'peso'				=> $peso_liquido,
								'dthr_movimentacao' => $this->input->post('dthr_pesagem'),
								'cpf_motorista'		=> $this->input->post('cpf_motorista'),
								'cnpj_dono'	 		=> 	$this->input->post('cnpj_dono'),
								'peso_tara'			=> 	$peso_tara,
								'peso_bruto'		=> 	$peso_bruto,
								'dthr_entrada'		=> 	$this->input->post('dthr_entrada'),
								'dthr_saida'		=> 	$this->input->post('dthr_saida') 	);	
			
			if( $info_erro == '' ){
				$dados 	= 	$this->transitoMM->buscaDadosTransito($this->input->post('cnpj_dono'));				
				if( $dados != '' ){
					
					$dadosInsert = array(	'dthr_movimento'	=> 	$this->input->post('dthr_pesagem'),
											'id_transito'		=> 	$dados['id_transito'],
											'nr_due' 			=> 	$dados['nr_due'],
											'placa'				=> 	$placa,
											'cpf_motorista'		=> 	$this->input->post('cpf_motorista'),
											'peso_aferido'		=> 	$peso_liquido,
											'cnpj'				=> 	$this->input->post('cnpj_dono'),
											'no_motorista'		=> 	$this->input->post('nome_motorista'),
											'peso_tara'			=> 	$peso_tara,
											'peso_bruto'		=> 	$peso_bruto,
											'dthr_entrada'		=> 	$this->input->post('dthr_entrada'),
											'dthr_saida'		=> 	$this->input->post('dthr_saida') );

					//if( $this->transitoMM->add( $dadosInsert ) ){
					if( $this->transitoMM->addProc($dadosInsert) ){	
						$insereLog['retorno'] = '1 - Enviado-Registrado com sucesso.';
						//se NÃO for para o terminal TLA, AGENDA NO PORTO.
						if($dados['id_terminal'] != 12){
							$this->enviaCargaPorto($dados['nr_due'],$this->input->post('placa'),$this->input->post('cpf_motorista'),$peso_liquido,1);
						}
						/*$id = $this->transitoMM->buscaUltimoId($this->input->post('placa'));
						$this->enviaApiAcesso($id,'E');
						$this->enviaApiAcesso($id,'S');
						$this->enviaApiAcessoVeiculos($id,'E');
						$this->enviaApiAcessoVeiculos($id,'S');
						$this->enviaApiPesagemVeiculoCargaR($placa,'ACESSO-PATIO-ENTREGA-'); */
						
						$this->logApiM->insere($insereLog);
						echo json_encode(array('retorno' => '1 - Enviado-Registrado com sucesso. '.$protocolo ));
					}else{
						$insereLog['retorno'] = '0 - Erro na atualização de dados, entre em contato com a MilSistemas.';
						$this->logApiM->insere($insereLog);
						echo json_encode(array('retorno' => '0 - Erro na atualizacao de dados, entre em contato com a MilSistemas. '.$protocolo ));
					}
				}else{
					$insereLog['retorno'] = '0 - Atencao! Não existe DUE em aberto para esse dono da carga.';
					$this->logApiM->insere($insereLog);
					echo json_encode(array('retorno' => '0 - Atencao! Não existe DUE em aberto para esse dono da carga. '.$protocolo));
				}

			}else{
				$insereLog['retorno'] = $info_erro;
				$this->logApiM->insere($insereLog);
				echo json_encode(array('retorno' => $info_erro.$protocolo));
			}

		}else{
			$insereLog['retorno'] = '0 - Erro: Chave Incorreta. ';
			$this->logApiM->insere($insereLog);
			echo json_encode(array('retorno' => '0 - Erro: Chave Incorreta.'.$protocolo));
		}
	}
	
	//public function enviaCargaPorto(){		
	public function enviaCargaPorto($nr_due,$placa,$cpf_motorista,$peso,$qtde=1){
		
		$soapUrl = "http://portosrs.com.br/portoweb/zf/transito-simplificado"; // asmx URL of WSDL
		$soapUser = "VANZIN";  //  username
		$soapPassword = "vanzin2020"; // password
		if(strpos($peso, ".") !== false){
			$peso_ = explode('.',$peso);
			$peso = $peso_[0];
		}		
		
		//xml post structure
		$xml_post_string = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tran="http://portosrs.com.br/portoweb/zf/transito-simplificado">
							   <soapenv:Header/>
							   <soapenv:Body>
								  <tran:recebeCarga soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
									 <request xsi:type="tran:Request">										
										<nr_due xsi:type="xsd:string">'.mb_strtoupper($nr_due).'</nr_due>
										<placa xsi:type="xsd:string">'.$placa.'</placa>
										<cpf_motorista xsi:type="xsd:string">'.$cpf_motorista.'</cpf_motorista>
										<peso xsi:type="xsd:int">'.$peso.'</peso>
										<qtde xsi:type="xsd:int">1</qtde>
									 </request>
									 <auth xsi:type="tran:AuthUser">										
										<user xsi:type="xsd:string">VANZIN</user>
										<password xsi:type="xsd:string">vanzin2020</password>
									 </auth>
								  </tran:recebeCarga>
							   </soapenv:Body>
							</soapenv:Envelope>';   // data from the form, e.g. some ID number

			$headers = 	array(
						 "Content-type: text/xml;charset=\"utf-8\"",
						 "Accept: text/xml",
						 "Cache-Control: no-cache",
						 "Pragma: no-cache",
						 "SOAPAction: http://portosrs.com.br/portoweb/zf/transito-simplificado#recebeCarga", 
						 "Content-length: ".strlen($xml_post_string),
						); //SOAPAction: your op URL

			$url = $soapUrl;

			// PHP cURL  for https connection with auth
			try{ 
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_POST, true); 
				curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				
				// converting
				$response = curl_exec($ch);
				curl_close($ch);
				
				$insertLogWebService = array(	'nr_due' 		=> 	$nr_due,
												'placa' 		=> 	$placa,
												'cpf_motorista' => 	$cpf_motorista,
												'peso' 			=> 	$peso,
												'qtd'			=> 	$qtde,
												'retorno'		=> 	$response	);
				
				$this->logApiM->insereLogWebService($insertLogWebService);	
			} catch (Exception $e) {
				
			}				
			 // converting
			 //$response1 = str_replace("<soap:Body>","",$response);
			 //$response2 = str_replace("</soap:Body>","",$response1);

			 // convertingc to XML
			 //$parser = new SimpleXMLElement($response);
			 //print_r($parser);die;
			 //echo $parser;
			 // user $parser to get your data out of XML response and to display it. 
	}

	private function enviaApiAcesso($id,$tipo){
		//Insere na tabela api_controle_acesso_pessoa
		if($id_api = $this->transitoMM->insereApiControleAcesso($id,$tipo)){
			$dados = $this->transitoMM->buscaDadosEnvioApi($id);
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_ocorrencia']))
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_registro']))
			$data_envio = date('Y-m-d\TH:i:s.150O',strtotime( $dados['dthr_ocorrencia'] ));
			$dadosEnvio = array( 	'tipoOperacao' 	=> 	$dados['tp_operacao'],
									'idEvento'		=> 	$dados['id_evento'],
									'dataHoraOcorrencia'	=>	$data_envio,
									'dataHoraRegistro'		=> 	$data_envio,
									'contingencia'			=> 	false,
									'codigoRecinto' 		=> 	'0302701',
									'cpf'					=> 	$dados['cpf'],
									'direcao'				=> 	$dados['direcao'],
									'nome'					=> 	$dados['nome'] );
			//Autenticação e envio para o siscomex						
			list($token, $x_csrf_token) = $this->autenticar();
			$retorno_envio = $this->enviarDados('/ext/acesso-pessoas', json_encode($dadosEnvio), $token, $x_csrf_token);
			$code = json_decode($retorno_envio);
			$atualizaControle = array(	'id' 				=> $id_api,
										'fl_envio' 			=> 'S',
										'retorno_envio' 	=> 	$retorno_envio,
										'id_retorno'		=> 	$code->http_code );
			$this->transitoMM->atualizaApiAcesso($atualizaControle); 
			
		}
	}	
	
	private function enviaApiAcessoVeiculos($id,$tipo){
		//Insere na tabela api_controle_acesso_pessoa
		if($id_api = $this->transitoMM->insereApiControleAcessoVeiculos($id,$tipo)){
			$dados = $this->transitoMM->buscaDadosEnvioApiVeiculos($id);
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_ocorrencia']))
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_registro']))
			$data_envio = date('Y-m-d\TH:i:s.150O',strtotime($dados['dthr_ocorrencia']));
			$dadosEnvio = array( 	'tipoOperacao' 	=> 	$dados['tp_operacao'],
									'idEvento'		=> 	$dados['id_evento'],
									'dataHoraOcorrencia'	=>	$data_envio,
									'dataHoraRegistro'		=> 	$data_envio,
									'contingencia'			=> 	false,
									'codigoRecinto' 		=> 	'0302701',
									'operacao'				=> 	'C',
									'direcao'				=> 	$dados['direcao'],
									'placa'					=> 	$dados['placa'],
									'motorista'				=> 	array( 'cpf' => $dados['cpf_motorista'], 'nome' => $dados['nome_motorista']	) 	);
			//Autenticação e envio para o siscomex						
			list($token, $x_csrf_token) = $this->autenticar();
			
			$retorno_envio = $this->enviarDados('/ext/acesso-veiculos', json_encode($dadosEnvio), $token, $x_csrf_token);
			$code = json_decode($retorno_envio);
			$atualizaControle = array(	'id' 				=> $id_api,
										'fl_envio' 			=> 'S',
										'retorno_envio' 	=> 	$retorno_envio,
										'id_retorno'		=> 	$code->http_code );
			$this->transitoMM->atualizaApiAcessoVeiculos($atualizaControle);
			
		}
	}
	
	private function enviaApiPesagemVeiculoCargaR($placa,$tipo){
		//Insere na tabela api_controle_acesso_pessoa
		if($id_api = $this->acessoM->insereApiPesagemVeiculoCarga($placa, $tipo)){
			$dados = $this->acessoM->buscaDadosEnvioApiPesagemCarga($id_api);
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_ocorrencia']))
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_registro']))
			$data_envio = date('Y-m-d\TH:i:s.150O',strtotime( $dados['dthr_ocorrencia'] ));
			$dadosEnvio = array( 	'tipoOperacao' 	=> 	$dados['tp_operacao'],
									'idEvento'		=> 	$dados['id_evento'],
									'dataHoraOcorrencia'	=>	$data_envio,
									'dataHoraRegistro'		=> 	$data_envio,
									'contingencia'			=> 	false,
									'codigoRecinto' 		=> 	'0302701',
									'placa'					=> 	$placa,									
									'taraConjunto'			=> 	$dados['tara_conjunto'],
									'pesoBrutoManifesto'	=> 	$dados['peso_bruto'] 	);
			//Autenticação e envio para o siscomex						
			list($token, $x_csrf_token) = $this->autenticar();
			$retorno_envio = $this->enviarDados('/ext/pesagem-veiculos-cargas', json_encode($dadosEnvio), $token, $x_csrf_token);
			$code = json_decode($retorno_envio);
			$atualizaControle = array(	'id' 				=> $id_api,
										'fl_envio' 			=> 'S',
										'retorno_envio' 	=> 	$retorno_envio,
										'id_retorno'		=> 	$code->http_code	);
										
			$this->acessoM->atualizaApiAcessoPesagem($atualizaControle);
			
		}
	}
		
}