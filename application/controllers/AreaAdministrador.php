<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
include __DIR__.'/src/SimpleXLSX.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use Dompdf\Dompdf;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class AreaAdministrador extends MY_Controller { 
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() ||  ($this->session->userdata('tipo_acesso') != 'administrador geral' && ($this->session->userdata('tipo_acesso') != 'administrador empresas') && ($this->session->userdata('tipo_acesso') != 'gate') && ($this->session->userdata('tipo_acesso') != 'gate terminal') && ($this->session->userdata('tipo_acesso') != 'estoque') )  ) {
			redirect(base_url('usuarios/login'));
		}
		
		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');		
		$this->load->model('CotasModel', 'cotasM');
		$this->load->model('AcessoModel', 'acessoM');
		$this->load->model('AgendamentoModel', 'agendaM');
		$this->load->model('TerminalModel', 'terminalM');
		$this->load->model('NotaFiscalModel', 'notaM');
		$this->load->model('TransitoModel', 'transitoM');
		$this->load->model('MotoristaModel', 'motoristaM');
		$this->load->model('TurnoModel', 'turnoM');
		$this->load->model('AreaEquipamentoModel', 'equipamentosM');		
		$this->load->model('VeiculosBloqueadosModel', 'veiculosBM');	
		$this->load->model('ApiRecintosModel', 'apiRecintosM');
		$this->load->helper('form');
		$this->load->helper('url');
		
		
	}
 
	public function index()
	{
		$parametros				=	$this->session->userdata();		
		$parametros['title']	=	"Área do Administrador do sistema";		
		$this->_load_view('area-administrador/index',$parametros);

	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){
			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-administrador/editar-cliente',	$parametros );	
			}

		}else{

			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-administrador/editar-cliente',	$parametros );	
		}
	
	}

	public function gestaoUsuarios()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();		
		$parametros['title']	=	"Gestão de Usuários";
		$this->_load_view('area-administrador/gestao-usuario/gestao-usuario',$parametros);
	}

	/****************************************************************************
	**************** Método Ajax - Alterar status do usuario ********************
	*****************************************************************************/
	public function alteraStatus()
	{
		
		$dados = array(	'id'		=>	$_POST['id'],
						'ativo'	=>	$_POST['ativo']	);

		if($this->usuariosM->atualizaStatus($dados)){
			$this->log('Área Administrador | ativa usuário','usuarios','EDIÇÂO', $this->session->userdata('usuario_id'), $dados,$dados,$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso'));
		
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}
	
	private function enviaEmailConfirmacao($email_destino){

 		$email = '	<html>
						<head></head>
						<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
							<div class="content" style="width: 600px; height: 100%;">
								<img src="http://www.wertco.com.br/bootstrap/img/confirmacaoExpopostos.png" />
							</div>
						</body>
					</html>';	
		
		$this->load->library('email');
		
		$result = $this->email
		    ->from('vendas@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to($email_destino)
		    ->subject('WERTCO - Confirmação de Acesso')
		    ->message($email)
		    ->send();

		return $result;	

 	}

	/****************************************************************************
	*****************************************************************************
	**************** Método Responsável por Editar Usuários	*********************
	*****************************************************************************
	*****************************************************************************/

	public function editarUsuario($id = null){

		if($this->input->post('salvar') == 1){
			
			$update = $this->input->post();			
			unset($update['salvar']);
			unset($update['senha2']);
			unset($update['senha']);	
			

			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha')));
			}
			 
			if($this->usuariosM->atualizaUsuario($update)){				
				$this->log('Área Administrador | atualiza usuário','usuarios','EDIÇÂO', $this->session->userdata('usuario_id'), $update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['title']	=	"Gestão de Usuários";
				$parametros['dados']	=	$this->usuariosM->getUsuarios();
				$this->_load_view('area-administrador/gestao-usuario/gestao-usuario',	$parametros );
			}
		}else{
			
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->usuariosM->getUsuario($id);
			$parametros['title']				=	"Editar Usuários";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();
			$parametros['subtipos']				= 	$this->usuariosM->buscaSubtipoCadastro();			
			$this->_load_view('area-administrador/gestao-usuario/editar-usuario',	$parametros );
		}
	}

	
    public function gestaoEmpresas($pesquisa = null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->empresasM->getEmpresas();
		$parametros['title']	=	"Gestão de Empresas";
		$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-administrador/empresas/gestao-empresas',$parametros);
	}

	public function visualizarEmpresa($id,$pesquisa=null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->empresasM->getEmpresasUsuarios($id);			
		$parametros['subtipos']	=	$this->empresasM->getSubtipoByTipo($parametros['dados'][0]->tipo_cadastro_id);
		$parametros['clientes'] = 	$this->empresasM->getClientesClassificadoraEmpresas($id);
		if($parametros['dados'][0]->tipo_cadastro_id == 17){
			$parametros['clientes_vinculados'] 	= 	$this->empresasM->getClientesVinculadosClassificadoras($id);	
		}elseif($parametros['dados'][0]->tipo_cadastro_id == 18){
			$parametros['clientes_vinculados'] 	= 	$this->empresasM->getClientesEmpresas($id);			
		}
		$parametros['title']	=	"Empresa #".$id;
		$parametros['pesquisa'] = 	$pesquisa;
		
		$this->_load_view('area-administrador/empresas/visualiza-empresa',$parametros);
	}

	public function cadastrarEmpresa()
	{
		if($this->input->post()){

			$dadosEmpresa = array(
		        'razao_social' 		=>	$this->input->post('razao_social'),
				'fantasia' 			=>	$this->input->post('fantasia'),
				'cnpj' 				=>	$this->input->post('cnpj'),
				'telefone' 			=>	$this->input->post('telefone'),
				'endereco' 			=>	$this->input->post('endereco'),
				'email' 			=>	$this->input->post('email'),
				'cidade' 			=>	$this->input->post('cidade'),
				'estado' 			=>	$this->input->post('estado'),
				'tipo_cadastro_id'	=> 	$this->input->post('tipo_cadastro_id'),
				'insc_estadual'		=> 	$this->input->post('inscricao_estadual'),
				'pais'				=> 	$this->input->post('pais'),
				'bairro'			=> 	$this->input->post('bairro'),
				'cep'				=> 	$this->input->post('cep'),
				'cartao_cnpj'		=>	$this->input->post('cartao_cnpj'),
				'id_usuario_cadastro' => $this->session->userdata('usuario_id')	
		    );
			
			$conteudo= 	"CNPJ: ". 	$this->input->post('cnpj')." <br/> ";
			$conteudo.= "Razão Social: ". 	$this->input->post('razao_social')." <br/> ";
			$conteudo.=	'Fantasia: '.	$this->input->post('fantasia').' <br/> ';
			$conteudo.=	'CNPJ: '.	$this->input->post('cnpj').' <br/> ';
			$conteudo.=	'Telefone: '.	$this->input->post('telefone').' <br/> ';
			$conteudo.=	'Endereço: '.	$this->input->post('endereco').' <br/> ';
			$conteudo.=	'Cidade:  '	.	$this->input->post('cidade').' <br/> ';
			$conteudo.=	'Estado:  '	.	$this->input->post('estado').' <br/> ';
			$conteudo.=	'País: 	  '.	$this->input->post('pais').' <br/> ';	
			$conteudo.=	'Usuário que cadastrou:	  '.	$this->session->userdata('nome');

			if($this->empresasM->add($dadosEmpresa))
			{
				$this->log('Área Administrador | cadastro empresa','empresas','INSERÇÃO', $this->session->userdata('usuario_id'),$dadosEmpresa,$dadosEmpresa,$_SERVER['REMOTE_ADDR']);
				$this->enviaEmail('webmaster@companytec.com.br','Empresa Cadastrada',$conteudo,$this->input->post('cartao_cnpj'));
				$this->session->set_flashdata('sucesso', 'ok');	
				$this->gestaoEmpresas();			
			}else{
				$this->session->set_flashdata('erro', 'erro');
				$this->gestaoEmpresas();
			}					 
			
		}else{


			$parametros 			= 	$this->session->userdata();		
			//$parametros['estados']	=	$this->icmsM->getEstados();
			$parametros['title']	=	"Cadastro de Empresas";

			$parametros 					= 	$this->session->userdata();		
			$parametros['estados']			=	$this->empresasM->getEstados();
			$parametros['tipo_cadastros']	=	$this->empresasM->getTiposCadastro();
			$parametros['title']			=	"Cadastro de Empresas";

			$this->_load_view('area-administrador/empresas/cadastra-empresa',$parametros);
		}
	}

	public function vincularClienteClass()
	{
		$dados_insert = $this->input->post();
		$erro=0;
		if( isset($dados_insert['classificadora_id']) ){
			$id_empresa = $dados_insert['classificadora_id'];
			if(!$this->empresasM->vincularCliente($dados_insert)){
				$erro=1;
			}
		}elseif(isset($dados_insert['administrador_id'])){
			$id_empresa = $dados_insert['administrador_id'];
			if(!$this->empresasM->insereClienteAdm($dados_insert)){
				$erro=1;
			}
		}

		if($erro==0){
			
			$this->session->set_flashdata('sucesso', 'ok');	
 		    redirect('/AreaAdministrador/visualizarEmpresa/'.$id_empresa);
		}
		
	}

	public function editarEmpresa($id = null, $pesquisa = null){

		if($this->input->post('salvar') == 1){

			$update = $this->input->post();
			
			unset($update['salvar']);
			unset($update['tipo_acesso_id']);
			$update['tipo_cadastro_id'] = $this->input->post('tipo_acesso_id');
			
			if($this->empresasM->atualizaEmpresas($update)){
				$this->session->set_flashdata('retorno', 'Sucessso');
				$this->log('Área Administrador | editar empresa','empresas','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				
				redirect('/AreaAdministrador/gestaoEmpresas');
			}

		}else{
			$row 								= 	$this->empresasM->getEmpresa($id);		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Empresas";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();			
			$parametros['pesquisa']				= 	$pesquisa;			 
			$this->_load_view('area-administrador/empresas/editar-empresa',	$parametros );	
		}
	}

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->send();
		}else{
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)
			    ->send();
		}

		return $result;		
	}

	public function ValidaCnpj(){
    	$cnpj 			= 	$this->input->post('cnpj');		
		$consulta_rf 	= 	$this->ConsultaCNPJRF($cnpj);

        if (isset($consulta_rf) && $consulta_rf->{'code'} == '1')
        {
            if ($consulta_rf->{'data'}->{'situacao_cadastral'} == 'ATIVA')
            {
            	$arr['retorno'] 	= 	$consulta_rf->{'data'};
            	$arr['save'] 		= 	$consulta_rf->{'Save_Receita'};
                $arr['result'] 		= 	true;
                $arr['situacao'] 	= 	$consulta_rf->{'data'}->{'situacao_cadastral'};
            	//echo json_encode($consulta_rf);            	
                
            	$arr['result'] = true;
				$arr['erro'] = 0;

            }else {
            	$arr['erro'] 	= 1;
            	$arr['msg'] 	= '* Situação cadastral '.$consulta_rf->{'data'}->{'situacao_cadastral'}.' na Receita Federal.'; 
            }
		}else {

            if (isset($consulta_rf)){
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 2 - '.$consulta_rf->{'code'}.')';
            }else{
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 3)';
            }

        }

		echo json_encode(array(	'receita' 	=> $arr));
	}
	
	/*
	*Métodos referentes as cotas por cliente
	
	*/
	public function gestaoCotasAjax(){
		
		$dados = array(	'id_semana'		=>	$_POST['id_semana']);
		
		$empresa_id = "";

		if( $this->session->userdata('tipo_acesso') == 'administrador empresas' ){
			$empresa_id = $this->session->userdata('empresa_id');
		}

		$parametros['cotas']	=	$this->cotasM->findCotas($_POST['id_semana'], $empresa_id);
		
		echo json_encode(array('retorno' => $parametros));
	}
	//Pega as cotas  
	 public function gestaoCotas($pesquisa = null)
	{
		$parametros 					= 	$this->session->userdata();
		$parametros['dados']			=	$this->cotasM->getCotas();
		if( $this->session->userdata('tipo_acesso') == 'administrador empresas' ){
			$parametros['dados_cliente']	=	$this->empresasM->getEmpresasPorCliente( $this->session->userdata('empresa_id') );
		}else{
			$parametros['dados_cliente']	=	$this->empresasM->getEmpresas();
		}
		$parametros['title']			=	"Gestão de Cotas";

		//$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-administrador/cotas/gestao-cotas',$parametros);
	}


	/*
	*Métodos referentes as cotas por cliente e Turno
	
	*/
	public function gestaoCotasTurnoAjax(){
		
		$dados = array(	'id_turno'		=>	$_POST['id_turno']);
		
		$empresa_id = "";

		if( $this->session->userdata('tipo_acesso') == 'administrador empresas' ){
			$empresa_id = $this->session->userdata('empresa_id');
		}

		$parametros['cotas']	=	$this->cotasM->findCotasTurno($_POST['id_turno'], $empresa_id);
		
		echo json_encode(array('retorno' => $parametros));
	}
	//Pega as cotas  
	 public function gestaoCotasTurno($pesquisa = null)
	{
		$parametros 					= 	$this->session->userdata();
		$parametros['dados']			=	$this->cotasM->getCotasTurno();
		$parametros['dados_turno']		=	$this->turnoM->buscaTurnos();
		if( $this->session->userdata('tipo_acesso') == 'administrador empresas' ){
			$parametros['dados_cliente']	=	$this->empresasM->getEmpresasPorCliente( $this->session->userdata('empresa_id') );
		}else{
			$parametros['dados_cliente']	=	$this->empresasM->getEmpresas();
		}
		$parametros['title']			=	"Gestão de Cotas por Turno";

		//$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-administrador/cotas/gestao-cotas-turno',$parametros);
	}

	//Importa as cotas da Mosaic
	public function importaCotas($pesquisa = null)
	{
		$parametros 					= 	$this->session->userdata();
		$parametros['dados']			=	$this->cotasM->getCotasImportadas();
		$parametros['dados_cliente']	=	$this->empresasM->getEmpresas();

		if( $this->session->userdata('tipo_acesso') == 'administrador empresas' ){
			$parametros['dados_cliente']	=	$this->empresasM->getEmpresasPorCliente( $this->session->userdata('empresa_id') );
		}else{
			$parametros['dados_cliente']	=	$this->empresasM->getEmpresas();
		}
		$parametros['title']			=	"Importa Cotas";
			
		//$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-administrador/cotas/importa-cotas',$parametros);
	}
	
	/**
	 * Realiza a importação das cotas - PORÉM SÓ FUNCIONA A PARTIR DA VERSÃO 7.2 DO PHP
	 * 
	 */
	public function lerCotasImportadas(){

		if(isset($_FILES['arquivo'])){
			$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
			

		  	$ext = strtolower(substr($_FILES['arquivo']['name'],-4)); //Pegando extensão do arquivo
		   	$new_name ="teste" .".". $ext; //Definindo um novo nome para o arquivo
		   	$dir = './../vanzin/uploads/'; //Diretório para uploads
	 		$nome_completo = $dir . $new_name;
			move_uploaded_file($_FILES['arquivo']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
			echo $nome_completo;
			$reader->setReadDataOnly(true);
			//$reader->setLoadSheetsOnly(["Sheet 1", "Terca"]);
			$spreadsheet = $reader->load($nome_completo);

			$sheet_count = $spreadsheet->getSheetCount();

			$terminal = 5;//Mosaic

			//verifica e realizar a exclusão dos itens da Mosaic
			if($sheet_count > 0){
				$this->cotasM->excluirCotaTerminal();
			}

			for ($i=0 ; $i < $sheet_count ; $i++) {
				$sheet = $spreadsheet->getSheet($i);
				$dia_semana = '';
				$dia = '';
				//$id_empresa = null;
				$peso = '';
				$qtd = 0;
				$linha_titulo = 1;//variavel que verifica se a linha a ser lida é do título
				//$celula = $sheet->getCell('A2');

				if ($sheet->getTitle() <> 'SEM COTA'){
					for($w=0; $w <= 50; $w++){
						
						$cliente = '';
						//Captura as informações da coluna A
						$cursorA = 'A'.$w;
						$celula = $sheet->getCell($cursorA);
						$celula_valor =  trim(strtoupper($celula->getFormattedValue()));
						//Verifica o dia da semana
						
						switch (trim(substr(strtoUpper($celula_valor),0,3))) {
							case 'SEG':
								$dia_semana = $celula_valor;
							break;
							case 'TER':
								$dia_semana = $celula_valor;
							break;
							case 'QUA':
								$dia_semana = $celula_valor;
							break;
							case 'QUI':
								$dia_semana = $celula_valor;
							break;
							case 'SEX':
								$dia_semana = $celula_valor;
							break;
							case 'SAB':
								$dia_semana = $celula_valor;
							break;
							case 'DOM':
								$dia_semana = $celula_valor;
							break;
						}
						
						if($dia_semana != '' and $celula_valor != null){
							//echo ' Dia: '.$dia_semana.'</br>';
							$dia = substr(strtolower($dia_semana),0,3);
						}
						
						
						//Captura o nome do cliente
						if(($celula_valor != 'NOME DO CLIENTE' and $celula_valor != null ) and ($cliente == '') and ($w > 2)){
							$cliente = $celula_valor;
							//echo ' Cliente: '.$cliente.'</br>';
							$linha_titulo = 0;
						}
						/*//Captura CNPJ
						$cursorB = 'B'.$w;
						$celulaB = $sheet->getCell($cursorB);

						$celula_valorB =  trim(strtoupper($celulaB->getFormattedValue()));
						if($celula_valorB != 'CNPJ' and $celula_valorB != ''){
							$dados_cliente['info_cliente']	=	$this->empresasM->getCnpj($celula_valorB);
							//echo ' CNPJ: '.$celula_valorB.'</br>';

							if(count($dados_cliente['info_cliente']) < 1){
								//echo 'Cadastra';

								$dadosEmpresa = array(
									'razao_social' 		=>	$cliente,
									'cnpj' 				=>	$celula_valorB,
									'tipo_cadastro_id'	=> 	1,
									'fl_importado'		=>	1
								);
								if($this->empresasM->add($dadosEmpresa)){

									$dados_cliente['info_cliente']	=	$this->empresasM->getCnpj($celula_valorB);
									//echo 'Cadastrou '.$dados_cliente['info_cliente'][0]->id;
									$id_empresa = $dados_cliente['info_cliente'][0]->id;
									
								}else{
									echo 'erro ao inserir Cliente'; 
								}
							}else{
								$id_empresa = $dados_cliente['info_cliente'][0]->id;
							}
							
						}
*/
						$cursorB = 'B'.$w;
						$celulaB = $sheet->getCell($cursorB);

						$celula_valorB =  trim(strtoupper($celulaB->getFormattedValue()));
		
						//Verifica o Peso
						switch ($celula_valorB) {
							case 'SEGUNDA':
								$peso = '';
							break;
							case 'TERÇA':
								$peso = '';
							break;
							case 'TERCA':
								$peso = '';
							break;
							case 'QUARTA':
								$peso = '';
							break;
							case 'QUINTA':
								$peso = '';
							break;
							case 'SEXTA':
								$peso = '';
							break;
							case 'SABADO':
								$peso = '';
							break;
							case 'DOMINGO':
								$peso = '';
							break;
							default:
								$peso = $celula_valorB;
							
						}

						$cursorC = 'C'.$w;
						$celulaC = $sheet->getCell($cursorC);

						$celula_valorC =  trim(strtoupper($celulaC->getFormattedValue()));
						if($celula_valorC == 'QTD CAMINHÕES'){
							$qtd = 0;
						}else{
							$qtd = $celula_valorC;
						}

						//if($linha_titulo < 1 and $id_empresa != ''){
							if($linha_titulo < 1 and $cliente != ''){
							$dadosCota = array(
								//'id_empresa' 		=>	$id_empresa,
								'razao_social'		=>	$cliente,
								'dia_semana' 		=>	$dia,
								'peso' 				=>	$peso,
								'quantidade'		=>	$qtd
								
							);
							
							if($this->cotasM->add_mosaic($dadosCota)){
								/*echo "<pre>";
								var_dump($dadosCota);
								echo "</pre>";*/
								$this->session->set_flashdata('sucesso', 'ok');	
							}else{
								$this->session->set_flashdata('erro', 'erro');	
							}
						}
						//$id_empresa = '';
						$cliente = '';
						$peso = '';
						$qtd = 0;
						$dia_semana = '';
					}
				}
				// processa os dados da planilh
			
			}
			
			redirect('/AreaAdministrador/importaCotas/');
			
			
		}else{
			echo "Arquivo não encontrado.";
		}

		


	} 
/* Esta sendo usa no Sistema devido a versão do PHP ser 7.028 */
public function importaCotaSimples(){

	if(isset($_FILES['arquivo'])){
		$ext = strtolower(substr($_FILES['arquivo']['name'],-4)); //Pegando extensão do arquivo
		$new_name ="planilha" .".". $ext; //Definindo um novo nome para o arquivo
		$dir = './../vanzin/uploads/'; //Diretório para uploads
		$nome_completo = $dir . $new_name;
		move_uploaded_file($_FILES['arquivo']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
		echo $nome_completo;

		
		//if ( $xlsx = SimpleXLSX::parse('./../vanzin/uploads/teste.xlsm') ) {
		if ( $xlsx = SimpleXLSX::parse($dir.$new_name) ) {
			//echo '<pre>'.print_r( $xlsx->sheetNames(), true ).'</pre>';
		
		// output worsheet 1

		$cliente = '';
		$peso = '';
		$qtd = 0;

		$dim = $xlsx->dimension();
		$num_cols = $dim[0];
		$num_rows = $dim[1];
		
		//verifica e realizar a exclusão dos itens da Mosaic
		if($num_rows > 2){
			$this->cotasM->excluirCotaTerminal();
		}

		if($xlsx->sheetName(0) == "SEGUNDA"){
			$dia = 'seg';
			//echo '<h2>'.$xlsx->sheetName(0).'</h2>';
			
			foreach ( $xlsx->rows( 0 ) as $r ) {
				
				for ( $i = 0; $i < 3; $i ++ ) {
					if(!empty($r[$i])){

						if(trim(strToUpper($r[ $i ])) != 'NOME DO CLIENTE' and trim(strToUpper($r[ $i ])) != 'SEGUNDA' and trim(strToUpper(substr($r[ $i ],0,3))) != 'QTD' ){

							if($i == 0){
								
								$cliente = $r[ $i ];
								//echo 'cliente '.$cliente.'</br>';
							}

							if($i == 1){
								if($r[ $i ] != ''){
									$peso = $r[ $i ];
									//echo 'peso '.$peso.'</br>';	
								}
								
							}

							if($i == 2){
								if($r[ $i ] != ''){
									$qtd = $r[ $i ];
									//echo 'qtd '.$qtd.'</br>';
								}
								
							}
							//echo '<td>' . ( ! empty( $r[ $i ] ) ? $r[ $i ] : '&nbsp;' ) . '</td>';
						}
					}
					
				}//fim do for

				if(trim(strToUpper(substr($cliente,0,5))) != 'SEGUN' and $cliente != '' and trim(strToUpper(substr($cliente,0,5))) != 'TOTAL'){
					$dadosCota = array(
						//'id_empresa' 		=>	$id_empresa,
						'razao_social'		=>	$cliente,
						'dia_semana' 		=>	$dia,
						'peso' 				=>	$peso,
						'quantidade'		=>	$qtd
						
					);

					if($this->cotasM->add_mosaic($dadosCota)){
						$this->session->set_flashdata('sucesso', 'ok');	
					}else{
						$this->session->set_flashdata('erro', 'erro');	
					}
					//$id_empresa = '';
					$cliente = '';
					$peso = '';
					$qtd = 0;
				}

			}
		}
		
		if($xlsx->sheetName(1) == "TERÇA"){
			$dia = 'ter';
			// output worsheet 2

			$dim = $xlsx->dimension( 2 );
			$num_cols = $dim[0];
			$num_rows = $dim[1];

			foreach ( $xlsx->rows( 1 ) as $r ) {
				
				for ( $i = 0; $i < 3; $i ++ ) {
					if(!empty($r[$i])){

						if(trim(strToUpper($r[ $i ])) != 'NOME DO CLIENTE' and trim(strToUpper($r[ $i ])) != 'TERÇA' and trim(strToUpper(substr($r[ $i ],0,3))) != 'QTD' ){

							if($i == 0){
								$cliente = $r[ $i ];
							}

							if($i == 1){
								if($r[ $i ] != ''){
									$peso = $r[ $i ];
								}
								
							}

							if($i == 2){
								if($r[ $i ] != ''){
									$qtd = $r[ $i ];
								}
								
							}
						}
					}
				}
				
				if(trim(strToUpper(substr($cliente,0,3))) != 'TER' and $cliente != '' and trim(strToUpper(substr($cliente,0,5))) != 'TOTAL'){
					$dadosCota = array(
						//'id_empresa' 		=>	$id_empresa,
						'razao_social'		=>	$cliente,
						'dia_semana' 		=>	$dia,
						'peso' 				=>	$peso,
						'quantidade'		=>	$qtd
						
					);
	
					if($this->cotasM->add_mosaic($dadosCota)){
						$this->session->set_flashdata('sucesso', 'ok');	
					}else{
						$this->session->set_flashdata('erro', 'erro');	
					}
					//$id_empresa = '';
					$cliente = '';
					$peso = '';
					$qtd = 0;
				}
			}	
		}	
		
		if($xlsx->sheetName(2) == "QUARTA"){
			$dia = 'qua';
			// output worsheet 2

			$dim = $xlsx->dimension( 2 );
			$num_cols = $dim[0];
			$num_rows = $dim[1];

			foreach ( $xlsx->rows( 2 ) as $r ) {
				
				for ( $i = 0; $i < 3; $i ++ ) {
					if(!empty($r[$i])){

						if(trim(strToUpper($r[ $i ])) != 'NOME DO CLIENTE' and trim(strToUpper($r[ $i ])) != 'QUARTA' and trim(strToUpper(substr($r[ $i ],0,3))) != 'QTD' ){

							if($i == 0){
								$cliente = $r[ $i ];
							}

							if($i == 1){
								if($r[ $i ] != ''){
									$peso = $r[ $i ];
								}
								
							}

							if($i == 2){
								if($r[ $i ] != ''){
									$qtd = $r[ $i ];
								}
								
							}
						}
					}
				}
				if(trim(strToUpper(substr($cliente,0,6))) != 'QUARTA' and $cliente != '' and trim(strToUpper(substr($cliente,0,5))) != 'TOTAL'){
					$dadosCota = array(
						//'id_empresa' 		=>	$id_empresa,
						'razao_social'		=>	$cliente,
						'dia_semana' 		=>	$dia,
						'peso' 				=>	$peso,
						'quantidade'		=>	$qtd
						
					);
	
					if($this->cotasM->add_mosaic($dadosCota)){
						$this->session->set_flashdata('sucesso', 'ok');	
					}else{
						$this->session->set_flashdata('erro', 'erro');	
					}
					//$id_empresa = '';
					$cliente = '';
					$peso = '';
					$qtd = 0;
				}
			}
			
		}

		if($xlsx->sheetName(3) == "QUINTA"){
			$dia = 'qui';
			// output worsheet 2

			$dim = $xlsx->dimension( 2 );
			$num_cols = $dim[0];
			$num_rows = $dim[1];

			foreach ( $xlsx->rows( 3 ) as $r ) {
				echo '<tr>';
				for ( $i = 0; $i < 3; $i ++ ) {
					if(!empty($r[$i])){

						if(trim(strToUpper($r[ $i ])) != 'NOME DO CLIENTE' and trim(strToUpper($r[ $i ])) != 'QUINTA' and trim(strToUpper(substr($r[ $i ],0,3))) != 'QTD' ){

							if($i == 0){
								$cliente = $r[ $i ];
							}

							if($i == 1){
								if($r[ $i ] != ''){
									$peso = $r[ $i ];
								}
								
							}

							if($i == 2){
								if($r[ $i ] != ''){
									$qtd = $r[ $i ];
								}
								
							}
						}
					}
				}
			
				if(trim(strToUpper(substr($cliente,0,6))) != 'QUINTA' and $cliente != '' and trim(strToUpper(substr($cliente,0,5))) != 'TOTAL'){
					$dadosCota = array(
						//'id_empresa' 		=>	$id_empresa,
						'razao_social'		=>	$cliente,
						'dia_semana' 		=>	$dia,
						'peso' 				=>	$peso,
						'quantidade'		=>	$qtd
						
					);
	
					if($this->cotasM->add_mosaic($dadosCota)){
						$this->session->set_flashdata('sucesso', 'ok');	
					}else{
						$this->session->set_flashdata('erro', 'erro');	
					}
					//$id_empresa = '';
					$cliente = '';
					$peso = '';
					$qtd = 0;
				}
			}
			
		}
		
		if($xlsx->sheetName(4) == "SEXTA"){
			$dia = 'sex';
			// output worsheet 2

			$dim = $xlsx->dimension( 2 );
			$num_cols = $dim[0];
			$num_rows = $dim[1];

			foreach ( $xlsx->rows( 4 ) as $r ) {

				for ( $i = 0; $i < 3; $i ++ ) {
					if(!empty($r[$i])){

						if(trim(strToUpper($r[ $i ])) != 'NOME DO CLIENTE' and trim(strToUpper($r[ $i ])) != 'SEXTA' and trim(strToUpper(substr($r[ $i ],0,3))) != 'QTD' ){

							if($i == 0){
								
								$cliente = $r[ $i ];
							}

							if($i == 1){
								if($r[ $i ] != ''){
									$peso = $r[ $i ];	
								}
								
							}

							if($i == 2){
								if($r[ $i ] != ''){
									$qtd = $r[ $i ];
								}
								
							}
						}
					}
				}

				if(trim(strToUpper(substr($cliente,0,6))) != 'SEXTA' and $cliente != '' and trim(strToUpper(substr($cliente,0,5))) != 'TOTAL'){
					$dadosCota = array(
						//'id_empresa' 		=>	$id_empresa,
						'razao_social'		=>	$cliente,
						'dia_semana' 		=>	$dia,
						'peso' 				=>	$peso,
						'quantidade'		=>	$qtd
						
					);
	
					if($this->cotasM->add_mosaic($dadosCota)){
						$this->session->set_flashdata('sucesso', 'ok');	
					}else{
						$this->session->set_flashdata('erro', 'erro');	
					}
					//$id_empresa = '';
					$cliente = '';
					$peso = '';
					$qtd = 0;
				}
			}
			
		}

		if($xlsx->sheetName(5) == "SABADO"){
			$dia = 'sab';
			// output worsheet 2

			$dim = $xlsx->dimension( 2 );
			$num_cols = $dim[0];
			$num_rows = $dim[1];

			foreach ( $xlsx->rows( 5 ) as $r ) {
	
				for ( $i = 0; $i < 3; $i ++ ) {
					if(!empty($r[$i])){

						if(trim(strToUpper($r[ $i ])) != 'NOME DO CLIENTE' and trim(strToUpper($r[ $i ])) != 'SABADO' and trim(strToUpper(substr($r[ $i ],0,3))) != 'QTD' ){

							if($i == 0){
								
								$cliente = $r[ $i ];
							}

							if($i == 1){
								if($r[ $i ] != ''){
									$peso = $r[ $i ];
								}
								
							}

							if($i == 2){
								if($r[ $i ] != ''){
									$qtd = $r[ $i ];
								}
								
							}
						}
					}
				}

				if(trim(strToUpper(substr($cliente,0,6))) != 'SABADO' and $cliente != '' and trim(strToUpper(substr($cliente,0,5))) != 'TOTAL'){
					$dadosCota = array(
						//'id_empresa' 		=>	$id_empresa,
						'razao_social'		=>	$cliente,
						'dia_semana' 		=>	$dia,
						'peso' 				=>	$peso,
						'quantidade'		=>	$qtd
						
					);
	
					if($this->cotasM->add_mosaic($dadosCota)){
						$this->session->set_flashdata('sucesso', 'ok');	
					}else{
						$this->session->set_flashdata('erro', 'erro');	
					}
					//$id_empresa = '';
					$cliente = '';
					$peso = '';
					$qtd = 0;
				}
			}
			
		}

		if($xlsx->sheetName(6) == "DOMINGO"){
			$dia = 'sab';
			// output worsheet 2

			$dim = $xlsx->dimension( 6 );
			$num_cols = $dim[0];
			$num_rows = $dim[1];

			foreach ( $xlsx->rows( 6 ) as $r ) {
			
				for ( $i = 0; $i < 3; $i ++ ) {
					if(!empty($r[$i])){

						if(trim(strToUpper($r[ $i ])) != 'NOME DO CLIENTE' and trim(strToUpper($r[ $i ])) != 'DOMINGO' and trim(strToUpper(substr($r[ $i ],0,3))) != 'QTD' ){

							if($i == 0){
								
								$cliente = $r[ $i ];
							}

							if($i == 1){
								if($r[ $i ] != ''){
									$peso = $r[ $i ];
								}
								
							}

							if($i == 2){
								if($r[ $i ] != ''){
									$qtd = $r[ $i ];
								}
								
							}
						}
					}
				}
				
				if(trim(strToUpper(substr($cliente,0,7))) != 'DOMINGO' and $cliente != '' and trim(strToUpper(substr($cliente,0,5))) != 'TOTAL'){
					$dadosCota = array(
						//'id_empresa' 		=>	$id_empresa,
						'razao_social'		=>	$cliente,
						'dia_semana' 		=>	$dia,
						'peso' 				=>	$peso,
						'quantidade'		=>	$qtd
						
					);
	
					if($this->cotasM->add_mosaic($dadosCota)){
						$this->session->set_flashdata('sucesso', 'ok');	
					}else{
						$this->session->set_flashdata('erro', 'erro');	
					}
					//$id_empresa = '';
					$cliente = '';
					$peso = '';
					$qtd = 0;
				}
			}
			
		}
		redirect('/AreaAdministrador/importaCotas/');
	} else {
		echo SimpleXLSX::parseError();
	}
		echo '<pre>';
		
	}
}

	//realiza o cadastro das cotas
	public function cadastrarCota()
	{
		if($this->input->post()){
			$this->db->trans_begin();

			$total_linhas = $this->input->post('total_linhas');
			$envia_email = 0;
			for($i=0; $i < $total_linhas; $i++){
				$s = (string) $i;//Preciso converter para string a variavel contadora
				
				//echo $this->input->post('id_empresa'.$s); die;
				$id_empresa 		= 	$this->input->post('id_empresa'.$s);
				$razao_social		=	$this->input->post('razao_social'.$s);
				$dia_semana 		=	$this->input->post('dia_semana');
				$peso 				=	$this->input->post('peso'.$s);
				$hr_limite_agenda 	=	$this->input->post('hr_limite_agenda'.$s);
				$hr_limite_acesso 	=	$this->input->post('hr_limite_acesso'.$s);
				$fl_sem_cota 		=	$this->input->post('fl_sem_cota'.$s);
				
				if(empty($fl_sem_cota)){
					$fl_sem_cota = '0';
				}
				/*//Comentado em 25/10/2021 devido a mudança de pensamento da vanzin em relação a salvar peso zero quando é sem cota
				if($peso < 1 ){
				
					$this->session->set_flashdata('erro', 'erro');
					$envia_email = 0;
					
					$this->db->trans_rollback();
					redirect('/AreaAdministrador/gestaoCotas/');
				
				}
*/
				if(strlen($hr_limite_agenda) < 4){
					$this->session->set_flashdata('erro', 'erro');
					$envia_email = 0;
					
					$this->db->trans_rollback();
					redirect('/AreaAdministrador/gestaoCotas/');
				}

				if(strlen($hr_limite_acesso) < 4){
					$this->session->set_flashdata('erro', 'erro');
					$envia_email = 0;
					
					$this->db->trans_rollback();
					redirect('/AreaAdministrador/gestaoCotas/');
				}		 

				$dadosCota = array(
					'id_empresa' 		=>	$id_empresa,
					'dia_semana' 		=>	$dia_semana,
					'peso' 				=>	str_replace('.','',$peso),
					'hr_limite_agenda' 	=>	str_replace(':','',$hr_limite_agenda),
					'hr_limite_acesso' 	=>	str_replace(':','',$hr_limite_acesso),
					'fl_sem_cota' 		=>	$fl_sem_cota 
					
				); 
				
				if(!empty($id_empresa)){
					$empresacota = $this->empresasM->getEmpresa($id_empresa);
					
					$conteudo= 	"CNPJ: ". 	$empresacota[0]['cnpj']." <br/> ";
					$conteudo.= "Razão Social: ". 	$razao_social." <br/> ";
					$conteudo.= "Dia da Semana: ". 	$dia_semana." <br/> ";
					$conteudo.=	'Peso: '.	$peso.' <br/> ';
					$conteudo.=	'Horário Limite Agendamento: '.	$hr_limite_agenda.' <br/> ';
					$conteudo.=	'Horário Limite Acesso: '.	$hr_limite_acesso.' <br/> ';
					$conteudo.=	'Sem Cota: '.	$fl_sem_cota.' <br/> ';

					$conteudo.=	'Usuário que cadastrou:	  '.	$this->session->userdata('nome');
				}
				
	 
				$this->cotasM->excluirCota($id_empresa, $dia_semana);
				
				if($this->cotasM->add($dadosCota))
				{
					$this->log('Área Administrador | cadastro Cota','cotas','INSERÇÃO', $this->session->userdata('usuario_id'),$dadosCota,$dadosCota,$_SERVER['REMOTE_ADDR']);
					$envia_email = 1;		
				}else{
					$this->session->set_flashdata('erro', 'erro');
					$envia_email = 0;
				}
			}
			if ($envia_email == 1){
				//$this->enviaEmail('wagnerom@gmail.com.br','Cota Cadastrada',$conteudo,$razao_social); 
				$this->session->set_flashdata('sucesso', 'ok');	
			}
			$this->db->trans_commit();
					
			redirect('/AreaAdministrador/gestaoCotas/');
		}else{

			$parametros 			= 	$this->session->userdata();		
			$parametros['title']	=	"Cadastro de Cotas por Cliente";
			$this->_load_view('area-administrador/cotas/gestao-cotas',$parametros);
		}
	}

	//realiza o cadastro das cotas
	public function cadastrarCotaTurno()
	{
		if($this->input->post()){
			$this->db->trans_begin();

			$total_linhas = $this->input->post('total_linhas');
			$envia_email = 0;
			for($i=0; $i < $total_linhas; $i++){
				$s = (string) $i;//Preciso converter para string a variavel contadora
				
				//echo $this->input->post('id_empresa'.$s); die;
				$id_empresa 		= 	$this->input->post('id_empresa'.$s);
				$razao_social		=	$this->input->post('razao_social'.$s);
				$id_turno 			=	$this->input->post('id_turno');
				$peso 				=	$this->input->post('peso'.$s);
				$hr_limite_agenda 	=	$this->input->post('hr_limite_agenda'.$s);
				$hr_limite_acesso 	=	$this->input->post('hr_limite_acesso'.$s);
				$fl_sem_cota 		=	$this->input->post('fl_sem_cota'.$s);
				
				if(empty($fl_sem_cota)){
					$fl_sem_cota = '0';
				}
				/*//Comentado em 25/10/2021 devido a mudança de pensamento da vanzin em relação a salvar peso zero quando é sem cota
				if($peso < 1 ){
				
					$this->session->set_flashdata('erro', 'erro');
					$envia_email = 0;
					
					$this->db->trans_rollback();
					redirect('/AreaAdministrador/gestaoCotas/');
				
				}
*/
				if(!empty($hr_limite_agenda)){
					if(strlen($hr_limite_agenda) < 4){
						$this->session->set_flashdata('erro', 'erro');
						$envia_email = 0;
						echo "teste 2"; die;
						$this->db->trans_rollback();
						redirect('/AreaAdministrador/gestaoCotasTurno/');
					}
				}
				if(!empty($hr_limite_acesso)){
					if(strlen($hr_limite_acesso) < 4){
						$this->session->set_flashdata('erro', 'erro');
						$envia_email = 0;
						echo "teste 1"; die;
						$this->db->trans_rollback();
						redirect('/AreaAdministrador/gestaoCotasTurno/');
					}
				}

				 

				$dadosCota = array(
					'id_empresa' 		=>	$id_empresa,
					'id_turno' 			=>	$id_turno,
					'peso' 				=>	str_replace('.','',$peso),
					'hr_limite_agenda' 	=>	str_replace(':','',$hr_limite_agenda),
					'hr_limite_acesso' 	=>	str_replace(':','',$hr_limite_acesso),
					'fl_sem_cota' 		=>	$fl_sem_cota 
					
				); 
				
				if(!empty($id_empresa)){
					$empresacota = $this->empresasM->getEmpresa($id_empresa);
					
					$conteudo= 	"CNPJ: ". 	$empresacota[0]['cnpj']." <br/> ";
					$conteudo.= "Razão Social: ". 	$razao_social." <br/> ";
					$conteudo.= "Turno: ". 	$id_turno." <br/> ";
					$conteudo.=	'Peso: '.	$peso.' <br/> ';
					$conteudo.=	'Horário Limite Agendamento: '.	$hr_limite_agenda.' <br/> ';
					$conteudo.=	'Horário Limite Acesso: '.	$hr_limite_acesso.' <br/> ';
					$conteudo.=	'Sem Cota: '.	$fl_sem_cota.' <br/> ';

					$conteudo.=	'Usuário que cadastrou:	  '.	$this->session->userdata('nome');
				}
				
	 
				$this->cotasM->excluirCotaTurno($id_empresa, $id_turno);
				
				$teste = $this->cotasM->addTurno($dadosCota);
				if($teste)
				{
					
					$this->log('Área Administrador | cadastro Cota Turno','cotas','INSERÇÃO', $this->session->userdata('usuario_id'),$dadosCota,$dadosCota,$_SERVER['REMOTE_ADDR']);
					$envia_email = 1;		
				}else{
					/*echo "<pre>";
					var_dump($dadosCota);
					echo "</pre></br>";
					echo $teste;*/
					$this->session->set_flashdata('erro', 'erro');
					$envia_email = 0;
				}
				
				
			}
			if ($envia_email == 1){
				//$this->enviaEmail('wagnerom@gmail.com.br','Cota Cadastrada',$conteudo,$razao_social); 
				$this->session->set_flashdata('sucesso', 'ok');	
			}
			$this->db->trans_commit();
				
			redirect('/AreaAdministrador/gestaoCotasTurno/');
		}else{

			$parametros 			= 	$this->session->userdata();		
			$parametros['title']	=	"Cadastro de Cotas Turno por Cliente";
			$this->_load_view('area-administrador/cotas/gestao-cotas-turno',$parametros);
		}
	}

	/*Busca informações do relatório de agendamentos */
	 public function relatorioAgendamento(){
		$parametros			 			=	$this->session->userdata();		
		$parametros['title']			=	"Relatório de Agendamentos";		
		$empresa_id	 					= 	$this->session->userdata('empresa_id');

		if( $parametros['tipo_acesso'] == 'administrador geral' || $parametros['tipo_acesso'] == 'gate' || $parametros['tipo_acesso'] == 'gate terminal'){
			$empresa_id = '';
			$parametros['empresas']		= 	$this->empresasM->getClientes();
		}
		$parametros['ag_resumido'] 		= 	$this->agendaM->buscaAgendamentosDoDiaResumido($empresa_id, $parametros['tipo_acesso']);
		$parametros['agendamentos'] 	= 	$this->agendaM->buscaAgendamentosDoDia($empresa_id,$parametros['tipo_acesso']);
		$parametros['turnos'] 			= 	$this->turnoM->buscaTurnos();
		$this->_load_view('area-administrador/relatorios/rel-agendamentos',$parametros);

	 }
	/*Busca informações do relatório de agendamentos em Ajax */
	public function retornaRelatorioAgendamentosAjax(){

    	$parametros 	= 	$this->input->post();
		
    	$filtros = "";
    	if($parametros['dt_ini'] != '' & $parametros['dt_fim'] != ''){
    		$dt_ini = explode('/', $parametros['dt_ini']);
    		$dt_fim = explode('/', $parametros['dt_fim']);
    		$filtros.=" and agendamento.dt_agenda between '".$dt_ini[2]."-".$dt_ini[1]."-".$dt_ini[0]."' and '".$dt_fim[2]."-".$dt_fim[1]."-".$dt_fim[0]."'";
    	}	    
    	
    	if($parametros['placa'] != ''){
    		$filtros.=" and agendamento.placa = '".$parametros['placa']."'";
    	}
    	
    	if($parametros['nr_nf'] != ''){
			$filtros.=" and nota_fiscal.nr_nf = ".$parametros['nr_nf'];     				
    	}
    	
		if($parametros['id_turno'] != ''){
			$filtros.=" and agendamento.id_turno = ".$parametros['id_turno'];     				
    	}

    	$empresa_id	 = 	$this->session->userdata('empresa_id');
		if( $this->session->userdata('tipo_acesso') == 'administrador geral' || $this->session->userdata('tipo_acesso') == 'gate'){
			$empresa_id = $parametros['empresa_id'];
		}
		$data['ag_resumido'] 	= 	$this->agendaM->buscaAgendamentosResumido($empresa_id, $filtros);
		$data['agendamentos']	= 	$this->agendaM->buscaAgendamentos($empresa_id, $filtros);
		$retorno 				= 	$this->load->view('/area-administrador/relatorios/rel-agendamentos-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno,
								'filtros' 	=> 	$filtros ));

	}
	/*Busca informações do relatório de saídas */
	public function relatorioSaida(){
		$parametros			 			=	$this->session->userdata();		
		$parametros['title']			=	"Relatório de Saída";		
		$empresa_id	 					= 	$this->session->userdata('empresa_id');

		if( $parametros['tipo_acesso'] == 'administrador geral' || $parametros['tipo_acesso'] == 'gate' || $parametros['tipo_acesso'] == 'gate terminal'){
			$empresa_id = '';
			$parametros['empresas']		= 	$this->empresasM->getClientes();
		}
		$parametros['ag_resumido'] 		= 	$this->agendaM->buscaSaidasResumido($empresa_id);
		$parametros['agendamentos'] 	= 	$this->agendaM->buscaSaidas($empresa_id);
		$parametros['destinos']			= 	$this->terminalM->buscaTerminais();
/*echo "<pre>";
var_dump($parametros);
echo "</pre>"; die;*/
		$this->_load_view('area-administrador/relatorios/rel-saidas',$parametros);

	 }
	/*Busca informações do relatório de saidas em Ajax */
	public function retornaRelatorioSaidasAjax(){

    	$parametros 	= 	$this->input->post();
    	$filtros = "";
    	if($parametros['dt_ini'] != '' & $parametros['dt_fim'] != ''){
    		$dt_ini = explode('/', $parametros['dt_ini']);
    		$dt_fim = explode('/', $parametros['dt_fim']);
    		$filtros.=" and date_format(acesso_patio.dthr_saida,'%Y-%m-%d') between '".$dt_ini[2]."-".$dt_ini[1]."-".$dt_ini[0]."' and '".$dt_fim[2]."-".$dt_fim[1]."-".$dt_fim[0]."'";
    	}	    
    	
    	if($parametros['placa'] != ''){
    		$filtros.=" and agendamento.placa = '".$parametros['placa']."'";
    	}
    	
    	if($parametros['nr_nf'] != ''){
			$filtros.=" and nota_fiscal.nr_nf = '".$parametros['nr_nf']."'";     				
    	}
    	
    	$empresa_id	 = 	$this->session->userdata('empresa_id');
		if( $this->session->userdata('tipo_acesso') == 'administrador geral' || $this->session->userdata('tipo_acesso') == 'gate'){
			$empresa_id = $parametros['empresa_id'];
			//$filtros.=" agendamento.id_empresa = '".$parametros['empresa_id']."'"; 
		}

		if($parametros['destino'] != ''){  
    		$filtros.=" and agendamento.id_terminal = '".$parametros['destino']."'";
    	}
		$data['ag_resumido'] 	= 	$this->agendaM->buscaSaidasResumido($empresa_id, $filtros);
		$data['agendamentos']	= 	$this->agendaM->buscaSaidas($empresa_id, $filtros);
		$retorno 				= 	$this->load->view('/area-administrador/relatorios/rel-saidas-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno ));

	}

	/*Busca informações do relatório de Caminhões Estacionados */
	public function relatorioEstacionados(){
		$parametros			 			=	$this->session->userdata();		
		$parametros['title']			=	"Relatório de Caminhões Estacionados";		
		$empresa_id	 					= 	$this->session->userdata('empresa_id');

		if( $parametros['tipo_acesso'] == 'administrador geral' || $parametros['tipo_acesso'] == 'gate' || $parametros['tipo_acesso'] == 'gate terminal'){
			$empresa_id = '';
			$parametros['empresas']		= 	$this->empresasM->getClientes();
		}
		$parametros['ag_resumido'] 		= 	$this->agendaM->buscaEstacionadosResumido($empresa_id);
		$parametros['agendamentos'] 	= 	$this->agendaM->buscaEstacionados($empresa_id);

		$this->_load_view('area-administrador/relatorios/rel-estacionados',$parametros);

	 }

	/*Busca informações do relatório de estacionados em Ajax */
	public function retornaRelatorioEstacionadosAjax(){

    	$parametros 	= 	$this->input->post();
    	$filtros = "";
    	if($parametros['dt_ini'] != '' & $parametros['dt_fim'] != ''){
    		$dt_ini = explode('/', $parametros['dt_ini']);
    		$dt_fim = explode('/', $parametros['dt_fim']);
    		$filtros.=" and agendamento.dt_agenda between '".$dt_ini[2]."-".$dt_ini[1]."-".$dt_ini[0]."' and '".$dt_fim[2]."-".$dt_fim[1]."-".$dt_fim[0]."'";
    	}	    
    	
    	if($parametros['placa'] != ''){
    		$filtros.=" and agendamento.placa = '".$parametros['placa']."'";
    	}
    	
    	if($parametros['nr_nf'] != ''){
			$filtros.=" and nota_fiscal.nr_nf = '".$parametros['nr_nf']."'";     				
    	}
    	
    	$empresa_id	 = 	$this->session->userdata('empresa_id');
		if( $this->session->userdata('tipo_acesso') == 'administrador geral' || $this->session->userdata('tipo_acesso') == 'gate'){
			$empresa_id = $parametros['empresa_id'];
		}
		$data['ag_resumido'] 	= 	$this->agendaM->buscaEstacionadosResumido($empresa_id, $filtros);
		$data['agendamentos']	= 	$this->agendaM->buscaEstacionados($empresa_id, $filtros);
		$retorno 				= 	$this->load->view('/area-administrador/relatorios/rel-estacionados-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno ));

	}

	/**
	 * Tela de Folha da carga
	 */
	/*Busca informações do relatório de Caminhões Estacionados */
	public function folhaCarga(){
		$parametros			 			=	$this->session->userdata();		
		$parametros['title']			=	"Folha de Carga de Mercadorias";		
		$empresa_id	 					= 	$this->session->userdata('empresa_id');

		if( $parametros['tipo_acesso'] == 'administrador geral' || $parametros['tipo_acesso'] == 'gate'){
			$empresa_id = '';
			$parametros['empresas']		= 	$this->empresasM->getClientes();
		}
		//$parametros['ag_resumido'] 		= 	$this->agendaM->buscaEstacionadosResumido($empresa_id);
		$parametros['cargas'] 	= 	$this->agendaM->buscaFolhaMercadorias('');

		$this->_load_view('area-administrador/relatorios/rel-folha-carga',$parametros);

	 }

	 /*Busca informações do relatório de estacionados em Ajax */
	public function folhaCargaAjax(){

    	$parametros 	= 	$this->input->post();
    	$filtros = "";
    	if($parametros['dt_ini'] != '' & $parametros['dt_fim'] != ''){
    		$dt_ini = explode('/', $parametros['dt_ini']);
    		$dt_fim = explode('/', $parametros['dt_fim']);
    		$filtros.=" and ('".$dt_ini[2]."-".$dt_ini[1]."-".$dt_ini[0]."' between dthr_atracacao and dthr_desatracacao  or '".$dt_fim[2]."-".$dt_fim[1]."-".$dt_fim[0]."' between dthr_atracacao and dthr_desatracacao)";
    	}	    
    	
    	if($parametros['nr_fundeio'] != ''){
    		$filtros.=" and nr_fundeio = '".$parametros['nr_fundeio']."'";
    	}
    	
    	if($parametros['navio'] != ''){
			$filtros.=" and navio like '%".$parametros['navio']."%'";     				
    	}

		if($parametros['nr_escala'] != ''){
			$filtros.=" and nr_escala = '".$parametros['nr_escala']."'";     				
    	}
    	
    	/*$empresa_id	 = 	$this->session->userdata('empresa_id');
		if( $this->session->userdata('tipo_acesso') == 'administrador geral' || $this->session->userdata('tipo_acesso') == 'gate'){
			$empresa_id = $parametros['empresa_id'];
		}*/
		//$data['ag_resumido'] 	= 	$this->agendaM->buscaEstacionadosResumido($empresa_id, $filtros);
		$data['cargas']	= 	$this->agendaM->buscaFolhaMercadorias($filtros);
		$retorno 		= 	$this->load->view('/area-administrador/relatorios/rel-folha-carga-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno, 'filtros' 	=> 	$filtros  ));

	}

//Metodo que chama a geração da Folha de mercadorias
	public function emiteFolha($id){
    
		$parametros 				= 	$this->session->userdata();
		$parametros['title']		=	"Emite Folha de MErcadorias";
	
		$parametros['listagem']	=	$this->agendaM->getFolhaMercadoria($id);		
		$this->_load_view('area-administrador/relatorios/emite-folha',$parametros);
				
	}

	public function classificacao()
	{
		$parametros						=	$this->session->userdata();
		$parametros['title']			=	"Classificação de Carga";	
		$parametros['classificacao']	= 	$this->acessoM->buscaCaminhoesPatio('',1);
		$parametros['classificado']		= 	$this->acessoM->buscaCaminhoesClassificadosHoje('',1);
		$parametros['destinos']			= 	$this->terminalM->buscaTerminais();
		$this->_load_view('area-administrador/classificacao/classificacao',$parametros);

	}

	public function classificaAcesso()
	{
		$atualizaAcesso = array(	'id'					=> 	$this->input->post('acesso_id'),
									'id_classificadora' 	=>	$this->session->userdata('empresa_id'),
									'id_usuario_classific'	=> 	$this->session->userdata('usuario_id'),
									'dthr_classific' 		=> 	$this->acessoM->dataHoraExata(),
									'fl_atende_classific'	=> 	$this->input->post('situacao'),
									'motivo_classific'		=> 	$this->input->post('motivo'),
									'id_terminal'			=> 	$this->input->post('terminal')	);
		
		if( $this->acessoM->atualizaAcesso($atualizaAcesso) ){

			$atualizaTerminal = array( 	'id' 			=> 	$this->input->post('agendamento_id'),
										'id_terminal'	=> 	$this->input->post('terminal') 	);
			if($this->agendaM->atualiza($atualizaTerminal)){
				echo json_encode(array('retorno' => 'sucesso' ));
			}else{
				echo json_encode(array('retorno' => 'erro' ));	
			}
		}else{
			echo json_encode(array('retorno' => 'erro' ));
		}
	}

	public function expedicao()
	{
		$parametros						=	$this->session->userdata();
		$parametros['title']			=	"Expedição do Caminhão";	
		$parametros['classificacao']	= 	$this->acessoM->buscaCaminhoesPatio('',2);
		$parametros['classificado']		= 	$this->acessoM->buscaCaminhoesClassificadosHoje('',2);
		$parametros['destinos']			= 	$this->terminalM->buscaTerminais();
		$this->_load_view('area-administrador/expedicao/expedicao',$parametros);
 
	}

	public function desvincularEmpresa()
	{
		$where = array(	'principal_id' 	=> 	$this->input->post('id'),
						'cliente_id'	=> 	$this->input->post('empresa_id'),
						'tipo'	 		=> 	$this->input->post('tipo') 	);		

		if( $this->empresasM->desvincularEmpresa($where) ){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function relNfeRecepcionadasCCT(){
		$parametros			 	=	$this->session->userdata();		
		$parametros['title']	=	"Relatório de Notas Fiscais Recepcionadas CCT";
		$parametros['notas'] 	= 	$this->notaM->buscaNotasCCT();
		$this->_load_view('area-administrador/relatorios/rel-nfe-recepcionadas-cct',$parametros);

	}

	public function retornaRelNfeCCTAjax(){

    	$parametros 	= 	$this->input->post();
    	$filtros = "";
    	if($parametros['dt_ini'] != '' & $parametros['dt_fim'] != ''){
    		$dt_ini = explode('/', $parametros['dt_ini']);
    		$dt_fim = explode('/', $parametros['dt_fim']);
    		$filtros.=" and date(log_siscomex.dthr_envio) between '".$dt_ini[2]."-".$dt_ini[1]."-".$dt_ini[0]."' and '".$dt_fim[2]."-".$dt_fim[1]."-".$dt_fim[0]."'";
    	}
    	    	    	
    	if($parametros['nr_nf'] != ''){
			$filtros.=" and nota_fiscal.nr_nf = '".$parametros['nr_nf']."' and nota_fiscal.serie_nf = '".$parametros['serie_nf']."'";
    	}

    	if($parametros['chave'] != ''){
			$filtros.=" and nota_fiscal.chave_acesso = '".$parametros['chave']."'";
    	}

    	if($parametros['status'] != ''){
    		
    		if($parametros['status'] == 0){
    			$filtros.=" and log_siscomex.cd_retorno <> 'CCTR-IN0001'";
    		}else{
    			$filtros.=" and log_siscomex.cd_retorno = 'CCTR-IN0001'";
    		}
			
    	}
    	
		$data['notas']	= 	$this->notaM->buscaNotasCCT($filtros);
		$retorno 		= 	$this->load->view('/area-administrador/relatorios/rel-nfe-recepcionadas-cct-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno ));

	}

	public function relEntregaCargasDUE(){
		$parametros			 	=	$this->session->userdata();		
		$parametros['title']	=	"Relatório Entrega de Cargas DUE";
		$parametros['entregas']	= 	$this->transitoM->buscaEntregaCargasDUE();
		$this->_load_view('area-administrador/relatorios/rel-entrega-cargas-due',$parametros);

	}

	public function relEntregaCargasDUEAjax(){

    	$parametros 	= 	$this->input->post();
    	$filtros = "";
    	if($parametros['dt_ini'] != '' & $parametros['dt_fim'] != ''){
    		$dt_ini = explode('/', $parametros['dt_ini']);
    		$dt_fim = explode('/', $parametros['dt_fim']);
    		$filtros.=" and date(log_siscomex.dthr_envio) between '".$dt_ini[2]."-".$dt_ini[1]."-".$dt_ini[0]."' and '".$dt_fim[2]."-".$dt_fim[1]."-".$dt_fim[0]."'";
    	}
    	    	    	
    	if($parametros['placa'] != ''){
			$filtros.=" and log_siscomex.placa = '".$parametros['placa']."'";
    	}

    	if($parametros['due'] != ''){
			$filtros.=" and log_siscomex.nr_due = '".$parametros['due']."'";
    	}

    	if($parametros['cpf'] != ''){
			$filtros.=" and log_siscomex.cpf_motorista = '".$parametros['cpf']."'";
    	}
		
		if($parametros['status'] == 1){
			$filtros.=" and log_siscomex.cd_retorno = 'CCTR-IN0002'";
		}elseif($parametros['status'] == '0'){
			$filtros.=" and log_siscomex.cd_retorno <> 'CCTR-IN0002'";
		}else{
			$filtros.=" ";
		}
    	
		$data['entregas']	= 	$this->notaM->buscaEntregaCargasDUE($filtros);
		$retorno 			= 	$this->load->view('/area-administrador/relatorios/rel-entrega-cargas-due-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno ));

	}

	public function retornaPesosDUE()
	{
		$peso_manif 	= 	$this->transitoM->buscaPesoManifDUE($this->input->post('due'));
		$peso_entregue 	= 	$this->transitoM->buscaPesoEntregueDUE($this->input->post('due'));
		
		if($peso_manif['peso_manif'] >= $peso_entregue['entregue']){
			$saldo 			= 	$peso_manif['peso_manif'] - $peso_entregue['entregue'];
		}else{
			$saldo 			= 	$peso_entregue['entregue'] - $peso_manif['peso_manif'];
		}
		
	
		$html = "Peso Manif.(Kg): <span class='peso'>".$peso_manif['peso_manif']."</span><br/>
				 Peso Entregue(Kg): <span class='peso'>".$peso_entregue['entregue']."</span><br/> 
				 Saldo(Kg): <span class='peso'>".$saldo."</span><br/>";

		echo json_encode(array('retorno' => $html));
	}

	public function motoristas()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->motoristaM->select();		
		$parametros['title']	=	"Gestão de Motoristas";
		$this->_load_view('area-administrador/motoristas/motoristas',$parametros);
	}

	public function alteraStatusMotorista()
	{
		$dadosUpdate = array(	'id' 					=> 	$this->input->post('id'),
								'fl_bloqueado'			=> 	$this->input->post('ativo'),
								'motivo_bloqueio'		=> 	($this->input->post('motivo') != '') ? $this->input->post('motivo') : '',
								'id_usuario_bloqueio' 	=> 	($this->input->post('ativo') == 'S') ? $this->session->userdata('usuario_id') : NULL	);			
		
		if( $this->motoristaM->atualiza($dadosUpdate) ){
			echo json_encode(array('retorno'	=> 	'sucesso'));
		}else{
			echo json_encode(array('retorno'	=> 	'erro'));
		}	
	}

	public function retornaAgendamentosEmpresa()
	{
		$agendamentos = $this->agendaM->listarAgendamentosCliente($this->input->post('empresa_id'), $this->input->post('filtros'));
		$html = "<table id='html_table_m'>
					<thead>
						<tr>
							<th>#</th>
							<th>Cliente</th>
							<th>Data</th>
							<th>Placa</th>
							<th>Produto</th>
							<th>Nr.NF</th>
							<th>Motorista</th>
						</tr>
					</thead>
					<tbody>";
		
		foreach($agendamentos as $agto){
			$html.="<tr>
						<td>".$agto['id']."</td>
						<td>".$agto['razao_social']."</td>
						<td>".$agto['dt_agenda']."</td>
						<td>".$agto['placa']."</td>
						<td>".$agto['descricao']."</td>
						<td>".$agto['nr_nf']."</td>
						<td>".$agto['nome']."-".$agto['cpf']."</td>
					</tr>";

		}

		$html.="</tbody></table>";
		echo json_encode(array('retorno' => $html));
	}

	
	public function relatorioCotasAgendamento(){
		$parametros			 			=	$this->session->userdata();		
		$parametros['title']			=	"Relatório de Agendamentos";		
		$empresa_id	 					= 	$this->session->userdata('empresa_id');

		if( $parametros['tipo_acesso'] == 'administrador geral' || $parametros['tipo_acesso'] == 'gate' || $parametros['tipo_acesso'] == 'gate terminal'){
			$empresa_id = '';
			$parametros['empresas']		= 	$this->empresasM->getClientes();
		}
		
		$parametros['agendamentos'] 	= 	$this->agendaM->buscaAgendamentosCotas('',date('Y-m-d'));

		$this->_load_view('area-administrador/relatorios/rel-cotas-agendamentos',$parametros);

	 }
	
	public function retornaRelatorioCotasAgendamentoAjax(){

    	$parametros 	= 	$this->input->post();
    	$filtros 		= 	"";
    	$dt_agendamento = $parametros['dt_ini'];
    	if($parametros['dt_ini'] != ''){
			$dt_ini = explode('/', $parametros['dt_ini']);    		
			$dt_agendamento = $dt_ini[2]."-".$dt_ini[1]."-".$dt_ini[0];
		}    	
		$empresa_id = $parametros['empresa_id'];		
		
		$data['agendamentos']	= 	$this->agendaM->buscaAgendamentosCotas($empresa_id, $dt_agendamento);
		$retorno 				= 	$this->load->view('/area-administrador/relatorios/rel-cotas-agendamentos-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno ));

	}	
	
	public function testarConexaoApiRecintos(){
		var_dump($this->autenticar());
	}

	/*Turnos */
	public function turnos()
	{
		$parametros						=	$this->session->userdata();
		$parametros['title']			=	"Cadastros de Turnos";	

		$parametros['turnos'] 	= 	$this->turnoM->buscaTurnos();

		$this->_load_view('area-administrador/turnos/turnos',$parametros);

	}


	/* Cadastrar Turnos */
	public function cadastrarTurno()
	{
		$parametros						=	$this->session->userdata();
		$parametros['title']			=	"Cadastros de Turnos";	

		if($this->input->post()){

			$dadosTurno = array(
		        'nome' 			=>	$this->input->post('nome'),
				'hora_inicial' 	=>	str_replace(':','',$this->input->post('hora_inicial')),
				'hora_final' 	=>	str_replace(':','',$this->input->post('hora_final')),
		    );
			
			
			if($this->turnoM->insere($dadosTurno))
			{
				
				$this->session->set_flashdata('sucesso', 'ok');	
		
			}else{
				$this->session->set_flashdata('erro', 'erro');
				//$this->gestaoEmpresas();
			}					 
			$this->_load_view('area-administrador/turnos/cadastra-turno',$parametros);
		}else{


			$parametros 			= 	$this->session->userdata();		
			//$parametros['estados']	=	$this->icmsM->getEstados();
			$parametros['title']	=	"Cadastro de Turnos";

			$parametros 					= 	$this->session->userdata();		
			
			$parametros['title']			=	"Cadastro de Turnos";

			$this->_load_view('area-administrador/turnos/cadastra-turno',$parametros);
		}
	}

	public function excluirTurno()
	{

		if( $this->turnoM->excluir($this->input->post('id')) ){
			unset($_SESSION['sucesso']);
			echo json_encode(array(	'retorno' 	=> 	true,
									'titulo' 	=> 	"Ok!",
									'texto'		=> 	"Turno excluído com sucesso.",
									'tipo' 		=> 	"success"	 	 ));
		}else{
			unset($_SESSION['erro']);
			echo json_encode(array(	'retorno' 	=> 	false,
									'titulo' 	=> 	"Atenção!",
									'texto'		=> 	"Problema ao excluir, entre em contato com o suporte técnico.",
									'tipo' 		=> 	"danger"	 	 ));
		}	

	}

	public function editarTurno($id = null, $pesquisa = null){

		if($this->input->post('salvar') == 1){
 
			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'hora_inicial'		=>	str_replace(':','',$this->input->post('hora_inicial')),
							'hora_final'		=>	str_replace(':','',$this->input->post('hora_final')));

			
			unset($update['salvar']);
			//unset($update['tipo_acesso_id']);
			//$update['tipo_cadastro_id'] = $this->input->post('tipo_acesso_id');
			
			if($this->turnoM->atualizaTurnos($update)){
				
				redirect('/AreaAdministrador/turnos');
			}

		}else{
			//$row 								= 	$this->empresasM->getEmpresa($id);		
			$parametros 						= 	$this->session->userdata();
			//$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Turnos";		
			$parametros['dados']				=	$this->turnoM->getTurno($id);			
			$parametros['pesquisa']				= 	$pesquisa;			 
			$this->_load_view('area-administrador/turnos/editar-turno',	$parametros );	
		}
	}

	public function equipamentos()
	{
		$parametros			 			=	$this->session->userdata();		
		$parametros['title']			=	"Equipamentos";	
		$parametros['equipamentos'] 	= 	$this->equipamentosM->buscaEquipamentos();

		$this->_load_view('area-administrador/equipamentos/equipamentos',$parametros);

	}

	public function excluirEquipamento()
	{				
		if( $this->equipamentosM->excluir($this->input->post('id')) ){
			$this->enviaApiGeorreferenciamento($this->input->post('id'),'E');		
			unset($_SESSION['sucesso']);
			echo json_encode(array(	'retorno' 	=> 	true,
									'titulo' 	=> 	"Ok!",
									'texto'		=> 	"Equipamento excluído com sucesso.",
									'tipo' 		=> 	"success"	 	 ));

		}else{
			unset($_SESSION['erro']);
			echo json_encode(array(	'retorno' 	=> 	false,
									'titulo' 	=> 	"Atenção!",
									'texto'		=> 	"Problema ao excluir, entre em contato com o suporte técnico.",
									'tipo' 		=> 	"danger"	 	 ));
		}	

	}

	public function cadastrarEquipamento()
	{
		$parametros				=	$this->session->userdata();		
		$parametros['title']	=	"Cadastrar Equipamentos";	
		$parametros['tipos'] 	= 	$this->equipamentosM->buscaTipoAreaEquipamento();

		$this->_load_view('area-administrador/equipamentos/cadastra-equipamentos',$parametros);
	}

	public function inserirEquipamento()
	{
		if($this->input->post()){
			$dados = array(	'nome' 		=> $this->input->post('nome'),
							'descricao' => 	$this->input->post('descricao'),
							'id_tipo'	=> 	$this->input->post('id_tipo'),
							'coord_x'	=> 	$this->input->post('coord_x'),
							'coord_y'	=> 	$this->input->post('coord_y'),
							'fl_ativo'	=> 	$this->input->post('fl_ativo'));
			if( $id = $this->equipamentosM->insere($dados)){
				$this->session->set_flashdata('retorno', 'sucesso');
				$this->enviaApiGeorreferenciamento($id,'I');	
			}else{
				$this->session->set_flashdata('retorno', 'erro');	
			}

			redirect('/AreaAdministrador/equipamentos');
		}
	}

	public function editarEquipamento($id)
	{
		$parametros				=	$this->session->userdata();		
		$parametros['title']	=	"Editar Equipamento";	
		$parametros['tipos'] 	= 	$this->equipamentosM->buscaTipoAreaEquipamento();
		$parametros['dados']	= 	$this->equipamentosM->buscaEquipamento($id);
		$this->_load_view('area-administrador/equipamentos/edita-equipamentos',$parametros);
	}

	public function atualizaEquipamento()
	{
		if($this->input->post()){
			$dados = array(	'id' 		=> 	$this->input->post('id'),
							'nome' 		=> 	$this->input->post('nome'),
							'descricao' => 	$this->input->post('descricao'),
							'id_tipo'	=> 	$this->input->post('id_tipo'),
							'coord_x'	=> 	$this->input->post('coord_x'),
							'coord_y'	=> 	$this->input->post('coord_y'),
							'fl_ativo'	=> 	$this->input->post('fl_ativo'));

			if( $this->equipamentosM->atualiza($dados)){
				$this->session->set_flashdata('retorno', 'sucesso');
				$this->enviaApiGeorreferenciamento($id,'R');		
			}else{
				$this->session->set_flashdata('retorno', 'erro');	
			}

			redirect('/AreaAdministrador/equipamentos');

		}
	}

	public function veiculosBloqueados()
	{
		$parametros			 		=	$this->session->userdata();		
		$parametros['title']		=	"Veículos Bloqueados";	
		$parametros['veiculos'] 	= 	$this->veiculosBM->buscaVeiculosBloqueados();

		$this->_load_view('area-administrador/veiculos-bloqueados/veiculos-bloqueados',$parametros);

	}	

	public function cadastrarVeiculoBloqueado()
	{
		$parametros				=	$this->session->userdata();		
		$parametros['title']	=	"Cadastrar Equipamentos";			

		$this->_load_view('area-administrador/veiculos-bloqueados/cadastra-veiculo',$parametros);
	}

	public function inserirVeiculoBloqueado()
	{
		if($this->input->post()){
			$dados = array(	'placa'		=> 	$this->input->post('placa'),
							'ds_motivo' => 	$this->input->post('ds_motivo') );
			if( $id = $this->veiculosBM->insere($dados)){
				$this->session->set_flashdata('retorno', 'sucesso');
				$this->enviaApiBloqueioVeiculo($id,'I');	
			}else{
				$this->session->set_flashdata('retorno', 'erro');	
			}

			redirect('/AreaAdministrador/veiculosBloqueados');
		}
	}

	public function excluirVeiculoBloqueado()
	{
		$this->enviaApiBloqueioVeiculo($this->input->post('id'),'E');	
		if( $this->veiculosBM->excluir($this->input->post('id')) ){
				
			unset($_SESSION['sucesso']);
			echo json_encode(array(	'retorno' 	=> 	true,
									'titulo' 	=> 	"Ok!",
									'texto'		=> 	"Veículo desbloqueado com sucesso.",
									'tipo' 		=> 	"success"	 	 ));

		}else{
			unset($_SESSION['erro']);
			echo json_encode(array(	'retorno' 	=> 	false,
									'titulo' 	=> 	"Atenção!",
									'texto'		=> 	"Problema ao excluir, entre em contato com o suporte técnico.",
									'tipo' 		=> 	"danger"	 	 ));
		}	

	}

	public function relatorioApiRecintos(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Relatório Api Recintos";
		$this->_load_view('area-administrador/relatorios/rel-api-recintos',$parametros );

    }

    public function gerarRelatorioApiRecintos(){

	    $dt_ini = '';
	    $dt_fim = '';
	    $data['periodo'] = '';
	    $data['status'] = '';
	    if($this->input->post('dt_ini') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_ini'));	    	
	    	$dt_ini 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    	$data['periodo'] = $this->input->post('dt_ini');
	    }

	    if($this->input->post('dt_fim') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_fim'));	    	
	    	$dt_fim 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    	$data['periodo'].= ' a '.$this->input->post('dt_fim');
	    } 	    

	    $filtros = array(	'dt_ini'	=> 	$dt_ini,
	    					'dt_fim'	=> 	$dt_fim,	    	
	    					'periodo'	=> 	$data['periodo'],
	    					'evento'	=> 	$this->input->post('evento')	 ); 	   
	    
	    $verifica 		= $this->apiRecintosM->verificaQtdRegistros($filtros);
	    if( $verifica['total'] >= 500){
	    	$this->session->set_flashdata('retorno', 'erro');	
	    	$this->session->set_flashdata('total', $verifica['total']);	
	    	redirect('/AreaAdministrador/relatorioApiRecintos/');	    	
	    }else{
		    $data['dados']	=	$this->apiRecintosM->selectApiRecintos($filtros);
			$this->geraPdfRelatorioApiRecinto($filtros, $data);
		}

    }

    private function geraPdfRelatorioApiRecinto($filtros, $data){
    	//Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('rel-api-recintos.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');

    	$this->html2pdf->html($this->load->view('/area-administrador/relatorios/'.$filtros['evento'].'-pdf', $data,true));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/rel-api-recintos.pdf'));
	    
		}

	}

	private function enviaApiGeorreferenciamento($id, $tipo_operacao){
		
		//Insere na tabela api_georreferenciamento
		if($id_api = $this->equipamentosM->insereApiGeorreferenciamento($id, $tipo_operacao)){ 
			$dados = $this->equipamentosM->buscaDadosApiGeorreferenciamento($id);
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_ocorrencia']))
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_registro']))
			$cpf = str_replace('-','',str_replace('.','',$dados['cpf']));
			$data_envio = date('Y-m-d\TH:i:s.150O',strtotime(date('Y-m-d H:i:s')));
			$dadosEnvio = array( 	'tipoOperacao' 	=> 	$dados['tp_operacao'],
									'idEvento'		=> 	$dados['id_evento'],
									'dataHoraOcorrencia'	=>	$data_envio,
									'dataHoraRegistro'		=> 	$data_envio,									
									'contingencia'			=> 	false,
									'codigoRecinto' 		=> 	'0302701',
									'nome'					=> 	$dados['nome'],
									'areaEquipamentoAtivo'	=> 	($dados['fl_status'] == 'S') ? true : false,
									'tipo'					=> 	$dados['tipo'],	
									'listaCoordenadas'		=> 	array(	'idElemento' 	=> 	$dados['id'],
																	 	'latitude' 		=> 	$dados['coord_x'],
																	 	'longitude'		=> 	$dados['coord_y'] ) 	);
			
			//Autenticação e envio para o siscomex						
			list($token, $x_csrf_token) = $this->autenticar();
			$retorno_envio = $this->enviarDados('/ext/evento-georreferenciamento', json_encode($dadosEnvio), $token, $x_csrf_token);
			$code = json_decode($retorno_envio);
			$atualizaControle = array(	'id' 				=> $id_api,
										'fl_envio' 			=> 'S',
										'retorno_envio' 	=> 	$retorno_envio,
										'id_retorno'		=> 	$code->http_code);
			$this->equipamentosM->atualizaApiGeorreferenciamento($atualizaControle);
			
		}
		
	}

	private function enviaApiBloqueioVeiculo($id, $tipo_operacao='I'){
		
		//Insere na tabela api_georreferenciamento
		if($id_api = $this->veiculosBM->insereApiBloqueioVeiculo($id, $tipo_operacao)){ 
			$dados = $this->veiculosBM->buscaDadosApiBloqueioVeiculo($id);
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_ocorrencia']))
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_registro']))			
			$data_envio = date('Y-m-d\TH:i:s.150O',strtotime(date('Y-m-d H:i:s')));
			$dadosEnvio = array( 	'tipoOperacao' 	=> 	$dados['tp_operacao'],
									'idEvento'		=> 	$dados['id_evento'],
									'dataHoraOcorrencia'	=>	$data_envio,
									'dataHoraRegistro'		=> 	$data_envio,									
									'contingencia'			=> 	false,
									'codigoRecinto' 		=> 	'0302701',
									'status'				=> 	($tipo_operacao == 'I') ? 'B' : 'D',
									'placa'					=> 	$dados['placa'],
									'setorSolicitante'		=> 	$dados['setor_solicitante'],	
									'motivo'				=> 	$dados['motivo'] );
			
			//Autenticação e envio para o siscomex						
			list($token, $x_csrf_token) = $this->autenticar();
			$retorno_envio = $this->enviarDados('/ext/bloqueio-desbloqueio-veiculo-carga', json_encode($dadosEnvio), $token, $x_csrf_token);
			$code = json_decode($retorno_envio);
			$atualizaControle = array(	'id' 				=> $id_api,
										'fl_envio' 			=> 'S',
										'retorno_envio' 	=> 	$retorno_envio,
										'id_retorno'		=> 	$code->http_code);
			$this->veiculosBM->atualizaApiBloqueioVeiculo($atualizaControle);
			
		}
		
	}
	
}//termina classe