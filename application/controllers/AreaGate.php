<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Dompdf\Dompdf;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');


class AreaGate extends MY_Controller { 
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() ||  ($this->session->userdata('tipo_acesso') != 'administrador geral' && ($this->session->userdata('tipo_acesso') != 'administrador empresas') && ($this->session->userdata('tipo_acesso') != 'gate') && ($this->session->userdata('tipo_acesso') != 'gate terminal') )  ) {
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');		
		$this->load->model('AcessoModel', 'acessoM');
		$this->load->model('AgendamentoModel', 'agendaM');
		$this->load->helper('form');
		$this->load->helper('url');
		
	}
 
	public function index()
	{
		$parametros						 	=	$this->session->userdata();		
		$parametros['title']			 	=	"Área de Entrada e Saída de Caminhões";		
		$this->_load_view('area-administrador/acesso/acesso-patio',$parametros);

	}


	/*Metodos referentes a Entrada e saída de veículos*/
	
	public function acessoPatio(){

		$parametros 			= 	$this->session->userdata();
/*
		echo "<pre>";
		print_r($parametros);
		echo "</pre>";
*/
		
		$parametros['dados_cliente']	=	$this->empresasM->getEmpresas();
		$parametros['title']	=	"Entrada/Saída de Veículos";
		
		//$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-administrador/acesso/acesso-patio',$parametros);
		
	}
	
	public function acessoPatioAjax(){
		
		$dados = array(	'placa'		=>	$_POST['placa']);
		
		$parametros['acesso']	=	$this->acessoM->findTipoAcesso($_POST['placa']);
		if ($parametros['acesso'][0]->existe > 0){//SAIDA
			$parametros['listagem']	=	$this->acessoM->getSaida($_POST['placa'],0,null);
		}else{//ENTRADA
			//necessário pra quando houver mais de uma agendamento
			$verificaRetornoAcesso		= $this->acessoM->VerificaRetornoAcesso($_POST['placa']);
		/*	echo $verificaRetornoAcesso[0]->existe.'</br>';
			echo $verificaRetornoAcesso[0]->id;
die;*/
			if(!empty($verificaRetornoAcesso[0]->existe) > 0){
				$parametros['listagem']		=	$this->acessoM->getAcesso($_POST['placa'],0,$verificaRetornoAcesso[0]->id);
			}else{
				$parametros['listagem']		=	$this->acessoM->getAcesso($_POST['placa'],0,null);
			}
	
			if(!empty($parametros['listagem'][0]->id_acesso)){
				$id_acesso 			= $parametros['listagem'][0]->id_acesso; 
				//$hora_saida_manut 	= substr($parametros['listagem'][0]->dthr_saida_manutencao,11,5);
				if(!empty($parametros['listagem'][0]->dthr_saida_manutencao)){
				
					//$hora_saida_manut 	= date('H:i:s', strtotime('+3 hours', strtotime($parametros['listagem'][0]->dthr_saida_manutencao)));
					$hora_saida_m		= substr($parametros['listagem'][0]->dthr_saida_manutencao,11,5);
					$hora_saida_manut 	= date('H:i:s',strtotime('+3 hours', strtotime($hora_saida_m)));
					
				}

				
				//echo ' >> '.$parametros['listagem'][0]->dthr_saida_manutencao.' --! '.$hora_saida_manut.'!</br>' ;
			}
			/*Verificação de horario de acesso */
			$verifica = $this->acessoM->verificaHorarioAcesso($_POST['placa']);
			$parametros['erro'] = '';
			if (count($verifica) > 0 ){
				
				if($verifica[0]->id_turno == null || $verifica[0]->id_turno == 0){
					$hora_limite	= $verifica[0]->hr_limite_acesso;
					$hora_atual 	= substr($verifica[0]->hora_atual,0,5);
					$hora_atual		= substr($hora_atual,0,2).substr($hora_atual,3,2);
				}else{
					/*Verifica o Turno */
					$verifica_turno = $this->acessoM->verificaHorarioTurnoAcesso($_POST['placa']);					
					$hora_limite	= $verifica_turno[0]->hr_limite_acesso;
					$hora_atual 	= substr($verifica_turno[0]->hora_atual,0,5);
					$hora_atual		= substr($hora_atual,0,2).substr($hora_atual,3,2);
				}
				

				if($hora_atual  > $hora_limite){
					$parametros['erro'] = 'Veículo ultrapassou o horário limite de acesso';
				}else{
					if(!empty($hora_saida_manut)){

						$hora_atual_servidor = date('H:i:s',strtotime($hora_atual));
//echo $hora_saida_manut .' - '.$hora_atual_servidor.' ---- '.$verifica[0]->hora_atual;
						if($hora_saida_manut < $hora_atual_servidor){ 
							$parametros['erro'] = 'Veículo ultrapassou o tempo limite de 3 horas para acesso após a manutenção.';
						}
					}
				}
			}
			/*Fim da verificação */

			 
			//Esse if verifica se esse caminhão deu entrada normal ou pra manutenção
			if (count($parametros['listagem']) == 0 ){
				$parametros['listagem']	=	$this->acessoM->getAcesso($_POST['placa'],0,null);
			} 
		}
	
		echo json_encode(array('retorno' => $parametros));
	}
		
	/*Registra a entrada ou saída do caminhão*/	
	public function registrarAcesso()
	{
		if($this->input->post()){
			$this->db->trans_begin();

            $_SESSION['pag_anterior']  = '';//Indica que a tela é de acesso e não a tela de reimpressao
			
			$dados = array(	'placa'		=>	$_POST['placa']);
		
			$parametros['acesso']	=	$this->acessoM->findTipoAcesso($_POST['placa']);
			
			$dataExata = $this->acessoM->dataHoraExata();

			if ($parametros['acesso'][0]->existe > 0){/////////////SAIDA
				$altera['id'] 					= $this->input->post('id');
				$altera['id_motorista_saida'] 	= $this->input->post('id_motorista_saida');
				
				$altera['id_usuario_saida'] 	= $_SESSION['usuario_id'];

				if ($this->input->post('saida_manut') <> null){
					$altera['dthr_saida_manutencao'] 	=  $dataExata;	
                    $tipo = 0; //indica que a saida é de manutenção
					$altera['dthr_retorno_manutencao'] 	= null;
					$altera['id_classificadora'] 	 	= null; 
					$altera['fl_atende_classific'] 	 	= null;
                    
				}else{//Grava o acesso de entrada de retorno da manutenção
					if($this->input->post('dthr_saida_manutencao') <> '' and $this->input->post('dthr_retorno_manutencao') == null){
						$altera['dthr_retorno_manutencao'] 	 = 	$dataExata;	
					}else{
						$altera['dthr_saida'] 			 	 = 	$dataExata;		
					}
					$tipo = 1;
				}

				//Verifica erro de não pegar os registros de Saida
				if(empty($this->input->post('id')) or empty($this->input->post('id_motorista_saida'))){
					$this->session->set_flashdata('erro', 'erro');
					$this->db->trans_rollback();
					redirect('/AreaGate/acessoPatio/');
				}else{

					if($this->acessoM->atualizaAcesso($altera)){
						
						$this->session->set_flashdata('sucesso', 'ok');
						$this->db->trans_commit();
						//$this->enviaApiAcesso($altera['id'] , 'S');						
						$this->enviaApiAcessoVeiculos($altera['id']  , 'S');
						redirect('/AreaGate/imprimeTicket/'.$_POST['placa'].'/'.$tipo.'/'.$altera['id']);
						
					}
					

				}
				
				
			}else{//ENTRADA
				
				$busca['listagem']	=	$this->acessoM->getAcesso($_POST['placa'],1,null);
	/*	echo "<pre>";
		var_dump($this->input->post('radio'));
		echo 'id agenda = '.$this->input->post('id_agenda');
		echo "</pre>"; 
		echo "Contagem ".count($busca['listagem']); die;*/
				if (count($busca['listagem']) == 0){//Quando a entrada é normal cai aqui

					if($this->input->post('radio') === NULL){
						$id_agenda = $this->input->post('id_agenda');
					}else{
						$id_agenda = $this->input->post('radio');
					}

					$dadosAcesso = array(
								//'placa' 				=> 	str_replace('-','',$this->input->post('placa')),
								'placa' 				=> 	$this->input->post('placa'),
								'id_agenda' 			=>	$id_agenda,
								'id_motorista_entrada' 	=>	$this->input->post('id_motorista_entrada'),
								'dthr_entrada'			=>	$dataExata,
								'id_usuario_entrada' 	=>	$_SESSION['usuario_id'],
								'observacao' 			=>	$this->input->post('observacao')
							);
					$dados_insert = $dadosAcesso;

					
					//Verifica erro de não pegar os registros de entrada
					if(empty($this->input->post('id_agenda')) or empty($this->input->post('id_motorista_entrada'))){
						$this->session->set_flashdata('erro', 'erro');
						$this->db->trans_rollback();
						redirect('/AreaGate/acessoPatio/');
					}else{
						if($id = $this->acessoM->add($dados_insert)){							
							$tipo = 0;
							$this->session->set_flashdata('sucesso', 'ok');
							$this->db->trans_commit();
							//$this->enviaApiAcesso($id , 'E');
							$this->enviaApiAcessoVeiculos($id  , 'E');
							redirect('/AreaGate/imprimeTicketEntrada/'.$_POST['placa'].'/'.$tipo.'/null');
						}

					}
					

					
				}else{//Quando a entrada é um retorno de manutenção
					$altera['id'] 						= $this->input->post('id');
					$altera['dthr_retorno_manutencao'] 	= $dataExata;	
					$tipo = 1;

				/*	echo "<pre>";
					echo $this->input->post('radio')."</br></br>";
					var_dump($this->input->post());
					echo 'altera = '.$altera;
					echo "</pre>"; 
					echo "Contagem ".count($busca['listagem']);
die;*/
					//Verifica erro de não pegar os registros de entrada
					if(empty($this->input->post('id'))){

						if(empty($this->input->post('radio'))){
							$this->session->set_flashdata('erro', 'erro');
							$this->db->trans_rollback();
							redirect('/AreaGate/acessoPatio/');		
						}else{
							//Entrada quando se tem mais de uma opção de escolha de agendamento
							$altera['id'] = $this->input->post('radio');							

							if($this->acessoM->atualizaAcesso($altera)){
								
								$this->session->set_flashdata('sucesso', 'ok');
								$this->db->trans_commit();
								//$this->enviaApiAcesso($altera['id']  , 'E');
								$this->enviaApiAcessoVeiculos($altera['id']  , 'E');
								redirect('/AreaGate/imprimeTicketEntrada/'.$_POST['placa'].'/'.$tipo.'/'.$altera['id']);
									 
							}
						}
	
					}else{
						if($this->acessoM->atualizaAcesso($altera)){							
							$this->session->set_flashdata('sucesso', 'ok');
							$this->db->trans_commit();
							//$this->enviaApiAcesso($altera['id']  , 'E');
							$this->enviaApiAcessoVeiculos($altera['id']  , 'E');
							redirect('/AreaGate/imprimeTicketEntrada/'.$_POST['placa'].'/'.$tipo.'/'.$altera['id']);
								 
						}
					}

					
				}

			}
		}else{
			$parametros 			= 	$this->session->userdata();		
			$parametros['title']	=	"Registra Acesso";
			$this->_load_view('area-administrador/acesso/acesso-patio',$parametros);
		}
	}
/** $placa = Placa do veiculo
 * $tipo = Indica se o veículo sai normal ou por manutenção
 * 0 = Manutenção
 * 1 = Normal 
 */
	public function imprimeTicket($placa,$tipo, $id){

		if($_SESSION['pag_anterior'] <> 'REIMPRESSAO'){
			$_SESSION['pag_anterior']  = '';
		}
		
		$parametros 				= 	$this->session->userdata();
		$parametros['title']		=	"Imprime Ticket";
		$parametros['placa']		=	$placa;

		
		$parametros['listagem']	=	$this->acessoM->getSaida($parametros['placa'],$tipo,$id);
		
		$this->_load_view('area-administrador/acesso/imprime-ticket',$parametros);
		
		
	}

	public function imprimeTicketEntrada($placa,$tipo, $id){
		if($_SESSION['pag_anterior'] <> 'REIMPRESSAO'){
			$_SESSION['pag_anterior']  = '';
		}
		
		$parametros 				= 	$this->session->userdata();
		$parametros['title']		=	"Imprime Ticket";
		$parametros['placa']		=	$placa;

		$parametros['listagem']	=	$this->acessoM->getAcesso($parametros['placa'],1,$id);
		$this->_load_view('area-administrador/acesso/imprime-ticket-entrada',$parametros);
		
		
	}

	public function reImpressao(){
		$_SESSION['pag_anterior'] 	= 	'REIMPRESSAO';
		$parametros 				= 	$this->session->userdata();	
		$parametros['dados']		=	$this->acessoM->buscaAcessoGeral($parametros['empresa_id']);
		$parametros['title']		=	"Reimpressão de Ticket";	
		$this->_load_view('area-administrador/acesso/re-impressao',$parametros);
				
	}
	
	/*private function enviaApiAcesso($id,$tipo){
		//Insere na tabela api_controle_acesso_pessoa
		if($id_api = $this->acessoM->insereApiControleAcesso($id,$tipo)){ 
			$dados = $this->acessoM->buscaDadosEnvioApi($id);
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_ocorrencia']))
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_registro']))
			$data_envio = date('Y-m-d\TH:i:s.150O',strtotime(date('Y-m-d H:i:s')));
			$dadosEnvio = array( 	'tipoOperacao' 	=> 	$dados['tp_operacao'],
									'idEvento'		=> 	$dados['id_evento'],
									'dataHoraOcorrencia'	=>	$data_envio,
									'dataHoraRegistro'		=> 	$data_envio,
									'contingencia'			=> 	false,
									'codigoRecinto' 		=> 	'0302701',
									'cpf'					=> 	$dados['cpf'],
									'direcao'				=> 	$dados['direcao'],
									'nome'					=> 	$dados['nome'] );
			//Autenticação e envio para o siscomex						
			list($token, $x_csrf_token) = $this->autenticar();
			$retorno_envio = $this->enviarDados('/ext/acesso-pessoas', json_encode($dadosEnvio), $token, $x_csrf_token);
			$atualizaControle = array(	'id' 				=> $id_api,
										'fl_envio' 			=> 'S',
										'retorno_envio' 	=> 	$retorno_envio);
			$this->acessoM->atualizaApiAcesso($atualizaControle);
			
		}
	}*/
	
	private function enviaApiAcessoVeiculos($id,$tipo){
		//Insere na tabela api_controle_acesso_pessoa
		if($id_api = $this->acessoM->insereApiControleAcessoVeiculos($id,$tipo)){
			$dados = $this->acessoM->buscaDadosEnvioApiVeiculos($id);
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_ocorrencia']))
			//date('Y-m-d\TH:i:s.uZ',strtotime($dados['dthr_registro']))
			$data_envio = date('Y-m-d\TH:i:s.150O',strtotime($dados['dthr_ocorrencia']));
			$dadosEnvio = array( 	'tipoOperacao' 	=> 	$dados['tp_operacao'],
									'idEvento'		=> 	$dados['id_evento'],
									'dataHoraOcorrencia'	=>	$data_envio,
									'dataHoraRegistro'		=> 	$data_envio,
									'contingencia'			=> 	false,
									'codigoRecinto' 		=> 	'0302701',
									'operacao'				=> 	'C',
									'direcao'				=> 	$dados['direcao'],
									'placa'					=> 	$dados['placa'],
									'motorista'				=> 	array( 'cpf' => $dados['cpf_motorista'], 'nome' => $dados['nome_motorista']	) 	);
			//Autenticação e envio para o siscomex						
			list($token, $x_csrf_token) = $this->autenticar();
			
			$retorno_envio = $this->enviarDados('/ext/acesso-veiculos', json_encode($dadosEnvio), $token, $x_csrf_token);
			$code = json_decode($retorno_envio);
			$atualizaControle = array(	'id' 				=> $id_api,
										'fl_envio' 			=> 'S',
										'retorno_envio' 	=> 	$retorno_envio,
										'id_retorno'		=> 	$code->http_code); 
			$this->acessoM->atualizaApiAcessoVeiculos($atualizaControle);
			
		}
	}

}//termina classe
