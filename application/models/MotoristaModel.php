<?php

class MotoristaModel extends CI_Model {

    public function select() {
        $sql = "SELECT m.*, group_concat(a.placa) as placas, u.nome as usuario_bloqueio  FROM motorista m                
                LEFT JOIN agendamento a ON a.id_motorista = m.id 
                LEFT JOIN usuarios u ON u.id = m.id_usuario_bloqueio
                group by m.id ";
        return $this->db->query($sql)->result();
    }

	public function insere($motorista)
	{
        $this->db->insert('motorista', $motorista);
		return $this->db->insert_id();
    }

    public function atualiza($dados){

        $this->db->where('id', $dados['id']);
        if($this->db->update('motorista', $dados)){
            return true;
        }else{
            return false;
        }
    }
    //Verifica se o CPF existe do motorista
    public function verificaCPF($cpf){
        $sql = "select id from motorista where replace(replace(cpf,'.',''),'-','') = ".$cpf;
        return $this->db->query($sql)->result();
    }

}