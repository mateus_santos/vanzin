<?php

class TransitoMovimentacaoModel extends CI_Model {
	
	public function add($data)
	{
		
	    $this->db->insert('transito_movimentacao', $data);
		return $this->db->insert_id();
	}

	public function update($dados)
	{
		$this->db->where('placa', $dados['placa']);

        if($this->db->update('transito_movimentacao', $dados)){
            return true;

        }else{
            return false;
        }
	}

	public function buscaDadosTransito($cnpj)
	{
		/*$cnpj_t = substr_replace($cnpj,'-',12, 0);
		$cnpj_b = substr_replace($cnpj_t,'/',8, 0);
		$cnpj_p1 = substr_replace($cnpj_b,'.',5, 0);
		$cnpj_p2 = substr_replace($cnpj_p1,'.',2, 0);
		$cnpj = $cnpj_p2;		
		$sql = "SELECT 	id_transito, nr_due, peso_manif
			 	FROM 	transito_simplificado_due,
			            empresas
		       WHERE 	transito_simplificado_due.id_empresa = empresas.id
		         AND 	transito_simplificado_due.fl_aberta = 0
		         AND 	empresas.cnpj = '".$cnpj."' 
				 AND 	id_transito in (SELECT max(id_transito) 
		                             	FROM transito_simplificado_due,
			                           		empresas
				               			WHERE transito_simplificado_due.id_empresa = empresas.id
				                    			AND transito_simplificado_due.fl_aberta    = 'S'
												AND empresas.cnpj  = '".$cnpj."'  )";*/
		$sql = "SELECT 	transito_simplificado_due.id_transito, transito_simplificado_due.nr_due, transito_simplificado_due.peso_manif,
						transito_simplificado.id_terminal
			 	FROM 	transito_simplificado_due,
			            empresas,
						transito_simplificado 
		       WHERE 	transito_simplificado_due.id_empresa = empresas.id
			     AND    transito_simplificado.id = transito_simplificado_due.id_transito 
 		         AND 	transito_simplificado_due.fl_aberta = 'S'
		         AND 	substr(trim(replace(replace(replace(empresas.cnpj, '-', ''), '.', ''), '/', '')) , 1, 8)  = substr('".$cnpj."',1,8)
				 AND id_transito in (SELECT max(id_transito) 
		                             FROM transito_simplificado_due, 
			                           empresas
               			WHERE transito_simplificado_due.id_empresa = empresas.id
                    			AND transito_simplificado_due.fl_aberta = 'S'
								AND substr(trim(replace(replace(replace(empresas.cnpj, '-', ''), '.', ''), '/', '')) , 1, 8)  = substr('".$cnpj."',1,8)  )"; 

		return $this->db->query($sql)->row_array();
	}
	
	public function buscaDadosProc($dados)
	{
		$sql = "select count(*) as total from transito_movimentacao tm where tm.dthr_movimento = '".$dados['dthr_movimento']."' and tm.nr_due = '".$dados['nr_due']."' and tm.placa = '".$dados['placa']."' and tm.cpf_motorista = '".$dados['cpf_motorista']."'";
		//return $this->db->query($sql);
		$query = $this->db->query($sql);

        return $query->result();
	}

	public function addProc($dados){
		$sql = "CALL p_insere_transito('".$dados['placa']."', '".$dados['cpf_motorista']."', '".$dados['dthr_movimento']."', '".$dados['cnpj']."', '".$dados['peso_aferido']."', '".$dados['no_motorista']."', '".$dados['peso_tara']."', '".$dados['peso_bruto']."' , '".$dados['dthr_entrada']."' , '".$dados['dthr_saida']."')";
		return $this->db->query($sql);
	}	
		
	public function insereApiControleAcesso($id, $tipo){
		$sql = "INSERT api_controle_acesso_pessoa (id_evento,
    				origem,
  				   tp_operacao,
   				   cpf_usuario, 
   			       dthr_ocorrencia,
   				   dthr_registro,
   				   fl_contingencia,
                   direcao,
                   nome
				)
				SELECT 'VANZIN-ACESSO_PESSOA-".$id."',
					concat('TRANSITO_MOVIMENTACAO-', transito_movimentacao.id),
					   'I',
						'00000000000',
						dthr_movimentacao,
						dthr_movimentacao,
						'N',
						'".$tipo."',
						no_motorista
				FROM transito_movimentacao
				WHERE id = ".$id;
		if( $this->db->query($sql) ){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	public function buscaDadosEnvioApi($id){
		$sql = "SELECT * FROM api_controle_acesso_pessoa
				WHERE api_controle_acesso_pessoa.id_acesso = ".$id;
		return $this->db->query($sql)->row_array();
	}
	
	public function buscaUltimoId($placa){
		$sql = "SELECT id FROM transito_movimentacao WHERE placa = '".$placa."'  
				ORDER BY transito_movimentacao.id  DESC
				LIMIT 1";
		return $this->db->query($sql)->row_array();
	}
	
	public function atualizaApiAcesso($dados){
		$this->db->where('id', $dados['id']);

        if($this->db->update('api_controle_acesso_pessoa', $dados)){
            return true;

        }else{
            return false;
        }
	}
	
	public function insereApiControleAcessoVeiculos($id, $tipo){
		$sql = "INSERT api_controle_acesso_veiculo  (id_evento,
    				origem,
  				   tp_operacao,
   				   cpf_usuario, 
   			       dthr_ocorrencia,
   				   dthr_registro,
   				   fl_contingencia,
                   direcao,
                   nome
				   id_acesso )
SELECT 'VANZIN-TRANSITO_MOVIMENTACAO-".$id."',
	concat('TRANSITO_MOVIMENTACAO-', id),
       'I',
        '00000000000',
        dthr_entrada,
        dthr_entrada,
        'N',
        'E',
        placa,
	cpf_motorista,
	no_motorista,
	id
FROM transito_movimentacao
WHERE id = ".$id;
  
		if( $this->db->query($sql) ){
			return $this->db->insert_id();
		}else{
			return false;
		}
	
	}
	
	public function buscaDadosEnvioApiVeiculos($id){
		$sql = "SELECT * FROM api_controle_acesso_veiculo  
				WHERE api_controle_acesso_veiculo .id_acesso = ".$id;
		return $this->db->query($sql)->row_array();
	}
	
	public function atualizaApiAcessoVeiculos($dados){
		$this->db->where('id', $dados['id']);

        if($this->db->update('api_controle_acesso_veiculo', $dados)){
            return true;

        }else{
            return false;
        }
	}
 
}