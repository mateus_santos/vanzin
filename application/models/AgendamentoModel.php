<?php

class AgendamentoModel extends CI_Model {

	public function insere($agendamento)
	{
        $this->db->insert('agendamento', $agendamento);
		return $this->db->insert_id();
    }

    public function insereItem($item)
	{
        $this->db->insert('agendamento_item', $item);
		return $this->db->insert_id();
    }

	public function insere_log_erro($agendamento)
	{
        $this->db->insert('log_agendamentos', $agendamento);
		return $this->db->insert_id();
    }


    public function atualiza($dados){

        $this->db->where('id', $dados['id']);
        if($this->db->update('agendamento', $dados)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaItem($dados){

        $this->db->where('id', $dados['id']);
        if($this->db->update('agendamento_item', $dados)){
            return true;
        }else{
            return false;
        }
    }

    public function excluirItens($id_agenda){
		$this->db->where('id_agenda', $id_agenda);
        if(	$this->db->delete('agendamento_item') ){
            return true;
        }else{
            return false;
        }
	}

	public function excluirNF($id_agenda){
		$sql = "DELETE FROM nota_fiscal_item WHERE id_nota_fiscal IN (SELECT id FROM nota_fiscal WHERE id_agenda = ".$id_agenda.") ";
		
        if(	$this->db->query($sql) ){
        	$this->db->where('id_agenda', $id_agenda);
           	if(	$this->db->delete('nota_fiscal') ){
	            return true;
	        }else{
	            return false;
	        }
        }else{
            return false;
        }
	}

	public function excluir($id){
		if( $this->excluirNF($id) ){
			if( $this->excluirItens($id) ){
				$this->db->where('id', $id);
		        if(	$this->db->delete('agendamento') ){
		            return true;
		        }else{
		            return false;
		        }
	    	}else{
	    		return false;
	    	}
    	}else{
    		return false;
    	}
	}
	//Busca erros dos logs de importação do agendamento
	public function getLogErrosAgenda(){

		$sql = "select * from log_agendamentos";
		return $this->db->query($sql)->result();

	} 
//Exclui todos os logs do agendamento importado
	public function excluirLogsAgenda(){
		$this->db->where('1','1');
        if(	$this->db->delete('log_agendamentos') ){
            return true;
        }else{
            return false;
        }
	}

	public function buscaAgendamentosDoDia($empresa_id,$tipo_acesso)
	{
		$where_empresa = '';
		if( $empresa_id != '' && $tipo_acesso != 'administrador empresas'){
			$where_empresa = " and 	agendamento.id_empresa = ".$empresa_id;
		}

		// se for administrador de empresas, busca apenas clientes vinculados
		if( $tipo_acesso == 'administrador empresas'){
			$where_empresa = " and 	agendamento.id_empresa in (select cliente_id from empresas_clientes where administrador_id = ".$empresa_id.")" ;
		}

		$sql = "SELECT	agendamento.id,
						agendamento.id_tipo_operacao,
	          			empresas.razao_social,
	          			date_format(agendamento.dt_agenda,'%d/%m/%Y') as dt_agenda,
						agendamento.placa,
						motorista.nome,
						motorista.cpf,
						motorista.fone,
						embalagem.no_embalagem,
						group_concat(nota_fiscal.nr_nf) as nr_nf,
						nota_fiscal.quant_total,
						agendamento_item.descricao,
						agendamento.cliente_final,
						agendamento.motorista,
						turnos.nome as turno
				FROM 	agendamento
				INNER JOIN empresas ON agendamento.id_empresa  = empresas.id
				INNER JOIN motorista ON agendamento.id_motorista = motorista.id
				LEFT JOIN turnos 		ON agendamento.id_turno = turnos.id
				LEFT JOIN agendamento_item ON agendamento.id = agendamento_item.id_agenda
				LEFT JOIN embalagem ON agendamento_item.id_embalagem = embalagem.id
				LEFT JOIN nota_fiscal ON agendamento.id = nota_fiscal.id_agenda
				where 1=1 
				".$where_empresa."
				and agendamento.dt_agenda = '".date('Y-m-d')."'	
				
				and 	agendamento.id not in (select id_agenda from acesso_patio)
				GROUP BY agendamento.id 
				ORDER BY agendamento.id DESC ";
		
		return $this->db->query($sql)->result_array();
	}

	public function buscaAgendamentosDoDiaResumido($empresa_id,$tipo_acesso)
	{
		$where_empresa = '';
		if( $empresa_id != '' && $tipo_acesso != 'administrador empresas'){
			$where_empresa = " and 	agendamento.id_empresa = ".$empresa_id;
		}
		// se for administrador de empresas, busca apenas clientes vinculados
		if( $tipo_acesso == 'administrador empresas' ){
			$where_empresa = " and 	agendamento.id_empresa in (select cliente_id from empresas_clientes where administrador_id = ".$empresa_id.")" ;	
		}

		$sql = "SELECT	empresas.id,
						empresas.razao_social,
						count(empresas.id) total
				FROM 	agendamento
				INNER JOIN empresas ON agendamento.id_empresa  = empresas.id
				INNER JOIN motorista ON agendamento.id_motorista = motorista.id								
				LEFT JOIN acesso_patio ON acesso_patio.id_agenda = agendamento.id								
				where  1=1 
				".$where_empresa."
				and acesso_patio.dthr_entrada is null
				and 	agendamento.dt_agenda = '".date('Y-m-d')."'	
				and 	agendamento.id not in (select id_agenda from acesso_patio)
				group by empresas.id,
				empresas.razao_social
				ORDER BY agendamento.id DESC ";
 		
		return $this->db->query($sql)->result_array();
	}

	public function buscaAgendamentos($empresa_id,$where=' and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and	agendamento.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT	agendamento.id,
				agendamento.id_tipo_operacao,
				empresas.razao_social,
				date_format(agendamento.dt_agenda,'%d/%m/%Y') as dt_agenda,
				agendamento.placa,
				motorista.nome,
				motorista.cpf,
				motorista.fone,
				embalagem.no_embalagem,
				nf.notas as nr_nf,
				nf.peso_liquido as quant_total,
				agendamento_item.descricao,
				agendamento.cliente_final,
				agendamento.motorista,
				turnos.nome as turno
				FROM 	agendamento
				INNER JOIN empresas 	ON agendamento.id_empresa  = empresas.id
				INNER JOIN motorista 	ON agendamento.id_motorista = motorista.id
				LEFT JOIN turnos 		ON agendamento.id_turno = turnos.id
				LEFT JOIN agendamento_item ON agendamento.id = agendamento_item.id_agenda
				LEFT JOIN embalagem 	ON agendamento_item.id_embalagem = embalagem.id
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = agendamento.id
				WHERE 1=1  								
				".$where_empresa." ".$where."
				and agendamento.id not in (select id_agenda from acesso_patio)
				GROUP BY agendamento.id
				ORDER BY agendamento.id DESC ";
		
		return $this->db->query($sql)->result_array();
	}
	/*Busca o total de agendamentos resumido */
	public function buscaAgendamentosResumido($empresa_id,$where=' and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and	agendamento.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT	empresas.id,
				concat(empresas.razao_social,'-',empresas.cnpj) as razao_social,
				count(empresas.id) total
				FROM 	agendamento
				INNER JOIN empresas ON agendamento.id_empresa  = empresas.id
				INNER JOIN motorista ON agendamento.id_motorista = motorista.id								
				LEFT JOIN acesso_patio ON acesso_patio.id_agenda = agendamento.id						
				WHERE   1=1
				".$where_empresa." ".$where."
				and acesso_patio.dthr_entrada is null
				group by empresas.id,
				concat(empresas.razao_social,'-',empresas.cnpj)
				ORDER BY agendamento.id DESC ";
		return $this->db->query($sql)->result_array();
	}

	public function buscaAgendamentoPorId($id_agenda)
	{
		$sql = "SELECT	agendamento.id,
						agendamento.id_tipo_operacao,
						agendamento.id_empresa,
          				empresas.razao_social,
          				date_format(agendamento.dt_agenda,'%d/%m/%Y') as dt_agenda,
						agendamento.placa,
						agendamento.id_motorista,
						motorista.nome,
						motorista.fone,
						motorista.cpf,
						motorista.nr_cnh,
						motorista.fl_estrangeiro,
						nota_fiscal.nr_nf,
						nota_fiscal.serie_nf,
						nota_fiscal.arquivo,
						agendamento.id_frete,
						agendamento.id_transportadora,
						agendamento.id_turno,
						concat(transp.cnpj,'-', transp.razao_social) as transportadora
				FROM 	agendamento
				INNER JOIN empresas ON agendamento.id_empresa  = empresas.id
				INNER JOIN motorista ON  agendamento.id_motorista = motorista.id				
				LEFT JOIN nota_fiscal ON agendamento.id = nota_fiscal.id_agenda 
				LEFT JOIN empresas transp on transp.id = agendamento.id_transportadora	
				WHERE agendamento.id = ".$id_agenda."
			ORDER BY agendamento.id DESC ";

		return $this->db->query($sql)->row_array();			
	}

	public function buscaEmbalagem()
	{
		$sql = "SELECT * FROM embalagem";
		return $this->db->query($sql)->result_array();
	}

	public function verificaCpfCnh($where)
	{
		if($where['cpf'] != ''){
			$situacao = "cpf = '".$where['cpf']."'" ;
		}
		if($where['cnh'] != ''){
			$situacao = "nr_cnh = '".$where['cnh']."'";	
		}

		$sql = "	SELECT 	id, nome, cpf, nr_cnh, fone, fl_bloqueado 
					FROM 	motorista 
					WHERE 	".$situacao;

		return $this->db->query($sql)->row_array();
	}

	public function verificaNotaAgendamento($chave)
	{
		$sql = "SELECT 	agendamento.id, agendamento.placa
				  FROM 	agendamento
				  LEFT JOIN nota_fiscal ON nota_fiscal.id_agenda = agendamento.id 
				  WHERE	agendamento.id NOT IN (select id_agenda from acesso_patio where fl_atende_classific='N')
					and nota_fiscal.chave_acesso = '".$chave."'";

		return $this->db->query($sql)->row_array();
	}

	public function verificaCotasAgendamento($empresa_id, $dt_agendamento)
	{
		$sql = "SELECT  hr_limite_agenda, hr_limite_acesso, peso, fl_sem_cota
				FROM 	cotas
				WHERE 	id_empresa = ".$empresa_id." 
				and 	dia_semana = (SELECT CASE 	WHEN DAYOFWEEK('".$dt_agendamento."') = 1 THEN 'DOM'          
                                                  	WHEN DAYOFWEEK('".$dt_agendamento."') = 2 THEN 'SEG' 
                                                 	WHEN DAYOFWEEK('".$dt_agendamento."') = 3 THEN 'TER' 
                                                 	WHEN DAYOFWEEK('".$dt_agendamento."') = 4 THEN 'QUA' 
                                                 	WHEN DAYOFWEEK('".$dt_agendamento."') = 5 THEN 'QUI' 
                                                	WHEN DAYOFWEEK('".$dt_agendamento."') = 6 THEN 'SEX' 
                                       				ELSE 'SAB' END as dia_semana from dual)";

		return $this->db->query($sql)->row_array();
	}

	public function verificaCotasTurnoAgendamento($empresa_id, $id_turno)
	{
		$sql = "SELECT  hr_limite_agenda, hr_limite_acesso, peso, fl_sem_cota
				FROM 	cotas_turno
				WHERE 	id_empresa = ".$empresa_id." 
				and 	id_turno = ".$id_turno;

		return $this->db->query($sql)->row_array();
	}

	public function buscaTotalAgendadoPeriodo($empresa_id, $periodo_ini)
	{
		$sql = "SELECT sum(nf.peso_liquido) as total				
				FROM 	agendamento
				INNER JOIN empresas 	ON agendamento.id_empresa  = empresas.id				
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = agendamento.id
				WHERE  agendamento.id_empresa = ".$empresa_id."
				and agendamento.dt_agenda = '".$periodo_ini."'";
				
		return $this->db->query($sql)->row_array();
	}

	public function buscaTotalAgendadoTurnoPeriodo($empresa_id, $id_turno)
	{
		$sql = "SELECT sum(nf.peso_liquido) as total				
				FROM 	agendamento
				INNER JOIN empresas 	ON agendamento.id_empresa  = empresas.id				
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = agendamento.id
				WHERE  agendamento.id_empresa = ".$empresa_id."
				and agendamento.id_turno = '".$id_turno."'";
				
		return $this->db->query($sql)->row_array();
	}

	public function verificaCfop($cfop)
	{
		$sql = "SELECT coalesce(count(*)) as total_cfop FROM cfop WHERE cfop =".$cfop;
		return $this->db->query($sql)->row_array();	
	}

	public function buscaItensAgendamento($id_agenda)
	{
		$sql = "SELECT * FROM agendamento_item WHERE id_agenda=".$id_agenda;
		return $this->db->query($sql)->result_array();	
	}

	public function verificaEntradaPatio($id_agenda)
	{
		$sql = "SELECT COALESCE(count(*),0) as total FROM acesso_patio WHERE id_agenda=".$id_agenda;
		return $this->db->query($sql)->row_array();	
	}

	/*Busca os caminhões que saíram */
	public function buscaSaidas($empresa_id,$where='and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and 	agendamento.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT  agendamento.id,
					IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'Mon','Segunda',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'tue','Terça',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'wed','Quarta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'thu','Quinta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'fri','Sexta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'sat','Sabado',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'sun','Domingo','err'))))))) as dia,
					
					empresas.razao_social,
					DATE_FORMAT(agendamento.dt_agenda, '%d/%m/%Y') AS dt_agenda,
					agendamento.placa,
					motorista.nome,
					motorista.cpf,
					motorista.fone,
					embalagem.no_embalagem,
					nf.notas as nr_nf,
					nf.peso_liquido as quant_total,
					agendamento_item.descricao,
					DATE_FORMAT(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i:%s') AS dthr_entrada,
					CASE 
						WHEN convert(date_format(acesso_patio.dthr_classific,'%d'),CHAR) > 0 THEN
							date_format(acesso_patio.dthr_classific,'%d/%m/%Y %H:%i:%s')
						ELSE ''
					END dthr_classific,
					DATE_FORMAT(acesso_patio.dthr_saida,'%d/%m/%Y %H:%i:%s') AS dthr_saida
					,turnos.nome as turno
				FROM agendamento					
				inner join empresas on agendamento.id_empresa = empresas.id 
				inner join	motorista on agendamento.id_motorista = motorista.id 
				LEFT join	agendamento_item on agendamento.id = agendamento_item.id_agenda 
				inner join embalagem on agendamento_item.id_embalagem = embalagem.id 
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = agendamento.id
				inner join	cotas on empresas.id = cotas.id_empresa 
				inner join	acesso_patio on agendamento.id = acesso_patio.id_agenda and agendamento.placa = acesso_patio.placa
				LEFT JOIN turnos 		ON agendamento.id_turno = turnos.id
				WHERE DATE_FORMAT(agendamento.dt_agenda, '%a') = IF(cotas.dia_semana = 'seg','Mon',IF(cotas.dia_semana = 'ter', 'tue', IF(cotas.dia_semana = 'qua','wed',IF(cotas.dia_semana = 'qui','thu',IF(cotas.dia_semana = 'sex','fri',IF(cotas.dia_semana = 'sab','sat',IF(cotas.dia_semana = 'dom','sun','err')))))))
					AND acesso_patio.dthr_saida is not null and CURRENT_DATE - INTERVAL 60 day  <= acesso_patio.dthr_saida 
				".$where_empresa." ".$where." 
				group by agendamento.id, acesso_patio.dthr_saida 
				ORDER BY agendamento.id DESC ";
		
		return $this->db->query($sql)->result_array();
	}

	/*Busca os totais de veiculos que sairam */
	public function buscaSaidasResumido($empresa_id,$where='and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and 	agendamento.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT
					empresas.id,
					empresas.razao_social,
				count(empresas.id) total
				FROM
					agendamento,
					empresas,
					motorista,
					(select DISTINCT id_agenda, id_embalagem from agendamento_item) agendamento_item,
					embalagem,					
					cotas,
					acesso_patio
				WHERE
					agendamento.id_empresa = empresas.id 
					AND agendamento.id = agendamento_item.id_agenda 
					AND agendamento.id_motorista = motorista.id 
					AND agendamento_item.id_embalagem = embalagem.id 					
					AND empresas.id = cotas.id_empresa 
					and agendamento.id = acesso_patio.id_agenda
					and agendamento.placa = acesso_patio.placa 
					AND DATE_FORMAT(agendamento.dt_agenda, '%a') = IF(cotas.dia_semana = 'seg','Mon',IF(cotas.dia_semana = 'ter', 'tue', IF(cotas.dia_semana = 'qua','wed',IF(cotas.dia_semana = 'qui','thu',IF(cotas.dia_semana = 'sex','fri',IF(cotas.dia_semana = 'sab','sat',IF(cotas.dia_semana = 'dom','sun','err')))))))
					AND acesso_patio.dthr_saida is not null and CURRENT_DATE - INTERVAL 60 day  <= acesso_patio.dthr_saida 
				and 1=1
				".$where_empresa." ".$where."
				group by empresas.id,
				empresas.razao_social
				ORDER BY agendamento.id DESC ";
		
		return $this->db->query($sql)->result_array();
	}

	/*Busca os caminhões que Entraram e estão aguardando saída */
	public function buscaEstacionados($empresa_id,$where='and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and 	agendamento.id_empresa = ".$empresa_id;
		}
 
		$sql = "SELECT agendamento.id,
					IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'Mon','Segunda',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'tue','Terça',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'wed','Quarta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'thu','Quinta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'fri','Sexta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'sat','Sabado',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'sun','Domingo','err'))))))) as dia,
					turnos.nome as turno,
					empresas.razao_social,
                    agendamento_item.descricao,
                    agendamento_item.nr_pedido,
                    embalagem.no_embalagem,
					nf.peso_liquido as quant_total,
                    IF(acesso_patio.fl_atende_classific = 'S','LIBERADO',IF(acesso_patio.fl_atende_classific = 'N','NÃO LIBERADO','SEM CLASSIFICAÇÃO')) AS fl_atende_classific,
					DATE_FORMAT(agendamento.dt_agenda, '%d/%m/%Y') AS dt_agenda,
					agendamento.placa,
					nf.notas as nr_nf,
					motorista.nome,
					motorista.cpf,
					motorista.fone,
					DATE_FORMAT(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i:%s') AS dthr_entrada,
					CASE 
						WHEN convert(date_format(acesso_patio.dthr_classific,'%d'),CHAR) > 0 THEN
							date_format(acesso_patio.dthr_classific,'%d/%m/%Y %H:%i:%s')
						ELSE ''
					END dthr_classific,
					DATE_FORMAT(acesso_patio.dthr_saida,'%d/%m/%Y %H:%i:%s') AS dthr_saida
				FROM
					agendamento
                INNER JOIN 	empresas 			on 	empresas.id = agendamento.id_empresa    
				INNER JOIN 	motorista 			on 	motorista.id = agendamento.id_motorista
				LEFT JOIN turnos				on  turnos.id	= agendamento.id_turno
				LEFT JOIN 	agendamento_item 	on 	agendamento_item.id_agenda = agendamento.id
				LEFT JOIN 	embalagem 			on 	embalagem.id = agendamento_item.id_embalagem
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = agendamento.id
				INNER JOIN 	acesso_patio 		on 	acesso_patio.id_agenda = agendamento.id
				WHERE 		acesso_patio.dthr_saida is null 			
                ".$where_empresa." ".$where."
                group by agendamento.id
				ORDER BY agendamento.id DESC";
		 
		return $this->db->query($sql)->result_array();
	}

	/*Busca os totais resumido dos caminhões que Entraram e estão aguardando saída */
	public function buscaEstacionadosResumido($empresa_id,$where='and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and 	agendamento.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT empresas.id,
				empresas.razao_social,
				count(empresas.id) total
				FROM agendamento
                INNER JOIN 	empresas 			on 	empresas.id = agendamento.id_empresa    
				INNER JOIN 	motorista 			on 	motorista.id = agendamento.id_motorista
				INNER JOIN 	acesso_patio 		on 	acesso_patio.id_agenda = agendamento.id
				WHERE 		acesso_patio.dthr_saida is null 			
                ".$where_empresa." ".$where."
				group by empresas.id,
				empresas.razao_social
				ORDER BY agendamento.id DESC";
		 
		return $this->db->query($sql)->result_array();
	}

	/*Informações da Folha de Carga de Mercadorias */
	public function buscaFolhaMercadorias($where)
	{
		
		$sql = "SELECT id,	DATE_FORMAT(dthr_atracacao,'%d/%m/%Y %H:%i') AS  dt_ini,
						DATE_FORMAT(dthr_desatracacao,'%d/%m/%Y %H:%i') AS  dt_fim,
						navio,
						nr_fundeio,
						nr_escala
				FROM transito_simplificado
				where 1 = 1  ".$where."
				order by dthr_atracacao desc";
		
		return $this->db->query($sql)->result_array();
	}

	/* Metodo utilizado para trazer as informações da impressão da Folha de MErcadorias  */
    public function getFolhaMercadoria($id){

        $sql = "select nr_fundeio, navio, nr_escala, imo, terminal.no_terminal,
						'VANZIN OPERAÇÕES PORTUÁRIAS S/A', 
						DATE_FORMAT(dthr_atracacao,'%d/%m/%Y %H:%i:%s') AS dthr_atracacao, 
						DATE_FORMAT(dthr_ini_operacao,'%d/%m/%Y %H:%i:%s') AS dthr_ini_operacao, 
						DATE_FORMAT(dthr_fim_operacao,'%d/%m/%Y %H:%i:%s') AS dthr_fim_operacao, 
						DATE_FORMAT(dthr_desatracacao,'%d/%m/%Y %H:%i:%s') AS dthr_desatracacao, 
						transito_simplificado.observacao, 
					empresas.razao_social,
						transito_simplificado_due.ncm,
						transito_simplificado_due.nr_due,
						transito_simplificado_due.ce_mercante,
						sum(transito_movimentacao.peso_aferido) peso
				from transito_simplificado,
						transito_simplificado_due,
					transito_movimentacao,
					empresas,
					terminal
				where transito_simplificado.id             = transito_simplificado_due.id_transito
				and transito_simplificado.id_terminal    = terminal.id
				and transito_simplificado_due.id_empresa = empresas.id
				and transito_simplificado_due.id_transito = transito_movimentacao.id_transito
				and transito_simplificado_due.nr_due = transito_movimentacao.nr_due
				and transito_simplificado.id             = ".$id."
				group by nr_fundeio, navio, nr_escala, terminal.no_terminal,
						dthr_atracacao, dthr_ini_operacao, dthr_fim_operacao,                 
						dthr_desatracacao,
						empresas.razao_social,
						transito_simplificado_due.ncm,
						transito_simplificado_due.nr_due,
						transito_simplificado_due.ce_mercante
				";
          
        $query = $this->db->query($sql);

        return $query->result();
    }

	public function listarAgendamentosCliente($empresa_id,$filtros=' and 1=1')
	{
		$sql = "SELECT distinct	agendamento.id,
				agendamento.id_tipo_operacao,
				empresas.razao_social,
				date_format(agendamento.dt_agenda,'%d/%m/%Y') as dt_agenda,
				agendamento.placa,
				motorista.nome,
				motorista.cpf,
				motorista.fone,
				embalagem.no_embalagem,
				nf.notas as nr_nf,
				nf.peso_liquido as quant_total,
				agendamento_item.descricao
				FROM 	agendamento
				INNER JOIN empresas 	ON agendamento.id_empresa  = empresas.id
				INNER JOIN motorista 	ON agendamento.id_motorista = motorista.id
				LEFT JOIN agendamento_item ON agendamento.id = agendamento_item.id_agenda
				LEFT JOIN embalagem 	ON agendamento_item.id_embalagem = embalagem.id
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = agendamento.id
				WHERE agendamento.id_empresa = ".$empresa_id." ".$filtros."
				and agendamento.id not in (select id_agenda from acesso_patio)
				GROUP BY agendamento.id
				ORDER BY agendamento.id DESC";
		return $this->db->query($sql)->result_array();
	}

	public function buscaAgendamentosDoDiaGeral($empresa_id,$tipo_acesso)
	{
		$where_empresa = '';
		if( $empresa_id != '' && $tipo_acesso != 'administrador empresas'){
			$where_empresa = " and 	agendamento.id_empresa = ".$empresa_id;
		}

		// se for administrador de empresas, busca apenas clientes vinculados
		if( $tipo_acesso == 'administrador empresas'){
			$where_empresa = " and 	agendamento.id_empresa in (select cliente_id from empresas_clientes where administrador_id = ".$empresa_id.")" ;
		}

		$sql = "SELECT	agendamento.id,
						agendamento.id_tipo_operacao,
	          			empresas.razao_social,
	          			date_format(agendamento.dt_agenda,'%d/%m/%Y') as dt_agenda,
						agendamento.placa,
						motorista.nome,
						motorista.cpf,
						motorista.fone,
						embalagem.no_embalagem,
						group_concat(nota_fiscal.nr_nf) as nr_nf,
						nota_fiscal.quant_total,
						agendamento_item.descricao,
						f.descricao as frete,
						CASE
                            WHEN a.dthr_entrada is null THEN 'A'
                            WHEN a.dthr_entrada is not null and a.id_classificadora is null THEN 'WC'
                            WHEN a.dthr_entrada is not null and a.id_classificadora is not null and a.dthr_saida is null THEN 'WE'
                            ELSE 'O'
                        END as status_agendamento,
						agendamento.cliente_final,
						agendamento.motorista,
						agendamento.id_turno
				FROM 	agendamento
				INNER JOIN empresas ON agendamento.id_empresa  = empresas.id
				INNER JOIN motorista ON agendamento.id_motorista = motorista.id
				LEFT JOIN agendamento_item ON agendamento.id = agendamento_item.id_agenda
				LEFT JOIN embalagem ON agendamento_item.id_embalagem = embalagem.id
				LEFT JOIN nota_fiscal ON agendamento.id = nota_fiscal.id_agenda
				LEFT JOIN frete f on f.id = agendamento.id_frete
				LEFT JOIN acesso_patio a ON a.id_agenda = agendamento.id
				where 1=1 
				".$where_empresa."
				and 	(agendamento.dt_agenda = '".date('Y-m-d')."' or agendamento.id_turno is not null)					
				GROUP BY agendamento.id 
				ORDER BY agendamento.dt_agenda desc, agendamento.id DESC ";
		
		return $this->db->query($sql)->result_array();
	}

	public function buscaAgendamentosGeral($empresa_id,$where=' and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and	agendamento.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT	agendamento.id,
				agendamento.id_tipo_operacao,
				empresas.razao_social,
				date_format(agendamento.dt_agenda,'%d/%m/%Y') as dt_agenda,
				agendamento.placa,
				motorista.nome,
				motorista.cpf,
				motorista.fone,
				embalagem.no_embalagem,
				nf.notas as nr_nf,
				nf.peso_liquido as quant_total,
				agendamento_item.descricao,
				f.descricao as frete,
				CASE
                    WHEN a.dthr_entrada is null THEN 'A'
                    WHEN a.dthr_entrada is not null and a.id_classificadora is null THEN 'WC'
                    WHEN a.dthr_entrada is not null and a.id_classificadora is not null and a.dthr_saida is null THEN 'WE'
                    ELSE 'O'
                END as status_agendamento 
				FROM 	agendamento
				INNER JOIN empresas 		ON agendamento.id_empresa  = empresas.id
				INNER JOIN motorista 		ON agendamento.id_motorista = motorista.id
				LEFT JOIN agendamento_item 	ON agendamento.id = agendamento_item.id_agenda
				LEFT JOIN embalagem 		ON agendamento_item.id_embalagem = embalagem.id
				LEFT JOIN frete f 			ON f.id = agendamento.id_frete
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = agendamento.id
				LEFT JOIN acesso_patio a ON a.id_agenda = agendamento.id
				WHERE 1=1  								
				".$where_empresa." ".$where."				
				GROUP BY agendamento.id
				ORDER BY agendamento.id DESC ";
		
		return $this->db->query($sql)->result_array();
	}

	public function verificaAcesso($id_agenda)
	{
		$sql = "SELECT COALESCE(count(*),0) as total from acesso_patio WHERE id_agenda =".$id_agenda;
		$retorno = $this->db->query($sql)->row_array();
		return $retorno['total'];
	}

	public function buscaAgendamentosCotas($empresa_id = "",$dt_agendamento)
	{

		if($empresa_id != ''){
			$empresa_id = " and c.id_empresa =".$empresa_id;
		}

		$sql="SELECT  	c.hr_limite_agenda, 
						c.hr_limite_acesso, 
						if((a.id_turno is null),c.peso, t.peso ) as peso , 
						c.fl_sem_cota, 
						concat(e.cnpj,'-', e.razao_social) as empresa, 
						sum(nf.peso_liquido) as agendado, 
						if((a.id_turno is null),c.peso, t.peso ) - SUM(nf.peso_liquido) AS saldo,
						a.dt_agenda 
				FROM 	cotas as c
                INNER JOIN empresas as e on e.id = c.id_empresa
                LEFT JOIN agendamento as a on a.id_empresa = c.id_empresa
                LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = a.id
				LEFT JOIN cotas_turno t ON t.id_empresa = a.id_empresa
				WHERE  if((a.id_turno is null), dia_semana = (SELECT CASE 	WHEN DAYOFWEEK('".$dt_agendamento."') = 1 THEN 'DOM'
                                                  	WHEN DAYOFWEEK('".$dt_agendamento."') = 2 THEN 'SEG'
                                                 	WHEN DAYOFWEEK('".$dt_agendamento."') = 3 THEN 'TER'
                                                 	WHEN DAYOFWEEK('".$dt_agendamento."') = 4 THEN 'QUA'
                                                 	WHEN DAYOFWEEK('".$dt_agendamento."') = 5 THEN 'QUI'
                                                	WHEN DAYOFWEEK('".$dt_agendamento."') = 6 THEN 'SEX'
                                       				ELSE 'SAB' END as dia_semana from dual)
					, a.id_turno is not null)									
                      and a.dt_agenda = '".$dt_agendamento."'
                      ".$empresa_id."
               GROUP by c.id_empresa";
        return $this->db->query($sql)->result_array();
	}

	public function buscaAgendamentosPendentes()
	{
		$sql = "SELECT agendamento.id,
				       agendamento.dt_agenda,
				       empresas.razao_social,
				       agendamento.placa,
				       motorista.nome,
				       empresas.email
				  FROM agendamento,
				       empresas,
				       motorista
				 where agendamento.id_empresa = empresas.id
				   and agendamento.id_motorista = motorista.id
				   and agendamento.dt_agenda < date(now())
				   and COALESCE(fl_email_enviado, 0) = 0
				   and not exists (select 'x' from acesso_patio where acesso_patio.id_agenda = agendamento.id)";
		
		return $this->db->query($sql)->result_array();

	}

	public function verificaVeiculo($placa)
	{
		$sql = "SELECT COALESCE(count(*),0) as total FROM veiculos_bloqueados WHERE placa = '".$placa."'";
		return $this->db->query($sql)->row_array();
	}

	public function buscaFretes()
	{
		$sql = "SELECT * FROM frete";
		return $this->db->query($sql)->result_array();
	}

	public function verificaFrete($frete)
	{
		$sql = "SELECT count(id) total, id FROM frete where descricao='".$frete."' group by id";
		return $this->db->query($sql)->result();
	}

	public function insereApiCredenciamentoVeiculo($id){
		
		$sql = "INSERT api_credenciamento_veiculo (id_evento,
						origem,
						tp_operacao,
						cpf_usuario, 
							dthr_ocorrencia,
						dthr_registro,
						fl_contingencia,
						placa,    
						marca, 
					modelo, 
					ano,
					dt_validade_ini,
					dt_validade_fim,					
					id_acesso)
					SELECT 'VANZIN-CRED_VEICULO-".$id."',
						concat('AGENDAMENTO-', agendamento.id),
						'I',
						usuarios.cpf,
							now(),
							now(),
							'N',
							placa,
							null, 
							null, 
							null,
							agendamento.dt_agenda,							
							agendamento.dt_agenda,							
							agendamento.id        
					FROM agendamento,
						usuarios
					WHERE agendamento.id_usuario = usuarios.id
					and agendamento.id =  ".$id;
		if( $this->db->query($sql) ){
			return $this->db->insert_id();
		}else{
			return false;
		}
	
	}
	
	public function buscaDadosCredencimantoEnvioApi($id){
		$sql = "SELECT * FROM api_credenciamento_veiculo
				WHERE api_credenciamento_veiculo.id_acesso = ".$id;
		return $this->db->query($sql)->row_array();
	}
	
	public function atualizaApiCredencimantoVeiculos($dados){
		$this->db->where('id', $dados['id']);

        if($this->db->update('api_credenciamento_veiculo', $dados)){
            return true;

        }else{
            return false;
        }
	}
	
	public function insereApiCredenciamentoPessoas($id){
		
		$sql = "INSERT api_credenciamento_pessoa (id_evento,
									origem,
								   tp_operacao,
								   cpf_usuario, 
									dthr_ocorrencia,
								   dthr_registro,
								   fl_contingencia,
								   nome,
								   cpf,
								dt_validade_ini,
								dt_validade_fim,
								motivo,
								fl_envio,
								id_agenda,
								cnh)
				SELECT 'VANZIN-CRED_PESSOA-".$id."',
						concat('AGENDAMENTO-', agendamento.id),
					   'I',
					   usuarios.cpf,
						now(),
						now(),
						'N',
						motorista.nome,
						motorista.cpf,        
						agendamento.dt_agenda,
						null,
						'ACESSO AO RECINTO',
						'N',
						".$id.",
						motorista.nr_cnh
				FROM agendamento,
					 motorista,
					 usuarios
				WHERE agendamento.id_usuario = usuarios.id
				 and agendamento.id_motorista = motorista.id
				  and agendamento.id =   ".$id;
				  
		if( $this->db->query($sql) ){
			return $this->db->insert_id();
		}else{
			return false;
		}
	
	}
	
	public function buscaDadosCredencimantoPessoasEnvioApi($id){
		$sql = "SELECT * FROM api_credenciamento_pessoa 
				WHERE id_agenda = ".$id;
		return $this->db->query($sql)->row_array();
	}
	
	public function atualizaApiCredencimantoPessoas($dados){
		$this->db->where('id', $dados['id']);

        if($this->db->update('api_credenciamento_pessoa', $dados)){
            return true;

        }else{
            return false;
        }
	}
	
	public function verificaEnvioCredenciamento($tabela, $id){
		$sql_pessoa = 'SELECT cnh FROM agendamento a INNER JOIN motorista m ON a.id_motorista = m.id WHERE a.id = '.$id;
		$sql_veiculo = 'SELECT placa FROM agendamento a WHERE a.id = '.$id;
		$where = ($tabela == 'api_credenciamento_pessoa') ? ' cnh in ('.$sql_pessoa.')' : ' placa in ('.$sql_veiculo.')'; 
		$sql = "SELECT COALESCE(count(*), 0) total FROM ".$tabela." where ".$where;
		return $this->db->query($sql)->row_array();
		
	}

	
}
