<?php

class AcessoModel extends CI_Model {

	public function add ($data) {

	    $this->db->insert('acesso_patio', $data);
		return $this->db->insert_id();
	}
	
	public function excluirCota($id, $dia){ 
        
        $this->db->where('id_empresa', $id);
		$this->db->where('dia_semana', $dia);
        if($this->db->delete('cotas') ){
            return true;
        }else{
            return false;
        }
    }


 	public function select() {
        $this->db->order_by('peso');
        return $this->db->get('cotas')->result();
    }

    public function findTipoAcesso($placa){
		
		//$placa = str_replace("-","",$placa);
		$sql =  "Select count(*) as existe from acesso_patio where placa = '".$placa."' and ((dthr_saida is null and dthr_saida_manutencao is null) or (dthr_saida is null and dthr_retorno_manutencao is not null))";
		$query = $this->db->query($sql);
        return $query->result();
    }
//necessário pra ver se existe algum veículo no patio pra retorno 
	public function VerificaRetornoAcesso($placa){
		
		//$placa = str_replace("-","",$placa);
		$sql =  "Select count(*) as existe, id from acesso_patio where placa = '".$placa."' and dthr_saida is null and dthr_saida_manutencao is not null and dthr_retorno_manutencao is null group by id";
		$query = $this->db->query($sql);
        return $query->result();
    }

/*Catptura as informações de entrada do veiculo*/
    public function getAcesso($placa,$imprime,$id)
     {
		$placa = str_replace("_","",$placa);
        
		if($imprime == 1){
			$sql =  "select agendamento.id,
					agendamento_item.nr_pedido,
					empresas.razao_social,
					motorista.id as motorista_id,
					motorista.nome,
					agendamento_item.descricao as no_produto,
					DATE_FORMAT(acesso_patio.dthr_retorno_manutencao,'%d/%m/%Y %H:%i:%s') AS dthr_retorno_manutencao,
					DATE_FORMAT(agendamento.dt_agenda,'%d/%m/%Y') as dt_agenda,
					agendamento.placa,
					DATE_FORMAT(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i:%s') AS dthr_entrada,
					motorista.nr_cnh,
					motorista.cpf,
					date_format(acesso_patio.dthr_saida_manutencao,'%d/%m/%Y %H:%i') as dthr_saida_manutencao,
					acesso_patio.id as id_acesso,
					nota_fiscal.nr_nf,
                    nota_fiscal.quant_total,
                    embalagem.no_embalagem
					from agendamento inner join empresas on agendamento.id_empresa  = empresas.id
					inner join motorista on agendamento.id_motorista = motorista.id 
					left join agendamento_item	 on agendamento.id = agendamento_item.id_agenda
					inner  join acesso_patio on acesso_patio.id_agenda = agendamento.id
					left join nota_fiscal on nota_fiscal.id_agenda = agendamento.id
                    left join embalagem on embalagem.id = agendamento_item.id_embalagem ";
			if($id == null or $id == 'null'){
				$sql .=" where agendamento.placa = '".$placa."'";
			}else{
				$sql .= " WHERE	acesso_patio.id = ".$id;
			}
			$sql .=" and acesso_patio.dthr_saida is null
					order by agendamento.id desc limit 1"; 
		}else{
			$sql =	"select agendamento.id, agendamento.id_tipo_operacao, tipo_operacao.descricao,
					 agendamento.id_terminal, 
					 agendamento.observacao, 
					 agendamento.id_usuario,
					agendamento_item.nr_pedido,
					agendamento_item.peso,
					empresas.razao_social,
					motorista.id as motorista_id,
					motorista.nome,
					agendamento_item.id_agenda as id_agenda_item,
					agendamento_item.descricao as no_produto,
					DATE_FORMAT(agendamento.dt_agenda,'%d/%m/%Y') as dt_agenda,
					motorista.cpf,
					motorista.nr_cnh,
					acesso_patio.id as id_acesso,
                    nota_fiscal.nr_nf,
                    nota_fiscal.quant_total,
                    embalagem.no_embalagem,
					date_format(acesso_patio.dthr_saida_manutencao,'%d/%m/%Y %H:%i') as dthr_saida_manutencao,
					date_format(acesso_patio.dthr_retorno_manutencao,'%d/%m/%Y %H:%i') as dthr_retorno_manutencao,
					agendamento.cliente_final,
					agendamento.transportadora,
					agendamento.motorista
					from agendamento inner join empresas on agendamento.id_empresa  = empresas.id
					inner join motorista on agendamento.id_motorista = motorista.id 
					left join agendamento_item	 on agendamento.id = agendamento_item.id_agenda
					inner join tipo_operacao on tipo_operacao.id = agendamento.id_tipo_operacao
					left  join acesso_patio on acesso_patio.id_agenda = agendamento.id
					left join nota_fiscal on nota_fiscal.id_agenda = agendamento.id
                    left join embalagem on embalagem.id = agendamento_item.id_embalagem
					where agendamento.placa = '".$placa."' AND agendamento.dt_agenda = CURRENT_DATE() ";
					if($id != null){
						$sql .= " and	acesso_patio.id = ".$id;
					}
					$sql .=" AND((acesso_patio.id_agenda IS NULL AND acesso_patio.dthr_saida_manutencao IS NULL) 
						OR(acesso_patio.dthr_retorno_manutencao IS NULL AND acesso_patio.dthr_saida_manutencao IS NOT NULL
						)
					) order by agendamento.id desc
					";
		} 
        //echo $sql; die;	
        $query = $this->db->query($sql);
        return $query->result();   
    }
	/*Catptura as informações de saída do veiculo*/
	public function getSaida($placa,$imprime,$id)
    {
		$placa = str_replace("_","",$placa);
        $sql =  "SELECT DISTINCT acesso_patio.id as id_acesso,
					agendamento.id,
					empresas.razao_social,
					motorista.id as id_motorista,
					motorista.nome,
					motorista.nr_cnh,
					motorista.cpf,
					agendamento_item.descricao as no_produto,
					acesso_patio.id_classificadora,
					b.razao_social as classificadora,
					agendamento.id_terminal,
					terminal.no_terminal,
					date_format(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i') as dthr_entrada,
					acesso_patio.placa,
					date_format(acesso_patio.dthr_saida ,'%d/%m/%Y %H:%i') as dthr_saida,
					nota_fiscal.quant_total,
					embalagem.no_embalagem,
					acesso_patio.fl_atende_classific,
					acesso_patio.dthr_saida_manutencao,
					date_format(acesso_patio.dthr_saida_manutencao,'%d/%m/%Y %H:%i') as dthr_saida_manutencao,
					date_format(acesso_patio.dthr_retorno_manutencao,'%d/%m/%Y %H:%i') as dthr_retorno_manutencao,
					agendamento.id_tipo_operacao
				FROM                    
					acesso_patio
					left join empresas b on acesso_patio.id_classificadora  = b.id  
                    inner join agendamento on acesso_patio.id_agenda = agendamento.id
					left join terminal on acesso_patio.id_terminal = terminal.id
                    inner join empresas on empresas.id = agendamento.id_empresa
					inner join motorista on agendamento.id_motorista = motorista.id 
                    inner join agendamento_item on agendamento.id = agendamento_item.id_agenda  
					left join nota_fiscal on nota_fiscal.id_agenda = agendamento.id  
					LEFT join embalagem on embalagem.id = agendamento_item.id_embalagem";
		if($id == null or $id == 'null'){
			$sql .=	" WHERE	agendamento.placa = '".$placa."'"; 
		}else{
			$sql .=	" WHERE	acesso_patio.id = ".$id; 
		}			
		if($placa <> 'semplaca'){//Indica que é uma reimpressao se vier SEMPLACA			
			if($imprime == 1 ){
				$sql .=	"	and acesso_patio.dthr_saida is not null ";
			}else{
				$sql .=	"	and acesso_patio.dthr_saida is  null ";
			}
		}
		$sql .=	"	ORDER BY
					agendamento.id
				DESC ";   
		 
        $query = $this->db->query($sql);
        return $query->result();   
    }
	/*
	Função pra retornar a data e hora exata do banco
	*/
	public function dataHoraExata(){
		$sql = 'SELECT CURRENT_TIMESTAMP  FROM dual';
		$query = $this->db->query($sql);
		$resultado =  $query->result();

		return $resultado[0]->CURRENT_TIMESTAMP;
	}

	 public function atualizaAcesso($dados){
		

        $this->db->where('id', $dados['id']);

        if($this->db->update('acesso_patio', $dados)){
            return true;

        }else{
            return false;
        }
	}
	
	public function ticketAcesso($placa){

		$resultado = $this->getSaida($placa);

		return $resultado;
	}

	//Verificar o horário de acesso do caminhão
	public function verificaHorarioAcesso($placa){
		$sql = "SELECT
					c.hr_limite_acesso, CURTIME() hora_atual, a.id_turno
				FROM
					agendamento a,
					cotas c
				WHERE
					a.id_empresa = c.id_empresa and a.placa = '".$placa."' and a.id_terminal <> 5  
					and a.dt_agenda = CURRENT_DATE()
					AND c.dia_semana = IF(
						WEEKDAY(CURDATE()) = 0,
						'seg',
						IF(
							WEEKDAY(CURDATE()) = 1,
							'ter',
							IF(
								WEEKDAY(CURDATE()) = 2,
								'qua',
								IF(
									WEEKDAY(CURDATE()) = 3,
									'qui',
									IF(
										WEEKDAY(CURDATE()) = 4,
										'sex',
										IF(
											WEEKDAY(CURDATE()) = 5,
											'sab',
											IF(WEEKDAY(CURDATE()) = 6,
											'dom',
											'')))
										))
									))order by hr_limite_acesso desc  LIMIT 1";
		
		$query = $this->db->query($sql);
		return $query->result();  
	}

	//Verificar o horário de acesso do caminhão pelo Turno
	public function verificaHorarioTurnoAcesso($placa){
		$sql = "SELECT
					c.hr_limite_acesso, CURTIME() hora_atual, a.id_turno
				FROM
					agendamento a, cotas_turno c
				WHERE
					a.id_empresa = c.id_empresa 
					and a.id_turno = c.id_turno and a.placa = '".$placa."' and a.id_terminal <> 5  
					and a.dt_agenda = CURRENT_DATE()
					";

		$query = $this->db->query($sql);
		return $query->result();  
	}
 
    public function getEmail($email)
    {

        $sql =  "select count(*) as total from usuarios WHERE email='".$email."'";        
        $query = $this->db->query($sql);
        return $query->result();   
    }

    //Busca todos caminhões no pátio q foram agendados cheios
    public function buscaCaminhoesPatio($empresa_id='',$tipo='')
    {
    	$where = ' 1=1';

    	if($empresa_id!='' && $tipo == 1){
    		$where = ' e.id in ( SELECT cliente_id FROM classificadora_clientes WHERE classificadora_id = '.$empresa_id.')';
    	}

    	$sql = "SELECT 	a.id, a.placa, date_format(a.dthr_entrada,'%d/%m/%Y %H:%i:%s') as dthr_entrada, m.nome as motorista, m.fone, COALESCE(e.fantasia,e.razao_social) as cliente, a.id_agenda
    			FROM 	acesso_patio a, motorista m, agendamento ag, empresas e
    			WHERE 	a.id_agenda = ag.id and 
    					a.id_motorista_entrada = m.id and 
    					ag.id_empresa = e.id and    					
    					a.id_classificadora is null and 
    					ag.id_tipo_operacao = ".$tipo." and
    					".$where."
    					ORDER BY a.dthr_entrada ASC";

    	return $this->db->query($sql)->result_array();
    }

    public function buscaCaminhoesClassificadosHoje($usuario_id='', $tipo='')
    {
    	$where = ' 1=1';
    	if($usuario_id!=''){
    		$where = ' a.id_usuario_classific = '.$usuario_id;
    	}
    	$sql = "SELECT 	a.id, a.placa, date_format(a.dthr_entrada,'%d/%m/%Y %H:%i:%s') as dthr_entrada, m.nome as motorista, m.fone, COALESCE(e.fantasia,e.razao_social) as cliente, a.fl_atende_classific, a.id_agenda, t.no_terminal
    			FROM 	acesso_patio a
                inner join motorista m on a.id_motorista_entrada = m.id
                inner join agendamento ag on a.id_agenda = ag.id 
                inner join empresas e on ag.id_empresa = e.id  
                left join terminal t on 	t.id = a.id_terminal
    			WHERE 	a.fl_atende_classific is not null and 
    					date(a.dthr_classific) = '".date('Y-m-d')."' and 
    					ag.id_tipo_operacao = ".$tipo." and 
    					".$where."
    					ORDER BY a.id DESC";

    	return $this->db->query($sql)->result_array();
    }
	//Utilizada na reimpressão de ticket
	public function buscaAcessoGeral($empresa_logada){
		$sql = "select acesso_patio.id, acesso_patio.placa, 
				date_format(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i') as dthr_entrada, 
				agendamento_item.descricao, date_format(acesso_patio.dthr_saida,'%d/%m/%Y %H:%i') as dthr_saida,
				date_format(acesso_patio.dthr_saida_manutencao,'%d/%m/%Y %H:%i') as dthr_saida_manutencao,
				date_format(acesso_patio.dthr_retorno_manutencao,'%d/%m/%Y %H:%i') as dthr_retorno_manutencao 
				from acesso_patio 
				inner join agendamento on acesso_patio.id_agenda = agendamento.id
				inner join agendamento_item on agendamento_item.id = agendamento.id
				inner join empresas on empresas.id = agendamento.id_empresa 
				where CURRENT_DATE - INTERVAL 60 day  <= acesso_patio.dthr_saida
				";
		$sql.=" order by 1,3,2,4,5 desc";

		$query = $this->db->query($sql);
        return $query->result();   
	}
    
	public function buscaAcessoPlaca($placa){ 
		
		$sql =  "SELECT count(*) as total FROM acesso_patio WHERE placa = '".$placa."'";
		return $this->db->query($sql)->row_array();
        
    }

    public function atualizaRecepcaoCarga($where)
	{

		$sql1 = "SELECT max(id) as id FROM acesso_patio WHERE placa = '".$where['placa']."'";
		$acesso_id = $this->db->query($sql1)->row_array();

		$sql = "UPDATE acesso_patio SET peso_aferido = ".$where['peso_aferido'].", dthr_pesagem = '".$where['dthr_pesagem']."', peso_tara = '".$where['peso_tara']."', peso_bruto = '".$where['peso_bruto']."'
				WHERE id in (".$acesso_id['id'].")";
		if( $this->db->query($sql) ){
			return true;
		}else{
			return false;
		}
	}
	
	public function insereApiControleAcesso($id, $tipo){
		$sql = "INSERT api_controle_acesso_pessoa (id_evento,
    				origem,
  				   tp_operacao,
   				   cpf_usuario, 
   			       dthr_ocorrencia,
   				   dthr_registro,
   				   fl_contingencia,
                   direcao,
                   nome,
					id_acesso)
				SELECT 'VANZIN-ACESSO_PESSOA-".$id."',
					concat('ACESSO_PATIO-', acesso_patio.id),
					   'I',
					   usuarios.cpf,
						now(),
						now(),
						'N',
						'".$tipo."',
						upper(motorista.nome),
						".$id." 
				FROM acesso_patio,
					 usuarios,
					 motorista
				WHERE acesso_patio.id_usuario_entrada = usuarios.id
				  AND acesso_patio.id_motorista_entrada = motorista.id
				  and acesso_patio.id = ".$id;
		if( $this->db->query($sql) ){
			return $this->db->insert_id();
		}else{
			return false;
		}
	
	}
	
	public function buscaDadosEnvioApi($id){
		$sql = "SELECT * FROM api_controle_acesso_pessoa
				WHERE api_controle_acesso_pessoa.id_acesso = ".$id;
		return $this->db->query($sql)->row_array();
	}
	
	public function atualizaApiAcesso($dados){
		$this->db->where('id', $dados['id']);

        if($this->db->update('api_controle_acesso_pessoa', $dados)){
            return true;

        }else{
            return false;
        }
	}
	
	public function insereApiControleAcessoVeiculos($id, $tipo){
		$sql = "INSERT api_controle_acesso_veiculo (id_evento,
    				origem,
  				   tp_operacao,
   				   cpf_usuario, 
   			       dthr_ocorrencia,
   				   dthr_registro,
   				   fl_contingencia,
                   direcao,
   				   placa,
                   nome_motorista,
                   cpf_motorista,
				   id_acesso )
SELECT 'VANZIN-ACESSO_VEICULO-".$id."',
	concat('ACESSO_PATIO-', acesso_patio.id),
       'I',
       usuarios.cpf,
        now(),
        now(),
        'N',
        '".$tipo."',
        placa ,
        motorista.nome,
        motorista.cpf,
		acesso_patio.id
FROM acesso_patio,
	 usuarios,
     motorista
WHERE acesso_patio.id_usuario_entrada = usuarios.id
  AND acesso_patio.id_motorista_entrada = motorista.id
  and acesso_patio.id = ".$id;
  
		if( $this->db->query($sql) ){
			return $this->db->insert_id();
		}else{
			return false;
		}
	
	}
	
	public function buscaDadosEnvioApiVeiculos($id){
		$sql = "SELECT * FROM api_controle_acesso_veiculo 
				WHERE api_controle_acesso_veiculo .id_acesso = ".$id;
		return $this->db->query($sql)->row_array();
	}
	
	public function atualizaApiAcessoVeiculos($dados){
		$this->db->where('id', $dados['id']);

        if($this->db->update('api_controle_acesso_veiculo', $dados)){
            return true;

        }else{
            return false;
        }
	}
	
	public function insereApiControleAcessoPesagem($id, $tipo){
		$sql = "INSERT api_pesagem_veiculo_carga (id_evento,
    				origem, 
  				   tp_operacao,
   				   cpf_usuario, 
   			       dthr_ocorrencia,
   				   dthr_registro,
   				   fl_contingencia,
                   fl_capturaautopeso,
                   fl_vazio,
                   tara_conjunto,
                   peso_bruto,
                   placa,
				   id_acesso
				)
				SELECT 'VANZIN-PESAGEM-".$id."',
					concat('ACESSO_PATIO-', acesso_patio.id),
					   'I',
						'00000000000',
						dthr_pesagem,
						dthr_pesagem,
						'N',
						'S',
						'N',
						peso_tara,
						peso_bruto,
						placa
				FROM acesso_patio
				WHERE id = ".$id;
  
		if( $this->db->query($sql) ){
			return $this->db->insert_id();
		}else{
			return false;
		}
	
	}
	
	public function buscaDadosEnvioApiPesagem($id){
		$sql = "SELECT * FROM api_pesagem_veiculo_carga  
				WHERE api_pesagem_veiculo_carga.id_acesso = ".$id;
		return $this->db->query($sql)->row_array();
	}
	
	public function atualizaApiAcessoPesagem($dados){
		$this->db->where('id', $dados['id']);

        if($this->db->update('api_pesagem_veiculo_carga ', $dados)){
            return true;

        }else{
            return false;
        }
	}

}
?>