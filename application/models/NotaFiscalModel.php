<?php

class NotaFiscalModel extends CI_Model {

	public function insere($nota)
	{
        $this->db->insert('nota_fiscal', $nota);
		return $this->db->insert_id();
    }

    public function insereItem($item)
	{
        $this->db->insert('nota_fiscal_item', $item);
		return $this->db->insert_id();
    }

    public function excluir($id_agenda){
		$this->db->where('id_agenda', $id_agenda);
        if(	$this->db->delete('nota_fiscal') ){
            return true;
        }else{
            return false;
        }
	}

    public function buscaNotasCCT($where=" and 1=1")
    {
        $sql = "SELECT  distinct nr_nf, serie_nf, 
                        date_format(dthr_envio,'%d/%m/%Y %H:%i:%s') as dthr_envio,
                        cd_retorno,
                        descricao,
                        log_siscomex.chave_acesso
                FROM    log_siscomex,
                        nota_fiscal,
                        agendamento
                WHERE   log_siscomex.chave_acesso   =   nota_fiscal.chave_acesso
                AND     agendamento.id              =   nota_fiscal.id_agenda
                AND     log_siscomex.operacao       =   'R' 
                ".$where ;

        return $this->db->query($sql)->result_array();
    }

    public function buscaEntregaCargasDUE($where=" and 1=1")
    {
        $sql = "SELECT  nr_due,
                        date_format(dthr_envio,'%d/%m/%Y %H:%i:%s') as dthr_envio,
                        placa,
                        cpf_motorista,
                        peso,
                        cd_retorno,
                        descricao
                FROM    log_siscomex
                WHERE   operacao = 'E' ".$where;
		
        return $this->db->query($sql)->result_array();
    }

	
}
