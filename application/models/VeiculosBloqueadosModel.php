<?php

class VeiculosBloqueadosModel extends CI_Model {

	public function insere($veiculos_bloqueados)
	{
        $this->db->insert('veiculos_bloqueados', $veiculos_bloqueados);
		return $this->db->insert_id();
    }
   
	
    public function atualiza($dados){

        $this->db->where('id', $dados['id']);
        if($this->db->update('veiculos_bloqueados', $dados)){
            return true;
        }else{
            return false;
        }

    }    

    public function excluir($id){
		$this->db->where('id', $id);
        if(	$this->db->delete('veiculos_bloqueados') ){
            return true;
        }else{
            return false;
        }
	}
	
	public function buscaVeiculosBloqueados()
	{
		$sql = "SELECT a.* FROM veiculos_bloqueados a ";
		return $this->db->query($sql)->result_array();
	}	
	
	public function buscaVeiculoBloqueado($id)
	{
		$sql = "SELECT * FROM veiculos_bloqueados WHERE id = ". $id;
		return $this->db->query($sql)->row_array();
	}

	public function insereApiBloqueioVeiculo($id, $tipo_operacao ){
		$status = ($tipo_operacao=='I') ? 'B' : 'D';
		$sql = "INSERT api_bloqueio_veiculo (id_evento,
                             origem,
                             tp_operacao,
                             cpf_usuario, 
                             dthr_ocorrencia,
                             dthr_registro,
                             fl_contingencia,
                             status,
						     placa,
					         setor_solicitante,
						     motivo,
						     id_bloq)
				SELECT 'VANZIN-BLOQUEIO-VEICULO-".$id."',
				    concat('VEICULOS-BLOQUEADOS-', veiculos_bloqueados.id),
				   '".$tipo_operacao."',
				    '00000000000',
				    now(),
				    now(),
				    'N',
				    '".$status."',
				    veiculos_bloqueados.placa,
				    'SETOR DE OPERAÇÕES',
				    veiculos_bloqueados.ds_motivo,
				    ".$id."
				FROM veiculos_bloqueados
				where id =   ".$id;
				  
		if( $this->db->query($sql) ){
			return $this->db->insert_id();
		}else{
			return false;
		}
	
	}
	
	public function buscaDadosApiBloqueioVeiculo($id){
		$sql = "SELECT * FROM api_bloqueio_veiculo 
				WHERE id_bloq = ".$id;
		return $this->db->query($sql)->row_array();
	}
	
	public function atualizaApiBloqueioVeiculo($dados){
		$this->db->where('id', $dados['id']);

        if($this->db->update('api_bloqueio_veiculo', $dados)){
            return true;

        }else{
            return false;
        }
	}
	
}
