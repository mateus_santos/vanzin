<?php

class CotasModel extends CI_Model {

	public function add ($data) {

	    $this->db->insert('cotas', $data);
		return $this->db->insert_id();
    }
    
    public function add_mosaic ($data) {

	    $this->db->insert('cotas_mosaic', $data);
		return $this->db->insert_id();
	}

    public function addTurno ($data) {

	   return $this->db->insert('cotas_turno', $data);
		
    }
	
	public function excluirCota($id, $dia){ 
        
        $this->db->where('id_empresa', $id);
		$this->db->where('dia_semana', $dia);
        if($this->db->delete('cotas') ){
            return true;
        }else{
            return false;
        }
    }

    public function excluirCotaTurno($id, $dia){ 
        
        $this->db->where('id_empresa', $id);
		$this->db->where('id_turno', $dia);
        if($this->db->delete('cotas_turno') ){
            return true;
        }else{
            return false;
        }
    }

    public function excluirCotaTerminal(){ 
        
        $this->db->where('1','1');
        if($this->db->delete('cotas_mosaic') ){
            return true;
        }else{
            return false;
        }
    }


 	public function select() {
        $this->db->order_by('peso');
        return $this->db->get('cotas')->result();
    }

    public function findCotas($dia, $empresa_id){
        $where = ' and 1=1';
        if($empresa_id != '' ){
            $where = ' and empresas.id in (SELECT cliente_id FROM empresas_clientes WHERE administrador_id = ".$empresa_id.")';    
        }


		if(empty($dia)){
			$sql =  "SELECT id, concat(razao_social,' - ',cnpj) as razao_social, cotas.dia_semana, cotas.hr_limite_acesso, cotas.hr_limite_agenda, cotas.peso, cotas.fl_sem_cota FROM `empresas` LEFT join cotas on empresas.id = cotas.id_empresa  where empresas.tipo_cadastro_id = 1   ". $where;	
        }else{
			 
			$sql =  "SELECT id, concat(razao_social,' - ',cnpj) as razao_social, cotas.dia_semana, cotas.hr_limite_acesso, cotas.hr_limite_agenda, cotas.peso, cotas.fl_sem_cota FROM `empresas` LEFT join cotas on empresas.id = cotas.id_empresa and cotas.dia_semana ='".$dia."'  where empresas.tipo_cadastro_id = 1  ". $where;
			$query = $this->db->query($sql);
			$total = $query->num_rows(); 
			
			if ($total < 1){
				$sql =  "SELECT id, concat(razao_social,' - ',cnpj) as razao_social, '' as dia_semana, null as hr_limite_acesso, null as hr_limite_agenda, null as peso, null as fl_sem_cota FROM `empresas` WHERE empresas.tipo_cadastro_id = 1  ". $where;	
				$query = $this->db->query($sql);
			}			
			
        }

        return $query->result();
    }

    public function getCotas()
     {

        $sql =  "SELECT id, razao_social, cotas.dia_semana, cotas.hr_limite_acesso, cotas.hr_limite_agenda, cotas.peso, cotas.fl_sem_cota FROM `empresas` LEFT join cotas on empresas.id = cotas.id_empresa ";        
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function getCotasImportadas()
     {

        $sql =  "SELECT id, razao_social, dia_semana,  peso, quantidade FROM  cotas_mosaic ";        
        $query = $this->db->query($sql);
        return $query->result();   
    }

	 
	//função responsável pelo retorno formatado do valor por dia
	public function separaCota($dia_semana,$valor){

		switch($dia_semana){
			case 'seg':
				return $valor;
			break;
			case 'ter':
				return $valor;	
			break;
			case 'qua':
				return $valor;	
			break;
			case 'qui':
				return $valor;	
			break;
			case 'sex':
				return $valor;
			break;	
			case 'sab':
				return $valor;	
			break;
			case 'dom':
				return $valor;	
			break;									
			default:
				return '';
		}
	}


    public function getEmail($email)
    {

        $sql =  "select count(*) as total from usuarios WHERE email='".$email."'";        
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function findCotasTurno($id_turno, $empresa_id){
        $where = ' and 1=1';
        if($empresa_id != '' ){
            $where = ' and empresas.id in (SELECT cliente_id FROM empresas_clientes WHERE administrador_id = ".$empresa_id.")';    
        }


		if(empty($id_turno)){
			$sql =  "SELECT id, concat(razao_social,' - ',cnpj) as razao_social, cotas_turno.id_turno, cotas_turno.hr_limite_acesso, cotas_turno.hr_limite_agenda, cotas_turno.peso, cotas_turno.fl_sem_cota FROM `empresas` LEFT join cotas_turno on empresas.id = cotas_turno.id_empresa  where empresas.tipo_cadastro_id = 1   ". $where;	
        }else{
			 
			$sql =  "SELECT id, concat(razao_social,' - ',cnpj) as razao_social, cotas_turno.id_turno, cotas_turno.hr_limite_acesso, cotas_turno.hr_limite_agenda, cotas_turno.peso, cotas_turno.fl_sem_cota FROM `empresas` LEFT join cotas_turno on empresas.id = cotas_turno.id_empresa and cotas_turno.id_turno ='".$id_turno."'  where empresas.tipo_cadastro_id = 1  ". $where;
			$query = $this->db->query($sql);
			$total = $query->num_rows(); 
			
			if ($total < 1){
				$sql =  "SELECT id, concat(razao_social,' - ',cnpj) as razao_social, null as id_turno, null as hr_limite_acesso, null as hr_limite_agenda, null as peso, null as fl_sem_cota FROM `empresas` WHERE empresas.tipo_cadastro_id = 1  ". $where;	
				$query = $this->db->query($sql);
			}			
			
        }

        return $query->result();
    }

    public function getCotasTurno()
     {

        $sql =  "SELECT id, razao_social, cotas.dia_semana, cotas.hr_limite_acesso, cotas.hr_limite_agenda, cotas.peso, cotas.fl_sem_cota FROM `empresas` LEFT join cotas on empresas.id = cotas.id_empresa ";        
        $query = $this->db->query($sql);
        return $query->result();   
    }


}
?>