<?php

class TransitoModel extends CI_Model {

    public function add ($data) {

        $this->db->insert('transito_simplificado', $data);
        return $this->db->insert_id();
    }
    //Exclui as dues do transito
    public function excluirTodasDue($id){ 
        
        $this->db->where('id_transito', $id);
        if($this->db->delete('transito_simplificado_due') ){
            return true;
        }else{
            return false;
        }
    }
    //Exclui o transito
    public function excluirTransito($id){ 
        
        $this->db->where('id', $id);
        if($this->db->delete('transito_simplificado') ){
            return true;
        }else{
            return false;
        }
    }

    public function existeMovimento($id){
        $sql ="select count(*) as existe from transito_movimentacao where id_transito = ".$id;
        $query = $this->db->query($sql);

        return $query->result();
    }


    public function getTerminais(){

        $sql =  "   SELECT t.*  FROM terminal t WHERE t.fl_ativo = 'S' and fl_local_atracacao = 1 ORDER BY t.no_terminal ASC";
                            
        return $this->db->query($sql)->result_array();
    }

    public function getTipo(){

        $sql =  "   SELECT t.*  FROM tipo_carga t  ORDER BY t.no_tipo ASC";
                            
        return $this->db->query($sql)->result_array();
    }

    public function atualizaTransito($dados){

        $this->db->where('id', $dados['id']);

        if($this->db->update('transito_simplificado', $dados)){
            return true;

        }else{
            return false;
        }
    }

    public function addDue ($data) {

        $this->db->insert('transito_simplificado_due', $data);
        return $this->db->insert_id();
    }
    
    //Pesquisa a listagem de transitos
    public function getTransito($dt_ini=null, $dt_fim=null, $navio='', $due=null, $nr_escala=null){

        $sql =  " SELECT DISTINCT t.id, t.dt_ini, t.dt_fim, t.navio, t.nr_escala  FROM transito_simplificado t LEFT JOIN transito_simplificado_due d ON (t.id = d.id_transito)";
        $sql .=  " WHERE 1 = 1 ";
        if($navio != ''){
            $sql .=  " and t.NAVIO LIKE '%".$navio."%'";
        }
        if($dt_ini != null){
            $sql .=  " and (t.dt_ini between '".$dt_ini."'  and '".$dt_fim."' ";
        }
        if($dt_fim != null){
            $sql .=  " or t.dt_fim between '".$dt_ini."'  and '".$dt_fim."' ) ";
        }
        if($due != null){
            $sql .=  " and d.due LIKE '%".$due."%'";
        }  
        if($nr_escala != null){
            $sql .=  " and t.nr_escala LIKE '%".$nr_escala."%'";
        }                      
        $sql .=  " order by t.dt_ini desc";
                            
        return $this->db->query($sql)->result_array();
    }

    public function buscaTransito($id){

        $sql =  " SELECT t.*, d.id_transito, d.nr_due, d.id_empresa, d.peso_manif, d.id_tipo, d.nr_ordem, if(d.fl_aberta ='0','ENCERRADA','ABERTA') as fl_aberta, e.razao_social  FROM transito_simplificado t LEFT JOIN transito_simplificado_due d ON (t.id = d.id_transito)  left join empresas e on d.id_empresa = e.id";
        $sql .=  " where t.id = ".$id;
        $sql .=  " order by d.nr_ordem asc";
        
        $query = $this->db->query($sql);

        return $query->result();
    }

    public function buscaDues($id){

        $sql =  " SELECT d.* FROM  transito_simplificado_due d ";
        $sql .=  " where d.id_transito = ".$id;
        $sql .=  " order by d.nr_ordem asc";
        $query = $this->db->query($sql);

        return $query->result();
    }

    public function verificaDue($due){

        $sql =  " SELECT count(d.nr_due) as total FROM  transito_simplificado_due d ";
        $sql .=  " where trim(d.nr_due) = trim('".$due."')";

        $query = $this->db->query($sql);

        return $query->result();
    }

    public function verificaPesagem($placa){

        $sql =  " SELECT COUNT(*) total FROM TRANSITO_MOVIMENTACAO WHERE placa = '".$placa."'";
        $sql .=  " AND DTHR_MOVIMENTO = CURRENT_DATE()";
        
        $query = $this->db->query($sql);

        return $query->result();
    }

    public function buscaDadosTransito($cnpj)
    {
        $cnpj_t = substr_replace($cnpj,'-',12, 0);
        $cnpj_b = substr_replace($cnpj_t,'/',8, 0);
        $cnpj_p1 = substr_replace($cnpj_b,'.',5, 0);
        $cnpj_p2 = substr_replace($cnpj_p1,'.',2, 0);
        $cnpj = $cnpj_p2;       
        $sql = "SELECT  id_transito, nr_due, peso_manif
                FROM    transito_simplificado_due,
                        empresas
               WHERE    transito_simplificado_due.id_empresa = empresas.id
                 AND    transito_simplificado_due.fl_aberta = 0
                 AND    empresas.cnpj = '".$cnpj."' ";

        return $this->db->query($sql)->row_array();
    }

<<<<<<< HEAD
/* Metodo utilizado para trazer as informsações da impressão do Extrato  */
    public function getExtrato($placa){

        $sql = "select transito_simplificado_due.nr_due,
                        transito_simplificado_due.peso_manif,
                        transito_movimentacao.dthr_movimento,
                        transito_movimentacao.placa,
                        transito_movimentacao.cpf_motorista,
                        transito_movimentacao.peso_aferido, 
                        empresas.cnpj,
                        empresas.razao_social,
                        transito_simplificado_due.id_tipo,
                        tipo_carga.no_tipo
                from transito_movimentacao, 
                        transito_simplificado_due, 
                        transito_simplificado,
                        empresas,
                        tipo_carga
                where transito_simplificado.id                  = transito_simplificado_due.id_transito
                    and transito_simplificado_due.id_transito   = transito_movimentacao.id_transito
                    and transito_simplificado_due.nr_due        = transito_movimentacao.nr_due
                    and transito_simplificado_due.id_empresa    = empresas.id
                    AND transito_simplificado_due.id_tipo       = tipo_carga.id
                    and transito_movimentacao.placa             = '".$placa."'
                    and transito_movimentacao.id   IN (select max(id) from transito_movimentacao where placa = '".$placa."')
                ";
            
        $query = $this->db->query($sql);

        return $query->result();

=======
    public function buscaPesoManifDUE($due)
    {
        $sql = "SELECT  peso_manif
                FROM    transito_simplificado_due
                WHERE   nr_due = '".$due."'";
        
        return  $this->db->query($sql)->row_array();
    }

    public function buscaPesoEntregueDUE($due)
    {
        $sql = "SELECT  sum(peso) as entregue
                FROM    log_siscomex
                WHERE   nr_due = ".$due."
                AND     cd_retorno = 'CCTR-IN0001'";

        return  $this->db->query($sql)->row_array();
    }

    public function buscaEntregaCargasDUE($where=" and 1=1")
    {
        $sql = "SELECT  nr_due,
                        dthr_envio,
                        placa,
                        cpf_motorista,
                        peso,
                        cd_retorno,
                        descricao       
                FROM    log_siscomex 
                WHERE   operacao = 'E'
                ".$where ;

        return $this->db->query($sql)->result_array();
>>>>>>> 074e22c1f45cd88cdf8743589ef5c4eb14786b56
    }

}

?>