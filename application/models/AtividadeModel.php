<?php

class AtividadeModel extends CI_Model {
	
	public function selectModelos($empresa_id)
    {
		$sql = "SELECT 	p.*,pi.qtd quantidade 
				FROM 	orcamentos o, pedidos pd, produtos p,pedido_itens pi 
				WHERE 	o.id = pd.orcamento_id and
						pi.produto_id = p.id and
						pi.pedido_id = pd.id and
						pd.orcamento_id = o.id and
						p.tipo_produto_id != 4 and
						o.empresa_id = ".$empresa_id." 
				group by p.id" ;
		
		return $this->db->query($sql)->result_array();
		
    }
	
	public function insert($atividade)
	{
        $this->db->insert('chamado_atividade', $atividade);
		return $this->db->insert_id();
    }
	
	public function find($id)
    {
		$this->db->select('cd.descricao defeito');
		$this->db->select('ca.*');
		$this->db->join('chamado_defeito cd', 'ca.defeito_id = cd.id');
		$this->db->where('ca.id', $id);
		return $this->db->get('chamado_atividade ca')->row_array();
    }
	
	public function update($atividade)
    {
    	
		$this->db->where('id', $atividade['id']);            
        return  $this->db->update('chamado_atividade', $atividade);   
    }
	
	public function select($chamado_id)
    {
        $this->db->select('cd.descricao defeito');
		$this->db->select('p.modelo modelo');
		$this->db->select('cas.descricao status');
		$this->db->select('u.nome usuario');
		$this->db->select('ca.*');
		$this->db->where('chamado_id', $chamado_id);
		$this->db->join('chamado_defeito cd', 'ca.defeito_id = cd.id');
		$this->db->join('usuarios u', 'u.id = ca.usuario_id');
		$this->db->join('produtos p', 'p.id = ca.modelo_id', 'left');
		$this->db->join('chamado_atividade_status cas', 'ca.status_id = cas.id');
		$this->db->order_by('ca.id');
		return $this->db->get('chamado_atividade ca')->result_array();
    }
	
	public function selectStatus(){
		return $this->db->get('chamado_atividade_status')->result_array();
	}

	public function verificaAnexo($atividade_id){

		$this->db->select('MAX(id) as ultimo');
		$this->db->where('atividade_id', $atividade_id);

		return $this->db->get('chamado_atividade_upload')->row_array();	
	}

	public function insertAnexo($insert){
		
		return $this->db->insert('chamado_atividade_upload', $insert);
	}

	public function listaAnexos($atividade_id){

		$this->db->select('*');
		$this->db->where('atividade_id', $atividade_id);
		return $this->db->get('chamado_atividade_upload')->result_array();
	}

	public function excluirAnexo($anexo_id){
		$this->db->where('id', $anexo_id);
        if(	$this->db->delete('chamado_atividade_upload') ){
            return true;
        }else{
            return false;
        }
	}

	public function verificaStatusAtividade($chamado_id){
		$this->db->select('count(*) as total');
		$this->db->where('chamado_id', $chamado_id);
		$this->db->where('status_id !=', '4');
		$this->db->where('status_id !=', '3');
		return $this->db->get('chamado_atividade')->row_array();
	}

	public function copiaAtividade($dados){
		$sql = "SELECT 	*
				FROM chamado_atividade
				WHERE id = ".$dados['atividade_id'];
		
		$insert = $this->db->query($sql)->row_array();
		$insert['modelo_id'] 	= $dados['modelo_id'];
		$insert['numero_serie'] = $dados['numero_serie'];
		$insert['observacao'] 	= $dados['observacao'];
		unset($insert['id']);
		$this->db->insert('chamado_atividade', $insert);
		return $this->db->insert_id();
	}

	public function copiaAtendimentos($atividade_id, $atividade_id_novo){
		$query = $this->db->query('	INSERT 	chamado_subatividade (atividade_id, inicio, fim, observacao, usuario_id,parceiro_id,contato_id)
                          			SELECT 	'.$atividade_id_novo.' as atividade_id, inicio, fim, observacao, usuario_id,parceiro_id,contato_id FROM chamado_subatividade
									WHERE 	atividade_id = '.$atividade_id);		
			
		return $this->db->insert_id();
	}

	public function copiaCausaSolucoes($atividade_id, $atividade_id_novo){

		$query = $this->db->query('	INSERT 	chamado_atividade_causa_solucao (atividade_id,causa_id,solucao_id,status)
                          			SELECT 	'.$atividade_id_novo.' as atividade_id, causa_id,solucao_id,status FROM chamado_atividade_causa_solucao
									WHERE 	atividade_id = '.$atividade_id);		
		
		return $this->db->insert_id();
		
	}

	public function copiaAnexos($atividade_id, $atividade_id_novo){
				
		$query = $this->db->query('	INSERT 	chamado_atividade_upload (atividade_id,arquivo,descricao)
                          			SELECT 	'.$atividade_id_novo.' as atividade_id,arquivo,descricao FROM chamado_atividade_upload
									WHERE 	atividade_id = '.$atividade_id);

		return $this->db->insert_id();

	}

}
?>