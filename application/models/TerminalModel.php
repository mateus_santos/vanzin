<?php

class TerminalModel extends CI_Model {

	public function insere($terminal)
	{
        $this->db->insert('terminal_fiscal', $terminal);
		return $this->db->insert_id();
    }
    
    public function excluir($id){
		$this->db->where('id', $id);
        if(	$this->db->delete('terminal') ){
            return true;
        }else{
            return false;
        }
	}

    public function buscaTerminais()
    {
        $sql = "SELECT * FROM terminal WHERE fl_ativo = 'S' order by no_terminal ASC ";
        return $this->db->query($sql)->result_array();
    }
	
    public function getTerminais()
    {
        $sql = "SELECT * FROM terminal WHERE fl_ativo = 'S' and fl_local_atracacao = 'S' order by no_terminal ASC ";
        return $this->db->query($sql)->result_array();
    }
    
}