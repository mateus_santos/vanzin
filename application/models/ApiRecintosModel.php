<?php

class ApiRecintosModel extends CI_Model {
		
	
    public function selectApiRecintos($filtros){
				
		$sql =  "SELECT a.*, ar.descricao as retorno FROM ".$filtros['evento']." as a
				INNER JOIN api_retorno ar on ar.id = a.id_retorno
				WHERE date(a.dthr_registro) between '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."'";
		
        return $this->db->query($sql)->result_array();
    }

    public function verificaQtdRegistros($filtros)
    {
    	$sql = "SELECT count(*) as total FROM ".$filtros['evento']." as a
				INNER JOIN api_retorno ar on ar.id = a.id_retorno
				WHERE date(a.dthr_registro) between '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."'";

		return $this->db->query($sql)->row_array();
    }

}
?>