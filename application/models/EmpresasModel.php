<?php

class EmpresasModel extends CI_Model {

	public function add ($data) {

        $this->db->insert('empresas', $data);
	    return $this->db->insert_id();
	}

    public function vincularCliente($data)
    {
        return $this->db->insert('classificadora_clientes', $data);
    }

    public function insereClienteAdm($data)
    {
        return $this->db->insert('empresas_clientes', $data);
    }

 	public function select() {
        
        $this->db->order_by('razao_social');
        return $this->db->get('empresas')->result_array();
    }

    public function getEmpresa($id){

        $sql =  "SELECT * FROM empresas WHERE id=".$id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getClientes(){

        $sql =  "   SELECT e.*  FROM empresas e WHERE e.tipo_cadastro_id = 1                         
                    ORDER BY e.razao_social ASC";
                            
        return $this->db->query($sql)->result_array();
    }

     public function getClientesEmpresas($empresa_id){

        $sql =  "   SELECT e.*  FROM empresas e WHERE e.tipo_cadastro_id = 1 and e.id in (SELECT cliente_id FROM empresas_clientes WHERE administrador_id = ".$empresa_id.") ORDER BY e.razao_social ASC";
                            
        return $this->db->query($sql)->result_array();
    }

    public function getTiposCadastro()
    {
        $sql = "SELECT * FROM tipo_cadastros";
        return $this->db->query($sql)->result_array();
    }

    public function getCnpj($cnpj){

        $sql =  "select * from empresas WHERE cnpj='".$cnpj."'";
        $query = $this->db->query($sql);
        return $query->result();   
    }
    //Retira a formatação do arquivo para utilizar na importação
    public function getCnpjNaoFormatado($cnpj){

        $sql =  "select * from empresas WHERE trim(replace(replace(replace(cnpj,'.',''),'-',''),'/','')) = '".trim($cnpj)."'";
        $query = $this->db->query($sql);
        return $query->result();   
    }
    
    public function getEmpresas(){

        $sql =  "SELECT e.*,t.descricao as tipo_cadastro from empresas e, tipo_cadastros t WHERE t.id= e.tipo_cadastro_id ";
        $query = $this->db->query($sql);
        return $query->result();       
    }

    public function getEmpresasPorCliente($empresa_id){

        $sql =  "SELECT e.*,t.descricao as tipo_cadastro from empresas e, tipo_cadastros t 
                WHERE   t.id= e.tipo_cadastro_id and 
                        e.id in (SELECT ec.cliente_id FROM empresas_clientes ec WHERE ec.administrador_id = ".$empresa_id.") ";

        $query = $this->db->query($sql);
        return $query->result();       
    } 

    public function getEmpresasUsuarios($id){
        
        $sql = "SELECT e.*,t.descricao as tipo_cadastro,u.nome, u.cpf, u.email as email_pessoal, u.dt_treinamento treinamento, u.tipo_cadastro_id tipo, u.id usuario_id
                FROM empresas e 
                INNER JOIN tipo_cadastros t ON t.id= e.tipo_cadastro_id  
                LEFT JOIN usuarios u ON u.empresa_id = e.id				
				WHERE e.id = ".$id;
        
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getSubtipoByTipo($tipo_cadastro_pai){
        
        $sql = "SELECT s.* FROM subtipo_cadastros s WHERE s.tipo_cadastro_pai = ".$tipo_cadastro_pai." ORDER BY s.nvl ASC";
        
        $query = $this->db->query($sql);
        return $query->result_array(); 
    }
    
    public function atualizaEmpresas($dados){

        $this->db->where('id', $dados['id']);

        if($this->db->update('empresas', $dados)){
            return true;

        }else{
            return false;
        }
    }

    public function getEmpresaAutocomplete($term){

        $sql =  "SELECT id, concat(razao_social,' - ',cnpj) as label FROM empresas WHERE cnpj like '%".$term."%' and tipo_cadastro_id = 1 ";
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getExportadoresAutocomplete($term){

        $sql =  "SELECT id, concat(razao_social,' - ',cnpj) as label FROM empresas WHERE razao_social like '%".$term."%' and tipo_cadastro_id = 1 ";
    //echo $sql;
        $query = $this->db->query($sql);
        return $query->result();;

    }

    public function empresaUsuario($usuario_id){

        $sql = "SELECT e.*, u.nome,u.cpf, u.fl_termos FROM usuarios u, empresas e where u.empresa_id = e.id and u.id=".$usuario_id;
        $query = $this->db->query($sql);
        return $query->row();   

    }
   
    public function getEstados()
    {
        $sql = "SELECT * FROM estados";
        return $this->db->query($sql)->result_array();        
    }

    public function getClientesClassificadoraEmpresas($id)
    {
        $sql = "SELECT  e.id, concat(e.razao_social,' - ',e.cnpj) as cliente 
                FROM    empresas e 
                WHERE   e.id not in (select cliente_id from classificadora_clientes WHERE classificadora_id = ".$id." UNION SELECT cliente_id FROM empresas_clientes WHERE administrador_id = ".$id.") and e.tipo_cadastro_id = 1";
        return $this->db->query($sql)->result_array();
    }

    public function getClientesVinculadosClassificadoras($classificadora_id)
    {
        $sql = "SELECT  clientes.* FROM empresas clientes, classificadora_clientes c
                WHERE   clientes.id = c.cliente_id and c.classificadora_id =". $classificadora_id;
        return $this->db->query($sql)->result_array();
    }

    public function desvincularEmpresa($where){        
        // se for classificadora exclui na tabela classificadora_clientes, se não, exclui na tabela empresas_clientes
        if($where['tipo'] == 17){
            $this->db->where('classificadora_id', $where['principal_id']);
            $this->db->where('cliente_id', $where['cliente_id']);
            $tabela = 'classificadora_clientes';
        }else{
            $this->db->where('administrador_id', $where['principal_id']);
            $this->db->where('cliente_id', $where['cliente_id']);
            $tabela = 'empresas_clientes';    
        }
        if( $this->db->delete($tabela) ){
            return true;
        }else{
            return false;
        }
    }

    public function buscaTransportadora($term){

        $sql =  "SELECT * FROM empresas WHERE concat(razao_social,cnpj) like '%".$term."%' and tipo_cadastro_id = 23";
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
}
?>