<?php

class AreaEquipamentoModel extends CI_Model {

	public function insere($area_equipamento)
	{
        $this->db->insert('area_equipamento', $area_equipamento);
		return $this->db->insert_id();
    }
   
	
    public function atualiza($dados){

        $this->db->where('id', $dados['id']);
        if($this->db->update('area_equipamento', $dados)){
            return true;
        }else{
            return false;
        }
    }    

    public function excluir($id){
		$this->db->where('id', $id);
        if(	$this->db->delete('area_equipamento') ){
            return true;
        }else{
            return false;
        }
	}
	
	public function buscaEquipamentos()
	{
		$sql = "SELECT a.*, t.ds_tipo as tipo FROM area_equipamento a 
				INNER JOIN tipo_area_equipamento t ON t.id = a.id_tipo";
		return $this->db->query($sql)->result_array();
	}
	
	public function buscaTipoAreaEquipamento()
	{
		$sql = "SELECT * FROM tipo_area_equipamento ";
		return $this->db->query($sql)->result_array();	
	}

	public function buscaEquipamento($id)
	{
		$sql = "SELECT * FROM area_equipamento WHERE id = ". $id;
		return $this->db->query($sql)->row_array();
	}

	public function insereApiGeorreferenciamento($id, $tipo_operacao='I' ){
		
		$sql = "INSERT api_georreferenciamento (id_evento,
                             	origem,
                             	tp_operacao,
                             	cpf_usuario, 
                             	dthr_ocorrencia,
                             	dthr_registro,
                             	fl_contingencia,
                             	nome,
								fl_ativo,
								tipo,
								coord_x,
								coord_y,
								id_equip)
				SELECT  'VANZIN-AREA_EQUIPAMENTO-".$id."',
				        concat('AREA_EQUIPAMENTO-', area_equipamento.id),
				       '".$tipo_operacao."',
				        '00000000000',
				        now(),
				        now(),
				        'N',
				        area_equipamento.nome,
				        area_equipamento.fl_ativo,
					tipo_area_equipamento.id_api_recintos,
				        area_equipamento.coord_x,
					area_equipamento.coord_y,
					".$id."
				FROM area_equipamento, tipo_area_equipamento
				WHERE area_equipamento.id_tipo = tipo_area_equipamento.id 
				AND area_equipamento.id =  ".$id;
				  
		if( $this->db->query($sql) ){
			return $this->db->insert_id();
		}else{
			return false;
		}
	
	}
	
	public function buscaDadosApiGeorreferenciamento($id){
		$sql = "SELECT * FROM api_georreferenciamento 
				WHERE id_equip = ".$id;
		return $this->db->query($sql)->row_array();
	}
	
	public function atualizaApiGeorreferenciamento($dados){
		$this->db->where('id', $dados['id']);

        if($this->db->update('api_georreferenciamento', $dados)){
            return true;

        }else{
            return false;
        }
	}
	
}
