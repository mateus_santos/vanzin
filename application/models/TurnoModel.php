<?php

class TurnoModel extends CI_Model {

	public function insere($turno)
	{
        $this->db->insert('turnos', $turno);
		return $this->db->insert_id();
    }


    public function atualizaTurnos($dados){

        $this->db->where('id', $dados['id']);
        if($this->db->update('turnos', $dados)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaTurnos()
    {
        $sql = "SELECT * FROM turnos  order by nome ASC ";
        return $this->db->query($sql)->result_array();
    }

    public function getTurno($id){

        $sql =  "SELECT * FROM turnos WHERE id=".$id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
	

    public function excluir($id){
		$this->db->where('id', $id);
        if(	$this->db->delete('turnos') ){
            return true;
        }else{
            return false;
        }
	}

	

}