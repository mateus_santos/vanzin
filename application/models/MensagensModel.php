<?php

class MensagensModel extends CI_Model {

	public function insere($mensagem)
	{
        $this->db->insert('mensagens', $mensagem);
		return $this->db->insert_id();
    }

    public function insereDestino($mensagem)
	{
        $this->db->insert('mensagem_destinatario', $mensagem);
		return $this->db->insert_id();
    }

    public function insereResposta($item)
	{
        $this->db->insert('mensagem_resposta', $item);
		return $this->db->insert_id();
    }

	public function insere_log_erro($mensagem)
	{
        $this->db->insert('log_mensagems', $mensagem);
		return $this->db->insert_id();
    }

    public function atualiza($dados){

        $this->db->where('id', $dados['id']);
        if($this->db->update('mensagens', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizaItem($dados){

        $this->db->where('id', $dados['id']);
        if($this->db->update('mensagem_item', $dados)){
            return true;
        }else{
            return false;
        }
    }

    public function excluirItens($id_agenda){
		$this->db->where('id_agenda', $id_agenda);
        if(	$this->db->delete('mensagem_item') ){
            return true;
        }else{
            return false;
        }
	}

	public function excluirNF($id_agenda){
		$sql = "DELETE FROM nota_fiscal_item WHERE id_nota_fiscal IN (SELECT id FROM nota_fiscal WHERE id_agenda = ".$id_agenda.") ";
		
        if(	$this->db->query($sql) ){
        	$this->db->where('id_agenda', $id_agenda);
           	if(	$this->db->delete('nota_fiscal') ){
	            return true;
	        }else{
	            return false;
	        }
        }else{
            return false;
        }
	}

	public function excluir($id){
		if( $this->excluirNF($id) ){
			if( $this->excluirItens($id) ){
				$this->db->where('id', $id);
		        if(	$this->db->delete('mensagens') ){
		            return true;
		        }else{
		            return false;
		        }
	    	}else{
	    		return false;
	    	}
    	}else{
    		return false;
    	}
	}

	//Busca erros dos logs de importação do mensagem
	public function getLogErrosAgenda(){

		$sql = "select * from log_mensagems";
		return $this->db->query($sql)->result();

	} 
//Exclui todos os logs do mensagem importado
	public function excluirLogsAgenda(){
		$this->db->where('1','1');
        if(	$this->db->delete('log_mensagems') ){
            return true;
        }else{
            return false;
        }
	}

	public function buscaMensagens()
	{			

		$sql = "SELECT	m.*, group_concat(' ',d.destino ) as destinatarios, o.nome as origem, eo.razao_social as empresa_origem 
				FROM 	mensagens m
				INNER JOIN usuarios o ON o.id = m.usuario_id
				INNER JOIN (SELECT m.id, m.mensagem_id, concat(u.nome,' - ',e.razao_social) as destino from mensagem_destinatario m 
							INNER JOIN usuarios u on u.id = m.usuario_id
							inner join empresas e on e.id = u.empresa_id ) d ON d.mensagem_id = m.id				
				INNER JOIN empresas eo on eo.id = o.empresa_id
				inner join mensagem_destinatario md on md.mensagem_id = m.id 
				WHERE md.usuario_id = ".$this->session->userdata('usuario_id')."		
				group by m.id 
				ORDER BY m.dthr_criacao DESC, m.status_id ASC ";
		
		return $this->db->query($sql)->result_array();
	}

	public function buscaMensagem($id)
	{
		$sql = "SELECT	m.*, o.nome as origem, eo.razao_social as empresa_origem,o.id as origem_id, group_concat(distinct ' ', dest.nome ) as destinos
				FROM 	mensagens m
				INNER JOIN usuarios o ON o.id = m.usuario_id
				INNER JOIN (SELECT m.id, m.mensagem_id, concat(u.nome,' - ',e.razao_social) as destino from mensagem_destinatario m 
							INNER JOIN usuarios u on u.id = m.usuario_id
							inner join empresas e on e.id = u.empresa_id ) d ON d.mensagem_id = m.id				
				INNER JOIN empresas eo on eo.id = o.empresa_id
				INNER JOIN mensagem_destinatario md on md.mensagem_id = m.id 
				inner join usuarios dest on dest.id = md.usuario_id  
				WHERE m.id =".$id."
				group by m.id";

		return $this->db->query($sql)->row_array();

	}

	public function buscaRespostas($mensagem_id)
	{
		$sql = "SELECT	r.*, o.nome as origem, eo.razao_social as empresa_origem
				FROM 	mensagem_resposta r
				INNER JOIN usuarios o ON o.id = r.usuario_id				
				INNER JOIN empresas eo on eo.id = o.empresa_id
				WHERE r.mensagem_id =".$mensagem_id;

		return $this->db->query($sql)->result_array();
		
	}

	public function buscamensagemsDoDiaResumido($empresa_id,$tipo_acesso)
	{
		$where_empresa = '';
		if( $empresa_id != '' && $tipo_acesso != 'administrador empresas'){
			$where_empresa = " and 	mensagem.id_empresa = ".$empresa_id;
		}
		// se for administrador de empresas, busca apenas clientes vinculados
		if( $tipo_acesso == 'administrador empresas' ){
			$where_empresa = " and 	mensagem.id_empresa in (select cliente_id from empresas_clientes where administrador_id = ".$empresa_id.")" ;	
		}

		$sql = "SELECT	empresas.id,
						empresas.razao_social,
						count(empresas.id) total
				FROM 	mensagem
				INNER JOIN empresas ON mensagem.id_empresa  = empresas.id
				INNER JOIN motorista ON mensagem.id_motorista = motorista.id								
				LEFT JOIN acesso_patio ON acesso_patio.id_agenda = mensagem.id								
				where  1=1 
				".$where_empresa."
				and acesso_patio.dthr_entrada is null
				and 	mensagem.dt_agenda = '".date('Y-m-d')."'	
				and 	mensagem.id not in (select id_agenda from acesso_patio)
				group by empresas.id,
				empresas.razao_social
				ORDER BY mensagem.id DESC ";
 		
		return $this->db->query($sql)->result_array();
	}

	public function buscamensagems($empresa_id,$where=' and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and	mensagem.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT	mensagem.id,
				mensagem.id_tipo_operacao,
				empresas.razao_social,
				date_format(mensagem.dt_agenda,'%d/%m/%Y') as dt_agenda,
				mensagem.placa,
				motorista.nome,
				motorista.cpf,
				motorista.fone,
				embalagem.no_embalagem,
				nf.notas as nr_nf,
				nf.peso_liquido as quant_total,
				mensagem_item.descricao
				FROM 	mensagem
				INNER JOIN empresas 	ON mensagem.id_empresa  = empresas.id
				INNER JOIN motorista 	ON mensagem.id_motorista = motorista.id
				INNER JOIN mensagem_item ON mensagem.id = mensagem_item.id_agenda
				LEFT JOIN embalagem 	ON mensagem_item.id_embalagem = embalagem.id
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = mensagem.id
				WHERE 1=1  								
				".$where_empresa." ".$where."
				and mensagem.id not in (select id_agenda from acesso_patio)
				GROUP BY mensagem.id
				ORDER BY mensagem.id DESC ";
		
		return $this->db->query($sql)->result_array();
	}
	/*Busca o total de mensagems resumido */
	public function buscamensagemsResumido($empresa_id,$where=' and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and	mensagem.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT	empresas.id,
				concat(empresas.razao_social,'-',empresas.cnpj) as razao_social,
				count(empresas.id) total
				FROM 	mensagem
				INNER JOIN empresas ON mensagem.id_empresa  = empresas.id
				INNER JOIN motorista ON mensagem.id_motorista = motorista.id								
				LEFT JOIN acesso_patio ON acesso_patio.id_agenda = mensagem.id						
				WHERE   1=1
				".$where_empresa." ".$where."
				and acesso_patio.dthr_entrada is null
				group by empresas.id,
				concat(empresas.razao_social,'-',empresas.cnpj)
				ORDER BY mensagem.id DESC ";
		return $this->db->query($sql)->result_array();
	}

	public function buscamensagemPorId($id_agenda)
	{
		$sql = "SELECT	mensagem.id,
						mensagem.id_tipo_operacao,
						mensagem.id_empresa,
          				empresas.razao_social,
          				date_format(mensagem.dt_agenda,'%d/%m/%Y') as dt_agenda,
						mensagem.placa,
						mensagem.id_motorista,
						motorista.nome,
						motorista.fone,
						motorista.cpf,
						motorista.nr_cnh,
						nota_fiscal.nr_nf,
						nota_fiscal.serie_nf,
						nota_fiscal.arquivo,
						mensagem.id_frete,
						mensagem.id_transportadora,
						concat(transp.cnpj,'-', transp.razao_social) as transportadora
				FROM 	mensagem
				INNER JOIN empresas ON mensagem.id_empresa  = empresas.id
				INNER JOIN motorista ON  mensagem.id_motorista = motorista.id				
				LEFT JOIN nota_fiscal ON mensagem.id = nota_fiscal.id_agenda 
				LEFT JOIN empresas transp on transp.id = mensagem.id_transportadora	
				WHERE mensagem.id = ".$id_agenda."
			ORDER BY mensagem.id DESC ";

		return $this->db->query($sql)->row_array();			
	}

	public function buscaEmbalagem()
	{
		$sql = "SELECT * FROM embalagem";
		return $this->db->query($sql)->result_array();
	}

	public function verificaCpfCnh($where)
	{
		if($where['cpf'] != ''){
			$situacao = "cpf = '".$where['cpf']."'" ;
		}
		if($where['cnh'] != ''){
			$situacao = "nr_cnh = '".$where['cnh']."'";	
		}

		$sql = "	SELECT 	id, nome, cpf, nr_cnh, fone, fl_bloqueado 
					FROM 	motorista 
					WHERE 	".$situacao;

		return $this->db->query($sql)->row_array();
	}

	public function verificaNotamensagem($chave)
	{
		$sql = "SELECT 	mensagem.id, mensagem.placa
				  FROM 	mensagem
				  LEFT JOIN nota_fiscal ON nota_fiscal.id_agenda = mensagem.id 
				  WHERE	mensagem.id NOT IN (select id_agenda from acesso_patio where fl_atende_classific='N')
					and nota_fiscal.chave_acesso = '".$chave."'";

		return $this->db->query($sql)->row_array();
	}

	public function verificaCotasmensagem($empresa_id, $dt_mensagem)
	{
		$sql = "SELECT  hr_limite_agenda, hr_limite_acesso, peso, fl_sem_cota
				FROM 	cotas
				WHERE 	id_empresa = ".$empresa_id." 
				and 	dia_semana = (SELECT CASE 	WHEN DAYOFWEEK('".$dt_mensagem."') = 1 THEN 'DOM'          
                                                  	WHEN DAYOFWEEK('".$dt_mensagem."') = 2 THEN 'SEG' 
                                                 	WHEN DAYOFWEEK('".$dt_mensagem."') = 3 THEN 'TER' 
                                                 	WHEN DAYOFWEEK('".$dt_mensagem."') = 4 THEN 'QUA' 
                                                 	WHEN DAYOFWEEK('".$dt_mensagem."') = 5 THEN 'QUI' 
                                                	WHEN DAYOFWEEK('".$dt_mensagem."') = 6 THEN 'SEX' 
                                       				ELSE 'SAB' END as dia_semana from dual)";

		return $this->db->query($sql)->row_array();
	}

	public function buscaTotalAgendadoPeriodo($empresa_id, $periodo_ini)
	{
		$sql = "SELECT sum(nf.peso_liquido) as total				
				FROM 	mensagem
				INNER JOIN empresas 	ON mensagem.id_empresa  = empresas.id				
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = mensagem.id
				WHERE  mensagem.id_empresa = ".$empresa_id."
				and mensagem.dt_agenda = '".$periodo_ini."'";
				
		return $this->db->query($sql)->row_array();
	}

	public function verificaCfop($cfop)
	{
		$sql = "SELECT coalesce(count(*)) as total_cfop FROM cfop WHERE cfop =".$cfop;
		return $this->db->query($sql)->row_array();	
	}

	public function buscaItensmensagem($id_agenda)
	{
		$sql = "SELECT * FROM mensagem_item WHERE id_agenda=".$id_agenda;
		return $this->db->query($sql)->result_array();	
	}

	public function verificaEntradaPatio($id_agenda)
	{
		$sql = "SELECT COALESCE(count(*),0) as total FROM acesso_patio WHERE id_agenda=".$id_agenda;
		return $this->db->query($sql)->row_array();	
	}

	/*Busca os caminhões que saíram */
	public function buscaSaidas($empresa_id,$where='and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and 	mensagem.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT  mensagem.id,
					IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'Mon','Segunda',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'tue','Terça',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'wed','Quarta',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'thu','Quinta',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'fri','Sexta',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'sat','Sabado',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'sun','Domingo','err'))))))) as dia,
					empresas.razao_social,
					DATE_FORMAT(mensagem.dt_agenda, '%d/%m/%Y') AS dt_agenda,
					mensagem.placa,
					motorista.nome,
					motorista.cpf,
					motorista.fone,
					embalagem.no_embalagem,
					nf.notas as nr_nf,
					nf.peso_liquido as quant_total,
					mensagem_item.descricao,
					DATE_FORMAT(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i:%s') AS dthr_entrada,
					CASE 
						WHEN convert(date_format(acesso_patio.dthr_classific,'%d'),CHAR) > 0 THEN
							date_format(acesso_patio.dthr_classific,'%d/%m/%Y %H:%i:%s')
						ELSE ''
					END dthr_classific,
					DATE_FORMAT(acesso_patio.dthr_saida,'%d/%m/%Y %H:%i:%s') AS dthr_saida
				FROM mensagem					
				inner join empresas on mensagem.id_empresa = empresas.id 
				inner join	motorista on mensagem.id_motorista = motorista.id 
				inner join	mensagem_item on mensagem.id = mensagem_item.id_agenda 
				inner join embalagem on mensagem_item.id_embalagem = embalagem.id 
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = mensagem.id
				inner join	cotas on empresas.id = cotas.id_empresa 
				inner join	acesso_patio on mensagem.id = acesso_patio.id_agenda and mensagem.placa = acesso_patio.placa
				WHERE DATE_FORMAT(mensagem.dt_agenda, '%a') = IF(cotas.dia_semana = 'seg','Mon',IF(cotas.dia_semana = 'ter', 'tue', IF(cotas.dia_semana = 'qua','wed',IF(cotas.dia_semana = 'qui','thu',IF(cotas.dia_semana = 'sex','fri',IF(cotas.dia_semana = 'sab','sat',IF(cotas.dia_semana = 'dom','sun','err')))))))
					AND acesso_patio.dthr_saida is not null
				".$where_empresa." ".$where." 
				group by mensagem.id, acesso_patio.dthr_saida 
				ORDER BY mensagem.id DESC ";
		
		return $this->db->query($sql)->result_array();
	}

	/*Busca os totais de veiculos que sairam */
	public function buscaSaidasResumido($empresa_id,$where='and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and 	mensagem.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT
					empresas.id,
					empresas.razao_social,
				count(empresas.id) total
				FROM
					mensagem,
					empresas,
					motorista,
					(select DISTINCT id_agenda, id_embalagem from mensagem_item) mensagem_item,
					embalagem,					
					cotas,
					acesso_patio
				WHERE
					mensagem.id_empresa = empresas.id 
					AND mensagem.id = mensagem_item.id_agenda 
					AND mensagem.id_motorista = motorista.id 
					AND mensagem_item.id_embalagem = embalagem.id 					
					AND empresas.id = cotas.id_empresa 
					and mensagem.id = acesso_patio.id_agenda
					and mensagem.placa = acesso_patio.placa 
					AND DATE_FORMAT(mensagem.dt_agenda, '%a') = IF(cotas.dia_semana = 'seg','Mon',IF(cotas.dia_semana = 'ter', 'tue', IF(cotas.dia_semana = 'qua','wed',IF(cotas.dia_semana = 'qui','thu',IF(cotas.dia_semana = 'sex','fri',IF(cotas.dia_semana = 'sab','sat',IF(cotas.dia_semana = 'dom','sun','err')))))))
					AND acesso_patio.dthr_saida is not null
				and 1=1
				".$where_empresa." ".$where."
				group by empresas.id,
				empresas.razao_social
				ORDER BY mensagem.id DESC ";
		
		return $this->db->query($sql)->result_array();
	}

	/*Busca os caminhões que Entraram e estão aguardando saída */
	public function buscaEstacionados($empresa_id,$where='and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and 	mensagem.id_empresa = ".$empresa_id;
		}
 
		$sql = "SELECT mensagem.id,
					IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'Mon','Segunda',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'tue','Terça',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'wed','Quarta',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'thu','Quinta',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'fri','Sexta',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'sat','Sabado',IF(DATE_FORMAT(mensagem.dt_agenda, '%a') = 'sun','Domingo','err'))))))) as dia,
					empresas.razao_social,
                    mensagem_item.descricao,
                    mensagem_item.nr_pedido,
                    embalagem.no_embalagem,
					nf.peso_liquido as quant_total,
                    IF(acesso_patio.fl_atende_classific = 'S','LIBERADO',IF(acesso_patio.fl_atende_classific = 'N','NÃO LIBERADO','SEM CLASSIFICAÇÃO')) AS fl_atende_classific,
					DATE_FORMAT(mensagem.dt_agenda, '%d/%m/%Y') AS dt_agenda,
					mensagem.placa,
					nf.notas as nr_nf,
					motorista.nome,
					motorista.cpf,
					motorista.fone,
					DATE_FORMAT(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i:%s') AS dthr_entrada,
					CASE 
						WHEN convert(date_format(acesso_patio.dthr_classific,'%d'),CHAR) > 0 THEN
							date_format(acesso_patio.dthr_classific,'%d/%m/%Y %H:%i:%s')
						ELSE ''
					END dthr_classific,
					DATE_FORMAT(acesso_patio.dthr_saida,'%d/%m/%Y %H:%i:%s') AS dthr_saida
				FROM
					mensagem
                INNER JOIN 	empresas 			on 	empresas.id = mensagem.id_empresa    
				INNER JOIN 	motorista 			on 	motorista.id = mensagem.id_motorista
				LEFT JOIN 	mensagem_item 	on 	mensagem_item.id_agenda = mensagem.id
				LEFT JOIN 	embalagem 			on 	embalagem.id = mensagem_item.id_embalagem
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = mensagem.id
				INNER JOIN 	acesso_patio 		on 	acesso_patio.id_agenda = mensagem.id
				WHERE 		acesso_patio.dthr_saida is null 			
                ".$where_empresa." ".$where."
                group by mensagem.id
				ORDER BY mensagem.id DESC";
		 
		return $this->db->query($sql)->result_array();
	}

	/*Busca os totais resumido dos caminhões que Entraram e estão aguardando saída */
	public function buscaEstacionadosResumido($empresa_id,$where='and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and 	mensagem.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT empresas.id,
				empresas.razao_social,
				count(empresas.id) total
				FROM mensagem
                INNER JOIN 	empresas 			on 	empresas.id = mensagem.id_empresa    
				INNER JOIN 	motorista 			on 	motorista.id = mensagem.id_motorista
				INNER JOIN 	acesso_patio 		on 	acesso_patio.id_agenda = mensagem.id
				WHERE 		acesso_patio.dthr_saida is null 			
                ".$where_empresa." ".$where."
				group by empresas.id,
				empresas.razao_social
				ORDER BY mensagem.id DESC";
		 
		return $this->db->query($sql)->result_array();
	}

	/*Informações da Folha de Carga de Mercadorias */
	public function buscaFolhaMercadorias($where)
	{
		
		$sql = "SELECT id,	DATE_FORMAT(dthr_atracacao,'%d/%m/%Y %H:%i') AS  dt_ini,
						DATE_FORMAT(dthr_desatracacao,'%d/%m/%Y %H:%i') AS  dt_fim,
						navio,
						nr_fundeio,
						nr_escala
				FROM transito_simplificado
				where 1 = 1  ".$where."
				order by dthr_atracacao desc";
		
		return $this->db->query($sql)->result_array();
	}

	/* Metodo utilizado para trazer as informações da impressão da Folha de MErcadorias  */
    public function getFolhaMercadoria($id){

        $sql = "select nr_fundeio, navio, nr_escala, imo, terminal.no_terminal,
						'VANZIN OPERAÇÕES PORTUÁRIAS S/A', 
						DATE_FORMAT(dthr_atracacao,'%d/%m/%Y %H:%i:%s') AS dthr_atracacao, 
						DATE_FORMAT(dthr_ini_operacao,'%d/%m/%Y %H:%i:%s') AS dthr_ini_operacao, 
						DATE_FORMAT(dthr_fim_operacao,'%d/%m/%Y %H:%i:%s') AS dthr_fim_operacao, 
						DATE_FORMAT(dthr_desatracacao,'%d/%m/%Y %H:%i:%s') AS dthr_desatracacao, 
						transito_simplificado.observacao, 
					empresas.razao_social,
						transito_simplificado_due.ncm,
						transito_simplificado_due.nr_due,
						transito_simplificado_due.ce_mercante,
						sum(transito_movimentacao.peso_aferido) peso
				from transito_simplificado,
						transito_simplificado_due,
					transito_movimentacao,
					empresas,
					terminal
				where transito_simplificado.id             = transito_simplificado_due.id_transito
				and transito_simplificado.id_terminal    = terminal.id
				and transito_simplificado_due.id_empresa = empresas.id
				and transito_simplificado_due.id_transito = transito_movimentacao.id_transito
				and transito_simplificado_due.nr_due = transito_movimentacao.nr_due
				and transito_simplificado.id             = ".$id."
				group by nr_fundeio, navio, nr_escala, terminal.no_terminal,
						dthr_atracacao, dthr_ini_operacao, dthr_fim_operacao,                 
						dthr_desatracacao,
						empresas.razao_social,
						transito_simplificado_due.ncm,
						transito_simplificado_due.nr_due,
						transito_simplificado_due.ce_mercante
				";
          
        $query = $this->db->query($sql);

        return $query->result();
    }

	public function listarmensagemsCliente($empresa_id,$filtros=' and 1=1')
	{
		$sql = "SELECT distinct	mensagem.id,
				mensagem.id_tipo_operacao,
				empresas.razao_social,
				date_format(mensagem.dt_agenda,'%d/%m/%Y') as dt_agenda,
				mensagem.placa,
				motorista.nome,
				motorista.cpf,
				motorista.fone,
				embalagem.no_embalagem,
				nf.notas as nr_nf,
				nf.peso_liquido as quant_total,
				mensagem_item.descricao
				FROM 	mensagem
				INNER JOIN empresas 	ON mensagem.id_empresa  = empresas.id
				INNER JOIN motorista 	ON mensagem.id_motorista = motorista.id
				INNER JOIN mensagem_item ON mensagem.id = mensagem_item.id_agenda
				LEFT JOIN embalagem 	ON mensagem_item.id_embalagem = embalagem.id
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = mensagem.id
				WHERE mensagem.id_empresa = ".$empresa_id." ".$filtros."
				and mensagem.id not in (select id_agenda from acesso_patio)
				GROUP BY mensagem.id
				ORDER BY mensagem.id DESC";
		return $this->db->query($sql)->result_array();
	}

	public function buscamensagemsDoDiaGeral($empresa_id,$tipo_acesso)
	{
		$where_empresa = '';
		if( $empresa_id != '' && $tipo_acesso != 'administrador empresas'){
			$where_empresa = " and 	mensagem.id_empresa = ".$empresa_id;
		}

		// se for administrador de empresas, busca apenas clientes vinculados
		if( $tipo_acesso == 'administrador empresas'){
			$where_empresa = " and 	mensagem.id_empresa in (select cliente_id from empresas_clientes where administrador_id = ".$empresa_id.")" ;
		}

		$sql = "SELECT	mensagem.id,
						mensagem.id_tipo_operacao,
	          			empresas.razao_social,
	          			date_format(mensagem.dt_agenda,'%d/%m/%Y') as dt_agenda,
						mensagem.placa,
						motorista.nome,
						motorista.cpf,
						motorista.fone,
						embalagem.no_embalagem,
						group_concat(nota_fiscal.nr_nf) as nr_nf,
						nota_fiscal.quant_total,
						mensagem_item.descricao,
						f.descricao as frete,
						CASE
                            WHEN a.dthr_entrada is null THEN 'A'
                            WHEN a.dthr_entrada is not null and a.id_classificadora is null THEN 'WC'
                            WHEN a.dthr_entrada is not null and a.id_classificadora is not null and a.dthr_saida is null THEN 'WE'
                            ELSE 'O'
                        END as status_mensagem
				FROM 	mensagem
				INNER JOIN empresas ON mensagem.id_empresa  = empresas.id
				INNER JOIN motorista ON mensagem.id_motorista = motorista.id
				INNER JOIN mensagem_item ON mensagem.id = mensagem_item.id_agenda
				LEFT JOIN embalagem ON mensagem_item.id_embalagem = embalagem.id
				LEFT JOIN nota_fiscal ON mensagem.id = nota_fiscal.id_agenda
				LEFT JOIN frete f on f.id = mensagem.id_frete
				LEFT JOIN acesso_patio a ON a.id_agenda = mensagem.id
				where 1=1 
				".$where_empresa."
				and 	mensagem.dt_agenda = '".date('Y-m-d')."'					
				GROUP BY mensagem.id 
				ORDER BY mensagem.id DESC ";
		
		return $this->db->query($sql)->result_array();
	}

	public function buscamensagemsGeral($empresa_id,$where=' and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and	mensagem.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT	mensagem.id,
				mensagem.id_tipo_operacao,
				empresas.razao_social,
				date_format(mensagem.dt_agenda,'%d/%m/%Y') as dt_agenda,
				mensagem.placa,
				motorista.nome,
				motorista.cpf,
				motorista.fone,
				embalagem.no_embalagem,
				nf.notas as nr_nf,
				nf.peso_liquido as quant_total,
				mensagem_item.descricao,
				f.descricao as frete,
				CASE
                    WHEN a.dthr_entrada is null THEN 'A'
                    WHEN a.dthr_entrada is not null and a.id_classificadora is null THEN 'WC'
                    WHEN a.dthr_entrada is not null and a.id_classificadora is not null and a.dthr_saida is null THEN 'WE'
                    ELSE 'O'
                END as status_mensagem 
				FROM 	mensagem
				INNER JOIN empresas 	ON mensagem.id_empresa  = empresas.id
				INNER JOIN motorista 	ON mensagem.id_motorista = motorista.id
				INNER JOIN mensagem_item ON mensagem.id = mensagem_item.id_agenda
				LEFT JOIN embalagem 	ON mensagem_item.id_embalagem = embalagem.id
				LEFT JOIN frete f 		ON f.id = mensagem.id_frete
				LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = mensagem.id
				LEFT JOIN acesso_patio a ON a.id_agenda = mensagem.id
				WHERE 1=1  								
				".$where_empresa." ".$where."				
				GROUP BY mensagem.id
				ORDER BY mensagem.id DESC ";
		
		return $this->db->query($sql)->result_array();
	}

	public function verificaAcesso($id_agenda)
	{
		$sql = "SELECT COALESCE(count(*),0) as total from acesso_patio WHERE id_agenda =".$id_agenda;
		$retorno = $this->db->query($sql)->row_array();
		return $retorno['total'];
	}

	public function buscamensagemsCotas($empresa_id = "",$dt_mensagem)
	{

		if($empresa_id != ''){
			$empresa_id = " and c.id_empresa =".$empresa_id;
		}

		$sql="SELECT  	c.hr_limite_agenda, 
						c.hr_limite_acesso, 
						c.peso, 
						c.fl_sem_cota, 
						concat(e.cnpj,'-', e.razao_social) as empresa, 
						sum(nf.peso_liquido) as agendado, 
						(c.peso - sum(nf.peso_liquido)) as saldo,
						a.dt_agenda 
				FROM 	cotas as c
                INNER JOIN empresas as e on e.id = c.id_empresa
                LEFT JOIN mensagem as a on a.id_empresa = c.id_empresa
                LEFT JOIN (select GROUP_CONCAT(nr_nf) as notas, id_agenda, sum(peso_liq_total) as peso_liquido from nota_fiscal GROUP by id_agenda ) as nf ON nf.id_agenda = a.id
				WHERE dia_semana = (SELECT CASE 	WHEN DAYOFWEEK('".$dt_mensagem."') = 1 THEN 'DOM'
                                                  	WHEN DAYOFWEEK('".$dt_mensagem."') = 2 THEN 'SEG'
                                                 	WHEN DAYOFWEEK('".$dt_mensagem."') = 3 THEN 'TER'
                                                 	WHEN DAYOFWEEK('".$dt_mensagem."') = 4 THEN 'QUA'
                                                 	WHEN DAYOFWEEK('".$dt_mensagem."') = 5 THEN 'QUI'
                                                	WHEN DAYOFWEEK('".$dt_mensagem."') = 6 THEN 'SEX'
                                       				ELSE 'SAB' END as dia_semana from dual)
                      and a.dt_agenda = '".$dt_mensagem."'
                      ".$empresa_id."
               GROUP by c.id_empresa";
        return $this->db->query($sql)->result_array();
	}

	public function buscamensagemsPendentes()
	{
		$sql = "SELECT mensagem.id,
				       mensagem.dt_agenda,
				       empresas.razao_social,
				       mensagem.placa,
				       motorista.nome,
				       empresas.email
				  FROM mensagem,
				       empresas,
				       motorista
				 where mensagem.id_empresa = empresas.id
				   and mensagem.id_motorista = motorista.id
				   and mensagem.dt_agenda < date(now())
				   and COALESCE(fl_email_enviado, 0) = 0
				   and not exists (select 'x' from acesso_patio where acesso_patio.id_agenda = mensagem.id)";
		
		return $this->db->query($sql)->result_array();

	}

	public function verificaVeiculo($placa)
	{
		$sql = "SELECT COALESCE(count(*),0) as total FROM veiculos_bloqueados WHERE placa = '".$placa."'";
		return $this->db->query($sql)->row_array();
	}

	public function buscaFretes()
	{
		$sql = "SELECT * FROM frete";
		return $this->db->query($sql)->result_array();
	}

	public function verificaFrete($frete)
	{
		$sql = "SELECT count(id) total, id FROM frete where descricao='".$frete."' group by id";
		return $this->db->query($sql)->result();
	}
	
}