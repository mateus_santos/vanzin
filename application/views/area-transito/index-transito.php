<div class="m-content">
	<!-- begin Portlet -->
<form accept-charset="utf-8" action="<?php echo base_url('AreaTransito/pesquisaTransito/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-calendar" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Trânsito Simplificado 
					</h3>	
					<h3 class="m-portlet__head-text" style=" padding-left: 10px;" >
						<a href="<?php echo base_url('AreaTransito/cadastrarTransito'); ?>"  style="color: #ffcc00; font-weight: bold;" data-toggle="m-tooltip" data-placement="top" title="Novo Trânsito Simplificado" id="Novo Transito Simplificado">
							<i class="la la-plus-circle" style="font-size: 38px;"></i>
						</a>
					</h3>										
				</div>
 
			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Período Atracação:</label>					
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="dt_ini" id="dthr_atracacao" value="" class="form-control m-input datepicker" placeholder="data inicial"style="width: 49%;float: left;" autocomplete='off' maxlength="10"/> 
						<input type="text" name="dt_fim" id="dthr_desatracacao" value="" class="form-control m-input datepicker" placeholder="data final"style="width: 49%;float: right;" autocomplete='off' maxlength="10"/>
					</div>				
				</div>
				<div class="col-lg-6">
					<label>DUE:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="nr_due" id="nr_due" class="form-control m-input" placeholder="Nr. DUE" style="width: 300px;" /> 
					</div>				
				</div>
			</div>	
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Navio:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="navio" id="navio" class="form-control m-input" placeholder="Navio" style="width: 300px;" />
					</div>				
				</div>			
			
			
				<div class="col-lg-6">
                <label>Nr. Escala:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="nr_escala" id="nr_escala" class="form-control m-input" placeholder="Nr. Escala" style="width: 300px;" /> 
					</div>		
				</div>
			
				
			</div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Filtrar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>							
					</div>
				</div>
			</div>	
		</div>					
	</div>
</form>
	<!--end::Portlet-->	

	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" >
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
							Listagem
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="" class="m-portlet__body " >

			<!--begin: Datatable -->
			<table class="" id="table2" width="100%">
				<thead>
					<tr>
						<th style="">#</th>												
						<th style="">Período de Atracação</th>				
						<th style="">Navio</th>															
						<th style="">Nr. Escala</th>
						<th style="">Ações</th>
					</tr>
				</thead>
				<tbody>					
				<?php 
					
					foreach($dados as $class){	
					
						$dt_ini = $class['dthr_atracacao'];
						$dt_hr = explode(" ",$dt_ini);
						$dt = explode("-",$dt_hr[0]);
						$dt_ini = $dt[2].'/'.$dt[1].'/'.$dt[0].' '.$dt_hr[1];

						if(!empty($class['dthr_desatracacao'])){
							$dt_fim = $class['dthr_desatracacao'];
							$dt_hr_f = explode(" ",$dt_fim);
							$dtf = explode("-",$dt_hr_f[0]);
							$dt_fim = ' - '.$dtf[2].'/'.$dtf[1].'/'.$dtf[0].' '.$dt_hr_f[1];
						}else{
							$dt_fim = '';
						}
						

					?> 
					<tr>
						<td style="text-align: center;"><?php echo $class['id']; ?></td>
						<td style=""><?php echo $dt_ini.$dt_fim; ?></td>											
						<td style=""><?php echo $class['nr_fundeio'].' - '.$class['navio']; ?></td>
						<td style=""><?php echo $class['nr_escala'];  ?></td>						
						
						<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
							<a href="<?php echo base_url('AreaTransito/atualizarTransito/'.$class['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-edit"></i></a>
							<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" target="blank" onclick="excluirTransito(<?=$class['id']?>)">
								<i class="la la-trash"></i>
							</a>				
						</td>
					</tr>
					<?php } ?> 
				</tbody>
			</table>			
		</div>
	</div>
</div>
<div class="modal fade" id="m_atualiza" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_atualiza_title" id="exampleModalLongTitle">
					
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="">					
				
				<div class="row">
					<div class="form-group m-form__group col-lg-12" >
						<label for="exampleSelect1">Verificação do Caminhão</label>							
						<select class="form-control" name="classificacao" id="classificacao">
							<option value="">Selecione</option>	
							<option value="S">Atende - Liberar Saída</option>
							<option value="N">Não Atende</option>
						</select>
					</div>
				</div>	
				
				<div class="row motivo_esconde" style="display: none;">
					<div class="form-group m-form__group col-lg-12" >
						<label for="exampleSelect1">Motivo</label>							
						<textarea class="form-control" name="motivo" id="motivo"></textarea>
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal" id="classificar" acesso_id="">Validar</button>
			</div>
		</div>
	</div>
</div>