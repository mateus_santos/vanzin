<?php /*
echo "<pre>";
var_dump($listagem);
echo "<pre>"; DIE;*/

for($i=0;$i < count($listagem);$i++){
?>
<div class="layout_extrato" id="impressao" style="page-break-after: always; margin-top:10%;">
	<div id="cabecacho">
		<table class=" formata_tab_siscomex" style="text-align:left;">
			<thead>
				<tr>
					<td class='td_extrato'><img src='../../bootstrap/img/logoSiscomex.png' id='img_extrato' alt="Logo Siscomex"/></td>
                    <td  class="tb_titulo" ><p>Extrato de Trânsito Simplificado </p></td>
                </tr>
				<tr>
					<td class=""><div>Tipo de Entrega:</div><div>DUE/RUC</div></td><td style="text-align:right; "><div style="margin-right:10%">Data/Hora:<?php echo " ".date('d/m/Y H:m',strtotime($data_hora));?></div></td>
				</tr>	
			</thead>
			<tbody>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;"></td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:left;">Local de Entrega</td>
				</tr> 
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;"><div class="linha_extrato"></div></td>
				</tr>
				<tr>
					<td class="">
						<div class="lb_text_extrato">Código do Recinto Recebedor:</div><div class="lb_text_extrato">1017700 - Porto do Rio Grande</div>
					</td>
					<td>
						<div>Recinto Aduaneiro:</div><div>0302701 - VANZIN TERMINAIS E SERVIÇOS ADUANEIROS LTDA</div>
					</td>
				</tr>
				<tr>
					<td class="">
						<div class="lb_text_extrato">CNPJ / CPF do Responsável:</div><div class="lb_text_extrato">09.308.437/0002-42 - VANZIN TERMINAIS E SERVIÇOS ADUANEIROS LTDA</div>
					</td>
					<td>
						<div>Latitude / Longitude:</div><div>-32.107126 / -52.117230</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:left;">Dados do Receptor</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;"><div class="linha_extrato"></div></td>
				</tr>
				<tr>
					<td class="">
						<div class="lb_text_extrato">CNPJ / CPF:</div><div class="lb_text_extrato"><?php echo $listagem[$i]->cpf_motorista;?></div>
					</td>
					<td>
						<div></div><div></div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:left;">Dados da Pesagem</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;"><div class="linha_extrato"></div></td>
				</tr>
				<tr>
					<td class="">
						<div class="lb_text_extrato">Peso Aferido (kg):</div><div class="lb_text_extrato "><?php echo number_format($listagem[0]->peso_aferido,3,',','.');?></div>
					</td>
					<td>
						<div></div><div> </div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:left;">Trânsito Simplificado</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;"><div class="linha_extrato"></div></td>
				</tr>
				<tr>
					<td class="">
						<div class="lb_text_extrato">Unidade da RFB de Destino:</div><div class="lb_text_extrato">1017700 - PORTO DE RIO GRANDE</div>
					</td>
					<td>
						<div>Recinto Aduaneiro de Destino</div><div>0301301 - PORTO DE RIO GRANDE - SUPERINT.DO PTO DE R.GDE - RIO GRANDE/RSPORTO MARITIMO ALFANDEGADO PUBLICO</div>
					</td>
				</tr>
				<tr>
					<td class="">
						<div class="lb_text_extrato">Nome - CPF do Condutor:</div><div class="lb_text_extrato"><?php echo $listagem[$i]->no_motorista.' - '.$listagem[$i]->cpf_motorista;?></div>
					</td>
					<td>
						<div>Exportador:</div><div><?php echo $listagem[$i]->cnpj." - ".$listagem[$i]->razao_social;?></div>
					</td>
				</tr>				
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:left;">Dados do Veículo</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;"><div class="linha_extrato"></div></td>
				</tr>
				<tr>
					<td class="">
						<div class="lb_text_extrato">Placa:</div><div class="lb_text_extrato"><?php echo $listagem[$i]->placa;?></div>
					</td>
					<td>
						<div></div><div></div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;"><div class="linha_extrato"></div></td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:left;">Documentos</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;"><div class="linha_extrato"></div></td>
				</tr>
				<tr>
					<td colspan="2" class="">
					<table class="table_interno">
						<tr>
							<td>
								<div class="lb_text_extrato">Tipo de Documento:</div><div class="lb_text_extrato">DU-E</div>
							</td>
							<td>
								<div class="lb_text_extrato">Nº do Documento:</div><div class="lb_text_extrato"><?php echo strtoupper($listagem[$i]->nr_due); ?></div>
							</td>
							<td>
								<div class="lb_text_extrato">Tipo de Operação:</div><div class="lb_text_extrato">Parcial</div>
							</td>
						</tr>
					</table>
				</tr>
				<tr>
					<td colspan="2" class="">
					<table class="table_interno">
						<tr>
							<td>
								<div class="lb_text_extrato">Tipo de Carga:</div><div class="lb_text_extrato"><?php echo $listagem[$i]->no_tipo; ?></div>
							</td>
							<td>
								<div class="lb_text_extrato">Qtde. Disponível:</div><div class="lb_text_extrato "><?php echo number_format($listagem[$i]->peso_manif,3,',','.'); ?></div>
							</td>
							<td>
								<div class="lb_text_extrato">Qtde. Entregue:</div><div class="lb_text_extrato "><?php echo number_format($listagem[$i]->peso_aferido,3,',','.'); ?></div>
							</td>
						</tr>
					</table>
				</tr>
				
			</tbody>
		</table>
		
	</div>
	
</div>
	

<?php } ?>			
