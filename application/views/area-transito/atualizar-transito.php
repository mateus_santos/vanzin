<div class="m-content">	
    <form accept-charset="utf-8" action="<?php echo base_url('AreaTransito/atualizarTransito/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('AreaTransito/index'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('AreaTransito/index'); ?>">Voltar</a>						
						</h3>
						<h3 class="m-portlet__head-text">	
							&nbsp;&nbsp;&nbsp;&nbsp;Edição Trânsito Simplificado
						</h3>
					</div>
                    			
                </div> 
                <div class="m-portlet__head-tools">
                    
                </div>
            </div>
            <?php 

                $dt_ini = $dados[0]->dthr_atracacao;
                $dt_hr = explode(" ",trim($dt_ini));
                $dt = explode("-",$dt_hr[0]);
                $dt_ini = $dt[2].'/'.$dt[1].'/'.$dt[0].' '.$dt_hr[1];

                if(!empty($dados[0]->dthr_desatracacao)){
                    $dt_fim = $dados[0]->dthr_desatracacao;
                    $dt_hr = explode(" ",trim($dt_fim));
                    $dtf = explode("-",$dt_hr[0]);
                    $dt_fim = $dtf[2].'/'.$dtf[1].'/'.$dtf[0].' '.$dt_hr[1];
                }else{
                    $dt_fim = null;
                }

                if(!empty($dados[0]->dthr_ini_operacao)){
                    $dt_ini_oper = $dados[0]->dthr_ini_operacao;
                    $dt_hr = explode(" ",trim($dt_ini_oper));
                    $dtf = explode("-",$dt_hr[0]);
                    $dt_ini_oper = $dtf[2].'/'.$dtf[1].'/'.$dtf[0].' '.$dt_hr[1];
                }else{
                    $dt_ini_oper = null;
                }

                if(!empty($dados[0]->dthr_fim_operacao)){
                    $dt_fim_oper = $dados[0]->dthr_fim_operacao;
                    $dt_hr = explode(" ",trim($dt_fim_oper));
                    $dtf = explode("-",$dt_hr[0]);
                    $dt_fim_oper = $dtf[2].'/'.$dtf[1].'/'.$dtf[0].' '.$dt_hr[1];
                }else{
                    $dt_fim_oper = null;
                }

            ?>
            <div class="m-portlet__body">		
                <div class="form-group m-form__group row">									
                    
                    <div class="col-lg-6">
                        <label>Período de Atracação:</label>	
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="dthr_atracacao" id="dthr_atracacao" value="<?php echo $dt_ini;?>" class="form-control m-input form_datetime" placeholder="data inicial de Atracação"style="width: 49%;float: left;" autocomplete='off' /> 
                            <input type="text" name="dthr_desatracacao" id="dthr_desatracacao" value="<?php echo $dt_fim;?>" class="form-control m-input form_datetime" placeholder="data de Desatracação"style="width: 49%;float: right;" autocomplete='off'/>						
                        </div>	
                    </div>	
                    <div class="col-lg-6">
                        <label>Período de Operação:</label>					
                        <div class="m-input-icon m-input-icon--right"><input type="hidden" name="id" id="id" value="<?php echo $dados[0]->id;?>" />
                            <input type="text" name="dthr_ini_operacao" id="dthr_ini_operacao" value="<?php echo $dt_ini_oper;?>" class="form-control m-input form_datetime" placeholder="data inicial de Operação"style="width: 49%;float: left;" autocomplete='off' /> 
                            <input type="text" name="dthr_fim_operacao" id="dthr_fim_operacao" value="<?php echo $dt_fim_oper;?>" class="form-control m-input form_datetime" placeholder="data final de Operação"style="width: 49%;float: right;" autocomplete='off'/>						
                        </div>	
                    </div>	
                </div>	
                <div class="form-group m-form__group row">
                    
                    <div class="col-lg-6" > 
                        <label class="">POP</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="nr_fundeio" id="nr_fundeio" class="form-control m-input" placeholder="" required value="<?php echo $dados[0]->nr_fundeio;?>">
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
                        </div>	
                    </div>
                    <div class="col-lg-6" >
                        <label class="">Navio:</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="navio" id="navio" class="form-control m-input" placeholder="" required value="<?php echo $dados[0]->navio;?>">
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
                        </div>	
                    </div>
                </div>	 			
                        
                
                <div class="form-group m-form__group row">
                    <div class="col-lg-6" >
                        <label class="">IMO</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="nr_imo" id="nr_imo" class="form-control m-input" placeholder=""  value="<?php echo $dados[0]->imo;?>">
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
                        </div>	
                    </div>
                    <div class="col-lg-6" id="campo_ie">
                        <label class="">Nr. Escala</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="nr_escala" id="nr_escala" class="form-control m-input" placeholder=""  value="<?php echo $dados[0]->nr_escala;?>">
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
                        </div>	
                    </div>	
                </div>	 
                <div class="form-group m-form__group row">									
                    <div class="col-lg-6">
                        <label>Terminal</label>
                        <?php if( $tipo_acesso == 'administrador geral' || $tipo_acesso == 'administrador empresas'){ ?>						
                            <div class="m-input-icon m-input-icon--right">
                                <select  name="id_terminal" id="id_terminal" class="form-control m-input" required>
                                    <option value="">	Selecione um terminal 	</option>
                                <?php foreach($terminais as $terminal) { ?>
                                    
                                    <option value="<?php echo $terminal['id'];?> " <?php if($terminal['id'] == $dados[0]->id_terminal){ echo "selected"; }  ?>><?php echo strtoupper($terminal['no_terminal']); ?></option>
                                <?php } ?>
                                </select>
                                <span class="m-input-icon__icon m-input-icon__icon--right">
                                    <span><i class="la la-building"></i></span>
                                </span>						
                            </div>
                        <?php }else{ ?>
                            <h4 class="m--font-warning"><?php echo $terminal; ?>	</h4>
                            <input type="hidden" name="id_terminal" id="id_terminal" value="<?php echo $id_terminal; ?>">
                        <?php } ?>
                        
                    </div>	
                    <div class="col-lg-6" >
                        <label class="">ENCERRAR OPERAÇÃO</label>
                        <div class="m-input-icon m-input-icon--right">
                            <select name="fl_encerrado" id="fl_encerrado" class="form-control">
                                <option value="N" <?=($dados[0]->fl_encerrado=='N') ? 'selected=selected' : ''?> >NÃO</option>
                                <option value="S" <?=($dados[0]->fl_encerrado=='S') ? 'selected=selected' : ''?>>SIM</option>
                            </select>
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
                        </div>  
                    </div>
                </div>	
                <div class="form-group m-form__group row">	
                    <div class="col-lg-12">
                        <div id="div_due"></div>
                        <table class="table" id="tabela_due" >
                            <thead>
                                <th style='width:15%;'>Nº DUE</th>
                                <th style='width:25%;'>Exportador</th>
                                <th style='width:8%;'>NCM</th>
                                <th style='width:9%;'>CE Mercante</th>
                                <th style='width:12%;'>Peso Manif.(kg)</th>
                                <th style='width:14%;'>Tipo Carga</th>
                                <th style='width:4%;'>Ordem</th>
                                <th style='width:10%;'>Status</th>
                                <th style='width:2%;'><img src='<?php echo base_url('./img_perfil/add_mini.png'); ?>' id='adiciona'></th>
                            </thead>
                            <tbody id="itens_due">
                                <?php  
                                
                                    foreach($dados as $i => $due){
                                        if($due->nr_due != ''){
                                ?>
                                <tr id='linha_<?php echo $i; ?>' >
                                    <td style=''>
                                        <input type='text' name='nr_due[]' id='nr_due_<?php echo $i; ?>' value='<?php echo $due->nr_due; ?>' class='form-control m-input campos_menor' required maxlength='20'/>    
                                    </td>
                                    <td style=''>
                                        <div><input type='text' style='width:80%; float:left;' name='exportador[]' id='exportador_<?php echo $i; ?>' autocomplete='off' value='<?php echo $due->razao_social; ?>' class='form-control m-input campos_menor' required/> 
                                            <input type='hidden'  name='id_empresa[]' id='id_empresa_<?php echo $i; ?>' value='<?php echo $due->id_empresa; ?>' />    
                                        <input type='button'  style='width:10%;  float:left;' id='btn_adiciona_cliente_<?php echo $i; ?>' value='+' class='form-control m-input campos_menor' /><div id='lista_<?php echo $i; ?>'></div></div>   
                                    </td>
                                    <td style=''>
                                        <input type='text' name='ncm[]' id='ncm_<?php echo $i; ?>' value='<?php echo $due->ncm; ?>' class='form-control m-input campos_menor' required maxlength='8'/>
                                    </td>
                                    <td style=''>
                                        <input type='text' name='ce_mercante[]' id='ce_mercante_<?php echo $i; ?>' value='<?php echo $due->ce_mercante; ?>' class='form-control m-input campos_menor'  maxlength='15'/>
                                    </td>
                                    <td style=''>
                                        <input type='text' name='peso[]' id='peso_<?php echo $i; ?>' value='<?php echo intval($due->peso_manif); ?>' class='form-control m-input peso campos_menor' required maxlength='15' />
                                    </td>
                                    <td style=''>
                                        <select name='tipo[]' id='tipo_<?php echo $i; ?>' class='form-control m-input campos_menor' required >
                                        <option value="">	Selecione o tipo 	</option>
                                        <?php foreach($tipo_carga as $tipo) { ?>
                                            
                                            <option value="<?php echo $tipo['id'];?> " <?php if($tipo['id'] == $due->id_tipo){ echo "selected"; }  ?>><?php echo mb_strtoupper($tipo['no_tipo']); ?></option>
                                        <?php } ?>
                                        </select>
                                    </td>
                                    <td style=''> 
                                        <input type='text' name='ordem[]' id='ordem_<?php echo $i; ?>' value='<?php echo $due->nr_ordem; ?>' class='form-control m-input ordem campos_menor' maxlength=1 required/>
                                    </td>
                                    <td style=''> 
                                        <select name='status[]' id='status_<?php echo $i; ?>' class='form-control m-input campos_menor' required >
                                            <option value="">	Selecione o tipo 	</option>
                                            <option value="S" <?php if($due->fl_aberta == 'ABERTA'){ echo "selected"; }  ?>>ABERTA</option>
                                            <option value="N" <?php if($due->fl_aberta == 'ENCERRADA'){ echo "selected"; }  ?>>ENCERRADA</option>
                                        </select>    
                                    </td>
                                    <td style='width:2%;'><img src='<?php echo base_url('./img_perfil/exc_mini.png'); ?>' id='exclui_<?php echo $i; ?>' onclick='exclui_linha(<?php echo $i; ?>)'></td>
                                </tr>
                                <?php
                                        } 
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
                                <button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
                            </div>
                        </div>
                    </div>
                </div>					
            </div>
            
        </div>
    </form>
</div>
	<?php echo $this->session->flashdata('erro');  ?>	
<?php if ($this->session->flashdata('erro') == 'erro_aberto'){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Existe mais de uma DUE aberta por cliente, por favor verifique e tente novamente!',
            'error'
        );
    </script>
<?php }elseif ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Já Existe Algum Movimento lançado ou aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Trânsito Simplificado salvo com Sucesso!',
                type: 'success'
            }).then(function() {
                //window.location = base_url+'AreaAdministrador/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>