<div class="m-content">	
    <form accept-charset="utf-8" action="<?php echo base_url('AreaTransito/cadastrarTransito/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="la la-building"></i>
                        </span>
                        <h3 class="m-portlet__head-text"> 
                            Novo Trânsito Simplificado
                        </h3>
                    </div>			
                </div> 
                <div class="m-portlet__head-tools">
                    
                </div>
            </div>
                    
            <div class="m-portlet__body">		
                <div class="form-group m-form__group row">									
                        
                    <div class="col-lg-6">
                        <label>Período de Atracação:</label>	
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="dthr_atracacao" id="dthr_atracacao" value="" class="form-control m-input form_datetime" placeholder="data inicial de Atracação"style="width: 49%;float: left;" autocomplete='off' /> 
                            <input type="text" name="dthr_desatracacao" id="dthr_desatracacao" value="" class="form-control m-input form_datetime" placeholder="data de Desatracação"style="width: 49%;float: right;" autocomplete='off'/>						
                        </div>	
                    </div>	
                    <div class="col-lg-6">
                        <label>Período de Operação:</label>					
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="dthr_ini_operacao" id="dthr_ini_operacao" value="" class="form-control m-input form_datetime" placeholder="data inicial de Operação"style="width: 49%;float: left;" autocomplete='off' /> 
                            <input type="text" name="dthr_fim_operacao" id="dthr_fim_operacao" value="" class="form-control m-input form_datetime" placeholder="data final de Operação"style="width: 49%;float: right;" autocomplete='off'/>						
                        </div>	
                    </div>	
         			
                </div>	
                <div class="form-group m-form__group row">  
                    <div class="col-lg-6" > 
                        <label class="">POP</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="nr_fundeio" id="nr_fundeio" class="form-control m-input" placeholder="" required value="">
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
                        </div>	
                    </div>
                    <div class="col-lg-6" >
                        <label class="">Navio:</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="navio" id="navio" class="form-control m-input" placeholder="" required value="">
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
                        </div>	
                    </div>
                </div>	 			
                        
                
                <div class="form-group m-form__group row">
                    <div class="col-lg-6" > 
                        <label class="">IMO</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="nr_imo" id="nr_imo" class="form-control m-input" placeholder=""  value="">
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
                        </div>	
                    </div>
                    <div class="col-lg-6" id="campo_ie">
                        <label class="">Nr. Escala</label>
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" name="nr_escala" id="nr_escala" class="form-control m-input" placeholder=""  value="">
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
                        </div>	
                    </div>	
                </div>

                 
                <div class="form-group m-form__group row">									
                    <div class="col-lg-12">
                        <label>Terminal</label>
                        <?php if( $tipo_acesso == 'administrador geral' || $tipo_acesso == 'administrador empresas'){ ?>						
                            <div class="m-input-icon m-input-icon--right">
                                <select  name="id_terminal" id="id_terminal" class="form-control m-input" required>
                                    <option value="">	Selecione um terminal 	</option>
                                <?php foreach($terminais as $terminal) { ?>
                                    <option value="<?php echo $terminal['id']; ?>"><?php echo strtoupper($terminal['no_terminal']); ?></option>
                                <?php } ?>
                                </select>
                                <span class="m-input-icon__icon m-input-icon__icon--right">
                                    <span><i class="la la-building"></i></span>
                                </span>						
                            </div>
                        <?php }else{ ?>
                            <h4 class="m--font-warning"><?php echo $terminal; ?>	</h4>
                            <input type="hidden" name="id_terminal" id="id_terminal" value="<?php echo $id_terminal; ?>">
                        <?php } ?>
                        
                    </div>	
                </div>	
                <div class="form-group m-form__group row" style="display:none;">	
                    <div class="col-lg-12">
                        <div id="div_due"></div>
                        <table class="table" id="tabela_due" >
                            <thead>
                                <th>Nº DUE</th>
                                <th style='width:40%;'>Exportador</th>
                                <th>NCM</th>
                                <th>Peso Manif.(kg)</th>
                                <th>Tipo Carga</th>
                                <th>Ordem</th>
                                <th>Status</th>
                                <th style='width:2%;'><img src='../img_perfil/add_mini.png' id='adiciona'></th>
                            </thead>
                            <tbody id="itens_due">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
                                <button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
                            </div>
                        </div>
                    </div>
                </div>					
            </div>
            
        </div>
    </form>
</div>
		

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Orçamento solicitado com sucessoo, em breve entraremos em contato!',
                type: 'success'
            }).then(function() {
                //window.location = base_url+'AreaAdministrador/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>