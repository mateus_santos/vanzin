<div class="m-content">
	<!-- begin Portlet -->
	<form accept-charset="utf-8" action="<?php echo base_url('AreaTransito/ajusteTransito/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
		<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<i class="la la-calendar" style="color: #464e3f;" ></i>
						</span>
						<h3 class="m-portlet__head-text" style="color: #000;">
							Ajuste de Saldo - Trânsito Simplificado 
						</h3>	
																
					</div>
	
				</div>
				<div class="m-portlet__head-tools"></div>
			</div>				
			<div class="m-portlet__body" >	
				<div class="form-group m-form__group row">  
					<div class="col-lg-6" > 
						<label class="">DUE</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="nr_due" id="nr_due" class="form-control m-input" placeholder="" required value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
						</div>	
					</div>
					<div class="col-lg-6" >
						<label class="">ID:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="id_transito" id="id_transito" class="form-control m-input" placeholder="" readonly style="background-color:#DCDCDC;">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
						</div>	
					</div>
				</div>			
				<div class="form-group m-form__group row">  
					
					<div class="col-lg-12" >
						<label class="">Navio:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="navio" id="navio" class="form-control m-input" placeholder="" readonly style="background-color:#DCDCDC;">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
						</div>	
					</div>
				</div>	
				<div class="form-group m-form__group row">  
					
					<div class="col-lg-12" >
						<label class="">Exportador:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="razao_social" id="razao_social" class="form-control m-input" placeholder="" readonly style="background-color:#DCDCDC;">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
						</div>	
					</div>
				</div>	
				<div class="form-group m-form__group row">								
					<div class="col-lg-6">
						<label>Peso:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="peso" id="peso" class="form-control m-input" placeholder="Peso" require style="width: 300px;" />
						</div>				
					</div>			
					<div class="col-lg-6">

						<label class="m-radio">
							<input type="radio" name="entrega" id="recepcao"  class="ativo"  value="0" require>Recepção &nbsp;
							<span></span>
						</label>									
						<label class="m-radio">
							<input type="radio" name="entrega" id="entrega" class="ativo"  value="1" require >Entrega &nbsp;
							<span></span>
						</label>								
						
					</div>
				
					
				</div>
				<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="button" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>	
			</div>					
		</div>
	</form>
	<!--end::Portlet-->	

	
</div>
 
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Ajuste Realizado com Sucesso!',
                type: 'success'
            }).then(function() {
                //window.location = base_url+'AreaAdministrador/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>