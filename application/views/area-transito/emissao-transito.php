<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
                        Emissão de Extrato de Trânsito Simplificado 	
					</h3>
					
				</div>
			</div>					
		</div>
		
    <div id="msg" class="msg" style="font-weight:bold; font-size:110%;" ></div>

    <form accept-charset="utf-8" action="<?php echo base_url('AreaTransito/emiteExtrato/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario" target="_blank">	
        <div class="tabela" id="tab_placa" style="border:0px; margin-bottom:3%; margin-top:2%;">
            <div class="linha" >
                <div class="coluna " style="width:40%; border:0px; ">
                    <label  class="placa">Placa:</label>
                </div>
                <div class="coluna " style="text-align:left;  ">
                    <div style='float:left;'>
                        <input type="text" class='form-control m-input hora' id="placa" name="placa" placeholder='' required>
                     </div>
                     
                </div>
                
            </div>
            
        </div>

        <div class="tabela"  style="border:0px; ">
            
            <div class="linha" style="border:0px; ">
                <div class="coluna " style="width:40%; border:0px; ">
                    <label  class="placa">Reimprimir:</label>
                </div>
                <div class="coluna " style="text-align:left;border:0px;   ">
                    <div>
                       <input type='checkbox' name='fl_reimprimir' id='fl_reimprimir' class='form-control m-checkbox checa'  value="1">
                    </div>   
                </div>
                
            </div>
        </div>
        <div class="tabela " style='border: 0px; margin-top:1%;' id="tab_botao_saida">
            <div class="linha">
                <div class="coluna_botoes">
                    <button type='submit' class='btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase' id='btn_emite'>Emitir</button>&nbsp; 
                </div>
            </div>
        </div>	        
        </div>	
    </form>
</div>