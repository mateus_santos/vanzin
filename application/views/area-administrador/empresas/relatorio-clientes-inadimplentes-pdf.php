<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

	<style type="text/css">

		body {
			background-color: #fff;
			margin: 0 auto;
			position: relative;
			font-family: Lucida Grande, Verdana, Sans-serif;
			font-size: 12px;
			color: #333;
			width: 100%;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 14px; 
			font-weight: bold;
			margin: 24px 0 2px 0; 	
		}

		.cabecalho{
			margin: 0 auto;
			position: relative;
		}

		table{	
			padding: 0;
			width: 100%;
			border: 1px solid #ffcc00;

		}

		table tr{	

		}

		table td{	
			border-bottom: 1px solid #f0f0f0;	
			font-size: 9px;	
			padding: 5px;
		}

	</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
				CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
				Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
				07400-230 - Arujá - SP<br>
				Tel: +55 11 3900-2565 | www.wertco.com.br<br>
			</p>
		</div>
		<div class="corpo">	
			<h3 style="text-align: center;margin-top: 15px;	">Relatório Clientes Inadimplentes</h3>	
			<?php if (count($dados) > 0 )  { ?>	
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;border: 1px solid #ffcc00;">
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>#ID</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>CNPJ</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Razão Social</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Cidade</b></td>	
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Estado</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Pedidos</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Situação</b></td>				
				</tr>				
				<?php  foreach($dados as $dado){ ?>
					<tr>
						<td style="border-top: 1px solid #ccc;"> <?php echo $dado['id']?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo $dado['cnpj']?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo mb_strtoupper($dado['razao_social']); ?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo $dado['cidade']?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo $dado['estado']?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo $dado['pedidos']?> </td>
						<td style="border-top: 1px solid #ccc;"> Inadimplente </td>
					</tr>
				<?php 						
				} 
				?>				
			</table>		
			<?php }else{ ?>
				<hr/>				
			<h2 style="text-align: center;">Nenhum resultado encontrado</h5>	
				<hr/>
			<?php }	?>
		</div>	
	</div>
</body>
</html>