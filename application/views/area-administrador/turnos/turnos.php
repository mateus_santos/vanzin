<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-calendar" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Cadastrar Turnos 
					</h3>	
					<h3 class="m-portlet__head-text" style=" padding-left: 10px;" >
						<a href="<?php echo base_url('AreaAdministrador/cadastrarTurno'); ?>"  style="color: #ffcc00; font-weight: bold;" data-toggle="m-tooltip" data-placement="top" title="Novo Turno" id="Novo Turno">
							<i class="la la-plus-circle" style="font-size: 38px;"></i>
						</a>
					</h3>									
				</div>

			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
							
	</div>
	<!--end::Portlet-->	

	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" >
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title" style="text-align: center;margin: 0 auto !important;">
					
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
							Turnos
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">#</th>
						<th style="width: 40%;">Turno</th>						
						<th style="width: 20%;">Hora Inicial</th>						
						<th style="width: 20%;">Hora Final</th>	
						<th style="width: 10%;">Ações</th>
					</tr>
				</thead>
				<tbody>					
				<?php foreach($turnos as $turno){
					$turno_inicial 	= substr($turno['hora_inicial'],0,2).':'.substr($turno['hora_inicial'],2,2);
					$hora_final 	= substr($turno['hora_final'],0,2).':'.substr($turno['hora_final'],2,2);	
				?>
					<tr>
						<td style="text-align: center; "><?php echo $turno['id']; ?></td>
						<td ><?php echo $turno['nome']; ?></td>											
						<td ><?php echo $turno_inicial; ?></td>
						<td ><?php echo $hora_final; ?></td>	
						<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;background: ">
							<a href="<?php echo base_url('AreaAdministrador/editarTurno/'.$turno['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="">
								<i class="la la-edit"></i>
							</a>
							<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" target="blank" onclick="excluirTurno(<?=$turno['id']?>)">
								<i class="la la-trash"></i>
							</a>				
						</td>
					</tr>
					<?php } ?> 
				</tbody>
			</table>			
		</div>
	</div>

	
	<!--end::Portlet-->	
</div>	
<div class="modal fade" id="m_lista_anexo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_lista_anexo_title" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
				<div class="row anexos">
					
				</div>				
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<!-- end:: Body -->
<?php if ($this->session->flashdata('retorno') == 'erro'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "Atenção!",
           	text: '<?php echo $this->session->flashdata('msg'); ?>',
           	type: "warning"
        }).then(function() {
		   	
		});
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('retorno') == 'sucesso'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Turno realizado com sucesso!',
           	type: "success"
        }).then(function() { });
	</script>	
<?php unset($_SESSION['sucesso']); } ?>
