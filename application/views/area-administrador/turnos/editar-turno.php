<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">Edição Turno</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarTurno');?>" method="post"  enctype="multipart/form-data">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>Nome Turno:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required	name="nome"	class="form-control m-input" placeholder="" required value="<?php echo $dados[0]['nome'];?>">
								<input type="hidden" name="id" 	class="form-control m-input" placeholder="" required value="<?php echo $dados[0]['id'];?>">		
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
								</span>
							</div>
						</div>
						
					</div>	 
					<div class="form-group m-form__group row">
                    <div class="col-lg-6">
							<label class="">Hora Inicial:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" id="hora_inicial"   name="hora_inicial" class="form-control m-input hora" placeholder="Insira a Hora Inicial" required value="<?php echo $dados[0]['hora_inicial'];?>" maxlength="5">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-clock-o"></i></span></span>
							</div>	
						</div>
						<div class="col-lg-6">
							<label class="">Hora Final:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="hora_final" required id="hora_final" class="form-control m-input hora" placeholder="Insira Hora Final" value="<?php echo $dados[0]['hora_final'];?>" maxlength="5">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-clock-o"></i></span></span>
							</div>
							
						</div>
					</div>	 														                
	            </div>	            	
				
				
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "OK!",
               	text: 'Sucesso',
               	type: "success"
        }); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>