<table class="" id="html_table"  >
	<thead>
		<tr>
			<th>Nº NF</th>
			<th>Data/Hora Envio</th>						
			<th>Cod. Retorno</th>
			<th>Descrição</th>	
			<th>Chave de Acesso</th>					
		</tr>
	</thead>
	<tbody>					
	<?php foreach($notas as $nota){	?>
		<tr>
			<td  style="text-align: center;"><?php echo $nota['nr_nf'].'-'.$nota['serie_nf']; ?></td>
			<td><?php echo $nota['dthr_envio']; ?></td>
			<td><?php echo $nota['cd_retorno']; ?></td>
			<td><?php echo $nota['descricao']; ?></td>
			<td><?php echo $nota['chave_acesso']; ?></td>
		</tr>
		<?php } ?> 
	</tbody>
</table>