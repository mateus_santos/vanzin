<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-calendar" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Consultar notas fiscais rececpcionadas CCT
					</h3>														
				</div>
			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Período:</label>					
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="dt_ini" id="dt_ini"  class="form-control m-input datepicker" placeholder="data inicial"style="width: 49%;float: left;" /> 
						<input type="text" name="dt_fim" id="dt_fim"  class="form-control m-input datepicker" placeholder="data final"style="width: 49%;float: right;"/>
					</div>
				</div>
				<div class="col-lg-6">
					<label>Nº Nota Fiscal:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="nr_nf" id="nr_nf" class="form-control m-input" placeholder="N° Nota" style="width: 25%;float: left;" />
						<input type="text" name="serie_nf" id="serie_nf" class="form-control m-input" placeholder="Série" style="width: 15%;"  />  
					</div>
				</div>
			</div>
			<div class="form-group m-form__group row">
				<div class="col-lg-6">
					<label>Chave Acesso:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="chave" id="chave" class="form-control m-input" placeholder="Chave de Acesso"  />
					</div>
				</div>
				<div class="col-lg-6">
					<label>Status Recepção:</label>
					<div class="m-input-icon m-input-icon--right">
						<select class="form-control" name="status" id="status" style="width: 40%;float: left;">
							<option value="">TODOS</option>
							<option value="1">SUCESSO</option>
							<option value="0">ERRO</option>
						</select>
					</div>
				</div>					
			</div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="button" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Filtrar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom limpar">Limpar</button>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" >
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text"></h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
							Notas Fiscais
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->
			<table class="" id="html_table"  >
				<thead>
					<tr>
						<th>Nº NF</th>
						<th>Data/Hora Envio</th>						
						<th>Cod. Retorno</th>
						<th>Descrição</th>	
						<th>Chave de Acesso</th>					
					</tr>
				</thead>
				<tbody>					
				<?php foreach($notas as $nota){	?>
					<tr>
						<td  style="text-align: center;"><?php echo $nota['nr_nf'].'-'.$nota['serie_nf']; ?></td>
						<td><?php echo $nota['dthr_envio']; ?></td>
						<td><?php echo $nota['cd_retorno']; ?></td>
						<td><?php echo $nota['descricao']; ?></td>
						<td><?php echo $nota['chave_acesso']; ?></td>
					</tr>
					<?php } ?> 
				</tbody>
			</table>			
		</div>
	</div>	
	<!--end::Portlet-->	
</div>	
