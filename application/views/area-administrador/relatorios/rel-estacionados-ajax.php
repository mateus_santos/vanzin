<table class="lista_resumido" id="html_table_res"  >
	<thead >
		<tr >
			<th colspan="2" class="formata_celula" style="">Caminhões Por Cliente</th>		
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="td_clientes">
				<table id="resultado_resumido" style="width:100%;">
					<tr>
						<?php
						if (count($ag_resumido) > 0){
							$porc_coluna = (100 / count($ag_resumido) );
						}
						?>
						<?php foreach($ag_resumido as $res_tit){	?>
							
							<td class="formata_celula" style="width:<?php echo $porc_coluna; ?>%"><?php echo substr($res_tit['razao_social'],0,20); ?></td>
							
						<?php } ?>
					</tr>
					<tr>
						<?php 
							$soma_total = 0;
							foreach($ag_resumido as $resumido){	 
						?>
							
							<td class="formata_celula" ><?php echo $resumido['total'];  ?></td>
							
						<?php 
							$soma_total = $soma_total + $resumido['total'];
							} 
						?>
					</tr>
				</table>
			</td>
			<td class="td_total">
				<table id="resultado_resumido_tot" style="width:100%">
					<tr>
						<td class="formata_celula" >Total</td>
					</tr>
					<tr>
						<td class="formata_celula" ><?php echo $soma_total;  ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</br>
<table class="" id="html_table" width="150%">
<thead>
	<tr>
		<th class="formata_celula" style="width: 5%;  ">Dia Cota</th>
		<th class="formata_celula" style="width: 20%;">Cliente</th>	
		<th class="formata_celula" style="width: 10%; ">Produto</th>
		<th class="formata_celula" style="width: 5%; ">Placa</th>
		<th class="formata_celula" style="width: 5%; ">Status</th>
		<th class="formata_celula" style="width: 5%; ">NF</th>
		<th class="formata_celula" style="width: 3%; ">Qtd.</th>
		<th class="formata_celula" style="width: 3%; ">Emb.</th>
		<th class="formata_celula" style="width: 10%; ">Motorista</th>	
		<th class="formata_celula" style="width: 7%; ">CPF</th>
		<th class="formata_celula" style="width: 7%; ">Telefone</th>				
		<th class="formata_celula" style="width: 5%; ">Agendamento</th>
		<th class="formata_celula" style="width: 5%; ">Data Entrada</th>
		<th class="formata_celula" style="width: 5%; ">Data Classificada</th>
		<th class="formata_celula" style="width: 5%; ">Data Saída</th>
		<!--	<th style="width: 5%;">Ações</th>-->			
	</tr>
</thead>
<tbody>					
	<?php foreach($agendamentos as $agendamento){	?>
		<tr>
        	<td class="formata_celula" ><?php echo $agendamento['dia']; ?></td>
			<td class="formata_celula" ><?php echo $agendamento['razao_social']; ?></td>	
			<td class="formata_celula"><?php echo $agendamento['descricao']; ?></td>										
			<td class="formata_celula"><?php echo $agendamento['placa']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['fl_atende_classific']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['nr_nf']; ?></td>
			<td class="formata_celula" style="text-align:right; padding-right:5px; "><?php echo $agendamento['quant_total']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['no_embalagem']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['nome']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['cpf']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['fone']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['dt_agenda']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['dthr_entrada']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['dthr_classific']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['dthr_saida']; ?></td>
		<!--	<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
				<a href="<?php echo base_url('AreaClientes/editarAgendamento/'.$agendamento['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-edit"></i>
				</a>				
			</td>-->
		</tr>
		<?php } ?> 
	</tbody>
</table>	