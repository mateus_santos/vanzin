<style type="text/css">
@keyframes bg {
	0% {
	    background-size:    0 3px,
	                        3px 0,
	                        0 3px,
	                        3px 0;
	}
	25% {
	    background-size:    100% 3px,
	                        3px 0,
	                        0 3px,
	                        3px 0;
	}
	50% {
	    background-size:    100% 3px,
	                        3px 100%,
	                        0 3px,
	                        3px 0;
	}
	75% {
	    background-size:    100% 3px,
	                        3px 100%,
	                        100% 3px,
	                        3px 0;
	}
	100% {
	    background-size:    100% 3px,
	                        3px 100%,
	                        100% 3px,
	                        3px 100%;
	}
}
div.mostrar_saldo {
    width: 100%;    
    margin: 2rem auto;
    padding: 2em;
    
    background-repeat: no-repeat;
    background-image:   linear-gradient(to right, #ffcc00 100%, #ffcc00 100%),
                        linear-gradient(to bottom, #ffcc00 100%, #ffcc00 100%),
                        linear-gradient(to right, #ffcc00 100%, #ffcc00 100%),
                        linear-gradient(to bottom, #ffcc00 100%, #ffcc00 100%);
    background-size:    100% 3px,
                        3px 100%,
                        100% 3px,
                        3px 100%;
    background-position:    0 0,
                            100% 0,
                            100% 100%,
                            0 100%;
    animation: bg 1.25s cubic-bezier(0.19, 1, 0.22, 1) 1;
    animation-play-state: paused;

}


</style>
<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-calendar" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Consultar Entrega de Cargas DUE
					</h3>														
				</div>
			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Período:</label>					 
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="dt_ini" id="dt_ini"  class="form-control m-input datepicker" placeholder="data inicial"style="width: 49%;float: left;" /> 
						<input type="text" name="dt_fim" id="dt_fim"  class="form-control m-input datepicker" placeholder="data final"style="width: 49%;float: right;"/>
					</div>
				</div>	
				<div class="col-lg-6">
					<label>Placa:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="placa" id="placa" class="form-control m-input" placeholder="Placa" style="" />
					</div>
				</div>				
			</div>
			<div class="form-group m-form__group row">				
				<div class="col-lg-6">
					<label>CPF:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="cpf" id="cpf" class="form-control m-input" placeholder="CPF" />
					</div>
				</div>
				<div class="col-lg-6">
					<label>Status Entregas:</label>
					<div class="m-input-icon m-input-icon--right">
						<select class="form-control" name="status" id="status" style="width: 40%;float: left;">
							<option value=" ">TODAS</option>
							<option value="1">SUCESSO</option>
							<option value="0">ERRO</option>
						</select>
					</div>
				</div>
			</div>
			<hr style="border-top: 1px solid #ffcc00;" />
			<div class="form-group m-form__group row">
				<div class="col-lg-6">
					<label>DUE:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="due" id="due" class="form-control m-input" placeholder="DUE" style="max-width: 70%;float: left;"  />
						<button class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom" id="calcular_peso" style="max-width: 30%;margin-left: 35px;float: right;">Saldo</button>
					</div>
				</div>
				<div class="col-lg-6 ">
					<div class="mostrar_saldo">
						
					</div>							
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="button" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Filtrar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom limpar">Limpar</button>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" >
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text"></h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
							Entregas
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->
			<table class="" id="html_table"  >
				<thead>
					<tr>
						<th>DUE</th>
						<th>Data/Hora Envio</th>						
						<th>Placa</th>
						<th>CPF Motorista</th>	
						<!--<th>Cod. Retorno</th>-->
						<th>Descricao</th>		
					</tr>
				</thead>
				<tbody>					
				<?php foreach($entregas as $entrega){	?>
					<tr>
						<td  style="text-align: center;"><?php echo $entrega['nr_due']; ?></td>
						<td><?php echo $entrega['dthr_envio']; ?></td>
						<td><?php echo $entrega['placa']; ?></td>
						<td><?php echo $entrega['cpf_motorista']; ?></td>
						<!--<td><?php echo $entrega['cd_retorno']; ?></td>-->
						<td><?php echo $entrega['descricao']; ?></td>
					</tr>
					<?php } ?> 
				</tbody>
			</table>			
		</div>
	</div>	
	<!--end::Portlet-->	
</div>	
