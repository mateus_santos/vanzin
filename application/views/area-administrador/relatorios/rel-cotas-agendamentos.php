<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-calendar" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Consulta Cotas X Agendamentos 
					</h3>	
														
				</div>

			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Dia Agendamento:</label>					
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="dt_ini" id="dt_ini" value="<?php echo date('d/m/Y'); ?>" class="form-control m-input datepicker" placeholder="data inicial"style="width: 49%;float: left;" /> 					
					</div>				
				</div>			
				<div class="col-lg-6">
					<label>Empresas:</label>
					<div class="m-input-icon m-input-icon--right">
						<select name="empresa_id" id="empresa_id" class="form-control m-input" >
							<option value="">	Selecione uma empresa 	</option>
							<?php foreach($empresas as $empresa) {?>
								<option value="<?php echo $empresa['id']; ?>"><?php echo strtoupper($empresa['razao_social']); ?></option>
							<?php } ?>
						</select>
					</div>				
				</div>				
			</div>	
			
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="button" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Filtrar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							<input type="hidden" id="filtros" value=" and agendamento.dt_agenda = '<?=date('Y-m-d')?>'" />
						</div>							
					</div>
				</div>
			</div>	
		</div>						
	</div>
	<!--end::Portlet-->	

	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" >
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
							Agendamentos
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>		
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->
			<table class="" id="html_table" >
				<thead>
					<tr>	
						<th>Dia</th>	
						<th>Cliente</th>						
						<th>Possui Cota</th>	
						<th>Peso Cota</th>
						<th>Total Agendado</th>
						<th>Saldo</th>
					</tr>
				</thead> 
				<tbody>					
					<?php $total_cotas = $total_agendado = $total_saldo = 0; foreach($agendamentos as $agendamento){	
						
						if(!empty($agendamento['agendado'])){
							$peso_agendado 	= explode('.', $agendamento['agendado']);
						}else{
							$peso_agendado 	= 0;
						}
						if(!empty($agendamento['saldo'])){
							$saldo 			= explode('.', $agendamento['saldo']);
						}else{
							$saldo = 0;
						}
						
						$peso_agendado 	= explode('.', $agendamento['agendado']);
						$saldo 			= explode('.', $agendamento['saldo']);
						
						$total_cotas 	= $total_cotas + $agendamento['peso'];
						$total_agendado = $total_agendado + $peso_agendado[0];
						$total_saldo 	= $total_saldo + $saldo[0];	
					?>
					<tr>
						<td><?=date('d/m/Y',strtotime($agendamento['dt_agenda']))?></td>
						<td><?php echo $agendamento['empresa']; ?></td>
						<td><?php echo ($agendamento['fl_sem_cota'] == 0) ? 'COM COTA' : 'SEM COTA'; ?></td>	
						<td class="qtd"  style=""><?php echo $agendamento['peso']; ?></td>											
						<td class="qtd" style=""><?php echo $peso_agendado[0]; ?></td>
						<td class="qtd" style=""><?php echo $saldo[0]; ?></td>
					</tr>
					<?php } ?> 

				</tbody>
			</table>
			<table class="table">
				<thead>
					<tr>
						<th colspan="4" style="text-align: right;background: #ffcc00;">Total de Cotas</th>
						<th style="text-align: right;background: #ffcc00;">Total de Agendamentos</th>
						<th style="text-align: right; background: #ffcc00;">Saldo Total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="qtd" colspan="4" style="text-align: right;"><?=$total_cotas?></td>
						<td class="qtd" style="text-align: right;"><?=$total_agendado?></td>
						<td class="qtd" style="text-align: right;"><?=$total_saldo?></td>
					</tr>
				</tbody>				
			</table>
		</div>
	</div>

	
	<!--end::Portlet-->	
</div>	
<div class="modal fade" id="m_listar_agendamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_listar_title" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 850px;">					
				<div id="tabela_empresa" class="row" >
					
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<!-- end:: Body -->
<?php if ($this->session->flashdata('retorno') == 'erro'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "Atenção!",
           	text: '<?php echo $this->session->flashdata('msg'); ?>',
           	type: "warning"
        }).then(function() {
		   	
		});
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('retorno') == 'sucesso'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Agendamento realizado com sucesso!',
           	type: "success"
        }).then(function() { });
	</script>	
<?php unset($_SESSION['sucesso']); } ?>
