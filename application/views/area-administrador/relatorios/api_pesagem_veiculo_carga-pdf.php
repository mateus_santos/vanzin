<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{	
	border-bottom: 1px solid #f0f0f0;	
	font-size: 9px;	
	padding: 2px;
}
.t_cabecalho td{
	border-bottom: none;	
	border: none;
	text-align: center;
	width: 33%;
	padding: 0px;
	margin: 0px;
	font-size: 11px;
	font-weight: 600;
}
.rodape{
	position: absolute;
	bottom: 0px;
}

</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/logo_vanzin.jpg" style="width: 110px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> VANZIN TERMINAIS E SERVICOS ADUANEIROS LTDA.<br></p>
		</div>
		<div class="corpo">				
			
			<table style="border: none;" class="t_cabecalho" style="width: 100%;">
				<tr>
					<td colspan="3" style="text-align: center;">
						<h3 style="text-align: center;margin-top: 15px;	">Relatório Controle Pesagem Veiculo Carga - API RECINTOS</h3>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;">&nbsp;
					
					</td>
					<td style="text-align: center;">
					<?php  if($periodo != ''){  ?>			
						Período <?php echo $periodo; ?>
					<?php } ?>
					</td>
					<td style="text-align: right;">
						<?php echo date('d/m/Y H:i:s'); ?>
					</td>
				</tr>
			</table>
			<hr  />
			<?php if(count($dados) > 0){ ?>
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;">
				<thead>
					<tr>
						<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>#ID</b></td>
						<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>TP. OPERAÇÃO</b></td>
						<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>TARA</b></td>
						<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>PESO BRUTO</b></td>
						<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>PLACA</b></td>		
						<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>DTHR. OCORRÊNCIA</b></td>									
						<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>RETORNO</b></td>
					</tr>				
				</thead>

				<?php foreach($dados as $dado){ ?>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan=""><?=$dado['id_evento']?></td>
					<td style="border: 1px solid #ccc;text-align: right;" colspan=""><?=($dado['tp_operacao'] == 'I') ? 'Inserção' : 'Correção'?></td>
					<td style="border: 1px solid #ccc;text-align: right;" colspan=""><?=$dado['tara_conjunto']?></td>
					<td style="border: 1px solid #ccc;text-align: right;" colspan=""><?=$dado['peso_bruto']?></td>
					<td style="border: 1px solid #ccc;text-align: right;" colspan=""><?=$dado['placa']?></td>
					<td style="border: 1px solid #ccc;text-align: right;" colspan=""><?=date('d/m/Y H:i:s',strtotime($dado['dthr_registro']))?></td>
					<td style="border: 1px solid #ccc;text-align: right;" colspan=""><?=$dado['retorno']?></td>

				</tr>
				<?php } ?>
				
			</table>
			<?php }else{ ?>
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc; margin-top: 300px;" />								
				<h2 style="text-align: center;">Nenhum resultado encontrado!</h2>
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc;" />						
			<?php } ?>
		</div>	
	</div>
</body>
</html>