<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-calendar" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Consulta Agendamentos 
					</h3>	
														
				</div>

			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Período:</label>					
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="dt_ini" id="dt_ini" value="<?php echo date('d/m/Y'); ?>" class="form-control m-input datepicker" placeholder="data inicial"style="width: 49%;float: left;" /> 
						<input type="text" name="dt_fim" id="dt_fim" value="<?php echo date('d/m/Y'); ?>" class="form-control m-input datepicker" placeholder="data final"style="width: 49%;float: right;"/>						
					</div>				
				</div>
				<div class="col-lg-6">
					<label>Nota Fiscal:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="nr_nf" id="nr_nf" class="form-control m-input" placeholder="Nota" style="width: 300px;" /> 
					</div>				
				</div>
			</div>	
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Placa:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="placa" id="placa" class="form-control m-input" placeholder="placa" style="width: 300px;" />
					</div>				
				</div>			
			<?php if($tipo_acesso == 'administrador geral' || $tipo_acesso == 'gate'){ ?>
			
				<div class="col-lg-6">
					<label>Empresas:</label>
					<div class="m-input-icon m-input-icon--right">
						<select name="empresa_id" id="empresa_id" class="form-control m-input" >
							<option value="">	Selecione uma empresa 	</option>
							<?php foreach($empresas as $empresa) {?>
								<option value="<?php echo $empresa['id']; ?>"><?php echo strtoupper($empresa['razao_social']); ?></option>
							<?php } ?>
						</select>
					</div>				
				</div>
			
			<?php } ?>	
			</div>
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Turno:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="id_turno" id="id_turno" >
							<option value="">Selecione o turno</option>
							<?php foreach($turnos as $linha_turno){?>
								<option value="<?php echo $linha_turno['id']; ?>"><?php echo $linha_turno['nome']; ?></option>
							<?php } ?>	
						</select>
					</div>				
				</div>			
			
			</div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="button" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Filtrar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							<input type="hidden" id="filtros" value=" and agendamento.dt_agenda = '<?=date('Y-m-d')?>'" />
						</div>							
					</div>
				</div>
			</div>	
		</div>						
	</div>
	<!--end::Portlet-->	

	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" >
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
							Agendamentos
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="rel_resumido" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded rel_resumido" >
			<table class="lista_resumido" id="html_table_res"  >
				<thead >
					<tr >
						<th colspan="2" class="formata_celula" style="">Caminhões Que Não Entraram - Por Cliente </th>		
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="td_clientes">
							<table id="resultado_resumido" style="width:100%;">
								<tr>
									<?php
										if (count($ag_resumido) > 0){
											$porc_coluna = (100 / count($ag_resumido) );
										}
									?>
									<?php foreach($ag_resumido as $res_tit){	?>
										
										<td class="formata_celula" style="width:<?php echo $porc_coluna; ?>%; cursor: pointer;"><a onclick="mostraAgendaCliente('<?=$res_tit['id']?>','<?=$res_tit['razao_social']?>')"><?php echo substr($res_tit['razao_social'],0,20); ?></a></td>
										
									<?php } ?>
								</tr>
								<tr>
									<?php 
										$soma_total = 0;
										foreach($ag_resumido as $resumido){	 
									?>
										
										<td class="formata_celula" style=" cursor: pointer;"><a onclick="mostraAgendaCliente('<?=$res_tit['id']?>','<?=$res_tit['razao_social']?>')"><?php echo $resumido['total'];  ?></a></td>
										
									<?php 
										$soma_total = $soma_total + $resumido['total'];
										} 
									?>
								</tr>
							</table>
						</td>
						<td class="td_total">
							<table id="resultado_resumido_tot" style="width:100%">
								<tr>
									<td class="formata_celula" >Total</td>
								</tr>
								<tr>
									<td class="formata_celula" ><?php echo $soma_total;  ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->
			<table class="" id="html_table"  >
				<thead>
					<tr>
					<th class="formata_celula" style="width: 1%;">#</th>
					<th class="formata_celula" style="width: 23%;">Cliente</th>						
					<th class="formata_celula" style="width: 4%;">Data</th>
					<th class="formata_celula" style="width: 4%;">Turno</th>
					<th class="formata_celula" style="width: 4%;">Placa</th>	
					<th class="formata_celula" style="width: 23%;">Produto</th>
					<th class="formata_celula" style="width: 4%;">NF</th>
					<th class="formata_celula" style="width: 5%;">Qtd.</th>
					<th class="formata_celula" style="width: 5%;">Emb.</th>
					<th class="formata_celula" style="width: 19%;">Motorista</th>
					<th class="formata_celula" style="width: 4%;">CPF</th>
					<th class="formata_celula" style="width: 5%;">Telefone</th>
					<!--	<th style="width: 5%;">Ações</th>-->	
					</tr>
				</thead>
				<tbody>					
				<?php foreach($agendamentos as $agendamento){	?>
					<tr>
						<td class="formata_celula" style="text-align: center;"><?php echo $agendamento['id']; ?></td>
						<td class="formata_celula" style=""><?php if($agendamento['cliente_final'] != null){ echo $agendamento['cliente_final']; }else{ echo $agendamento['razao_social']; }  ?></td>											
						<td class="formata_celula" style=""><?php echo $agendamento['dt_agenda']; ?></td>
						<td class="formata_celula" style=""><?php echo $agendamento['turno']; ?></td>
						<td class="formata_celula" style=""><?php echo $agendamento['placa']; ?></td>
						<td class="formata_celula" style=""><?php echo $agendamento['descricao']; ?></td>
						<td class="formata_celula" style=""><?php echo $agendamento['nr_nf']; ?></td>
						<td class="formata_celula" style=""><?php echo $agendamento['quant_total']; ?></td>
						<td class="formata_celula" style=""><?php echo $agendamento['no_embalagem']; ?></td>
						<td class="formata_celula" style=""><?php if($agendamento['motorista'] != null){ echo $agendamento['motorista']; }else{ echo $agendamento['nome']; } ?></td>
						<td class="formata_celula" style=""><?php echo $agendamento['cpf']; ?></td>
						<td class="formata_celula" style=""><?php echo $agendamento['fone']; ?></td>
					<!--	<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
							<a href="<?php echo base_url('AreaClientes/editarAgendamento/'.$agendamento['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
								<i class="la la-edit"></i>
							</a>				
						</td>-->
					</tr>
					<?php } ?> 
				</tbody>
			</table>			
		</div>
	</div>

	
	<!--end::Portlet-->	
</div>	
<div class="modal fade" id="m_listar_agendamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_listar_title" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 850px;">					
				<div id="tabela_empresa" class="row" >
					
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<!-- end:: Body -->
<?php if ($this->session->flashdata('retorno') == 'erro'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "Atenção!",
           	text: '<?php echo $this->session->flashdata('msg'); ?>',
           	type: "warning"
        }).then(function() {
		   	
		});
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('retorno') == 'sucesso'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Agendamento realizado com sucesso!',
           	type: "success"
        }).then(function() { });
	</script>	
<?php unset($_SESSION['sucesso']); } ?>
