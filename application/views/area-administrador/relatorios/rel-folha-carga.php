<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-calendar" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Folha de Carga de Mercadorias 
					</h3>	
														
				</div>

			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Período:</label>					
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="dt_ini" id="dt_ini" value="<?php echo date('d/m/Y'); ?>" class="form-control m-input datepicker" placeholder="data inicial"style="width: 49%;float: left;" /> 
						<input type="text" name="dt_fim" id="dt_fim" value="<?php echo date('d/m/Y'); ?>" class="form-control m-input datepicker" placeholder="data final"style="width: 49%;float: right;"/>						
					</div>				
				</div>
				<div class="col-lg-6">
					<label>POP:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="nr_fundeio" id="nr_fundeio" class="form-control m-input" placeholder="POP" style="width: 300px;" /> 
					</div>				
				</div>
			</div>	
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Navio:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="navio" id="navio" class="form-control m-input" placeholder="Navio" style="width: 300px;" />
					</div>				
				</div>	
                <div class="col-lg-6">
					<label>Nr. Escala:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="nr_escala" id="nr_escala" class="form-control m-input" placeholder="Nr. Escala" style="width: 300px;" />
					</div>				
				</div>			
			
			</div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="button" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Filtrar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							<input type="hidden" id="filtros" value=" " />
						</div>							
					</div>
				</div>
			</div>	
		</div>						
	</div>
	<!--end::Portlet-->	

	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" >
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
							Folha de Carga
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead >
					<tr >
					<th class="formata_celula" style="width:20%;">Período</th>
					<th class="formata_celula" style="width:40%;">Navio</th>	
					<th class="formata_celula" style="width:20%;">POP</th>
					<th class="formata_celula" style="width:10%;">Escala</th>
					<th class="formata_celula" style="width:10%;">Ações</th>	
					</tr>
				</thead>
				<tbody  >					
				<?php foreach($cargas as $carga){	?>
					
					<tr  >
						
						<td class="formata_celula" ><?php echo $carga['dt_ini']. ' - '.$carga['dt_fim']; ?></td>
						<td class="formata_celula" ><?php echo $carga['navio']; ?></td>	
						<td class="formata_celula"><?php echo $carga['nr_fundeio']; ?></td>										
						<td class="formata_celula"><?php echo $carga['nr_escala']; ?></td>
						
						<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
							<a href="<?php echo base_url('AreaAdministrador/emiteFolha/'.$carga['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
								<i class="la la-edit"></i>
							</a>				
						</td>
					
					</tr>
					
					<?php } ?> 
				</tbody>
			</table>			
		</div>
	</div>

	
	<!--end::Portlet-->	
</div>	
<div class="modal fade" id="m_lista_anexo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_lista_anexo_title" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
				<div class="row anexos">
					
				</div>				
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<!-- end:: Body -->
<?php if ($this->session->flashdata('retorno') == 'erro'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "Atenção!",
           	text: '<?php echo $this->session->flashdata('msg'); ?>',
           	type: "warning"
        }).then(function() {
		   	
		});
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('retorno') == 'sucesso'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Agendamento realizado com sucesso!',
           	type: "success"
        }).then(function() { });
	</script>	
<?php unset($_SESSION['sucesso']); } ?>
