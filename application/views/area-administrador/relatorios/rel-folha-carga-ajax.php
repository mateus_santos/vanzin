<table class="" id="html_table"  width="100%"  >
	<thead >
		<tr >
		<th class="formata_celula" style="width:20%;">Período</th>
		<th class="formata_celula" style="width:40%;">Navio</th>	
		<th class="formata_celula" style="width:20%;">POP</th>
		<th class="formata_celula" style="width:10%;">Escala</th>
		<th class="formata_celula" style="width:10%;">Ações</th>	
		</tr>
	</thead>
	<tbody  >					
	<?php foreach($cargas as $carga){	?>
		<tr id="<?php echo $carga['id']; ?>" >
			<td class="formata_celula" ><?php echo $carga['dt_ini']. ' - '.$carga['dt_fim']; ?></td>
			<td class="formata_celula" ><?php echo $carga['navio']; ?></td>	
			<td class="formata_celula"><?php echo $carga['nr_fundeio']; ?></td>										
			<td class="formata_celula"><?php echo $carga['nr_escala']; ?></td>
			<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
				<a href="<?php echo base_url('AreaAdministrador/emiteFolha/'.$carga['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-edit"></i>
				</a>				
			</td>
		</tr>
		<?php } ?> 
	</tbody>
</table>			
