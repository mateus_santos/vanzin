<table class="lista_resumido" id="html_table_res"  >
	<thead >
		<tr >
			<th colspan="2" class="formata_celula" style="">CAMINHÕES QUE NÃO ENTRARAM - POR CLIENTE</th>		
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="td_clientes">
				<table id="resultado_resumido" style="width:100%;">
					<tr>
						<?php
						if (count($ag_resumido) > 0){
							$porc_coluna = (100 / count($ag_resumido) );
						}
						?>
						<?php foreach($ag_resumido as $res_tit){	?>
							
							<td class="formata_celula" style="width:<?php echo $porc_coluna; ?>%; cursor: pointer;"><a onclick="mostraAgendaCliente('<?=$res_tit['id']?>','<?=$res_tit['razao_social']?>')"><?php echo substr($res_tit['razao_social'],0,20); ?></a></td>
							
						<?php } ?>
					</tr>
					<tr>
						<?php 
							$soma_total = 0;
							foreach($ag_resumido as $resumido){	 
						?>
							
							<td class="formata_celula" style="cursor: pointer;">
								<a onclick="mostraAgendaCliente('<?=$resumido['id']?>','<?=$res_tit['razao_social']?>')"><?php echo $resumido['total'];  ?></a>
							</td>
							
						<?php 
							$soma_total = $soma_total + $resumido['total'];
							} 
						?>
					</tr>
				</table>
			</td>
			<td class="td_total">
				<table id="resultado_resumido_tot" style="width:100%">
					<tr>
						<td class="formata_celula" >Total</td>
					</tr>
					<tr>
						<td class="formata_celula" ><?php echo $soma_total;  ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</br>
<table class="" id="html_table" width="150%">
<thead>
	<tr>
		<th class="formata_celula" style="width: 1%;">#</th>
		<th class="formata_celula" style="width: 23%;">Cliente</th>						
		<th class="formata_celula" style="width: 4%;">Data</th>	
		<th class="formata_celula" style="width: 4%;">Turno</th>	
		<th class="formata_celula" style="width: 4%;">Placa</th>
		<th class="formata_celula" style="width: 23%;">Produto</th>
		<th class="formata_celula" style="width: 4%;">NF</th>
		<th class="formata_celula" style="width: 5%;">Qtd.</th>
		<th class="formata_celula" style="width: 5%;">Emb.</th>	
		<th class="formata_celula" style="width: 19%;">Motorista</th>
		<th class="formata_celula" style="width: 4%;">CPF</th>
		<th class="formata_celula" style="width: 5%;">Telefone</th>
		<!--	<th style="width: 5%;">Ações</th>-->			
	</tr>
</thead>  
<tbody>					
	<?php foreach($agendamentos as $agendamento){	?>
		<tr>
			<td class="formata_celula" style="text-align: center;"><?php echo $agendamento['id']; ?></td>
			<td class="formata_celula" style=""><?php if($agendamento['cliente_final'] != null){ echo $agendamento['cliente_final']; }else{ echo $agendamento['razao_social']; }?></td>											
			<td class="formata_celula" style=""><?php echo $agendamento['dt_agenda']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['turno']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['placa']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['descricao']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['nr_nf']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['quant_total']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['no_embalagem']; ?></td>	
			<td class="formata_celula" style=""><?php if($agendamento['motorista'] != null){ echo $agendamento['motorista']; }else{ echo $agendamento['nome']; }; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['cpf']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['fone']; ?></td>
		<!--	<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
				<a href="<?php echo base_url('AreaClientes/editarAgendamento/'.$agendamento['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-edit"></i>
				</a>				
			</td>-->
		</tr>
		<?php } ?> 
	</tbody>
</table>	