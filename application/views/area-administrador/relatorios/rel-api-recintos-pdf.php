<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{	
	border-bottom: 1px solid #f0f0f0;	
	font-size: 9px;	
	padding: 2px;
}
.t_cabecalho td{
	border-bottom: none;	
	border: none;
	text-align: center;
	width: 33%;
	padding: 0px;
	margin: 0px;
	font-size: 11px;
	font-weight: 600;
}
.rodape{
	position: absolute;
	bottom: 0px;
}

</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br/>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br/>
										07400-230 - Arujá - SP<br/>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br/>
			</p>
		</div>
		<div class="corpo">				
			<!--<h3 style="text-align: center;margin-top: 15px;	">Relatório de Pedidos <?php echo ($tipo=='h') ? 'por Histórico' : 'por Status Atual'?></h3>-->
			<table style="border: none;" class="t_cabecalho" style="width: 100%;">
				<tr>
					<td colspan="3" style="text-align: center;">
						<h3 style="text-align: center;margin-top: 15px;	">Relatório de Pedidos <?php echo ($tipo=='h') ? 'por Histórico' : 'por Status Atual'?></h3>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;">
					<?php if($status_desc != ''){ ?>			
						Status: <?php echo $status_ordem." - ".ucfirst(mb_strtolower($status_desc)); ?>
					<?php } ?>
					</td>
					<td style="text-align: center;">
					<?php  if($periodo != ''){  ?>			
						Período <?php echo $periodo; ?>
					<?php } ?>
					</td>
					<td style="text-align: right;">
						<?php echo date('d/m/Y H:i:s'); ?>
					</td>
				</tr>
			</table>				
			<?php  
				if(count($dados['orcamento']) > 0){
					$colspan = 15;
			?>			
			<hr  />
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;">
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>#ID</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Cliente</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Cidade/UF</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Contato</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Status Atual</b></td>
				<?php if($tipo=='h'){ $colspan = 16;?>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Status Atual</b></td>
				<?php } ?>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Indicador</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Bombas</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Bicos</b></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Andamento</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Dias</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Semana</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Resp.</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Nr.NF</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;"><b>Dt. Emissão NF</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 2px;text-align: right;"><b>Valor (R$)</b></td>					
				</tr>				
				<?php $total = $total_bombas = $total_bicos = 0; foreach($dados['orcamento'] as $dado){ ?>
				<tr>
					<td style="border: 1px solid #ccc;padding: 2px;"><?php echo $dado['id']?> </td>
					<td style="border: 1px solid #ccc;padding: 2px;"><?php echo mb_strtoupper($dado['cliente']); ?> </td>
					<td style="border: 1px solid #ccc;padding: 2px;"><?php echo $dado['cidade'].' / '.$dado['estado']; ?> </td>
					<td style="border: 1px solid #ccc;padding: 2px;"><?php echo mb_strtoupper($dado['contato']). ' / ' .$dado['telefone'].' / '.$dado['email']; ?> </td>
					<td style="border: 1px solid #ccc;padding: 2px;"><?php echo $dado['ordem'].'-'.$dado['status']; ?> </td>
				<?php if($tipo=='h'){ ?>
					<td style="border: 1px solid #ccc;padding: 2px;"><?php echo $dado['dt_status_atual']; ?> </td>
				<?php } ?>
					<td style="border: 1px solid #ccc;padding: 2px;"><?php echo ($dado['indicador_id'] != 2) ? mb_strtoupper($dado['indicador']) : ''; ?> </td>
					<td style="border: 1px solid #ccc;padding: 2px;text-align: right;"><?php echo $dado['total_bombas'];?> </td>
					<td style="border: 1px solid #ccc;padding: 2px;text-align: right;"><?php echo $dado['total_bicos'];?> </td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b><?php echo $dado['dthr_andamento'];?></b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;text-align: right;"><b><?php echo $dado['dias'];?></b></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;text-align: right;"><b><?php echo ($dado['dt_semana_prog'] != '') ? date('W',strtotime($dado['dt_semana_prog'])) : '';?></b></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b><?php echo ucfirst($dado['consultor_resp']);?></b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b><?php echo $dado['nr_nf'];?></b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b><?php echo ($dado['nr_nf'] != "") ? date('d/m/Y',strtotime($dado['dt_emissao_nf'])) : '';?></b></td>
					<td style="border: 1px solid #ccc;padding: 2px;text-align: right;"><?php echo  number_format($dado['valor_total'], 2, ',', '.');?> </td>
				</tr>
			<?php 	
					$total 			= 	$dado['valor_total'] + $total;
					$total_bombas 	= 	$dado['total_bombas'] + $total_bombas;
					$total_bicos 	= 	$dado['total_bicos'] + $total_bicos;
					
					
			} 
			?>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan="<?php echo $colspan; ?>">Total: R$ <?php echo number_format($total,2,',','.');?> </td>
				</tr>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan="<?php echo $colspan; ?>">Total de Bombas: <?php echo $total_bombas;?> </td>
				</tr>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan="<?php echo $colspan; ?>">Total de Bicos: <?php echo $total_bicos;?> </td>
				</tr>
			</table>
			<?php }else { ?>	
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc; margin-top: 300px;" />								
				<h2 style="text-align: center;">Nenhum resultado encontrado!</h2>
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc;" />							
			<?php } ?>				
		
	</div>	
</div>
</body>
</html>