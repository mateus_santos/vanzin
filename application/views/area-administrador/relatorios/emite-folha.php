<?php /*
echo "<pre>";
var_dump($listagem);
echo "<pre>";*/
?>
<div class="layout_extrato" id="impressao" >
	<div id="cabecacho">
		<table class=" formata_tab_siscomex" style="text-align:left;">
			<thead>
				<tr>
					<td class='td_extrato'><img src='../../bootstrap/img/logoSiscomex.png' id='img_extrato' alt="Logo Siscomex"/></td>
                    <td  class="tb_titulo" ><p>VANZIN TERMINAIS E SERVIÇOS ADUANEIROS LTDA </p></td>
                </tr>
				<tr>
					<td class='td_extrato'>&nbsp;</td>
                    <td  class="tb_titulo" ><p>ARMAZENAGEM </p></td>
                </tr>
				<tr>
					<td class='td_extrato'>&nbsp;</td>
                    <td  class="tb_titulo" ><p>FOLHA DE CARGA DE MERCADORIAS </p></td>
                </tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;"></td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;"><div class="linha_extrato"></div></td>
				</tr>
				<tr>
					<td class="">
						<div class="lb_text_extrato" style="float:left;">POP: <?php echo $listagem[0]->nr_fundeio;?></div>
					</td>
					<td>
					<div class="lb_text_extrato" style="float:left;">Navio: <?php echo $listagem[0]->navio;?></div>
					</td>
				</tr>
				<tr>
					<td class="">
						<div class="lb_text_extrato" style="float:left;">IMO: <?php echo $listagem[0]->imo;?></div>
					</td>
					<td>
					<div class="lb_text_extrato" style="float:left;">Nr. Escala: <?php echo $listagem[0]->nr_escala;?></div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;">&nbsp;</td>
				</tr>
				<tr>
					<td class="">
						<div class="lb_text_extrato" style="float:left;">Local: <?php echo $listagem[0]->no_terminal;?></div>
					</td>
					<td>
					<div class="lb_text_extrato" style="float:left;">Operador: VANZIN OPERAÇÕES PORTUÁRIAS S/A</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center; padding-top:3%;"><div class="linha_extrato"></div></td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:left;">
						<table id='lista_operacao' class="table_interno" >
							<thead>
								<tr>
									<td >
										<div class="lb_text_extrato" style="text-align:center;">ENTRADA</div>
									</td>
									<td>
										<div class="lb_text_extrato"  style="text-align:center;">OPERAÇÃO</div>
									</td>
									<td>
										<div class="lb_text_extrato"  style="text-align:center;">SAÍDA</div>
									</td>
								</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									<div class="lb_text_extrato form_data"  style="text-align:center;"><?php echo $listagem[0]->dthr_atracacao; ?></div>
								</td>
								<td>
									<div class="lb_text_extrato form_data "  style="text-align:center;"><?php echo $listagem[0]->dthr_ini_operacao.' - '.$listagem[0]->dthr_fim_operacao; ?></div>
								</td>
								<td>
									<div class="lb_text_extrato  form_data"  style="text-align:center;"><?php echo $listagem[0]->dthr_desatracacao; ?></div>
								</td>
							</tr>
							</tbody>
						</table>


					</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center; padding-top:3%;"><div class="linha_extrato"></div></td>
				</tr>
				<tr>
					<td colspan="2">
						<div class="lb_text_extrato" style="text-align:center;">CARGA DE GRANEL</div>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center;"><div class="linha_extrato"></div></td>
				</tr>
				
				<tr>
					<td colspan="2" class="">
					<table class="table_interno" style="width:96%; margin-top:1%;">
						<thead style="font-weight: bolder;">
							<tr>
								<td>
									<div class="lb_text_extrato">EXPORTADOR</div>
								</td>
								<td>
									<div class="lb_text_extrato">MERCADORIA</div>
								</td>
								<td>
									<div class="lb_text_extrato">NR. DU-E</div>
								</td>
								<td>
									<div class="lb_text_extrato">CE MERCANTE</div>
								</td>
								<td>
									<div class="lb_text_extrato">PESO (KG)</div> 
								</td>
							</tr>
						</thead>
						<tbody> <?php $total_peso = 0; ?>
							<?php foreach($listagem as $linha){ ?>
							<tr>
								<td>
									<div class="lb_text_extrato"><?php echo $linha->razao_social; ?></div>
								</td>
								<td>
									<div class="lb_text_extrato"><?php echo $linha->ncm; ?></div>
								</td>
								<td>
									<div class="lb_text_extrato"><?php echo $linha->nr_due; ?></div>
								</td>
								<td>
									<div class="lb_text_extrato"><?php echo $linha->ce_mercante; ?></div>
								</td>
								<td>
									<div class="lb_text_extrato peso"><?php echo $linha->peso; $total_peso = $total_peso + $linha->peso; ?></div>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</tr>
				
				<tr>
					<td colspan="2" class="tb_titulo" style="text-align:center; padding-top:3%; padding-bottom:3%;"><?php echo $listagem[0]->observacao; ?></td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo" >
						<table  style="width:97%; margin-top:1%; border:0px solid red; font-weight: bolder;">
							<tr>
								<td style="width:90%"><div class="lb_text_extrato" style="text-align:right; ">Total:</div>	</td>
								<td><div id="total_peso" class="lb_text_extrato" style="text-align:right;   "> <?php echo $total_peso; ?></div></td>
							</tr>
						</table>
					</td>
					
				</tr>
			</tbody>
		</table>
		
	</div>
	
</div>	
			
