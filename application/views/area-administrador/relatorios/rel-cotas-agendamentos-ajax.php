<table class="" id="html_table" >
	<thead>
		<tr>	
			<th>Dia</th>	
			<th>Cliente</th>						
			<th>Possui Cota</th>	
			<th>Peso Cota</th>
			<th>Total Agendado</th>
			<th>Saldo</th>
		</tr>
	</thead> 
	<tbody>					
		<?php $total_cotas = $total_agendado = $total_saldo = 0; foreach($agendamentos as $agendamento){	
			$peso_agendado 	= explode('.', $agendamento['agendado']);
			$saldo 			= explode('.', $agendamento['saldo']);
			
			$total_cotas 	= $total_cotas + $agendamento['peso'];
			$total_agendado = $total_agendado + $peso_agendado[0];
			$total_saldo 	= $total_saldo + $saldo[0];	
		?>
		<tr>
			<td><?=date('d/m/Y',strtotime($agendamento['dt_agenda']))?></td>
			<td><?php echo $agendamento['empresa']; ?></td>
			<td><?php echo ($agendamento['fl_sem_cota'] == 0) ? 'COM COTA' : 'SEM COTA'; ?></td>	
			<td class="qtd"  style=""><?php echo $agendamento['peso']; ?></td>											
			<td class="qtd" style=""><?php echo $peso_agendado[0]; ?></td>
			<td class="qtd" style=""><?php echo $saldo[0]; ?></td>
		</tr>
		<?php } ?> 

	</tbody>
</table>
<table class="table">
	<thead>
		<tr>
			<th colspan="4" style="text-align: right;background: #ffcc00;">Total de Cotas</th>
			<th style="text-align: right;background: #ffcc00;">Total de Agendamentos</th>
			<th style="text-align: right; background: #ffcc00;">Saldo Total</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="qtd" colspan="4" style="text-align: right;"><?=$total_cotas?></td>
			<td class="qtd" style="text-align: right;"><?=$total_agendado?></td>
			<td class="qtd" style="text-align: right;"><?=$total_saldo?></td>
		</tr>
	</tbody>				
</table>	