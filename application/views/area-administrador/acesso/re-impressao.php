﻿
<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Reimpressão dos Tickets
					</h3>
				</div>
			</div>			
		</div>
		<div id='reimpressao' style='visibility:hidden;'>SIM</div>
		<div class="m-portlet__body">
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="Field #1" width="5%">Ticket</th>
						<th title="Field #2" width="10%">Placa</th>
						<th title="Field #3" width="15%">Data Entrada</th>
						<th title="Field #4" width="25%">Produto</th>
						<th title="Field #4" width="10%">Data Saída</th>
						<th title="Field #6" width="15%">Data Saída Manutenção</th>
						<th title="Field #5" width="15%">Data Retorno</th>
						<th title="Field #9" width="5%"> Ações</th>

					</tr>
				</thead>
				<tbody>
					<tr>
					<?php foreach($dados as $dado){ ?>
						<td style="text-align: center;"><?php echo $dado->id; ?></td>
						<td><?php echo $dado->placa; ?></td>
						<td><?php echo $dado->dthr_entrada; ?></td>
						<td><?php echo $dado->descricao; ?></td>
						<td><?php echo $dado->dthr_saida; ?></td>
						<td><?php echo $dado->dthr_saida_manutencao; ?></td>
						<td><?php echo $dado->dthr_retorno_manutencao; ?></td>
						<td data-field="Actions" class="m-datatable__cell">
							<span style="overflow: visible; width: 110px;">								
								<a href="<?php 
								if($dado->dthr_saida <> null){
									echo base_url('AreaGate/imprimeTicket/semplaca/1/'.$dado->id); 
								}else{ 
									if($dado->dthr_saida_manutencao <> null and $dado->dthr_retorno_manutencao == null){
										echo base_url('AreaGate/imprimeTicket/semplaca/1/'.$dado->id); 	
									}else if($dado->dthr_saida_manutencao <> null and $dado->dthr_retorno_manutencao <> null){
										echo base_url('AreaGate/imprimeTicketEntrada/semplaca/1/'.$dado->id); 	
									}else{
										echo base_url('AreaGate/imprimeTicketEntrada/semplaca/1/'.$dado->id); 
									}
								}
								?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Reimprime ">
									<i class="fa fa-print"></i>
								</a>
							</span>
						</td>
					</tr>
						<?php } ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>		        
</div>				
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Atualização realizada com sucesso!',
           	type: "success"
        }).then(function() {
			
		});  
	</script>	
<?php unset($_SESSION['sucesso']); } ?>