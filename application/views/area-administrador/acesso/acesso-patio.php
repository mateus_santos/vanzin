<div class="m-content">
<form accept-charset="utf-8" action="<?php echo base_url('AreaGate/registrarAcesso/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
	<div class="m-portlet" style="min-height: 500px; overflow: auto;">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-building"></i>
					</span>
					<h3 class="m-portlet__head-text"> 
						Novo Agendamento
					</h3>
				</div>			
			</div> 
			<div class="m-portlet__head-tools">
				
			</div>
		</div>
		<div class="m-portlet__body" style="max-height: 400px; overflow: auto;">	
			<div id="msg" class="msg" style="font-weight:bold; font-size:110%;" ></div>
			<div id="msg" class="msg_tipo" style="font-weight:bold; font-size:110%; display:none;" ></div>
	
		<div class="form-group m-form__group row" id="tab_placa" style="border:0px; margin-bottom:3%; margin-top:2%;">
			<div class="linha" >
				<div class="coluna " style="width:40%; border:0px; ">
					<label  class="placa">Placa:</label>
				</div>
				<div class="coluna " style="text-align:left; ">
					<div ><input type="text" class='form-control m-input hora' id="placa" name="placa" placeholder='' required></div>
				</div>
			</div>
		</div>
		<div class="form-group m-form__group row" id="dv_saida_manut" style="display:none;" >
			<div class="coluna " style="width:100%; border:0px solid red; text-align:right; ">
				<div style="font-size:70%;"><input type="checkbox" class='' id="saida_manut" name="saida_manut" placeholder=''><label style="margin:1%; ">Saída para Manutenção</label></div>
				<input type="hidden"  id="dthr_saida_manutencao" name="dthr_saida_manutencao" value="" />
				<input type="hidden"  id="dthr_retorno_manutencao" name="dthr_retorno_manutencao" value="" />
			</div>
		</div>
		<div class=" esc_tabela form-group m-form__group row" id="tab_resultado">
			<div class="linha_titulo">
				<div class="coluna col_cliente">
					<label class="titulo">Cliente</label>
				</div>
				<div class="coluna" style="width:28%">
					<label class="titulo">Tipo de Operação</label>
				</div>
				<div class="coluna col_cliente">
					<label class="titulo">Data do Agendamento</label>
				</div>
			</div>
			
		</div>
		<div class=" esc_tabela form-group m-form__group row" id="tab_resultado_lista">
			
			
		</div>
		<div class=" esc_tabela form-group m-form__group row" id="tab_observa">

			<div class="coluna col_textarea">
				<label class="tit_observa">Observação</label>
				<label class="tit_observa"><textarea class="form-textarea" id="observacao" name="observacao"></textarea></label>
			</div>

		</div>
		<div class=" form-group m-form__group row" style='border: 0px; margin-top:1%;' id="tab_botao_entrada">
			
			<div class="coluna_botoes">
				<button type='submit' class='btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase' id='btn_entrada'>Confirma Acesso</button>&nbsp;
			</div>
			
		</div>
		<div class=" form-group m-form__group row" style='border: 0px; margin-top:10px;' id="tab_botao_saida">
			
			<div class="coluna_botoes">
				
				<button type='submit' class='btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase' id='btn_saida'>Confirma Saída</button>&nbsp; 
			</div>
			
		</div>
		</div>
	</div>
</form>	
</div>	
			
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Atualização realizada com sucesso!',
           	type: "success"
        }).then(function() {
		   //	window.location = base_url+'AreaAdministrador/AcessoPatio';
		   
		});
	</script>	
<?php unset($_SESSION['sucesso']); } ?>