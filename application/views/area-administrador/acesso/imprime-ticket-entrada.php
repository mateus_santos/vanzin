<?php 
if($_SESSION['pag_anterior'] == 'REIMPRESSAO'){
	$retorna_pagina = 1;
	$_SESSION['pag_anterior'] = '';
}else{
	$retorna_pagina = 0;
}

?>
<div id='reimpressao' style='visibility:hidden;'><?php echo $retorna_pagina; ?></div>
<div class="layout_ticket formata_celula" style="text-align:left;" id="impressao" >
	<div id="cabecacho">
		<table class="ticket">
			<thead>
				
				<tr>
					<td colspan="2" class="tb_titulo" ><p>Registro de Entrada 
						<?php 
							if($listagem[0]->dthr_retorno_manutencao <> null ){
							 echo 'MANUTENÇÃO';
							}
						?>
						</p>
					</td>
				</tr>
				<tr>
					<td class="td_label">Operador:</td><td><?php echo $nome;?></td>
				</tr>
				<tr>
					<td colspan="2" class="tb_titulo">Pátio Triagem</td>
				</tr>	
			</thead>
			<tbody>
				<tr>
					<td class="td_label lb_ticket_maior">
						<p>Placa:</p>
					</td>
					<td class="lb_ticket_maior">
						<p><?php echo $listagem[0]->placa;?></p>
					</td>
				</tr>
				<tr>
					<td class="td_label">
						Empresa:
					</td>
					<td>
						<?php echo $razao_social;?>
					</td>
				</tr>
				<tr>
					<td class="td_label">
						Data Entrada:
					</td>
					<td>
						
						<?php 
							if ($listagem[0]->dthr_retorno_manutencao <> null){
								echo $listagem[0]->dthr_retorno_manutencao;	
							}else{
								echo $listagem[0]->dthr_entrada;	
							}
							
						?>
					</td>
				</tr>
				<tr>
					<td class="td_label">
						Prev. Chegada:
					</td>
					<td>
						<?php 
							echo $listagem[0]->dt_agenda;	
						?>
					</td>
				</tr>
				<?php 
					if($listagem[0]->nr_nf == NULL){
				?>
				<tr>
					<td class="td_label">
						Ordem:
					</td>
					<td>
						<?php 
							echo $listagem[0]->nr_pedido;	
						?>
					</td>
				</tr>
				<?php }else{ ?>
					<tr>
					<td class="td_label">
						Nota Fiscal:
					</td>
					<td>
						<?php 
							echo $listagem[0]->nr_nf;	
						?>
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td class="td_label">
						Formulação:
					</td>
					<td>
						<?php 
							echo $listagem[0]->no_produto;	
						?>
					</td>
				</tr>
				<tr>
					<td class="td_label">
						Quantidade:
					</td>
					<td>
						<?php 
							echo $listagem[0]->quant_total;	
						?>
					</td>
				</tr>
				<tr>
					<td class="td_label">
						Embalagem:
					</td>
					<td>
						<?php 
							echo $listagem[0]->no_embalagem;	
						?>
					</td>
				</tr>
				<tr>
					<td class="td_label lb_ticket_maior">
						Cliente:
					</td>
					<td class="lb_ticket_maior">
						<?php echo $listagem[0]->razao_social;?>
					</td>
				</tr>
				<tr>
					<td class="td_label">
						Motorista:
					</td>
					<td>
						<?php echo $listagem[0]->nome;?>
					</td>
				</tr>
				<tr>
					<td class="td_label">
						CPF:
					</td>
					<td>
						<?php echo $listagem[0]->cpf;?>
					</td>
				</tr>
				<tr>
					<td class="td_label">
						Número:
					</td>
					<td>
						<?php echo $listagem[0]->id_acesso;?>
					</td>
				</tr>
			</tbody>
		</table>
		
	</div>
	
</div>	
			
