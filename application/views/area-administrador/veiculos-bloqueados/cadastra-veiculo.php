<div class="m-content">	
	<form accept-charset="utf-8" action="<?php echo base_url('AreaAdministrador/inserirVeiculoBloqueado/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-building"></i>
					</span>
					<h3 class="m-portlet__head-text"> 
						Bloquear Veículo
					</h3>
				</div>			
			</div> 
			<div class="m-portlet__head-tools">
				
			</div>
		</div>				
		<div class="m-portlet__body">		
			<div class="form-group m-form__group row">									
				<div class="col-lg-6">
					<label>Placa:</label>
					<div class="m-input-icon m-input-icon--right" style="width: 80%;">
						<input type="text" name="placa" id="placa" class="form-control" placeholder="" required />						
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-file-text"></i></span></span>
					</div>					
				</div>
				<div class="col-lg-6">
					<label>Motivo:</label>
					<div class="m-input-icon m-input-icon--right" style="width: 80%;">
						<textarea name="ds_motivo" id="ds_motivo" class="form-control" placeholder="" required ></textarea>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-file-text"></i></span></span>						
						
					</div>					
				</div>		
			</div>				
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="form-group row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>
					</div>
				</div>
			</div>					
		</div>
		
	</div>
</form>
</div>

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Veículo bloqueado com sucessoo!',
                type: 'success'
            }).then(function() {
                //window.location = base_url+'AreaAdministrador/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>