<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-calendar" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Veículos Bloqueados 
					</h3>	
					<h3 class="m-portlet__head-text" style=" padding-left: 10px;" >
						<a href="<?php echo base_url('AreaAdministrador/cadastrarVeiculoBloqueado'); ?>"  style="color: #ffcc00; font-weight: bold;" data-toggle="m-tooltip" data-placement="top" title="Novo" id="Novo">
							<i class="la la-plus-circle" style="font-size: 38px;"></i>
						</a>
					</h3>									
				</div>

			</div> 
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
			<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">#</th>
						<th style="width: 20%;">PLACA</th>						
						<th style="width: 70%;">MOTIVO</th>						
						<th style="width: 5%;">Ações</th>
					</tr>
				</thead>
				<tbody>					
				<?php foreach($veiculos as $veiculo){				?>
					<tr>
						<td style="text-align: center;"><?php echo $veiculo['id']; ?></td>
						<td ><?php echo $veiculo['placa']; ?></td>											
						<td ><?php echo $veiculo['ds_motivo']; ?></td>						
						<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">							
							<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" target="blank" onclick="excluirVeiculo(<?=$veiculo['id']?>)">
								<i class="la la-trash"></i>
							</a>				
						</td>
					</tr>
					<?php } ?> 
				</tbody>
			</table>			
		</div>	
		</div>						
	</div>
</div>
<!-- end:: Body -->
<?php if ($this->session->flashdata('retorno') == 'erro'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "Atenção!",
           	text: '<?php echo $this->session->flashdata('msg'); ?>',
           	type: "warning"
        }).then(function() {
		   	
		});
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('retorno') == 'sucesso'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Operação realizada com sucesso!',
           	type: "success"
        }).then(function() { });
	</script>	
<?php unset($_SESSION['sucesso']); } ?>
