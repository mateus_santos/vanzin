<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Gestão de Cotas 	
					</h3>
					
				</div>
			</div>					
		</div>
		<div id="msg" class="msg" ></div>
<form accept-charset="utf-8" action="<?php echo base_url('AreaAdministrador/cadastrarCota/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
	<div class="tabela"  style="border:0px">
		<div class="linha" style="border:0px; margin-bottom:4%;">
			<div class="coluna col_cliente" style="border:0px">
				<label class="placa">Dia da Semana:</label>
			</div>
			<div class="coluna_total">
				<div class="m-input-icon m-input-icon--right">
	
					<select   maxlength="250" name="dia_semana" class="form-control" placeholder="Selecione o Cliente" id="id_semana" required>
					<option value="">Selecione o dia da Semana</option>
						<option value="seg">Segunda-feira</option>  
						<option value="ter">Terça-feira</option>
						<option value="qua">Quarta-feira</option>
						<option value="qui">Quinta-feira</option>
						<option value="sex">sexta-feira</option>
						<option value="sab">Sabado</option>
						<option value="dom">Domingo</option>							
					</select>
					
				</div>
			</div>
		</div>
	</div>
	<div class="tabela" id="tab_resultado">
		<div class="linha_titulo" id="linha_titulo">
			<div class="coluna col_cliente">
				<label class="titulo">Cliente</label>
			</div>
			<div class="coluna">
				<label class="titulo">Horário Limite Acesso</label>
			</div>
			<div class="coluna">
				<label class="titulo">Horário Limite Agendamento</label>
			</div>
			<div class="coluna">
				<label class="titulo">Peso</label>
			</div>
			<div class="coluna">
				<label class="titulo">Cota Ilimitada</label>
			</div>
		</div>
	</div>
	<div class="tabela" style='border: 0px; margin-top:1%;' id="tab_botoes">
		<div class="linha">
				<div class="coluna_botoes">
					<button type='submit' class='btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase'>Salvar</button>&nbsp;
					<button type='button' class='btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom' id="limpa">Limpar</button>&nbsp;
					<button type='button' class='btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom' id="duplica">Duplicar</button>
					 <input type='hidden' name='total_linhas' id='total_linhas' value='0' />
					 <input type='hidden' name='duplicou' id='duplicou' value='0' />
				</div>
		</div>
	</div>
	</div>		        
</div>	
</form>			
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Atualização realizada com sucesso!',
           	type: "success"
        }).then(function() {
		   	window.location = base_url+'AreaAdministrador/gestaoCotas';
		});
	</script>	
<?php unset($_SESSION['sucesso']); } ?>