<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<?php
		if(!empty($dados)){ ?>
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Listagem das Cotas importadas 	
					</h3>
					
				</div>
			</div>					
		</div>
		<?php /*
		echo "<div><pre>";
		var_dump($dados);
		echo "</div></pre>";*/
		?>
		<div id="msg" class="msg" ></div>
		<div class="tabela" id="tab_resultado"  style="border:0px;">
			<div class="linha_titulo" id="linha_titulo_dias"  style="border:0px;">
				<div class="coluna " style="width:21%; border:0px;">
					
				</div>
				<div class="coluna" style="width:10%;">
					<label class="titulo">Segunda</label>
				</div>
				<div class="coluna" style="width:10%;">
					<label class="titulo">Terça</label>
				</div>
				<div class="coluna" style="width:10%;">
					<label class="titulo">Quarta</label>
				</div>
				<div class="coluna" style="width:10%;">
					<label class="titulo">Quinta</label>
				</div>
				<div class="coluna" style="width:10%;">
					<label class="titulo">Sexta</label>
				</div>
				<div class="coluna" style="width:10%;">
					<label class="titulo">Sabado</label>
				</div>
				<div class="coluna" style="width:10%;">
					<label class="titulo">Domingo</label>
				</div>
				<div class="coluna" style="width:9%;">
					<label class="titulo">Total</label>
				</div>
			</div>
			<div class="linha_titulo" id="linha_titulo">
				<div class="coluna" style="width:21%;">
					<label class="titulo_menor" >Cliente</label>
				</div>
				
				<div class="coluna_menor">
					<label class="titulo_menor">Peso</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Quantidade</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Peso</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Quantidade</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Peso</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Quantidade</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Peso</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Quantidade</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Peso</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Quantidade</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Peso</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Quantidade</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Peso</label>
				</div>
				<div class="coluna_menor">
					<label class="titulo_menor">Quantidade</label>
				</div>
				<div class="coluna_menor" >
					<label class="titulo_menor"  >Peso</label>
				</div>
				<div class="coluna_menor" style="width:4%;">
					<label class="titulo_menor">Quant.</label>
				</div>
				
			</div>
		
			<?php } ?>
			<div id="resultado">
			<?php 
			if(!empty($dados)){
				$controle_cliente = array();
				$controle_geral = array();
				foreach($dados as $dado){
					
					//Verifica se existe já foi listado o cliente
					if(empty($controle_cliente['razao_social'][$dado->razao_social])){
						$controle_cliente['razao_social'][$dado->razao_social] = $dado->razao_social;
						$controle_geral['controle'][$dado->razao_social]['razao_social'][0] = $dado->razao_social;
						
						//array_push($controle_geral['controle'][$dado->razao_social],array(['razao_social'=> $dado->razao_social]));
					}

					if($dado->dia_semana == 'seg'){
						
						$controle_geral['controle'][$dado->razao_social]['dia_semana'][1]		=  'seg';
						$controle_geral['controle'][$dado->razao_social]['peso'][1]				=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['quantidade'][1]		=  $dado->quantidade;
						$controle_geral['controle'][$dado->razao_social]['total_peso'][1]		=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['total_quantidade'][1]	=  $dado->quantidade;
						
					}

					if($dado->dia_semana == 'ter'){
						
						$controle_geral['controle'][$dado->razao_social]['dia_semana'][2]		=  'ter';
						$controle_geral['controle'][$dado->razao_social]['peso'][2]				=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['quantidade'][2]		=  $dado->quantidade;
						$controle_geral['controle'][$dado->razao_social]['total_peso'][2]		=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['total_quantidade'][2]	=  $dado->quantidade;
					
					}

					if($dado->dia_semana == 'qua'){
						
						$controle_geral['controle'][$dado->razao_social]['dia_semana'][3]		=  'qua';
						$controle_geral['controle'][$dado->razao_social]['peso'][3]				=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['quantidade'][3]		=  $dado->quantidade;
						$controle_geral['controle'][$dado->razao_social]['total_peso'][3]		=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['total_quantidade'][3]	=  $dado->quantidade;
					
					}

					if($dado->dia_semana == 'qui'){
						
						$controle_geral['controle'][$dado->razao_social]['dia_semana'][4]		=  'qui';
						$controle_geral['controle'][$dado->razao_social]['peso'][4]				=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['quantidade'][4]		=  $dado->quantidade;
						$controle_geral['controle'][$dado->razao_social]['total_peso'][4]		=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['total_quantidade'][4]	=  $dado->quantidade;
					
					}


					if($dado->dia_semana == 'sex'){
						
						$controle_geral['controle'][$dado->razao_social]['dia_semana'][5]		=  'sex';
						$controle_geral['controle'][$dado->razao_social]['peso'][5]				=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['quantidade'][5]		=  $dado->quantidade;
						$controle_geral['controle'][$dado->razao_social]['total_peso'][5]		=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['total_quantidade'][5]	=  $dado->quantidade;
					
					}

					if($dado->dia_semana == 'sab'){
						
						$controle_geral['controle'][$dado->razao_social]['dia_semana'][6]		=  'sab';
						$controle_geral['controle'][$dado->razao_social]['peso'][6]				=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['quantidade'][6]		=  $dado->quantidade;
						$controle_geral['controle'][$dado->razao_social]['total_peso'][6]		=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['total_quantidade'][6]	=  $dado->quantidade;
					
					}

					if($dado->dia_semana == 'dom'){
						
						$controle_geral['controle'][$dado->razao_social]['dia_semana'][7]		=  'dom';
						$controle_geral['controle'][$dado->razao_social]['peso'][7]				=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['quantidade'][7]		=  $dado->quantidade;
						$controle_geral['controle'][$dado->razao_social]['total_peso'][7]		=  $dado->peso;
						$controle_geral['controle'][$dado->razao_social]['total_quantidade'][7]	=  $dado->quantidade;
					
					}
					
				}
				
				foreach($controle_geral['controle'] as $indice){
						//echo " Nome: ".$indice["razao_social"][0]."<br>";
						$peso_total = 0;
						$qtd_total 	= 0;
				?>
				
				<div class="linha_titulo" id="linha_titulo">
					<div class="coluna" style="width:21%; ">
						<label class="resultado" style="text-align:left;" >
							<?php echo $indice["razao_social"][0]; ?> 
						</label>
					</div>
				
					<div class="coluna_menor">
						<label  class="resultado" >
							<?php
								if(!empty($indice["peso"][1])){
									echo $indice["peso"][1]."<br>"; 
									$peso_total = $peso_total + $indice["peso"][1];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label  class="resultado" >
							<?php
								if(!empty($indice["quantidade"][1])){
									echo $indice["quantidade"][1]."<br>"; 
									$qtd_total = $qtd_total + $indice["quantidade"][1];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["peso"][2])){
									echo $indice["peso"][2]."<br>"; 
									$peso_total = $peso_total + $indice["peso"][2];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["quantidade"][2])){
									echo $indice["quantidade"][2]."<br>"; 
									$qtd_total = $qtd_total + $indice["quantidade"][2];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["peso"][3])){
									echo $indice["peso"][3]."<br>"; 
									$peso_total = $peso_total + $indice["peso"][3];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["quantidade"][3])){
									echo $indice["quantidade"][3]."<br>"; 
									$qtd_total = $qtd_total + $indice["quantidade"][3];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["peso"][4])){
									echo $indice["peso"][4]."<br>"; 
									$peso_total = $peso_total + $indice["peso"][4];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["quantidade"][4])){
									echo $indice["quantidade"][4]."<br>"; 
									$qtd_total = $qtd_total + $indice["quantidade"][4];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["peso"][5])){
									echo $indice["peso"][5]."<br>"; 
									$peso_total = $peso_total + $indice["peso"][5];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["quantidade"][5])){
									echo $indice["quantidade"][5]."<br>"; 
									$qtd_total = $qtd_total + $indice["quantidade"][5];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["peso"][6])){
									echo $indice["peso"][6]."<br>"; 
									$peso_total = $peso_total + $indice["peso"][6];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["quantidade"][6])){
									echo $indice["quantidade"][6]."<br>"; 
									$qtd_total = $qtd_total + $indice["quantidade"][6];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["peso"][7])){
									echo $indice["peso"][7]."<br>"; 
									$peso_total = $peso_total + $indice["peso"][7];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor">
						<label class="resultado">
							<?php
								if(!empty($indice["quantidade"][7])){
									echo $indice["quantidade"][7]."<br>"; 
									$qtd_total = $qtd_total + $indice["quantidade"][7];
								}
							?>
						</label>
					</div>
					<div class="coluna_menor" >
						<label class="resultado">
							<?php
								echo $peso_total; 
							?>
						</label>
					</div>
					<div class="coluna_menor" style="width:4%;">
					<label class="resultado">
							<?php
								echo $qtd_total; 
							?>
						</label>
					</div>
				
			</div>
			<?php
				
			} 
		}
			?>
			</div>
		</div>
	</div>
</div>
	
	</br><p>&nbsp;</p>
<div style="border: 0px solid red; float:left; margin-top:2%; width:100%;">
	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" style="padding-top:5%">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
						Importação de Cotas 	
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="" class="m-portlet__body " >

			<!--begin: Datatable -->
			<form action="<?php echo base_url('AreaAdministrador/importaCotaSimples/'); ?>" method="post" id="form" name="form" enctype="multipart/form-data">
				<div class="linha">
					<div class="coluna_botoes_mosaic">
						<label for="arquivo"><input type="file" name="arquivo" required /></label>
					</div>
					<div class="coluna_botoes_mosaic" > 
						<button type="submit" class='btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase' style="margin-left:5%; margin-bottom:5%;">Continuar</button>
					</div>
				</div>
				
			</form>			
		</div>
	</div>
</div> 


<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Atualização realizada com sucesso!',
           	type: "success"
        }).then(function() {
		   	window.location = base_url+'AreaAdministrador/importaCotas';
		});
	</script>	
<?php unset($_SESSION['sucesso']); } ?>