<div class="m-content">	
	<form accept-charset="utf-8" action="<?php echo base_url('AreaAdministrador/atualizaEquipamento/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-building"></i>
					</span>
					<h3 class="m-portlet__head-text"> 
						Equipamento #<?=$dados['id']?>
					</h3>
				</div>			
			</div> 
			<div class="m-portlet__head-tools">
				
			</div>
		</div>				
		<div class="m-portlet__body">		
			<div class="form-group m-form__group row">									
				<div class="col-lg-6">
					<label>Nome:</label>
					<div class="m-input-icon m-input-icon--right" style="width: 80%;">
						<input type="text" name="nome" id="nome" class="form-control" placeholder="" required value="<?=$dados['nome']?>" />						
						<input type="hidden" name="id" id="id" class="form-control" placeholder="" required value="<?=$dados['id']?>" />						
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-file-text"></i></span></span>
					</div>					
				</div>
				<div class="col-lg-6">
					<label>Descrição:</label>
					<div class="m-input-icon m-input-icon--right" style="width: 80%;">
						<textarea name="descricao" id="descricao" class="form-control" placeholder="" required><?=$dados['descricao']?></textarea>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-file-text"></i></span></span>						
						
					</div>					
				</div>		
			</div>	
			<div class="form-group m-form__group row">									
				<div class="col-lg-4">
					<label>Tipo</label>					
					<div class="m-input-icon m-input-icon--right">
						<select name="id_tipo" id="id_tipo" class="form-control m-input" required>
							<option value="">	Selecione o tipo de equipamento 	</option>
						<?php foreach($tipos as $tipo) { ?>
							<option value="<?php echo $tipo['id']; ?>" <?=($dados['id_tipo'] == $tipo['id']) ? 'selected="selected"' : '' ?>><?php echo strtoupper($tipo['ds_tipo']); ?></option>
						<?php } ?>
						</select>
						<span class="m-input-icon__icon m-input-icon__icon--right">
							<span><i class="la la-truck"></i></span>
						</span>						
					</div>					
				</div>
				<div class="col-lg-4">
					<label>Coordenada X:</label>
					<div class="m-input-icon m-input-icon--right" style="width: 80%;">
						<input type="text" name="coord_x" id="coord_x" class="form-control" placeholder="" required value="<?=$dados['coord_x']?>"/>						
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>											
					</div>					
				</div>
				<div class="col-lg-4">
					<label>Coordenada Y:</label>
					<div class="m-input-icon m-input-icon--right" style="width: 80%;">
						<input type="text" name="coord_y" id="coord_y" class="form-control" placeholder=""  value="<?=$dados['coord_y']?>" required />						
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>											
					</div>					
				</div>					
			</div>	
			<div class="form-group m-form__group row">
				<div class="col-lg-4">
					<label>Ativo</label>					
					<div class="m-input-icon m-input-icon--right">
						<select name="fl_ativo" id="fl_ativo" class="form-control m-input" required>
							<option value="S" <?=($dados['fl_ativo'] == 'S') ? 'selected="selected"' : '' ?>>	Ativo 	</option>
							<option value="N" <?=($dados['fl_ativo'] == 'N') ? 'selected="selected"' : '' ?>>	Não 	</option>
						</select>											
					</div>					
				</div>
			</div>		
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="form-group row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>
					</div>
				</div>
			</div>					
		</div>
		
	</div>
</form>
</div>

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Agendamento solicitado com sucessoo!',
                type: 'success'
            }).then(function() {
                //window.location = base_url+'AreaAdministrador/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>