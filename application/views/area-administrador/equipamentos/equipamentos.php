<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-calendar" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Equipamentos 
					</h3>	
					<h3 class="m-portlet__head-text" style=" padding-left: 10px;" >
						<a href="<?php echo base_url('AreaAdministrador/cadastrarEquipamento'); ?>"  style="color: #ffcc00; font-weight: bold;" data-toggle="m-tooltip" data-placement="top" title="Novo Equipamento" id="Novo Equipamento">
							<i class="la la-plus-circle" style="font-size: 38px;"></i>
						</a>
					</h3>									
				</div>

			</div> 
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
			<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">#</th>
						<th style="width: 20%;">NOME</th>						
						<th style="width: 25%;">DESCRICAO</th>						
						<th style="width: 15%;">TIPO</th>	
						<th style="width: 10%;">COORDENADA X</th>
						<th style="width: 10%;">COORDENADA Y</th>
						<th style="width: 10%;">ATIVO</th>
						<th style="width: 5%;">Ações</th>
					</tr>
				</thead>
				<tbody>					
				<?php foreach($equipamentos as $equipamento){				?>
					<tr>
						<td style="text-align: center;"><?php echo $equipamento['id']; ?></td>
						<td ><?php echo $equipamento['nome']; ?></td>											
						<td ><?php echo $equipamento['descricao']; ?></td>
						<td ><?php echo $equipamento['tipo']; ?></td>	
						<td ><?php echo $equipamento['coord_x']; ?></td>
						<td ><?php echo $equipamento['coord_y']; ?></td>
						<td style="text-align: center !important;"><?php echo $equipamento['fl_ativo']; ?></td>
						<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
							<a href="<?php echo base_url('AreaAdministrador/editarEquipamento/'.$equipamento['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
								<i class="la la-edit"></i>
							</a>
							<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" target="blank" onclick="excluirEquipamento(<?=$equipamento['id']?>)">
								<i class="la la-trash"></i>
							</a>				
						</td>
					</tr>
					<?php } ?> 
				</tbody>
			</table>			
		</div>	
		</div>						
	</div>
</div>
<!-- end:: Body -->
<?php if ($this->session->flashdata('retorno') == 'erro'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "Atenção!",
           	text: '<?php echo $this->session->flashdata('msg'); ?>',
           	type: "warning"
        }).then(function() {
		   	
		});
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('retorno') == 'sucesso'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Operação realizada com sucesso!',
           	type: "success"
        }).then(function() { });
	</script>	
<?php unset($_SESSION['sucesso']); } ?>
