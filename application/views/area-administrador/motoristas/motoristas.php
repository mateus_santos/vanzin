<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						<i class="fa fa-id-card-o"></i>   Gestão de Motoristas
					</h3>
				</div>
			</div>			
		</div>
		<div class="m-portlet__body">
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="Field #1" >ID</th>
						<th title="Field #2" >Nome</th>
						<th title="Field #3" >CPF</th>
						<th title="Field #4" >CNH</th>
						<th title="Field #5" >Fone</th>																		
						<th title="Field #5" >Placas</th>
						<th title="Field #8" >Motivo</th>
						<th title="Field #8" >Usuário Bloq.</th>
						<th title="Field #8" >Bloqueado</th>
					</tr>
				</thead>
				<tbody>
					<tr>
					<?php foreach($dados as $dado){ ?>
						<td style="text-align: center;"><?php echo $dado->id; ?></td>
						<td><?php echo $dado->nome; ?></td>
						<td><?php echo $dado->cpf; ?></td>
						<td><?php echo $dado->nr_cnh; ?></td>
						<td><?php echo $dado->fone; ?></td>						
						<td><?php echo $dado->placas; ?></td>	
						<td><?php echo $dado->motivo_bloqueio; ?></td>
						<td><?php echo $dado->usuario_bloqueio; ?></td>						
						<td>
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" name="ativo<?php echo $dado->id;?>" onclick="atualizarStatus('S',<?php echo $dado->id?>);" class="ativo" <?php if($dado->fl_bloqueado=='S') echo 'checked="checked"' ?> value="S" user_id="<?php echo $dado->id; ?>"> Sim
									<span></span>
								</label>									
								<label class="m-radio">
									<input type="radio" name="ativo<?php echo $dado->id;?>" class="ativo" <?php if($dado->fl_bloqueado=='N') echo 'checked="checked"' ?> value="N" user_id="<?php echo $dado->id; ?>" onclick="atualizarStatus('N',<?php echo $dado->id?>);" > Não
									<span></span>
								</label>								
							</div>
						</td>						
					</tr>
						<?php } ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>		        
</div>		
<div class="modal fade" id="m_atualiza" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_atualiza_title" id="exampleModalLongTitle">
					Motivo do bloqueio do motorista
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="">											
				<div class="row motivo_esconde" >
					<div class="form-group m-form__group col-lg-12" >
						<label for="exampleSelect1">Motivo</label>							
						<textarea class="form-control" name="motivo" id="motivo"></textarea>
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal" id="inativar" acesso_id="">Bloquear</button>
			</div>
		</div>
	</div>
</div>		
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Atualização realizada com sucesso!',
           	type: "success"
        }).then(function() {
			
		});  
	</script>	
<?php unset($_SESSION['sucesso']); } ?>