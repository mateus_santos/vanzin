<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/editarCliente');?>">
		<i class="m-menu__link-icon la la-user" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>					
<li class="m-menu__item " aria-haspopup="true" >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/gestaoEmpresas');?>">
		<i class="m-menu__link-icon la la-building-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gestão de Empresas</span>
			</span>
		</span>
	</a>
</li>			
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/gestaoUsuarios');?>">
		<i class="m-menu__link-icon fa fa-users" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gestão de Usuários</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/motoristas');?>">
		<i class="m-menu__link-icon fa fa-id-card-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gestão de Motoristas</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/gestaoCotas');?>">
		<i class="m-menu__link-icon fa fa-pie-chart" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Cotas por Cliente</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/gestaoCotasTurno');?>">
		<i class="m-menu__link-icon fa fa-pie-chart" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Cotas por Turno Cliente</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/importaCotas');?>">
		<i class="m-menu__link-icon fa fa-pie-chart" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Importação de Cotas</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaClientes/mensagens');?>">
		<i class="m-menu__link-icon fa fa-send-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Mensagens</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaClientes/agendamentos/');?>">
		<i class="m-menu__link-icon la la-truck" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Agendamentos</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaClientes	/importarAgendamentos/');?>">
		<i class="m-menu__link-icon la la-truck" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Importar Agendamentos</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/classificacao/');?>">
		<i class="m-menu__link-icon fa fa-list-alt" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Classificação</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/expedicao/');?>">
		<i class="m-menu__link-icon fa fa-list-alt" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Expedição</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaGate/acessoPatio/');?>">
		<i class="m-menu__link-icon fa fa-car" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Entrada\Saída de Veículos</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaTransito/');?>">
		<i class="m-menu__link-icon fa fa-users" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Trânsito Simplificado</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/turnos/');?>">
		<i class="m-menu__link-icon fa fa-users" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Cadastra Turnos</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/equipamentos/');?>">
		<i class="m-menu__link-icon fa fa-cog" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Equipamentos</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/veiculosBloqueados/');?>">
		<i class="m-menu__link-icon fa fa-car" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Veículos Bloqueados</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaGate/reImpressao/');?>">
		<i class="m-menu__link-icon fa fa-print" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Reimpressão de Tickect</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/folhaCarga/');?>">
		<i class="m-menu__link-icon fa fa-print" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Folha de Carga</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaTransito/emissaoTransito/');?>">
		<i class="m-menu__link-icon fa fa-print" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Emissão de Extrato de Trânsito Simplificado</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaTransito/ajusteTransito/');?>">
		<i class="m-menu__link-icon fa fa-print" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Ajuste de Saldo Trânsito Simplificado</span>
			</span>
		</span>
	</a>
</li>


<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relatorioAgendamento/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório Caminhões Agendados</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relatorioCotasAgendamento/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório Cotas X Agendamentos</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relatorioEstacionados/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório Caminhões no Estacionamento</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relatorioSaida/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório de Saída</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relNfeRecepcionadasCCT/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório NF. Recepcionadas CCT</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relEntregaCargasDUE/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório Entrega de Cargas DUE</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relatorioApiRecintos/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório API Recintos</span>
			</span>
		</span>
	</a>
</li>

