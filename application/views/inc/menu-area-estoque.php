<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/editarCliente');?>">
		<i class="m-menu__link-icon la la-user" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>	
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relNfeRecepcionadasCCT/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório NF. Recepcionadas CCT</span>
			</span>
		</span>
	</a>
</li>