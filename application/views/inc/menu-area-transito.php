<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaTransito/');?>">
		<i class="m-menu__link-icon fa fa-users" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Trânsito Simplificado</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaTransito/emissaoTransito/');?>">
		<i class="m-menu__link-icon fa fa-print" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Emissão de Extrato de Trânsito Simplificado</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaTransito/ajusteTransito/');?>">
		<i class="m-menu__link-icon fa fa-print" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Ajuste de Saldo Trânsito Simplificado</span>
			</span>
		</span>
	</a>
</li>