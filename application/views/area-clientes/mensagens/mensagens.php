<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Mensagens 	<a href="<?php echo base_url('AreaClientes/cadastrarMensagem'); ?>" style="color: #ffcc00; font-weight: bold;" ><i class="la la-plus-circle" style="font-size: 38px;"></i>
						</a>
					</h3>
				</div>
			</div>					
		</div>
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">#</th>
						<th style="width: 25%;">Titulo</th>						
						<th style="width: 25%;">Descrição</th>						
						<th style="width: 20%;">Origem</th>	
						<th style="width: 10%;">Destino</th>
						<th style="width: 10%;">Dthr. Envio</th>
						<th style="width: 5%;">Ações</th>
					</tr>
				</thead>
				<tbody>					
				<?php foreach($mensagens as $mensagem){
					$cor = "";
					switch($mensagem['status_id']){
						case '1':
							$cor = '#eaeaea';
							break;
						case '2':
							$cor = '#ff9800; color: #fff !important;';
							break;
						
					}

				?>
					<tr>
						<td style="text-align: center; background: <?=$cor;?>"><?php echo $mensagem['id']; ?></td>
						<td style="background: <?=$cor;?>"><?php echo $mensagem['titulo']; ?></td>											
						<td style="background: <?=$cor;?>"><?php echo $mensagem['descricao']; ?></td>
						<td style="background: <?=$cor;?>"><?php echo $mensagem['origem'].' - '. $mensagem['empresa_origem']; ?></td>	
						<td style="background: <?=$cor;?>"><?php echo $mensagem['destinatarios']; ?></td>
						<td style="background: <?=$cor;?>"><?php echo date('d/m/Y H:i:s', strtotime($mensagem['dthr_criacao'])); ?></td>
						<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;background: <?=$cor;?>">
							<a href="<?php echo base_url('AreaClientes/visualizarMensagem/'.$mensagem['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
								<i class="la la-edit"></i>
							</a>
							<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" target="blank" onclick="excluirmensagem(<?=$mensagem['id']?>)">
								<i class="la la-trash"></i>
							</a>				
						</td>
					</tr>
					<?php } ?> 
				</tbody>
			</table>			
		</div>	
	</div>						
</div>
	<!--end::Portlet-->	

	

	
	<!--end::Portlet-->	
</div>	
<div class="modal fade" id="m_lista_anexo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_lista_anexo_title" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
				<div class="row anexos">
					
				</div>				
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<!-- end:: Body -->
<?php if ($this->session->flashdata('retorno') == 'erro'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "Atenção!",
           	text: '<?php echo $this->session->flashdata('msg'); ?>',
           	type: "warning"
        }).then(function() {
		   	
		});
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('retorno') == 'sucesso'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Mensagem cadastrada e enviada com sucesso!',
           	type: "success"
        }).then(function() { });
	</script>	
<?php unset($_SESSION['sucesso']); } ?>
