<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<a href="<?php echo base_url('AreaAdministrador/mensagens/');?>"><i class="la la-arrow-left"></i></a>
					</span>
					<h3 class="m-portlet__head-text" style="padding-right: 10px;">	
						<a href="<?php echo base_url('AreaAdministrador/mensagens/');?>">Voltar</a>						
					</h3>
					<span class="m-portlet__head-icon">
						<i class="la la-building-o"></i>
					</span>

					<h3 class="m-portlet__head-text">
						Mensagem #<?php  echo $dados['id']; ?>						
					</h3>
				
				</div>	

			</div>													
		</div>
		<div class="m-portlet__body">
			<div class="form-group m-form__group row">
				<div class="col-lg-4 col-xs-12">
					<h5>Origem: 
						<small class="text-muted"> <?php echo $dados['origem'] . ' - '. $dados['empresa_origem'];?></small>
					</h5>
				</div>					
				<div class="col-lg-4 col-xs-12">
					<h5>Título: 
						<small class="text-muted"> <?php echo $dados['titulo'];?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12" >
					<h5>Mensagem: 
						<small class="text-muted"> <?php echo $dados['descricao'];?></small>
					</h5>
				</div>
			</div>	
			<div class="form-group m-form__group row">
				<div class="col-lg-12 col-xs-12">
					<h5>Destinatário(s): 
						<small class="text-muted"> <?php echo $dados['destinos'];?></small>
					</h5>
				</div>
			</div>		
			<form action="<?php echo base_url('AreaClientes/enviarResposta'); ?>" method="post">
			<div class="form-group m-form__group row" style="" >									
				<div class="col-lg-12">
					<h5>Responder</h5>
					<div class="m-radio-inline">
						<textarea class="form-control" name="descricao" id="texto" required ></textarea>		
						<input type="hidden" 	class="form-control" name="destinatario_id" id="destinatario_id" value="<?php echo $dados['usuario_id'];?>"  />		
						<input type="hidden" 	class="form-control" name="mensagem_id" id="mensagem_id" value="<?php echo $dados['id'];?>"  />						
					</div>
				</div>
			</div>
			<div class="m-form__actions m-form__actions--solid">
				<div class="form-group row">
					<div class="col-lg-6">
						<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
						<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
					</div>
				</div>
			</div>
			</form>				
		</div>
	</div>	
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">	
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>Respostas</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<table class="table m-table--head-separator-warning">
			  	<thead>
			    	<tr>				    		
			      		<th>Nome</th>
			      		<th>Empresa</th>
			      		<th>Resposta</th>						
			      		<th>Dthr. Resposta</th>
			      		<!--<th>Ação</th>-->
			    	</tr>
			  	</thead>
			  	<tbody>
			  		<?php foreach($respostas as $resposta){ ?>
			    	<tr>
				      	<td><?php echo $resposta['origem']; ?></td>
				      	<td><?php echo $resposta['empresa_origem']; ?></td>
				      	<td><?php echo $resposta['descricao']; ?></td>						
				      	<td><?php echo date('d/m/Y H:i:s', strtotime($resposta['descricao'])); ?></td>	
						<!--<td>
							<a class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill abrirModalResposta" mensagem_id="<?php echo $resposta['mensagem_id']; ?>" destinatario_id="<?=$resposta['destinatario_id']?>" >
							<i class="la la-edit editar"></i>
							</a>
						</td>-->
			    	</tr>			 
			    	<?php } ?>    	
			  	</tbody>
			</table>				
		</div>
	</div>					
</div>
<div class="modal fade" id="modal_resposta" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" >Cadastrar Novo Funcionário</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="max-width: 750px;">
					<form action="<?php echo base_url('AreaClientes/enviarResposta'); ?>" method="post">
						<div class="form-group m-form__group row" style="" >									
							<div class="col-lg-12">
								<h5>Responder</h5>
								<div class="m-radio-inline">
									<textarea class="form-control" name="descricao" id="texto_" required ></textarea>		
									<input type="hidden" 	class="form-control" name="destinatario_id" id="destinatario_id_" value="<?php echo $dados['usuario_id'];?>"  />		
									<input type="hidden" 	class="form-control" name="mensagem_id" id="mensagem_id_" value="<?php echo $dados['id'];?>"  />						
								</div>
							</div>
						</div>
						<div class="m-form__actions m-form__actions--solid">
							<div class="form-group row">
								<div class="col-lg-6">
									<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
									<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
								</div>
							</div>
						</div>
					</form>	
				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>

					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="adicionar_funcionario()">Adicionar Funcionário</button>

				</div>

			</div>

		</div>

	</div>

<!-- Modal Anexar nfe ao pedido -->
<div class="modal fade" id="vincularCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/vincularClienteClass');?>" method="post"  enctype="multipart/form-data">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-anexar-nfe-title" >Vincular Cliente a <b><?php echo $dados[0]->razao_social; ?></b></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="max-width: 750px;">					
				<div class="row">
					<div class="form-group m-form__group col-lg-12 col-xs-12">						
						<label for="">Cliente</label>
					<?php if( $dados[0]->tipo_cadastro_id == 17){ ?>
						<input type="hidden" name="classificadora_id" value="<?php  echo $dados[0]->id; ?>">
					<?php }else{ ?>
						<input type="hidden" name="administrador_id" value="<?php  echo $dados[0]->id; ?>">
					<?php } ?>	
						<select class="form-control" name="cliente_id" required="required">
							<option value="" > Selecione um cliente </option>
						<?php 	foreach($clientes as $cliente){	?>
							<option value="<?php echo $cliente['id']; ?>"> <?php echo $cliente['cliente']; ?> </option>
						<?php } ?>
						</select>
					</div>						
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="submit" class="btn btn-primary" >Enviar</button>
			</div>
		</div>			
	</div>
	</form>
</div>
<?php if ($this->session->flashdata('retorno') == 'erro'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "Atenção!",
           	text: 'erro',
           	type: "warning"
        }).then(function() {
		   	
		});
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('retorno') == 'sucesso'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Resposta enviada com sucesso!',
           	type: "success"
        }).then(function() { });
	</script>	
<?php unset($_SESSION['sucesso']); } ?>
