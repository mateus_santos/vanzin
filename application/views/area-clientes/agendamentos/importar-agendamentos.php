<?php if(!empty($importou)){ ?>
<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<?php
		if(!empty($dados)){ ?>
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Listagem de ocorrências dos Agendamentos importados 	
					</h3>
					
				</div>
			</div>					
		</div>
		<?php 
	/*	echo "<div><pre>";
		var_dump($dados);
		echo "</div></pre>";*/
		?>
		<div id="msg" class="msg" ></div>
		<div class="tabela" id="tab_resultado"  style="border:0px;">
			<div class="linha_titulo" id="linha_titulo_dias"  style="border:0px;">
				<div class="coluna"  style="width:10%; border:0px;">
					<label class="titulo">Número</label>
				</div>
				
				<div class="coluna" style="width:90%;">
					<label class="titulo">Erro</label>
				</div>
			</div>
			
			<?php } ?>
			<div id="resultado" >
			<?php 
			if(!empty($dados)){
				$controle_cliente = array();
				$controle_geral = array();
				foreach($dados as $dado){

				?>
				
				<div class="linha_titulo" id="linha_titulo" >
					<div class="coluna" style="width:10%;">
						<label class="resultado" style="text-align:right; width:10%; margin-top:0%;" >
							<?php echo $dado->nr_arquivo; ?> 
						</label>
					</div>
				
					<div class="coluna" style="width:90%;">
						<label  class="resultado"  style="text-align:left; width:80%; margin-top:0%;" >
							<?php
								if(!empty($dado->ds_erro)){
									echo $dado->ds_erro; 
									
								}
							?>
						</label>
					</div>
			</div>
			<?php
				
			} 
		}
			?>
			</div>
		</div>
	</div>
</div>
<?php } ?>	
	</br><p>&nbsp;</p>
<div style="border: 0px solid red; float:left; margin-top:2%; width:100%;">
	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" style="padding-top:5%">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
						Importação de Agendamentos 	
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="" class="m-portlet__body " >

			<!--begin: Datatable -->
			<form action="<?php echo base_url('AreaClientes/importaArquivoAgendamentos/'); ?>" method="post" id="form" name="form" enctype="multipart/form-data">
				<div class="linha">
					<div class="coluna_botoes_mosaic">
						<label for="arquivo"><input type="file" name="arquivo" required /></label>
					</div>
					<div class="coluna_botoes_mosaic" > 
						<button type="submit" class='btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase' style="margin-left:5%; margin-bottom:5%;">Continuar</button>
					</div>
				</div>
				
			</form>			
		</div>
	</div>
</div> 


<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Atualização realizada com sucesso!',
           	type: "success"
        }).then(function() {
		   	window.location = base_url+'AreaClientes/importarAgendamentos';
		});
	</script>	
<?php unset($_SESSION['sucesso']); } ?>