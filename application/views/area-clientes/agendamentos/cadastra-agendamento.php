<div class="m-content">	
	<form accept-charset="utf-8" action="<?php echo base_url('AreaClientes/inserirAgendamento/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-building"></i>
					</span>
					<h3 class="m-portlet__head-text"> 
						Novo Agendamento
					</h3>
				</div>			
			</div> 
			<div class="m-portlet__head-tools">
				
			</div>
		</div>
				
		<div class="m-portlet__body">		
			<div class="form-group m-form__group row">									
				<div class="col-lg-12">
					<label>Tipo de Agendamento</label>
					<div class="m-radio-inline">
						<label class="m-radio">
							<input type="radio" name="id_tipo_operacao" class="tp_operacao" checked="checked" id="tipo_operacao_clas" value="1" > Classificação (Caminhão Cheio)
							<span></span>
						</label>									
						<label class="m-radio">
							<input type="radio" name="id_tipo_operacao" class="tp_operacao" id="tipo_operacao_exp" value="2"> Expedição (Caminhão Vazio)
							<span></span>
						</label>								
					</div>					
				</div>
			</div>	
			<div class="form-group m-form__group row fretes" style="display: none;">									
				<div class="col-lg-3">
					<label>Fretes</label>					
					<div class="m-input-icon m-input-icon--right">
						<select  name="id_frete" id="id_frete" class="form-control m-input" >
							<option value="">	Selecione um frete 	</option>
						<?php foreach($fretes as $frete) { ?>
							<option value="<?php echo $frete['id']; ?>"><?php echo strtoupper($frete['descricao']); ?></option>
						<?php } ?>
						</select>
						<span class="m-input-icon__icon m-input-icon__icon--right">
							<span><i class="la la-truck"></i></span>
						</span>						
					</div>					
				</div>
				<div class="col-lg-6">
					<label>Transportadora:</label>
					<div class="m-input-icon m-input-icon--right" style="width: 80%;">
						<input type="text" name="transportadora" id="transportadora" class="form-control" placeholder="Pesquise por uma Transportadora" />
						<input type="hidden" name="id_transportadora" id="id_transportadora"  />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search"></i></span></span>						
						<span class="m-input-icon__icon m-input-icon__icon--right" style="right: -40px;" id="add_empresa" data-toggle="m-tooltip" data-placement="top" title="Adicionar Transportadora"><span><i class="la la-plus-circle" style="color: #ffcc00;font-size: 28px;"></i></span></span>
					</div>					
				</div>					
			</div>				
			<div class="form-group m-form__group row">									
				<div class="col-lg-12">
					<label>Empresa</label>
					<?php if( $tipo_acesso == 'administrador geral' || $tipo_acesso == 'administrador empresas'){ ?>						
						<div class="m-input-icon m-input-icon--right">
							<select  name="id_empresa" id="id_empresa" class="form-control m-input" required>
								<option value="">	Selecione uma empresa 	</option>
							<?php foreach($empresas as $empresa) { ?>
								<option value="<?php echo $empresa['id']; ?>"><?php echo strtoupper($empresa['razao_social'].' - '.$empresa['cnpj']); ?></option>
							<?php } ?>
							</select>
							<span class="m-input-icon__icon m-input-icon__icon--right">
								<span><i class="la la-building"></i></span>
							</span>						
						</div>
					<?php }else{ ?>
						<h4 class="m--font-warning"><?php echo $empresa; ?>	</h4>
						<input type="hidden" name="id_empresa" id="id_empresa" value="<?php echo $id_empresa; ?>">
					<?php } ?>
					
				</div>	
			</div>			
			<div class="form-group m-form__group row">
				<div class="col-lg-6">
					<label id>Data</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="data" id="data" class="form-control m-input datepicker" required  />
						<input type="hidden" id="data_atual" value="<?php echo date('d/m/Y'); ?>"  />
						<span class="m-input-icon__icon m-input-icon__icon--right">
							<span><i class="la la-calendar"></i></span>
						</span>						
					</div>
				</div>
				<div class="col-lg-6">
					<label >Turno</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="id_turno" id="id_turno" >
							<option value="">Selecione o turno</option>
							<?php foreach($turnos as $linha_turno){?>
								<option value="<?php echo $linha_turno['id']; ?>"><?php echo $linha_turno['nome']; ?></option>
							<?php } ?>	
						</select>
					</div>
				</div>
			</div>
			<div class="form-group m-form__group row">
				
				<div class="col-lg-6">
					<label >Placa</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required	name="placa" id="placa" class="form-control m-input placa" placeholder="">						
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-truck"></i></span>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group m-form__group row">
				<div class="col-lg-4">
					<label>Tipo de Motorista</label>
					<div class="m-radio-inline">
						<label class="m-radio">
							<input type="radio" name="fl_motorista" class="fl_motorista" checked="checked" id="fl_motorista_0" value="0" /> Brasileiro
							<span></span>
						</label>									
						<label class="m-radio">
							<input type="radio" name="fl_motorista" class="fl_motorista" id="fl_motorista_1" value="1"> Estrangeiro 
							<span></span>
						</label>								
					</div>					
				</div>
				<div class="col-lg-4" >
					<label class="">CPF</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="cpf" id="cpf" class="form-control m-input" placeholder="" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
					</div>	
				</div>
				<div class="col-lg-4" id="campo_ie">
					<label class="">CNH</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="cnh" id="cnh" class="form-control m-input" placeholder="" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
					</div>	
				</div>	
			</div>	 
			<div class="form-group m-form__group row">	
				<div class="col-lg-6">
					<label>Motorista</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text"  name="motorista" id="motorista" class="form-control m-input" placeholder="" value="" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>							
				</div>			
				<div class="col-lg-6">
					<label>Telefone:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="telefone" id="telefone" class="form-control m-input telefone" placeholder="Insira um telefone" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
					</div>							
				</div>
			</div>
			<hr/>
			<div class="form-group m-form__group row div_nota_fiscal">	
				<div class="col-lg-12">
					<label>Nota Fiscal (XML)</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="file" id="nfe" class="form-control m-input" placeholder="Selecione uma nota fiscal" value="" accept=".xml" required="required" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>							
				</div>
			</div>
			<div class="form-group m-form__group row">	
				<div class="col-lg-12">
					<div id="tabela_exp"></div>
					<table class="table" id="tabela_produtos" style="display: none;">
						<thead>
							<th>Nº Pedido</th>
							<th>Produto</th>
							<th>Embalagem</th>
							<th>Quant.(KG)</th>
						</thead>
						<tbody id="produtos_xml">
							
						</tbody>
					</table>
					<div id="div-nfe" style="display: none;">

					</div>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="form-group row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>
					</div>
				</div>
			</div>					
		</div>
		
	</div>
</form>
</div>

<div class="modal fade" id="m_add_empresa" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-status-title" id="exampleModalLongTitle">Cadastrar Transportadora</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 950px;">
			<div name="form_transportadora" id="form_transportadora" accept-charset="utf-8" method="post" >		
				<div class="form-group m-form__group row">
					<div class="col-lg-3">
						<label id='label_documento'>Documento (CNPJ):</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required name="cnpj" id="cnpj" class="form-control m-input" placeholder="Insira um cnpj" required value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							<input type="hidden"  id="salvar" maxlength="15" name="salvar" value="1"    required />
							<input type="hidden"  id="cartao_cnpj" name="cartao_cnpj"  />
	                        <input type="hidden"  id="empresa_id" maxlength="15" name="empresa_id" value=""  />                        
	                        <input type="hidden"  id="tipo_cadastro_id" maxlength="15" name="tipo_cadastro_id" value="23"  />                        
						</div>	
					</div>
					<div class="col-lg-3">
						<label id="label_razao">Razão Social:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required	name="razao_social" id="razao_social" class="form-control m-input" placeholder="" required value="">
							<input type="hidden" name="id" 	class="form-control m-input" placeholder="Insira uma razão social" required value="">								
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
							</span>
						</div>
					</div>
					<div class="col-lg-3" id="campo_fantasia">
						<label class="">Fantasia:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="fantasia" id="fantasia" class="form-control m-input" placeholder="Insira uma fantasia" required value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
						</div>	
					</div>
					<div class="col-lg-3" id="campo_ie">
						<label class="">Inscrição Estadual:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control m-input" placeholder="Inscrição Estadual" required value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
						</div>	
					</div>					
					
				</div>	 
				<div class="form-group m-form__group row">	
					<div class="col-lg-3">
						<label>E-mail:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="email"  name="email" id="email" class="form-control m-input" placeholder="Insira um email" value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
						</div>							
					</div>			
					<div class="col-lg-3">
						<label>Telefone:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required name="telefone" id="telefone_empresa" class="form-control m-input telefone" placeholder="Insira um telefone" required value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
						</div>							
					</div>
					<div class="col-lg-3">
						<label>Cep:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required name="cep" id="cep" class="form-control m-input" placeholder="Insira um cep" required value="" >
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
						</div>							
					</div>
					<div class="col-lg-3">
						<label>Endereço:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required name="endereco" id="endereco" class="form-control m-input" placeholder="Insira um endereço" required value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
						</div>							
					</div>
					
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-3">
						<label>Bairro:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required name="bairro" id="bairro" class="form-control m-input" placeholder="Insira o bairro" required value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
						</div>							
					</div>	
					<div class="col-lg-3">
						<label class="">Cidade:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="cidade" required id="cidade" class="form-control m-input" placeholder="Insira um cidade" value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
						</div>						
					</div>
					<div class="col-lg-3">
						<label class="">Estado:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="hidden" name="estado" id="estadoE" class="form-control" placeholder="Insira um estado" />
							<select  class="form-control"  maxlength="250" name="estado" placeholder="Estado" id="estado" required>
	                            <option value="">Selecione um Estado</option>
	                            <option value="AC">Acre</option>
	                            <option value="AL">Alagoas</option>
	                            <option value="AP">Amapá</option>
	                            <option value="AM">Amazonas</option>
	                            <option value="BA">Bahia</option>
	                            <option value="CE">Ceará</option>
	                            <option value="DF">Distrito Federal</option>
	                            <option value="ES">Espírito Santo</option>
	                            <option value="GO">Goiás</option>
	                            <option value="MA">Maranhão</option>
	                            <option value="MT">Mato Grosso</option>
	                            <option value="MS">Mato Grosso do Sul</option>
	                            <option value="MG">Minas Gerais</option>
	                            <option value="PA">Pará</option>
	                            <option value="PB">Paraíba</option>
	                            <option value="PR">Paraná</option>
	                            <option value="PE">Pernambuco</option>
	                            <option value="PI">Piauí</option>
	                            <option value="RJ">Rio de Janeiro</option>
	                            <option value="RN">Rio Grande do Norte</option>
	                            <option value="RS">Rio Grande do Sul</option>
	                            <option value="RO">Rondônia</option>
	                            <option value="RR">Roraima</option>
	                            <option value="SC">Santa Catarina</option>
	                            <option value="SP">São Paulo</option>
	                            <option value="SE">Sergipe</option>
	                            <option value="TO">Tocantins</option>
	                        </select>
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
						</div>							
					</div>					
					<div class="col-lg-3">
						<label>País:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required name="pais" id="pais" class="form-control m-input" placeholder="Insira um pais" required value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map"></i></span></span>
						</div>							
					</div>
				</div>				
			</div>
		</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" id ="cadastrar" class="btn btn-primary" >Cadastrar</button>
			</div>

		</div>
	</div>
</div>	
		

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Agendamento solicitado com sucessoo!',
                type: 'success'
            }).then(function() {
                //window.location = base_url+'AreaAdministrador/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>