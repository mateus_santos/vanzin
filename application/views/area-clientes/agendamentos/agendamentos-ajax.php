<table class="" id="html_table" width="100%">
<thead>
	<tr>
		<th style="width: 5%;">#</th>
		<th style="width: 35%;">Cliente</th>						
		<th style="width: 15%;">Data</th>						
		<th style="width: 20%;">Placa</th>	
		<th style="width: 10%;">Motorista</th>
		<th style="width: 10%;">Frete</th>
		<th style="width: 5%;">Ações</th>
	</tr>
</thead>
<tbody>					
	<?php foreach($agendamentos as $agendamento){	
		switch($agendamento['status_agendamento']){
			case 'A':
				$cor = '#eaeaea';
				break;
			case 'WC':
				$cor = '#ff9800; color: #fff !important;';
				break;
			case 'WE':
				$cor = '#5867dd; color: #fff !important;';
				break;
			case 'O':
				$cor = '#34bfa3; color: #fff !important;';
				break;
		}
	?>
		<tr>
			<td style="text-align: center;background: <?=$cor;?>"><?php echo $agendamento['id']; ?></td>
			<td style="background: <?=$cor;?>"><?php echo $agendamento['razao_social']; ?></td>											
			<td style="background: <?=$cor;?>"><?php echo $agendamento['dt_agenda']; ?></td>
			<td style="background: <?=$cor;?>"><?php echo $agendamento['placa']; ?></td>	
			<td style="background: <?=$cor;?>"><?php echo $agendamento['nome']; ?></td>
			<td style="background: <?=$cor;?>"><?php echo $agendamento['frete']; ?></td>
			<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;background: <?=$cor;?>">
				<a href="<?php echo base_url('AreaClientes/editarAgendamento/'.$agendamento['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-edit"></i>
				</a>	
				<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" target="blank" onclick="excluirAgendamento(<?=$agendamento['id']?>)">
					<i class="la la-trash"></i>
				</a>			
			</td>
		</tr>
		<?php } ?> 
	</tbody>
</table>	