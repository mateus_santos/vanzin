-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 15-Ago-2020 às 20:30
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `wertco`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_cadastros`
--

CREATE TABLE `tipo_cadastros` (
  `id` int(11) NOT NULL,
  `descricao` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipo_cadastros`
--

INSERT INTO `tipo_cadastros` (`id`, `descricao`) VALUES
(1, 'clientes'),
(2, 'representantes'),
(3, 'mecanicos'),
(4, 'tecnicos'),
(5, 'administrador comercial'),
(6, 'indicadores'),
(7, 'administrador tecnico'),
(8, 'administrador geral'),
(9, 'assistencia tecnica'),
(10, 'indicadores2'),
(11, 'feira'),
(12, 'representantes2'),
(13, 'financeiro'),
(14, 'almoxarifado'),
(15, 'qualidade'),
(16, 'suprimentos');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `tipo_cadastros`
--
ALTER TABLE `tipo_cadastros`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `tipo_cadastros`
--
ALTER TABLE `tipo_cadastros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
