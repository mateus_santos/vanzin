  $(document).ready(function(){  
   $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI); 

   var mask = function (val) {
        val = val.split(":");
        return (parseInt(val[0]) > 19)? "HZ:M0" : "H0:M0";
    }

    pattern = {
        onKeyPress: function(val, e, field, options) {
            field.mask(mask.apply({}, arguments), options);
    },
    translation: {
        'H': { pattern: /[0-2]/, optional: false },
        'Z': { pattern: /[0-3]/, optional: false },
        'M': { pattern: /[0-5]/, optional: false}
    }
};

    $('#hora_inicial').mask("99:99",pattern);
    $('#hora_final').mask("99:99",pattern);

    $('#hora_inicial').blur(function(){
        if(($(this).val().length < 5) & ($(this).val().length > 0)){
            swal({
                title: "Ops!",
                text: "Hora inválida, por favor preencha a hora por completo",
                type: 'warning'
            }).then(function() {
                
            });
            $(this).val(''); 	
        }
    });	

    $('#hora_final').blur(function(){
        if(($(this).val().length < 5) & ($(this).val().length > 0)){
            swal({
                title: "Ops!",
                text: "Hora inválida, por favor preencha a hora por completo",
                type: 'warning'
            }).then(function() {
                
            });
            $(this).val(''); 	
        }
    });	

 });

