$(document).ready(function(){
	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);	

	$('#html_table').DataTable({
	    	"scrollX": true,
	    	"order": [[ 1, "desc" ]],
			"language": {
				"sEmptyTable": "Nenhum registro encontrado",
			    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
			    "sInfoPostFix": "",
			    "sInfoThousands": ".",
			    "sLengthMenu": "_MENU_    Resultados por página",
			    "sLoadingRecords": "Carregando...",
			    "sProcessing": "Processando...",
			    "sZeroRecords": "Nenhum registro encontrado",
			    "sSearch": "Pesquisar",
			    "oPaginate": {
			        "sNext": '<i class="la la-angle-double-right"></i>',
			        "sPrevious": '<i class="la la-angle-double-left"></i>',
			        "sFirst": "Primeiro",
			        "sLast": "Último"
			    },
			    "oAria": {
			        "sSortAscending": ": Ordenar colunas de forma ascendente",
			        "sSortDescending": ": Ordenar colunas de forma descendente"
			    }
			}
		});
	    
	 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
		$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
		$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
		$('.dataTables_filter').css('float','right');
		$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
		$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
		$('table thead').css('background-color', '#f4f3f8');
		//$('table thead').css('height', '150px');
		$('table tbody tr:odd').addClass('zebraUm');
		$('table tbody tr:even').addClass('zebraDois');

   		$('#table2').DataTable({
	    	"scrollX": true,
	    	"order": [[ 0, "desc" ]],
			"language": {
				"sEmptyTable": "Nenhum registro encontrado",
			    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
			    "sInfoPostFix": "",
			    "sInfoThousands": ".",
			    "sLengthMenu": "_MENU_    Resultados por página",
			    "sLoadingRecords": "Carregando...",
			    "sProcessing": "Processando...",
			    "sZeroRecords": "Nenhum registro encontrado",
			    "sSearch": "Pesquisar",
			    "oPaginate": {
			        "sNext": '<i class="la la-angle-double-right"></i>',
			        "sPrevious": '<i class="la la-angle-double-left"></i>',
			        "sFirst": "Primeiro",
			        "sLast": "Último"
			    },
			    "oAria": {
			        "sSortAscending": ": Ordenar colunas de forma ascendente",
			        "sSortDescending": ": Ordenar colunas de forma descendente"
			    }
			}
		});
		$('#table2_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
		$('#table2_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
		$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
		$('.dataTables_filter').css('float','right');
		$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
		$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
		$('table thead').css('background-color', '#f4f3f8');
		//$('table thead').css('height', '150px');
		$('table tbody tr:odd').addClass('zebraUm');
		$('table tbody tr:even').addClass('zebraDois');

		/*$('.classificar').bind('click', function(){			
			acesso_id 	= $(this).attr('acesso_id');
			placa 		= $(this).attr('placa');
			agendamento = $(this).attr('agendamento');
			$('.m_atualiza_title').empty();
			$('.m_atualiza_title').text('Classificar #'+acesso_id+' Placa: '+placa);
			$('#classificar').attr('acesso_id', acesso_id);
			$('#classificar').attr('agendamento', agendamento);
			$('#m_atualiza').modal('show');

		});*/

		$('.classificar_edit').bind('click', function(){

			acesso_id = $(this).attr('acesso_id');
			placa = $(this).attr('placa');
			situacao = $(this).attr('situacao');
			$('.m_atualiza_title').empty();
			$('.m_atualiza_title').text('Alterar Classificação #'+acesso_id+' Placa: '+placa);
			$('#classificar').attr('acesso_id', acesso_id);
			$('#classificacao option[value="'+situacao+'"]').attr('selected','selected');
			$('#m_atualiza').modal('show');

		});

		$('#classificacao').bind('change', function(){
				
			if($(this).val()=='N' ){
				$('.motivo_esconde').slideDown('slow');
				$('.destino_esconde').slideUp('slow');
			}else{
				$('.motivo_esconde').slideUp('slow');
				$('.destino_esconde').slideDown('slow');
			}

		});	


		$('#classificar').bind('click', function(){
			
			var situacao 		= 	$('#classificacao').val();
			var motivo 			= 	$('#motivo').val();			
			var destino 		= 	$('#destino').val();
			var erro 			= 	0;
			if( situacao == '' ){
				swal({
					title: 'Atenção!',
					text:  'Selecione uma classificação para essa(s) placa(s)!',
					type:  'warning'
				}).then(function() {}); 

			}else{
				$('.classificar').each(function(){				
					if( $(this).is(':checked') ){
						var acesso_id	 	= $(this).val();
						var agendamento_id 	= $(this).attr('agendamento_id');
						var placa 			= $(this).attr('placa');
						$.ajax({
							method: "POST",
							url: base_url+"AreaClassificadora/classificaAcesso",
							async: false,
							data: { acesso_id 		: 	acesso_id,
									situacao 		: 	situacao,
									motivo  		: 	motivo,
									agendamento_id 	: 	agendamento_id,
									terminal 		: 	destino	}
						}).done(function( data ) {
							var dados = $.parseJSON(data);
							if(dados.retorno != 'sucesso'){
								erro = 1;									
								swal({
									title: 'Erro!',
									text:  'Atenção, houve algum erro, entre em contato com o suporte técnico!',
									type:  'warning'
								}).then(function() {					
									window.location.href=base_url+"AreaClassificadora/";
									
								}); 		
							}
						});
					}
				});
			}
			
			if(erro == 0)	{
				swal({
					title: 'Ok!',
					text:  'Classificação realizadas com sucesso!',
					type:  'success'
				}).then(function() {					
					window.location.href=base_url+"AreaClassificadora/";
					
				}); 
			}
			
		})

});

function classificar(){
	
	$('.m_atualiza_title').empty();
	$('.m_atualiza_title').text('Classificar selecionados');		
	$('#m_atualiza').modal('show');
	
}

function datatable(){

	$('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');



}


