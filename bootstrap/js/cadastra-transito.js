$(document).ready(function(){
    $.fn.datetimepicker.defaults.language = 'pt-BR'; 
	$('.form_datetime').mask('99/99/9999 00:00');
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('.datepicker').datepicker({
	    format: 'dd/mm/yyyy'
	    
	});

    $ ( ".form_datetime" ). datetimepicker ({
        format: 'dd/mm/yyyy hh:ii',
        autoclose : true , 
        todayBtn : true , 
        locale : 'pt-BR',
        language : 'pt-BR',
        minuteStep : 10 
    });

    //Verifica se a data de atracação não tem conflito com as outras datas
    $('#dthr_atracacao').change(function(e){
        if($('#dthr_desatracacao').val() != ''){

            var dt_atrac    = retornaDataHoraInteira($('#dthr_atracacao').val());
            var dt_desatrac = retornaDataHoraInteira($('#dthr_desatracacao').val());

            if(dt_atrac > dt_desatrac){
                swal({
                    title: "Ops!",
                    text: "A data da atracação não pode ser maior que a data de desatracação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_atracacao').val('');
                }); 
            }
        }
        if($('#dthr_ini_operacao').val() != ''){

            var dt_atrac    = retornaDataHoraInteira($('#dthr_atracacao').val());
            var dt_ini_oper = retornaDataHoraInteira($('#dthr_ini_operacao').val());

            if(dt_atrac > dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da atracação não pode ser maior que a data de Início de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_atracacao').val('');
                }); 
            }
        }
        
        if($('#dthr_fim_operacao').val() != ''){

            var dt_atrac    = retornaDataHoraInteira($('#dthr_atracacao').val());
            var dt_fim_oper = retornaDataHoraInteira($('#dthr_fim_operacao').val());

            if(dt_atrac >= dt_fim_oper){
                swal({
                    title: "Ops!",
                    text: "A data da atracação não pode ser maior ou igual que a data de fim de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_atracacao').val('');
                }); 
            }
        }
        
    });
    
    //Verifica se a data de desatracação não tem conflito com as outras datas
    $('#dthr_desatracacao').change(function(e){ 

        var dt_atrac    = retornaDataHoraInteira($('#dthr_atracacao').val());
        var dt_desatrac = retornaDataHoraInteira($('#dthr_desatracacao').val());
        var dt_ini_oper = retornaDataHoraInteira($('#dthr_ini_operacao').val());
        var dt_fim_oper = retornaDataHoraInteira($('#dthr_fim_operacao').val());

        if($('#dthr_atracacao').val() != ''){

            if(dt_atrac > dt_desatrac){
                swal({
                    title: "Ops!",
                    text: "A data da desatracação não pode ser menor que a data de atracação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_desatracacao').val('');
                }); 
            }
        }
        if($('#dthr_ini_operacao').val() != ''){
            if(dt_desatrac <= dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da desatracação não pode ser menor ou igual a data de Início de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_desatracacao').val('');
                }); 
            }
        }
        if($('#dthr_fim_operacao').val() != ''){
            if(dt_desatrac < dt_fim_oper){
                swal({
                    title: "Ops!",
                    text: "A data da desatracação não pode ser menor que a data de fim de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_desatracacao').val('');
                }); 
            }
        }
        
    });

    //Verifica se a data de Início de operação não tem conflito com as outras datas
    $('#dthr_ini_operacao').change(function(e){ 

        var dt_atrac    = retornaDataHoraInteira($('#dthr_atracacao').val());
        var dt_desatrac = retornaDataHoraInteira($('#dthr_desatracacao').val());
        var dt_ini_oper = retornaDataHoraInteira($('#dthr_ini_operacao').val());
        var dt_fim_oper = retornaDataHoraInteira($('#dthr_fim_operacao').val());

        if($('#dthr_atracacao').val() != ''){
            if(dt_atrac > dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da atracação não pode ser maior que a data de início de operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_ini_operacao').val('');
                }); 
            }
        }
        if($('#dthr_desatracacao').val() != ''){
            if(dt_desatrac <= dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da desatracação não pode ser menor ou igual a data de Início de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_ini_operacao').val('');
                }); 
            }
        }
        if($('#dthr_fim_operacao').val() != ''){
            if(dt_fim_oper < dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da final de operação não pode ser menor que a data de início de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_ini_operacao').val('');
                }); 
            }
        }
        
    });

    //Verifica se a data de Fim de operação não tem conflito com as outras datas
    $('#dthr_fim_operacao').change(function(e){ 

        var dt_atrac    = retornaDataHoraInteira($('#dthr_atracacao').val());
        var dt_desatrac = retornaDataHoraInteira($('#dthr_desatracacao').val());
        var dt_ini_oper = retornaDataHoraInteira($('#dthr_ini_operacao').val());
        var dt_fim_oper = retornaDataHoraInteira($('#dthr_fim_operacao').val());

        if($('#dthr_atracacao').val() != ''){
            if(dt_atrac > dt_fim_oper){
                swal({
                    title: "Ops!",
                    text: "A data da atracação não pode ser maior que a data de fim de operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_fim_operacao').val('');
                }); 
            }
        }
        if($('#dthr_desatracacao').val() != ''){
            if(dt_desatrac < dt_fim_oper){
                swal({
                    title: "Ops!",
                    text: "A data da desatracação não pode ser menor que a data de fim de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_fim_operacao').val('');
                }); 
            }
        }
        if($('#dthr_ini_operacao').val() != ''){
            if(dt_fim_oper < dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da final de operação não pode ser menor que a data de início de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_fim_operacao').val('');
                }); 
            }
        }
        
    });

    $('#dt_fim').bind('focusout', function(){ 
		var dataFim = $('#dt_fim').val();
		if(dataFim != '' && (dataFim != undefined && dataFim != '') ){ 
			var partesData = dataFim.split("/");
			var dataFim = new Date(partesData[2], partesData[1] - 1, partesData[0]);
			var dataIni 	= 	$('#dt_ini').val();
			var partesData	= 	dataIni.split("/");
			var dataIni 	= 	new Date(partesData[2], partesData[1] - 1, partesData[0]);
			console.log(dataFim);
			console.log(dataIni);
			if( dataFim < dataIni ){
				swal({
					title: "Ops!",
					text: "DATA FINAL menor que a DATA INICIAL",
					type: 'warning'
				}).then(function() {
					$('#dt_fim').val('');
				}); 
			}
		}
	});


    
	$('#adiciona').click(function(e){

        var total_linhas = $("#tabela_due tr").length;

        var div_linha = "<tr >";
            div_linha += "<td >";
            div_linha += "<input type='text' name='nr_due[]' id='nr_due_"+total_linhas+"' value='' class='form-control m-input' />";
            div_linha += "</td>";
            div_linha += "<td >";
            div_linha += "<div><input type='text' style='width:85%; float:left;' name='exportador[]' id='exportador_"+total_linhas+"' autocomplete='off' value='' class='form-control m-input' />";
            div_linha += "<input type='hidden'  name='id_empresa[]' id='id_empresa_"+total_linhas+"'  />";
            div_linha += "<input type='button'  style='width:10%;  float:left;' id='btn_adiciona_cliente_"+total_linhas+"' value='+' class='form-control m-input' /><div id='lista_"+total_linhas+"'></div></div>";
            div_linha += "</td>";
            div_linha += "<td>"; 
            div_linha += "<input type='text' name='ncm[]' id='ncm_"+total_linhas+"' value='' class='form-control m-input' />";
            div_linha += "</td>";
            div_linha += "<td>"; 
            div_linha += "<input type='text' name='peso[]' id='peso_"+total_linhas+"' value='' class='form-control m-input peso' />";
            div_linha += "</td>";
            div_linha += "<td>"; 
            div_linha += "<select name='tipo[]' id='tipo_"+total_linhas+"' class='form-control m-select' />";
            div_linha += "<option value = ''></option>";
            div_linha += "</select>";
            div_linha += "</td>";
            div_linha += "<td>"; 
            div_linha += "<input type='text' name='ordem[]' id='ordem_"+total_linhas+"' value='' class='form-control m-input' />";
            div_linha += "</td>"; 
            div_linha += "<td>"; 
            div_linha += "<input type='text' name='status[]' id='status_"+total_linhas+"' readonly class='form-control m-input' />";
            div_linha += "</td>";
            div_linha += "</tr>";

            $("#itens_due").append(div_linha);

            $(".peso").mask("999.999.999", { reverse : true});

            $(".ordem").mask("9", { reverse : true});

            $("#btn_adiciona_cliente_"+total_linhas).click(function(e){
                var myWindow = window.open("../AreaAdministrador/cadastrarEmpresa", "Cadastro de Empresa", "width=600,height=500");
                
            });

            $("#exportador_"+total_linhas).keyup(function(e){
                var valor = this.value;
                var id = this.id;
                var divide = id.split('_');

                var num_empresa = divide[1];

                if(valor.length > 2){
                    $.ajax({
                        method: "POST",
                        url: base_url+'areaTransito/buscaExportadorAjax/',
                        async: true,
                        data: { exportador    :   valor }
                    }).done(function(data){
                        $("#lista_"+num_empresa).html(''); 
                        var dados = $.parseJSON(data);
                        var total_linhas = dados.retorno.exportador.length;
                        if(total_linhas > 0){
                            
                            var codigo = dados.retorno.exportador[0].id;
                            var nome    = dados.retorno.exportador[0].label;

                            var div = "<div style='background-color:white; float:left;'>";
                            $.each(dados.retorno.exportador, function(chave, valor){
                                div += "<div id='codigo_exp_"+chave+"' onclick='seleciona_exp("+num_empresa+","+valor.id+",\""+valor.label+"\")'>"+valor.label+"</div>";
                            });
                             div += "</div>";

                            
                            $("#lista_"+num_empresa).append(div);
                           
                        }
                         
                    });
                }else{
                    $("#lista_"+num_empresa).html('');
                }
                
            });
//Select do tipo
            $.ajax({
                method: "POST",
                url: base_url+'areaTransito/buscaTipoAjax/',
                async: true,
                data: { }
            }).done(function(data){
                
                var dados = $.parseJSON(data);
    
                var total_tipos = dados.retorno.tipos.length;
                if(total_tipos > 0){

                    $("#tipo_"+total_linhas).append("<option value=''>Selecione o Tipo</option>");
                    for (var i = 0; i < total_tipos; i++) {
                        
                        var codigo = dados.retorno.tipos[i].id;
                        var no_tipo = dados.retorno.tipos[i].no_tipo;

                        $("#tipo_"+total_linhas).append("<option value='"+codigo+"'>" + no_tipo + "</option>");
                    }
    
                }
    
            });
    });
   
	
});

//Seleciona registro do ajax e joga o valor no id
function seleciona_exp(num, codigo, nome){
    $("#id_empresa_"+num).val(codigo);
    $("#exportador_"+num).val(nome);
    $("#lista_"+num).html('');
 //   alert(id+' aqui '+valor);
}

;(function($){
	$.fn.datetimepicker.dates['pt-BR'] = {
        format: 'dd/mm/yyyy',
		days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo"],
		daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
		daysMin: ["Do", "Se", "Te", "Qu", "Qu", "Se", "Sa", "Do"],
		months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
		monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
		today: "Hoje",
		suffix: [],
		meridiem: []
	};
}(jQuery));

//Função feita para retornar a data em ordem para comparação
function retornaDataHoraInteira(vl_data) {

    var datahora = '';    

    if (vl_data != ''){

        var divide = vl_data.split(" ");
        var dv_data = divide[0].split("/");
        var dv_hora = divide[1].split(":");

        var datahora = dv_data[2]+dv_data[1]+dv_data[0]+dv_hora[0]+dv_hora[1];
    }
	return datahora;
}