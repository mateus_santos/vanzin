$('.datepicker').mask('99/99/9999');
$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
$('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: 'bottom'    
});

$('.tp').bind('click', function(){
	$('.filtros').slideDown('slow');
	if( $(this).val() == 's' ){
		$('.filtro_atual').fadeIn('slow');
		$('.filtro_hist').fadeOut('slow');
		$('#status_id').attr('required',false);
		$('#dt_ini_hist').attr('required',false);
		$('#dt_fim_hist').attr('required',false);
		$('#dt_ini_hist').val('');
		$('#dt_fim_hist').val('');
	}else{
		$('.filtro_atual').fadeOut('slow');
		$('.filtro_hist').fadeIn('slow');
		$('#status_id').attr('required',true);
		$('#dt_ini_hist').attr('required',true);
		$('#dt_fim_hist').attr('required',true);
		$('#dt_ini').val('');
		$('#dt_fim').val('');
	}
});

$('#dt_fim').bind('focusout', function(){
	var dataFim = $('#dt_fim').val();
	if(dataFim != '' && dataFim != undefined){
		var partesData = dataFim.split("/");
		var dataFim = new Date(partesData[2], partesData[1] - 1, partesData[0]);

		var dataIni 	= 	$('#dt_ini').val();
		var partesData	= 	dataIni.split("/");
		var dataIni 	= 	new Date(partesData[2], partesData[1] - 1, partesData[0]);
		console.log(dataFim);
		console.log(dataIni);
		if( dataFim < dataIni ){
			swal({
				title: "Ops!",
				text: "DATA FINAL menor que a DATA INICIAL",
				type: 'warning'
			}).then(function() {
				$('#dt_fim').val('');
			}); 
		}
	}
});

$('#dt_fim_fatur').bind('focusout', function(){
	var dataFim = $('#dt_fim_fatur').val();
	if(dataFim != '' && dataFim != undefined){
		var partesData = dataFim.split("/");
		var dataFim = new Date(partesData[2], partesData[1] - 1, partesData[0]);

		var dataIni 	= 	$('#dt_ini_fatur').val();
		var partesData	= 	dataIni.split("/");
		var dataIni 	= 	new Date(partesData[2], partesData[1] - 1, partesData[0]);
		console.log(dataFim);
		console.log(dataIni);
		if( dataFim < dataIni ){
			swal({
				title: "Ops!",
				text: "DATA FINAL menor que a DATA INICIAL",
				type: 'warning'
			}).then(function() {
				$('#dt_fim_fatur').val(''); 
			}); 
		}
	}
});

$('#indicadores').autocomplete({

	source: base_url+"AreaAdministrador/retornaIndicadores",
	minLength: 3,
	select: function( event, ui ) {
		console.log(ui.item);
		$('#indicador_id').val(ui.item.empresa_id);
	
	},

	response: function (event, ui) {
        if (!ui.content.length ) {

            swal({
				title: "Atenção!",
				text: 'Não existe nenhum ítem cadastrado com essa descrição',
				type: 'warning'
			}).then(function() {
				$('#indicadores').val('');
				
			});

        }

    }

});

$('#status_id').bind('change', function(){

	$('#status_desc').val($('#status_id option:selected').text());	
	$('#status_ordem').val($('#status_id option:selected').attr('ordem'));

})