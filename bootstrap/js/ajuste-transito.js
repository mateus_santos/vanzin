$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	
	$('#nr_due').bind('focusout', function(){
		if( $(this).val() != '' ){
			
            $.ajax({
                method: "POST",
                url: base_url+'areaTransito/buscaDueAjax/',
                async: true,
                data: { nr_due    :   $(this).val() }
            }).done(function(data){
                //$("#lista_"+num_empresa).html(''); 
                var dados = $.parseJSON(data);
               
                var total_linhas = dados.retorno.dados.length;
                if(total_linhas > 0){
                    
                    var codigo          = dados.retorno.dados[0].id;
                    var navio           = dados.retorno.dados[0].navio;
                    var razao_social    = dados.retorno.dados[0].razao_social;
                    var cnpj            = dados.retorno.dados[0].cnpj;

                    $('#id_transito').val(codigo);
                    $('#navio').val(navio);
                    $('#razao_social').val(razao_social+'  '+cnpj);
                    
                    
                }else{
                    swal(
                        'Ops!',
                        'DUE inexistente ou incorreta para Envio ao Siscomex, por favor verifique!',
                        'warning'
                    );
                    $('#nr_due').val('');
                }
                 
            });
		}
	});

    $('#enviar').bind('click', function(){
        if($('#nr_due').val() == ''){
            swal(
                'Ops!',
                'Por favor informe a DUE para Envio ao Siscomex!',
                'warning'
            )
        }else if(!($('#entrega')[0].checked) && !($('#recepcao')[0].checked)){
            swal(
                'Ops!',
                'Por favor Escolha o tipo de Envio ao Siscomex!',
                'warning'
            );
        }else if($('#peso').val() == '' || $('#peso').val() == 0){
            swal(
                'Ops!',
                'Por favor informe o Peso para Envio ao Siscomex!',
                'warning'
            );

        }else{
            $('#formulario').submit();
        }
    });
});