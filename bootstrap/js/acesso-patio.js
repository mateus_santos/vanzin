$(document).ready(function(){
	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$("body").css("position","fixed");
	$(".esc_tabela").css('display','none');
	$("#tab_botao_entrada").css('display','none');
	$("#tab_botao_saida").css('display','none');
	$("#placa").inputmask({mask: ['AAA-9999','AAA-9A99']});
	
	$('#placa').keypress(function(e){
		var k = e.which || e.keyCode; 
		if(k==13){
			e.preventDefault(); 
		}
	});

	/*$('#placa').change(function(){
		alert('teste');
		busca_placa($('#placa').val());

	});*/

	$('#placa').bind('focusout', function(){
		
		busca_placa($('#placa').val());

	});
});

function busca_placa(placa){
	
	var v_placa 		= '';
	
	
	$.ajax({
		method: "POST",
		url: base_url+'areaGate/acessoPatioAjax/',
		async: true,
        data: { placa    :   placa }
	}).done(function(data){
		var dados = $.parseJSON(data);
		
		var saida = dados.retorno.acesso[0].existe;
		
		$("#tab_resultado_lista").empty();
		var total_linhas = dados.retorno.listagem.length;
		
		if (total_linhas > 0 ){
			
			if (saida == 0){

				/*Verifica erros no acesso de entrada */
				var erro_acesso 		= dados.retorno.erro;
				var lista_mercadorias	= Array();
				if(erro_acesso != ''){
					swal({
						title: "Ops!",
						text: erro_acesso,
						type: 'warning'
					}).then(function() {
						
					});
					$('#placa').val(''); 
					$("#tab_resultado_lista").css('display','none');
					$(".msg").empty();
					$("#tab_botao_entrada").css('display','none');
					$("#tab_botao_saida").css('display','none');
					return 
				}

				var saida_manutencao 	= dados.retorno.listagem[0].dthr_saida_manutencao;
				var entrada_retorno 	= dados.retorno.listagem[0].dthr_retorno_manutencao;

				if((entrada_retorno == null && saida_manutencao == null)||(typeof saida_manutencao ==='undefined')) {

					var id_agenda_temp = null;
					var v_id_agenda = 0;
					var v_id_agenda_item = 0;
					$('#msg').html('ENTRADA');
					$(".esc_tabela").css('display','block');
					$(".esc_tabela").css('border','0px');
					$("#tab_botao_entrada").css('display','block');
					$("#tab_botao_saida").css('display','none');
					$("#tab_resultado").css('display','block');
					$("#tab_observa").css('display','block');
					$("#dv_saida_manut").css('display','none');
					var linha_agenda = 0;
					//Limita a 1 registro o for, pois o total_linhas é usado pra listar as Notas, se tiver mais de uma
					for(var i=0; i < total_linhas; i++) {
//alert(id_agenda_temp +' - '+ v_id_agenda);
						
							v_cliente_final 	= null;
							v_motorista_timac	= null;
							
							v_id_agenda 		= dados.retorno.listagem[i].id;
							v_razao_social 		= dados.retorno.listagem[i].razao_social;
							v_dt_agenda 		= dados.retorno.listagem[i].dt_agenda;
							v_motorista 		= dados.retorno.listagem[i].nome;
							v_id_motorista 		= dados.retorno.listagem[i].motorista_id;
							v_motorista_cpf		= dados.retorno.listagem[i].cpf;
							v_motorista_cnh		= dados.retorno.listagem[i].nr_cnh;
							v_motorista_fone	= dados.retorno.listagem[i].fone;
							v_tipo_operacao		= dados.retorno.listagem[i].descricao;
							v_observacao		= dados.retorno.listagem[i].observacao;
							v_cliente_final		= dados.retorno.listagem[i].cliente_final;
							v_transportadora	= dados.retorno.listagem[i].transportadora;
							v_motorista_timac	= dados.retorno.listagem[i].motorista;
							//Para quando for do Tipo de Frete FOB pega essa informção
							if(v_cliente_final != null){
								v_razao_social = v_cliente_final;
							}

							if(v_motorista_timac != null){
								v_motorista = v_motorista_timac;
							}

						if(id_agenda_temp != v_id_agenda){	
							id_agenda_temp = v_id_agenda;
							linha_agenda++;
							//v_dthr_retorno		= dados.retorno.listagem[i].dthr_retorno_manutencao;
							//v_id_produto		= dados.retorno.listagem[i].produto_id;
							
							//if(v_id_agenda_item != 0){//Para não incluir na primeira vez

								var div_linha = "<div class='linha' id='linha_"+linha_agenda+"'>";
									div_linha += "<div class='coluna col_cliente'>";
									div_linha += "<label class='listagem'><div id='dv_razao_social'><input type='radio' name='radio' id='radio_"+linha_agenda+"' value='"+v_id_agenda+"'/></div>  <input type='hidden' name='id_agenda_"+linha_agenda+"' id='id_agenda_"+linha_agenda+"' value='"+v_id_agenda+"' />  </label>";
									div_linha += "</div>";
									div_linha += "<div class='coluna' style='width:28%'>";
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem'><div>Nr. Agendamento </div></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "<div class='coluna col_cliente'>"; 
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem'><div id='dv_dt_agenda'>"+v_id_agenda+" </div></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "</div>";
/*comentado em 05/02/2023 por Wagner
									div_linha += "<div class='linha_titulo'>";
									div_linha += "<div class='coluna col_cliente'>";
									div_linha += "<label class='titulo'>Cliente</label>";
									div_linha += "</div>";
									div_linha += "<div class='coluna' style='width:28%'>";
									div_linha += "<label class='titulo'>Tipo Operação</label>";
									div_linha += "</div>";
									div_linha += "<div class='coluna col_cliente'>";
									div_linha += "<label class='titulo'>Data do Agendamento</label>";
									div_linha += "</div>";
									div_linha += "</div>";
									
								*/
									div_linha += "<div class='linha'>";
									div_linha += "<div class='coluna col_cliente'>";
									div_linha += "<label class='listagem'><div id='dv_razao_social'>"+v_razao_social+" </div>  <input type='hidden' name='id_agenda' id='id_agenda' value='"+v_id_agenda+"' />  </label>";
									div_linha += "</div>";
									div_linha += "<div class='coluna' style='width:28%'>";
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem'><div id='dv_tp_operacao'>"+v_tipo_operacao+" </div><input type='hidden' 	name='id_motorista_entrada' id='id_motorista_entrada' value='"+v_id_motorista+"'></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "<div class='coluna col_cliente'>"; 
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem'><div id='dv_dt_agenda'>"+v_dt_agenda+" </div></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "</div>";
								
							/*}else{
								
									//VErifica pra adicionar o radiobutton
									
								var div_linha = "<div class='linha'>";
									div_linha += "<div class='coluna col_cliente'>";
									div_linha += "<label class='listagem'><div id='dv_razao_social'><input type='radio' name='nr_agenda_escolhido'/></div>  <input type='hidden' name='id_agenda' id='id_agenda' value='"+v_id_agenda+"' />  </label>";
									div_linha += "</div>";
									div_linha += "<div class='coluna' style='width:28%'>";
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem'><div>Nr. Agendamento </div></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "<div class='coluna col_cliente'>"; 
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem'><div id='dv_dt_agenda'>"+v_id_agenda+" </div></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "</div>";
										
									
									div_linha += "<div class='linha'>";
									div_linha += "<div class='coluna col_cliente'>";
									div_linha += "<label class='listagem'><div id='dv_razao_social'>"+v_razao_social+" </div>  <input type='hidden' name='id_agenda' id='id_agenda' value='"+v_id_agenda+"' />  </label>";
									div_linha += "</div>";
									div_linha += "<div class='coluna' style='width:28%'>";
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem'><div id='dv_tp_operacao'>"+v_tipo_operacao+" </div><input type='hidden' 	name='id_motorista_entrada' id='id_motorista_entrada' value='"+v_id_motorista+"'></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "<div class='coluna col_cliente'>"; 
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem'><div id='dv_dt_agenda'>"+v_dt_agenda+" </div></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "</div>";
									
									
							}*/
								

								div_linha += "<div class='esc_tabela ' id='tab_resultado'>";
								div_linha += "<div class='linha_titulo lista_acesso' id='linha_tit_motorista_"+linha_agenda+"'>";
								div_linha += "<div class='coluna col_cliente'>";
								div_linha += "<label class='titulo'>Motorista</label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna' style='width:28%'>";
								div_linha += "<label class='titulo'>CNH</label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna col_cliente'>";
								div_linha += "<label class='titulo'>CPF</label>";
								div_linha += "</div>";
								div_linha += "</div>";
								div_linha += "</div>";

								div_linha += "<div class='linha lista_acesso' id='linha_res_motorista_"+linha_agenda+"'>";
								div_linha += "<div class='coluna col_cliente'>";
								div_linha += "<label class='listagem'><div id='dv_motorista'>"+v_motorista+" </div>  <input type='hidden' name='id_agenda' id='id_agenda' value='"+v_id_agenda+"' />  </label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna' style='width:28%'>";
								div_linha += "<div class='m-input-icon m-input-icon--right'>";
								div_linha += "<label class='listagem'><div id='dv_motorista_cnh'>"+v_motorista_cnh+" </div><input type='hidden' 	name='id_motorista_entrada' id='id_motorista_entrada' value='"+v_id_motorista+"'></label>";
								div_linha += "</div>";
								div_linha += "</div>";
								div_linha += "<div class='coluna col_cliente'>"; 
								div_linha += "<div class='m-input-icon m-input-icon--right'>";
								div_linha += "<label class='listagem'><div id='dv_motorista_cpf'>"+v_motorista_cpf+" </div></label>";
								div_linha += "</div>";
								div_linha += "</div>";
								div_linha += "</div>";

								div_linha += "<div class=' esc_tabela lista_merc' id='tab_resultado'>";
								div_linha += "<div class='linha_titulo' id='linha_tit_merc_"+linha_agenda+"'>";
								div_linha += "<div class='coluna col_cliente' style='width:15%'>";
								div_linha += "<label class='titulo'>Nota Fiscal</label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna' style='width:15%'>";
								div_linha += "<label class='titulo'>Nr. Pedido</label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna col_cliente' style='width:40%'>";
								div_linha += "<label class='titulo'>Produto</label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna col_cliente' style='width:15%'>";
								div_linha += "<label class='titulo'>Embalagem</label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna col_cliente' style='width:15%'>";
								div_linha += "<label class='titulo'>Peso</label>";
								div_linha += "</div>";
								div_linha += "</div>";
								div_linha += "</div>";
								div_linha += "<div id='linha_res_merc_"+linha_agenda+"'>";
							for(var w = 0; w < total_linhas; w++){
	
								v_id_agenda_item	= dados.retorno.listagem[w].id_agenda_item;
								v_produto			= dados.retorno.listagem[w].no_produto;
								v_no_embalagem		= dados.retorno.listagem[w].no_embalagem;
								v_nr_pedido			= dados.retorno.listagem[w].nr_pedido;
								v_ag_peso			= dados.retorno.listagem[w].peso;
								v_nr_nf				= dados.retorno.listagem[w].nr_nf;

								if(v_id_agenda == v_id_agenda_item){

									div_linha += "<div class='linha lista_merc' >";
									div_linha += "<div class='coluna col_cliente' style='width:15%'>";
									div_linha += "<label class='listagem'><div id='dv_nr_nf'>"+v_nr_nf+" </div>  <input type='hidden' name='id_agenda' id='id_agenda' value='"+v_id_agenda+"' />  </label>";
									div_linha += "</div>";
									div_linha += "<div class='coluna' style='width:15%'>";
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem'><div id='dv_nr_pedido'>"+v_nr_pedido+" </div><input type='hidden' 	name='id_motorista_entrada' id='id_motorista_entrada' value='"+v_id_motorista+"'></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "<div class='coluna col_cliente' style='width:40%'>"; 
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem'><div id='dv_produto'>"+v_produto+" </div></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "<div class='coluna col_cliente' style='width:15%'>"; 
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem'><div id='dv_emb'>"+v_no_embalagem+" </div></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "<div class='coluna col_cliente' style='width:15%'>"; 
									div_linha += "<div class='m-input-icon m-input-icon--right'>";
									div_linha += "<label class='listagem peso'><div id='dv_peso'>"+v_ag_peso+" </div></label>";
									div_linha += "</div>";
									div_linha += "</div>";
									div_linha += "</div>";
									
								}
							}
							div_linha += "</div>";	

							$("#tab_resultado_lista").append(div_linha);

							
						}
						

					}
					//Possibilita a escolha de um agendamento
					if(linha_agenda > 1){

						for(var z=1; z <= linha_agenda;z++){
							
							$("#linha_"+z).attr('onclick','expande_agenda('+z+')');
							$("#linha_"+z).attr('onmouseover','marca_agenda('+z+')');
							$("#linha_"+z).attr('onmouseout','desmarca_agenda('+z+')');
							$("#linha_tit_motorista_"+z).hide();
							$("#linha_res_motorista_"+z).hide();
							$("#linha_tit_merc_"+z).hide();
							$("#linha_res_merc_"+z).hide();
							
						}
						

					}else{
						$("#linha_1").hide();
					}

					//alert($("#id_agenda_1").val());
				//	alert($("#id_agenda_2").val());
				}else{
					 //Entrada de retorno por manutenção
					$('#msg').html('ENTRADA DE RETORNO DE MANUTENÇÃO');
					$(".esc_tabela").css('display','block');
					$(".esc_tabela").css('border','0px');
					$("#tab_botao_entrada").css('display','block');
					$("#tab_botao_saida").css('display','none');
					$("#tab_resultado").css('display','block');
					$("#tab_observa").css('display','block');
					$("#dv_saida_manut").css('display','none');

					for(var i=0; i < 1; i++) {

						v_cliente_final 		= null;
						v_motorista_timac		= null;

						v_dt_agenda 			= dados.retorno.listagem[i].dt_agenda;
						v_motorista_cpf			= dados.retorno.listagem[i].cpf;
						v_motorista_cnh			= dados.retorno.listagem[i].nr_cnh;
						v_motorista_fone		= dados.retorno.listagem[i].fone;
						v_tipo_operacao			= dados.retorno.listagem[i].descricao;
						//v_observacao			= dados.retorno.listagem[i].observacao;
						v_id_acesso 			= dados.retorno.listagem[i].id_acesso;
						v_id_agenda 			= dados.retorno.listagem[i].id;
						v_razao_social 			= dados.retorno.listagem[i].razao_social;
						v_motorista 			= dados.retorno.listagem[i].nome;
						v_id_motorista 			= dados.retorno.listagem[i].motorista_id;
					//	v_produto				= dados.retorno.listagem[i].no_produto;
						v_dthr_saida_manutencao	= dados.retorno.listagem[i].dthr_saida_manutencao;
						//v_id_produto		= dados.retorno.listagem[i].produto_id;
						v_cliente_final			= dados.retorno.listagem[i].cliente_final;
						v_transportadora		= dados.retorno.listagem[i].transportadora;
						v_motorista_timac		= dados.retorno.listagem[i].motorista;

						//Para quando for do Tipo de Frete FOB pega essa informção
						if(v_cliente_final != null){
							v_razao_social = v_cliente_final;
						}

						if(v_motorista_timac != null){
							v_motorista = v_motorista_timac;
						}
						
						$("#dthr_saida_manutencao").val(v_dthr_saida_manutencao);
					/*
						var div_linha = "<div class='linha'>";
							div_linha += "<div class='coluna col_cliente'>";
							div_linha += "<label class='listagem'><div id='dv_razao_social'>"+v_razao_social+" </div>  <input type='hidden' name='id' id='id' value='"+v_id_acesso+"' />  </label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna' style='width:28%'>";
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_motorista'>"+v_motorista+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente'>"; 
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_produto'>"+v_produto+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							
							div_linha += "</div>";*/
						
						var div_linha = "<div class='linha'>";
							div_linha += "<div class='coluna col_cliente'>";
							div_linha += "<label class='listagem'><div id='dv_razao_social'>"+v_razao_social+" </div>  <input type='hidden' name='id' id='id' value='"+v_id_acesso+"' />  </label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna' style='width:28%'>";
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_tp_operacao'>"+v_tipo_operacao+" </div><input type='hidden' 	name='id_motorista_entrada' id='id_motorista_entrada' value='"+v_id_motorista+"'></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente'>"; 
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_dt_agenda'>"+v_dt_agenda+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "</div>";
							

							div_linha += "<div class=' esc_tabela' id='tab_resultado'>";
							div_linha += "<div class='linha_titulo'>";
							div_linha += "<div class='coluna col_cliente'>";
							div_linha += "<label class='titulo'>Motorista</label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna' style='width:28%'>";
							div_linha += "<label class='titulo'>CNH</label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente'>";
							div_linha += "<label class='titulo'>CPF</label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "</div>";

							div_linha += "<div class='linha'>";
							div_linha += "<div class='coluna col_cliente'>";
							div_linha += "<label class='listagem'><div id='dv_motorista'>"+v_motorista+" </div>  <input type='hidden' name='id_agenda' id='id_agenda' value='"+v_id_agenda+"' />  </label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna' style='width:28%'>";
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_motorista_cnh'>"+v_motorista_cnh+" </div><input type='hidden' 	name='id_motorista_entrada' id='id_motorista_entrada' value='"+v_id_motorista+"'></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente'>"; 
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_motorista_cpf'>"+v_motorista_cpf+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "</div>";

							div_linha += "<div class=' esc_tabela' id='tab_resultado'>";
							div_linha += "<div class='linha_titulo'>";
							div_linha += "<div class='coluna col_cliente' style='width:15%'>";
							div_linha += "<label class='titulo'>Nota Fiscal</label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna' style='width:15%'>";
							div_linha += "<label class='titulo'>Nr. Pedido</label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente' style='width:40%'>";
							div_linha += "<label class='titulo'>Produto</label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente' style='width:15%'>";
							div_linha += "<label class='titulo'>Embalagem</label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente' style='width:15%'>";
							div_linha += "<label class='titulo'>Peso</label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "</div>";
						
						for(var w = 0; w < total_linhas; w++){

							v_produto			= dados.retorno.listagem[w].no_produto;
							v_no_embalagem		= dados.retorno.listagem[w].no_embalagem;
							v_nr_pedido			= dados.retorno.listagem[w].nr_pedido;
							v_ag_peso			= dados.retorno.listagem[w].peso;
							v_nr_nf				= dados.retorno.listagem[w].nr_nf;

							div_linha += "<div class='linha'>";
							div_linha += "<div class='coluna col_cliente' style='width:15%'>";
							div_linha += "<label class='listagem'><div id='dv_nr_nf'>"+v_nr_nf+" </div>  <input type='hidden' name='id_agenda' id='id_agenda' value='"+v_id_agenda+"' />  </label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna' style='width:15%'>";
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_nr_pedido'>"+v_nr_pedido+" </div><input type='hidden' 	name='id_motorista_entrada' id='id_motorista_entrada' value='"+v_id_motorista+"'></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente' style='width:40%'>"; 
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_produto'>"+v_produto+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente' style='width:15%'>"; 
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_no_emb'>"+v_no_embalagem+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente' style='width:15%'>"; 
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem peso'><div id='dv_peso'>"+v_ag_peso+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "</div>";
						}	
							
						$("#tab_resultado_lista").append(div_linha);

						
					}
				}
				$('#observacao').focus(); 
			}else{
				var saida_manutencao 	= dados.retorno.listagem[0].dthr_saida_manutencao;
				var entrada_retorno 	= dados.retorno.listagem[0].dthr_retorno_manutencao;
				
				if((entrada_retorno == null && saida_manutencao == null) || (entrada_retorno != null && saida_manutencao != null)) {

					$('#msg').html('SAÍDA');
					$("#tab_botao_saida").css('display','block');
					$("#tab_botao_entrada").css('display','none');
					$("#tab_resultado").css('display','block');
					$("#tab_resultado_lista").css('display','block');
					$(".esc_tabela").css('border','0px');
					$("#tab_observa").css('display','none');
					$("#dv_saida_manut").css('display','block');

					for(var i=0; i < 1; i++) {
						
						v_id_acesso 			= dados.retorno.listagem[i].id_acesso;
						v_id_agenda 			= dados.retorno.listagem[i].id;
						v_razao_social 			= dados.retorno.listagem[i].razao_social;
						v_id_motorista 			= dados.retorno.listagem[i].id_motorista;
						v_motorista 			= dados.retorno.listagem[i].nome;
						//v_id_produto			= dados.retorno.listagem[i].produto_id;
						v_produto				= dados.retorno.listagem[i].no_produto;
						v_id_classificadora		= dados.retorno.listagem[i].id_classificadora;
						v_classificadora		= dados.retorno.listagem[i].classificadora;
						v_id_terminal			= dados.retorno.listagem[i].id_terminal;
						v_no_terminal			= dados.retorno.listagem[i].no_terminal;
						v_dthr_entrada			= dados.retorno.listagem[i].dthr_entrada;
						v_fl_atende_classific	= dados.retorno.listagem[i].fl_atende_classific;
						v_dthr_saida_manutencao	= dados.retorno.listagem[i].dthr_saida_manutencao;
						v_id_tipo_operacao		= dados.retorno.listagem[i].id_tipo_operacao;

						if (v_classificadora == null && v_id_tipo_operacao == 1){

							var div_linha = "<div class='msg' style='font-weight:bold; font-size:110%;' >Veículo não foi classificado para saída, retorne ao pátio para classificação!</div>";
							$("#tab_botao_saida").css('display','none');
							$("#tab_resultado").css('display','none');
							$("#dv_saida_manut").css('display','none');
							$("#tab_resultado_lista").append(div_linha);	
						}else{
							
							if(v_fl_atende_classific == null && v_id_tipo_operacao == 1){
								var div_linha = "<div class='msg' style='font-weight:bold; font-size:110%;' >Veículo classificado para saída porém não foi informado se atende a classificação, retorne ao pátio para classificação!</div>";
								$("#tab_botao_saida").css('display','none');
								$("#tab_resultado").css('display','none');
								$("#dv_saida_manut").css('display','none');
								$("#tab_resultado_lista").append(div_linha);		
							}else{

								var div_linha_cancel = null;
								var div_linha_tipo	 = null;
								if(v_fl_atende_classific == 'N'){
									div_linha_cancel = "<div class='msg' style='font-weight:bold; font-size:110%;' >CANCELADA</div>";	
								}

								if(v_dthr_saida_manutencao != null){
									div_linha_tipo = "<div class='msg_tipo' style='font-weight:bold; font-size:110%;' >SAÍDA PARA MANUTENÇÃO</div>";	
								}

								var div_linha = "<div class='linha'>"; 	
								div_linha += "<div class='coluna col_cliente'>";
								div_linha += "<label class='listagem'><div id='dv_razao_social'>"+v_razao_social+" </div>  <input type='hidden' name='id' id='id' value='"+v_id_acesso+"' />  </label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna' style='width:28%'>";
								div_linha += "<div class='m-input-icon m-input-icon--right'>";
								div_linha += "<label class='listagem'><div id='dv_motorista'>"+v_motorista+" </div><input type='hidden' name='id_motorista_saida' id='id_motorista_saida' value='"+v_id_motorista+"'></label>";
								div_linha += "</div>";
								div_linha += "</div>";
								div_linha += "<div class='coluna col_cliente'>"; 
								div_linha += "<div class='m-input-icon m-input-icon--right'>";
								div_linha += "<label class='listagem'><div id='dv_produto'>"+v_produto+" </div></label>";
								div_linha += "</div>";
								div_linha += "</div>";
								div_linha += "</div>";
								

								div_linha += "<div class='linha_titulo'>";
								div_linha += "<div class='coluna col_cliente'>";
								div_linha += "<label class='titulo'>Data de Entrada</label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna' style='width:28%'>";
								div_linha += "<label class='titulo'>Terminal</label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna col_cliente'>";
								div_linha += "<label class='titulo'>Classificadora</label></div>";
								div_linha += "</div>";
								div_linha += "<div class='linha'>";
								div_linha += "<div class='coluna col_cliente'>";
								div_linha += "<label class='listagem'><div id='dv_hora_entrada'>"+v_dthr_entrada+" </div>  </label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna' style='width:28%'>";
								div_linha += "<div class='m-input-icon m-input-icon--right'>";
								div_linha += "<label class='listagem'><div id='dv_saida_terminal'>"+v_no_terminal+" </div></label>";
								div_linha += "</div>";
								div_linha += "</div>";
								div_linha += "<div class='coluna col_cliente'>"; 
								div_linha += "<div class='m-input-icon m-input-icon--right'>";
								if (v_id_tipo_operacao == 2){
									div_linha += "<label class='listagem'><div id='dv_classificadora'>Expedição </div><input type='hidden' name='id_classificadora' id='id_classificadora' value='"+v_id_classificadora+"'></label>";
								}else{
									div_linha += "<label class='listagem'><div id='dv_classificadora'>"+v_classificadora+" </div><input type='hidden' name='id_classificadora' id='id_classificadora' value='"+v_id_classificadora+"'></label>";
								}
								
								div_linha += "</div>";
								div_linha += "</div>";
								
								div_linha += "</div>";
								if(div_linha_cancel != null){
									$("#msg").append(div_linha_cancel);
								}
								if(div_linha_tipo != null){
									$("#msg_tipo").append(div_linha_tipo);
								}
								$("#tab_resultado_lista").append(div_linha);
								
							}
						}
					}
				}else{ //Entrada de retorno por manutenção
					$('#msg').html('ENTRADA DE RETORNO DE MANUTENÇÃO');
					$(".esc_tabela").css('display','block');
					$(".esc_tabela").css('border','0px');
					$("#tab_botao_entrada").css('display','block');
					$("#tab_botao_saida").css('display','none');
					$("#tab_resultado").css('display','block');
					$("#tab_observa").css('display','block');
					$("#dv_saida_manut").css('display','none');

					for(var i=0; i < 1; i++) {

						v_cliente_final 		= null;
						v_motorista_timac		= null;

						v_id_acesso 			= dados.retorno.listagem[i].id_acesso;
						//v_id_agenda 			= dados.retorno.listagem[i].id;
						v_razao_social 			= dados.retorno.listagem[i].razao_social;
						v_motorista 			= dados.retorno.listagem[i].nome;
						//v_id_motorista 			= dados.retorno.listagem[i].motorista_id;
						v_produto				= dados.retorno.listagem[i].no_produto;
						v_dthr_saida_manutencao	= dados.retorno.listagem[i].dthr_saida_manutencao;
						//v_id_produto		= dados.retorno.listagem[i].produto_id;
						v_cliente_final			= dados.retorno.listagem[i].cliente_final;
						v_transportadora		= dados.retorno.listagem[i].transportadora;
						v_motorista_timac		= dados.retorno.listagem[i].motorista;

						//Para quando for do Tipo de Frete FOB pega essa informção
						if(v_cliente_final != null){
							v_razao_social = v_cliente_final;
						}

						if(v_motorista_timac != null){
							v_motorista = v_motorista_timac;
						}
						
						$("#dthr_saida_manutencao").val(v_dthr_saida_manutencao);
					
						var div_linha = "<div class='linha'>";
							div_linha += "<div class='coluna col_cliente'>";
							div_linha += "<label class='listagem'><div id='dv_razao_social'>"+v_razao_social+" </div>  <input type='hidden' name='id' id='id' value='"+v_id_acesso+"' />  </label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna' style='width:28%'>";
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_motorista'>"+v_motorista+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente'>"; 
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_produto'>"+v_produto+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							
							div_linha += "</div>";
							$("#tab_resultado_lista").append(div_linha);
					}
					
				}
				$('#btn_saida').focus();
				 
			}	

		}else{
			
			
			$("#tab_resultado").css('display','none');
			$("#tab_observa").css('display','none');
			$(".msg").empty();
			$("#tab_botao_entrada").css('display','none');
			$("#tab_botao_saida").css('display','none');
			var div_linha = "<div class='msg' style='font-weight:bold; font-size:110%;' >Placa não encontrada ou não agendada</div>";
			$("#tab_resultado_lista").append(div_linha);	
			
			$("#tab_resultado_lista").css('display','block');
		}

	});
	
	
}
//Abre informações de motorista e mercadorias
function expande_agenda(numero){
	$("#linha_tit_motorista_"+numero).show();
	$("#linha_res_motorista_"+numero).show();
	$("#linha_tit_merc_"+numero).show();
	$("#linha_res_merc_"+numero).show();
	
	$("#linha_"+numero).attr('onclick','fecha_agenda('+numero+')');
}

//Fecha as informações de motoristas e mercadorias
function fecha_agenda(numero){
	$("#linha_tit_motorista_"+numero).hide();
	$("#linha_res_motorista_"+numero).hide();
	$("#linha_tit_merc_"+numero).hide();
	$("#linha_res_merc_"+numero).hide();

	$("#linha_"+numero).attr('onclick','expande_agenda('+numero+')');
}

//Marca a linha para abrir
function marca_agenda(numero){ 
	$("#linha_"+numero).css('cursor','pointer');
	$("#linha_"+numero).css('background','#DCDCDC');
	
}

//Desmarca a linha para abrir
function desmarca_agenda(numero){
	$("#linha_"+numero).css('cursor','default');
	$("#linha_"+numero).css('background','white');
	
}

