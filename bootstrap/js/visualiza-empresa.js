$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('.date').datepicker({
		orientation: 'bottom',
		format: 'dd/mm/yyyy'
	});

	$('#vincularClienteBtn').bind('click', function(){
		$("#vincularCliente").modal({
	    	show: true
		});
	});

	$('#cpf').mask('000.000.000-00');
	$('#telefone').mask('(00) 0000 - 00000');
	$('#celular').mask('(00) 0000 - 00000');

	$('#cpf').bind('focusout', function(){
		var cpf = $(this).val();
		if(cpf != ""){
			$.ajax({
				method: "POST",
				url: base_url+'clientes/verifica_cpf',
				async: true,
				data: { cpf 	: 	cpf },
				success: function( data ) {
					var dados = $.parseJSON(data);
					if(dados.status == 'erro'){
						swal({
							title: 'Atenção!',
							text: dados.mensagem,
							type: 'warning'
						}).then(function() {
							$('#cpf').val('');
							$('#cpf').focus();
						});
					} 
				}
			});
		}
	});
	
	$('#email').bind('focusout', function(){
		var email = $(this).val();
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test(email)){
			swal(
				'Ops!',
				'E-mail inválido',
				'error'
			).then(function() {
				$('#email').val('');				
			});
		}else{
			$.ajax({
				method: "POST",
				url: base_url+'clientes/verifica_email',
				async: true,
				data: {
					email 	: 	$('#email').val()
				},
				success: function( data ) {
					var dados = $.parseJSON(data);
					if(dados.status == 'erro'){
						swal({
							title: 'Atenção!',
							text: dados.mensagem,
							type: 'warning'
						}).then(function() {
							$('#email').val('');
							
						});
					}
				}
			});
		}
	});
	
});

function adicionar_funcionario(){
	if($('#nome').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o nome do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	
	if($('#email').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o email do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	if($('#telefone').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o telefone do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	var subtipo_cadastro_id = '';
	if( $('#tipo_cadastro_id').val() == 4 && $('#subtipo_cadastro_id').val() == "" ){
		swal({
			title: "Campo em branco",
			text: "Preencha o telefone do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});

		subtipo_cadastro_id = $('#subtipo_cadastro_id').val();

		return;	
	}

	if( $('#senha').val() != $('#senha2').val() ){
		swal({
			title: "Atenção",
			text: "As senhas não conferem",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});

		return;
	}

	$.ajax({
		method: "POST",
		url: base_url+'usuarios/cadastrar',
		async: true,
		data: {
			nome: 		$('#nome').val(),
			cpf: 		$('#cpf').val(),
			email: 		$('#email').val(),
			telefone: 	$('#telefone').val(),
			celular: 	$('#celular').val(),
			empresa_id: $('#empresa_id').val(),
			tipo_cadastro_id: $('#tipo_cadastro_id').val(),
			subtipo_cadastro_id : subtipo_cadastro_id,
			senha 				: $("#senha").val()
		},
		success: function( data ) {
			var dados = $.parseJSON(data);
			swal({
				title: dados.titulo,
				text: dados.texto,
				type: dados.tipo
			}).then(function() {
				if(dados.tipo == 'success') location.reload();
			});
		}
	});
}

function desvincularEmpresa(id, empresa_id,tipo){
	swal({
		  title: 'Excluir?',
		  text: "Deseja realmente desvincular essa empresa?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Sim!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					method: "POST",
					url:  base_url+"AreaAdministrador/desvincularEmpresa", 
					async: false,
					data: { id 			: id,	
							empresa_id 	: empresa_id,
							tipo 		: tipo		 }
				}).done(function(data) {
					var dados = $.parseJSON(data);
					
					if(dados.retorno == 'sucesso'){
						swal({
				   			title: "OK!",
				   			text: "Empresa desvinculada com sucesso",
				   			type: 'success'
				    	}).then(function() {
				    	   	window.location = base_url+'AreaAdministrador/visualizarEmpresa/'+id;
				    	}); 
					}
				});	
			}
		})
}


