$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('#cpf').mask('999.999.999-99');
	
	$('#senha').bind('focusout', function(){
		if(	$(this).val() != ""	){
			$('#senha2').attr('required','required');
		}else{
			$('#senha2').removeattr('required');
		}
	});

	$('#senha2').bind('focusout', function(){
		if( $(this).val() != $('#senha').val() )
		{
			swal(
		  		'Ops!',
		  		'As senhas não correspondem, tente novamente!',
		  		'warning'
			);

			$(this).val('');
			$('#senha').val('');
		}
	});

	if($('#tipo_cadastro_id').val() != 4){
		$('#subtipo').fadeOut('slow');
	}

	$('#tipo_cadastro_id').bind('change',function(){
		if( $(this).val() != 4 ){
			$('#subtipo').fadeOut('slow');
		}else{
			$('#subtipo').fadeIn('slow');
		}
	});	
});