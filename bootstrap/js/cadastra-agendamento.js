$(document).ready(function(){ 
    $('.datepicker').css('z-index: 99999 !important');
    $('.datepicker').mask('99/99/9999');
    $('#cpf').mask('000.000.000-00');
    //$('.telefone').mask('(00) 00000 - 0000');
    $('#cnh').mask('00000000000', {reverse: true});
    $('.qtd').mask("#.##0", {reverse: true});     
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    
    var hoje = new Date();
    var finalData = '+1d';
    
    var feriados = ['1/1/2021','2/2/2021','15/2/2021','16/2/2021','17/2/2021','2/4/2021','4/4/2021','21/4/2021','1/5/2021', '9/5/2021', '3/6/2021', '8/8/2021','7/9/2021','20/9/2021','12/10/2021','2/11/2021','15/11/2021','25/12/2021']; 
    
    if(hoje.getDay() == 5 || verificaVesperaFeriado(feriados) == 2){
        finalData = '+3d';
    }

    if(hoje.getDay() == 6 || verificaVesperaFeriado(feriados) == 1 ){
        finalData = '+2d';
    }    
    
    var behavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behavior.apply({}, arguments), options);
        }
    };

    $('#id_turno').attr('disabled','disabled');
    $('#id_turno').removeAttr('required');

    $('.telefone').mask(behavior, options);

    /*$('.datepicker').datepicker({
        startDate: '0d',
        endDate   : finalData,
        daysOfWeekDisabled : [0],
        format: 'dd/mm/yyyy',
        datesDisabled: feriados
    });

    $('.datepicker').datepicker({
        startDate: '0d',
        endDate   : finalData,    
        format: 'dd/mm/yyyy',
        datesDisabled: feriados
    });*/

    $('.datepicker').datepicker({
        startDate: '0d',        
        format: 'dd/mm/yyyy',
        datesDisabled: feriados
    });

    maskMercosul('#placa');
    
    $('.fl_motorista').bind('click', function(){
        $('#cpf').val('');
        $('#cnh').val('');
        $('#motorista').val('');
        $('#telefone').val('');
        $('#id_motorista').remove();

        if($(this).val()=='1'){
            $('#cpf').unmask();   
            $('#cpf').removeAttr('maxlength'); 
           
        }else{
            $('#cpf').mask('000.000.000-00');
            $('#id_motorista').val('');
        }
    });

    $('.tp_operacao').bind('click', function(){
              
        if($(this).val()=='2'){            
            var codigo_html =   '<table class="table">';
                codigo_html+=   '   <thead>';
                codigo_html+=   '       <tr>';
                codigo_html+=   '           <th>N°</th>';
                codigo_html+=   '           <th>Mercadoria</th>';
                codigo_html+=   '           <th>qtd</th>';
                codigo_html+=   '       </tr>';
                codigo_html+=   '   </thead>';
                codigo_html+=   '   <tbody>';
                codigo_html+=   '       <tr>';
                codigo_html+=   '           <td><input type="text" class="form-control" name="nr_pedido" required /></td>';
                codigo_html+=   '           <td><input type="text" class="form-control" name="mercadoria" required value="" /></td>';                        
                codigo_html+=   '           <td><input type="text" class="form-control qtd" name="qtd" required value="" /></td>';
                codigo_html+=   '       </tr>';
                codigo_html+=   '   </tbody>';
                codigo_html+=   '</table>';
               
            $('#tabela_exp').empty();
            $('#tabela_exp').append(codigo_html);            
            $('.qtd').unmask();
            $('.qtd').mask("#.##0", {reverse: true}); 
            $('.div_nota_fiscal').fadeOut('slow');
            $('#nfe').removeAttr('required');

            $('.fretes').fadeIn('slow');
            $('#id_frete').attr('required','required');
        }else{
            $('#tabela_produtos').fadeOut('slow');
            $('#tabela_exp').empty();
            $('.div_nota_fiscal').fadeIn('slow');
            $('#nfe').attr('required','required');

            $('.fretes').fadeOut('slow');
            $('#id_frete').removeAttr('required');
        }
    });

    $('#transportadora').autocomplete({

        source: base_url+"AreaClientes/retornaTransportadoras",
        minLength: 3,
        select: function( event, ui ) {
            console.log(ui.item);
            $('#id_transportadora').val(ui.item.id);
        
        },

        response: function (event, ui) {
            if (!ui.content.length ) {

                swal({
                    title: "Atenção!",
                    text: 'Não existe nenhuma transportadora com essa descrição, cadastre!',
                    type: 'warning'
                }).then(function() {
                    $('#transportadora').val('');
                    
                });

            }

        }

    });

    $('#id_empresa').bind('change', function(){
        
        $('#id_turno').attr('disabled','disabled');
        $('#id_turno').removeAttr('required');
        $('#data').val('');
        $('#id_turno').val('');
        
    });

    $('#data').bind('focusout', function(){
        data_atual = $('#data_atual').val(); 
        if( data_atual == $(this).val() ){  
            $.ajax({
                method: "POST",
                url: base_url+'AreaClientes/verificaHoraLimiteAgendamento',
                async: true,
                data: { empresa_id     :   $('#id_empresa').val(),
                        data           :   formataStringData(data_atual) }
            }).done(function( data ) {
                var dados = $.parseJSON(data);
                
                if(dados.retorno == 'erro' ){
                    
                    
                    if(dados.peso == 0){
                       
                        $('#id_turno').removeAttr('disabled');
                        $('#id_turno').attr('required','required');
                        $('#id_turno').focus();
                    }else{
                        
                        if(dados.hora_limite == null){
                            /*swal({
                                title: "Atenção!",
                                text:  "Hora limite de agendamento para hoje esta sem lançamento para o Dia. Por favor Verifique!",
                                type:  'warning'
                            }).then(function() {
                                $('#data').val('');
                                $('#data').focus();
                            });*/
                        }else{
                            swal({
                                title: "Atenção!",
                                text:  "Hora limite de agendamento para hoje passou ("+dados.hora_limite+"). Agende para amanhã!",
                                type:  'warning'
                            }).then(function() {
                                $('#data').val('');
                                $('#data').focus();
                            });
                        }
                    }
  
                }

            }); 
        }else{
            $('#id_turno').removeAttr('required');
        }

       
    });


    //Cadastro de transportadora
    $('#cadastrar').bind('click', function(){

        if($('#cnpj').val() != ''){

            $.ajax({
                method: "POST",
                url: base_url+'AreaClientes/cadastrarTransportadora',
                async: true,
                data: { dados     :   $('div#form_transportadora :input').serializeArray() }
            }).done(function( data ) {
                var dados = $.parseJSON(data);
                if(dados.retorno == 'sucesso' ){
                    swal({
                        title: "Ok!",
                        text:  "Transportadaora cadastrada com sucesso!",
                        type:  'success'
                    }).then(function() {
                        $('#id_transportadora').val(dados.empresa_id);
                        $('#transportadora').val(dados.empresa);
                        $('#m_add_empresa').modal('hide');

                    });      
                }

            }); 
        }else{
            swal({
                title: "Atenção!",
                text:  "Preencha corretamente os dados da transportadora",
                type:  'warning'
            }).then(function() {
                
            });      
        }
    });

    $('#nfe').bind('change', function() {
        var file_data = $('#nfe').prop('files')[0];   
        var form_data = new FormData();                  
        form_data.append('file', file_data);        
        $.ajax({
            method: "POST",
            url: base_url+'areaClientes/uploadNfe',
            async: false,           
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,            
            success: function(data){
                var dados = $.parseJSON(data); 

                if( dados.retorno == 'ja existe' ){
                    swal({
                        title: "Atenção!",
                        text:  "Já existe um agendamento para essa nota fiscal. Agendamento:"+dados.agendamento+" - Placa: "+dados.placa,
                        type:  'warning'
                    }).then(function() {
                        $('#nfe').val('');
                    });
                // envia para o php os dados da nota     
                }else if(dados.retorno == 'sucesso'){
                    //$('#div-nfe').empty();
                    $('#div-nfe').append(dados.nfe);
                    //$('#arquivo_xml').remove();
                    $('#nfe').after(dados.arquivo);
                    $('#tabela_produtos').slideUp();
                    //$('#produtos_xml').empty();
                    $('#produtos_xml').append(dados.html);
                    $('#tabela_produtos').slideDown();    
                    $('.qtd').unmask();
                    $('.qtd').mask("#.##0", {reverse: true});                
                }else if(dados.retorno == 'cfop errado'){

                    swal({
                        title: "Atenção!",
                        text: "Mercadoria Com CFOP errado, verifique a nota e tente novamente.",
                        type: 'warning'
                    }).then(function() {
                        $('#nfe').val('');
                        $('#produtos_xml').empty();
                        $('#tabela_produtos').slideUp();
                        $('#div-nfe').empty();
                    }); 

                }else if(dados.retorno == 'Erro de upload' || dados.retorno == 'Nfe errada'){

                    swal({
                        title: "Atenção!",
                        text: "Problema ao enviar a nota fiscal, tente novamente com outra nota fiscal.",
                        type: 'warning'
                    }).then(function() {
                        $('#nfe').val('');
                        $('#produtos_xml').empty();
                        $('#tabela_produtos').slideUp();
                        $('#div-nfe').empty();
                    }); 
                }else if(dados.retorno == 'padrao errado'){

                    swal({
                        title: "Atenção!",
                        text: "A nota fiscal está com o padrão errado, verifique os dados da nota e tente novamente.",
                        type: 'warning'
                    }).then(function() {
                        $('#nfe').val('');
                        $('#produtos_xml').empty();
                        $('#tabela_produtos').slideUp();
                        $('#div-nfe').empty();
                    }); 
                }           
           }
        });
    });

    $('#cpf').bind('focusout', function(){
        var cpf = $(this).val();
        
        if(cpf != "" && $('.fl_motorista:checked').val() == 0 ){
            if(isCpfValid(cpf) ){
                verificaMotoristaCpf(cpf);
                
            }else{
                swal({
                    title: "Ops!",
                    text: "Cpf Inválido",
                    type: 'warning'
                }).then(function() {
                    $('#cpf').val('');
                    $('#cpf').focus();
                }); 
            }
        }else if($('.fl_motorista:checked').val() == 1){
            verificaMotoristaCpf(cpf); 
        }
    });

    $('#id_turno').bind('change', function(){
        var id_turno = $(this).val();
        data_atual = $('#data_atual').val();
        if( data_atual == $('#data').val() ){ 
            $.ajax({
                method: "POST",
                url: base_url+'AreaClientes/verificaHoraLimiteAgendamento',
                async: true,
                data: { empresa_id     :   $('#id_empresa').val(),
                        id_turno       :   id_turno }
            }).done(function( data ) {
                var dados = $.parseJSON(data);
                if(dados.retorno == 'erro' ){
                    if(dados.peso == 0){
                        swal({
                            title: "Atenção!",
                            text:  "Cota sem lançamento para o Turno. Por favor Verifique!",
                            type:  'warning'
                        }).then(function() {
                            $('#id_turno').val('');
                            $('#id_turno').focus();
                        });
                    }else{
                        if(dados.hora_limite == 'undefined'){
                            swal({
                                title: "Atenção!",
                                text:  "Hora limite de agendamento para o Turno passou ("+dados.hora_limite+"). Tente outro turno!",
                                type:  'warning'
                            }).then(function() {
                                $('#data').val('');
                            });   
                        }else{
                            swal({
                                title: "Atenção!",
                                text:  "Hora limite de agendamento para o Turno passou . Tente outro turno!",
                                type:  'warning'
                            }).then(function() {
                                $('#data').val('');
                            }); 
                        }
                    }
                        
                }

            }); 
        }
        
    });


    // verifica peso total ultrapassa o agendamento
    $('form').bind('submit', function(e){
         
        total_qtd = 0; 
        $('.pesoL_nfe').each(function(){
            total_qtd = parseInt(total_qtd) + parseInt($(this).val().replace('.',''));
        });
        
        if($('#id_turno').val() == ''){

            $.ajax({
                method: "POST",
                url: base_url+'AreaClientes/verificaCotasAgendamento',
                async: true,
                data: { empresa_id      :   $('#id_empresa').val(),
                        dt_agendamento  :   formataStringData($('#data').val()),
                        total           :   total_qtd }
            }).done(function( data ) {
                var dados = $.parseJSON(data);
                if(dados.retorno == 'erro'){
                    
                    swal({
                        title: "Atenção!",
                        text: dados.mensagem,
                        type: 'warning'
                    }).then(function() {
                        $('#nfe').val('');
                        $('#tabela_produtos').slideUp();
                        $('#div-nfe').empty();
                        $('#produtos_xml').empty();
                    }); 
                    
                }else{
                    //sucesso, submete o formulário
                    $('form').unbind();
                    $('form').submit();
                }

            });
        }else{
            
            /*Verifica o Turno*/
            $.ajax({
                method: "POST",
                url: base_url+'AreaClientes/verificaCotasAgendamento',
                async: true,
                data: { empresa_id      :   $('#id_empresa').val(),
                        id_turno        :   $('#id_turno').val(),
                        total           :   total_qtd }
            }).done(function( data ) {
                var dados = $.parseJSON(data);
                if(dados.retorno == 'erro'){
                    
                    swal({
                        title: "Atenção!",
                        text: dados.mensagem,
                        type: 'warning'
                    }).then(function() {
                        $('#nfe').val('');
                        $('#tabela_produtos').slideUp();
                        $('#div-nfe').empty();
                        $('#produtos_xml').empty();
                    }); 
                    
                }else{
                    //sucesso, submete o formulário
                    $('form').unbind();
                    $('form').submit();
                }

            });
        }
         
        e.preventDefault();
    });

    $('#placa').bind('focusout', function(){
        var placa = $(this).val();
        $.ajax({
            method: "POST",
            url: base_url+'AreaClientes/verificaVeiculo',
            async: true,
            data: { placa : placa }
        }).done(function( data ) {
            var dados = $.parseJSON(data);
            console.log(dados.total);
            if(dados.total > 0){
                
                swal({
                    title: "Atenção!",
                    text: "Veículo bloqueado para acessar o pátio",
                    type: 'warning'
                }).then(function() {
                    $('#placa').val('');
                    
                }); 
                
            }

        });
    });

    $('#add_empresa').bind('click', function(){
        $('#m_add_empresa').modal({
            show: true
        }); 
    });

    $('#cnpj').mask('00.000.000/0000-00');
    verificaCnpj();
    $('#cnpj').mask('00.000.000/0000-00');
                        
    $('#inscricao_estadual').attr('required', true);
    $('#fantasia').attr('required', true);
    $('#cartao_cnpj').attr('required', true);
    $('#estadoE').removeAttr('name');
    $('#estado').attr('name','estado');
    $('#estadoE').fadeOut('slow');
    $('#estado').fadeIn('slow');

});

function verificaVesperaFeriado(feriados){
    var hoje = new Date();
    var dia_vespera = (parseInt(hoje.getDate())+1)+'/'+(parseInt(hoje.getMonth())+1)+'/'+hoje.getFullYear();    
    if( feriados.indexOf(dia_vespera) > -1 && hoje.getDay() < 6  ){
        return 1;
    }else if(feriados.indexOf(dia_vespera) > -1 && hoje.getDay() >= 6 ){
        return 2;
    }else{
        return 3;
    }
}

function verificaMotorista(cnh, cpf){
    $.ajax({
        method: "POST",
        url: base_url+'AreaClientes/verificaCpfAgendamento',
        async: true,
        data: { cpf     :   cpf,
                cnh     :   cnh }
    }).done(function( data ) {
        var dados = $.parseJSON(data);
        if(dados != null){
            $('#motorista').val(dados.nome);
            $('#motorista').attr('disabled',true);
            $('#telefone').val(dados.fone);
            $('#telefone').attr('disabled',true);
            $('#motorista').after('<input type="hidden" name="id_motorista" id="id_motorista" value="'+dados.id+'" /> ')
        }else{
            $('#motorista').attr('disabled',false);
            $('#motorista').val('');
            $('#telefone').attr('disabled',false);
            $('#telefone').val('');
        }

    }); 
}

function verificaMotoristaCpf(cpf){

    $.ajax({
        method: "POST",
        url: base_url+'AreaClientes/verificaCpfAgendamento',
        async: true,
        data: { cpf     :   cpf }
    }).done(function( data ) {
        var dados = $.parseJSON(data);
        if(dados != null){  
            if(dados.fl_bloqueado == 'S'){
                swal({
                    title: "Atenção!",
                    text: "Motorista temporariamente bloqueado.",
                    type: 'warning'
                }).then(function() {
                    $('#id_motorista').remove();
                    $('#motorista').attr('disabled',false);
                    $('#motorista').val('');
                    $('#telefone').attr('disabled', false);
                    $('#telefone').val('');
                    $('#cnh').val('');
                    $('#cpf').val('');

                });    
            }else if(dados != null){
                $('#motorista').val(dados.nome);            
                $('#telefone').val(dados.fone);
                $('#cnh').val(dados.nr_cnh);            
                $('#id_motorista').remove();
                $('#motorista').after('<input type="hidden" name="id_motorista" id="id_motorista" value="'+dados.id+'" /> ')
            }else{

                $('#id_motorista').remove();
                $('#motorista').attr('disabled',false);
                $('#motorista').val('');
                $('#telefone').attr('disabled', false);
                $('#telefone').val('');
                $('#cnh').val('');            
            }
        }

    });     
}

function verificaMotoristaCnh(cnh){
    $.ajax({
        method: "POST",
        url: base_url+'AreaClientes/verificaCnhAgendamento',
        async: true,
        data: { cnh     :   cnh }
    }).done(function( data ) {
        var dados = $.parseJSON(data);
        if(dados != null){
            $('#motorista').val(dados.nome);            
            $('#telefone').val(dados.fone);
            $('#cpf').val(dados.cpf);            
            $('#id_motorista').remove();
            $('#motorista').after('<input type="hidden" name="id_motorista" id="id_motorista" value="'+dados.id+'" /> ')
        }else{

            $('#id_motorista').remove();
            $('#motorista').attr('disabled',false);
            $('#motorista').val('');
            $('#telefone').attr('disabled', false);
            $('#telefone').val('');
            $('#cpf').val('');
        }

    });     
}

function isCpfValid(cpf) {
    
    cpf = cpf.replace(/[^\d]+/g,''); 
    if(cpf == '') return false; 
    // Elimina CPFs invalidos conhecidos    
    if (cpf.length != 11 || 
        cpf == "00000000000" || 
        cpf == "11111111111" || 
        cpf == "22222222222" || 
        cpf == "33333333333" || 
        cpf == "44444444444" || 
        cpf == "55555555555" || 
        cpf == "66666666666" || 
        cpf == "77777777777" || 
        cpf == "88888888888" || 
        cpf == "99999999999")
            return false;       
    // Valida 1o digito 
    add = 0;    
    for (i=0; i < 9; i ++)      
        add += parseInt(cpf.charAt(i)) * (10 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11)     
            rev = 0;    
        if (rev != parseInt(cpf.charAt(9)))     
            return false;       
    // Valida 2o digito 
    add = 0;    
    for (i = 0; i < 10; i ++)       
        add += parseInt(cpf.charAt(i)) * (11 - i);  
    rev = 11 - (add % 11);  
    if (rev == 10 || rev == 11) 
        rev = 0;    
    if (rev != parseInt(cpf.charAt(10)))
        return false;       
    return true;   
};

    
function maskMercosul(selector) {
    var MercoSulMaskBehavior = function (val) {
        var myMask = 'AAA0A00';
        var mercosul = /([A-Za-z]{3}[0-9]{1}[A-Za-z]{1})/;
        var normal = /([A-Za-z]{3}[0-9]{2})/;
        var replaced = val.replace(/[^\w]/g, '');
        if (normal.exec(replaced)) {
            myMask = 'AAA-0000';
        } else if (mercosul.exec(replaced)) {
            myMask = 'AAA-0A00';
        }
        return myMask;
    },
    mercoSulOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(MercoSulMaskBehavior.apply({}, arguments), options);
        }
    };
    $(function() {
        $(selector).bind('paste', function(e) {
            $(this).unmask();
        });
        $(selector).bind('input', function(e) {
            $(selector).mask(MercoSulMaskBehavior, mercoSulOptions);
        });
    });
}

function formataStringData(data) {
  var dia  = data.split("/")[0];
  var mes  = data.split("/")[1];
  var ano  = data.split("/")[2];

  return ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);
  // Utilizo o .slice(-2) para garantir o formato com 2 digitos.
}

function verificaEmbalagem(nf, embalagem){
              
    var tipo            =   $(embalagem).val();
    var peso_bruto      =   $('#pesoB_'+nf).val();
    var peso_liquido    =   $('#pesoL_'+nf).val();
    var qtd             =   0;
    $('.qtd_produto_v').each( function(){
        qtd = qtd + parseInt($(this).val());
    });

    if($('#produtos_xml tr').length == 1){
        // se for apenas granel
        if( tipo == 0 ){
            if (peso_bruto != peso_liquido){                
                swal({
                    title: "Atenção!",
                    text:  "Peso bruto diferente do peso líquido, verifique o tipo de embalagem ou utilize outra nota fiscal.",
                    type:  'warning'
                }).then(function() {
                    $(embalagem).val('');
                    $('#nfe').val('');
                    $('#tabela_produtos').slideUp();
                    $('#produtos_xml').empty();
                    $('#div-nfe').empty();
                });                    
            }else if( peso_liquido != qtd ){
                //var dif = peso_liquido / 1000;                
                if (parseInt(peso_liquido) != parseInt(qtd)) {
                   /* swal({
                        title: "Atenção!",
                        text:  "Peso líquido/bruto diferente da quantidade, verifique a nota fiscal e tente novamente.",
                        type:  'warning'
                    }).then(function() {
                        $('#nfe').val('');
                        $('#produtos_xml').empty();
                        $('#tabela_produtos').slideUp();
                        $('#div-nfe').empty();
                    });  */  
                }
            }
        }
        // se for big bags por unidade
        if( tipo == 1){
            $('#qtd_'+nf).val(parseInt(peso_liquido));
            if (peso_bruto <= peso_liquido){
                swal({
                    title: "Atenção!",
                    text:  "Peso bruto menor ou igual ao peso líquido, verifique o tipo de embalagem ou utilize outra nota fiscal.",
                    type:  'warning'
                }).then(function() {
                    $(embalagem).val('');
                    $('#nfe').val('');
                    $('#produtos_xml').empty();
                    $('#tabela_produtos').slideUp();
                    $('#div-nfe').empty();
                });                    
            }else if( peso_liquido != qtd ){
                var dif = peso_liquido / 1050;
                console.log('peso_liquido:'+peso_liquido);
                console.log('qtd:'+qtd);
                console.log('dif:'+dif);
                if (dif != qtd){
                    swal({
                        title: "Atenção!",
                        text:  "Peso líquido/bruto diferente da quantidade, verifique a nota fiscal e tente novamente.",
                        type:  'warning'
                    }).then(function() {
                        $('#nfe').val('');
                        $('#produtos_xml').empty();
                        $('#tabela_produtos').slideUp();
                        $('#div-nfe').empty();
                    });    
                }
            }   
        }

        // se for big bags por kg
        if( tipo == 2){
            $('#qtd_'+nf).val(parseInt(peso_liquido));
            if (peso_bruto <= peso_liquido){
                swal({
                    title: "Atenção!",
                    text:  "Peso bruto menor ou igual ao peso líquido, verifique o tipo de embalagem ou utilize outra nota fiscal.",
                    type:  'warning'
                }).then(function() {
                    $(embalagem).val('');
                    $('#nfe').val('');
                    $('#produtos_xml').empty();
                    $('#tabela_produtos').slideUp();
                    $('#div-nfe').empty();
                });                    
            }   
        }
    }
}

function isCNPJValid(cnpj) {  
    var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
    if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
        return false;
    for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
    if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
    if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    return true; 
};

function verificaCnpj(){
    $('#cnpj').bind('focusout', function(){
            var cnpj = $(this).val(); 
            var tp_cadastro = $('.tp_cadastro:checked').val();
            var erro = 0;

            if(cnpj != ""){ 

                if(  tp_cadastro == '0' ) {
                    if(!isCNPJValid(cnpj)){
                        swal({
                            title: "Atenção!",
                            text: "CNPJ inválido!",
                            type: 'warning'
                        }).then(function() {
                            $('#cnpj').val('');
                        });
                        erro = 1;
                    }else{
                        erro = 0;
                    }
                }
                if(  erro == 0 ){
                    $.ajax({
                    method: "POST",
                    url: base_url+'clientes/verifica_cnpj',
                    async: true,
                    data: { cnpj    :   cnpj }
                    }).done(function( data ) {
                        var dados = $.parseJSON(data);                          
                        if(dados.length > 0){
                            
                            swal({
                                title: "Atenção!",
                                text: "CNPJ já cadastrado!",
                                type: 'warning'
                            }).then(function() {
                                $('#cnpj').val('');
                            });    
                            
                        }else{                                  
                            var cnpj_sem_mascara = cnpj.replace(/[^\d]+/g,'');

                            $.ajax({
                                method: "POST",
                                url: base_url+'clientes/ValidaCnpj',
                                async: true,
                                data: { cnpj    :   cnpj_sem_mascara }
                            }).done(function( data ) {
                                var dados = $.parseJSON(data);   
                                
                                if (dados.receita.erro == 0 && dados.receita.situacao == "ATIVA" ){
                                    $('#razao_social').attr('disabled', false);     
                                    $('#razao_social').val(dados.receita.retorno.razao_social);
                                    $('#razao_social').attr('style','border-color: #5fda17;');
                                    $('#fantasia').attr('disabled', false);
                                    $('#fantasia').val(dados.receita.retorno.nome_fantasia );
                                    $('#fantasia').attr('style','border-color: #5fda17;');
                                    $('#telefone_empresa').attr('disabled', false);
                                    $('#telefone_empresa').val(dados.receita.retorno.telefone);
                                    $('#telefone_empresa').attr('style','border-color: #5fda17;');
                                    $('#endereco').attr('disabled', false); 
                                    $('#endereco').val(dados.receita.retorno.logradouro+', '+dados.receita.retorno.numero+' '+dados.receita.retorno.complemento);                                
                                    $('#endereco').attr('style','border-color: #5fda17;');
                                    $('#email').attr('disabled', false); 
                                    $('#email').val('');                                                                                                
                                    $('#cidade').attr('disabled', false); 
                                    $('#cidade').val(dados.receita.retorno.municipio_ibge);  
                                    $('#cidade').attr('style','border-color: #5fda17;');
                                    $('#estado').attr('disabled', false); 
                                    $('#estado').val(dados.receita.retorno.uf);
                                    $('#estado').attr('style','border-color: #5fda17;');
                                    $('#bairro').attr('disabled', false); 
                                    $('#bairro').val(dados.receita.retorno.bairro);
                                    $('#bairro').attr('style','border-color: #5fda17;');
                                    $('#cep').attr('disabled', false); 
                                    $('#cep').val(dados.receita.retorno.cep);                          
                                    $('#cep').attr('style','border-color: #5fda17;');
                                    $('#pais').attr('disabled', false);
                                    $('#pais').attr('style','border-color: #5fda17;');                                                
                                    $('#inscricao_estadual').attr('disabled', false);
                                    $('#inscricao_estadual').val('');                        
                                    $('#cartao_cnpj').val(dados.receita.save);                    
                                    
                                    $('#pais').val('Brasil');         
                                    $('#salvar').val('1');
                                }else{

                                   swal({
                                        title: "Atenção!",
                                        text: dados.msg,
                                        type: 'warning'
                                    }).then(function() {
                                        $('#cnpj').val('');
                                    });

                                }
                            });
                        }                           
                    });    
                
                }            
            }
        }); 
}