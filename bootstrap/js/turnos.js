$(document).ready(function(){

	$('.datepicker').mask('99/99/9999');
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('.datepicker').datepicker({
	    format: 'dd/mm/yyyy'
	    
	});

		

});

function excluirTurno(id){ 

	swal({
        title: 'Exclusão de Turno',
        text: 'Tem certeza que deseja excluir este Turno?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: "swal2-confirm btn btn-danger m-btn m-btn--custom",
        confirmButtonText: "Excluir",
        cancelButtonText: "Cancelar"
    }).then(function(isConfirm) {
        
        if (isConfirm.value) {
        	$.ajax({
				method: "POST",
				url: base_url+"AreaAdministrador/excluirTurno",
				async: true,
				data: { id 	: 	id 	}
			}).done(function( data ) {
                
				var dados = $.parseJSON(data);
			
				swal({
					title: dados.titulo,
					text:  dados.texto,
					type:  dados.tipo
				}).then(function() {
					if( dados.retorno){
						window.location.href=base_url+"AreaAdministrador/turnos";
					}
				}); 
				
				
			});
        }
    });
}
