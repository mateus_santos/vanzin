$(document).ready(function(){
	$(".peso").mask("999.999.999", { reverse : true});
	$('.datepicker').mask('99/99/9999');
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('.datepicker').datepicker({
	    format: 'dd/mm/yyyy'
	    
	});

	$('.limpar').bind('click', function(){
		$('input').val('');
	});
 
	$('#dt_fim').bind('focusout', function(){
		var dataFim = $('#dt_fim').val();
		if(dataFim != '' && (dataFim != undefined && dataFim != '') ){ 
			var partesData = dataFim.split("/");
			var dataFim = new Date(partesData[2], partesData[1] - 1, partesData[0]);
			var dataIni 	= 	$('#dt_ini').val();
			var partesData	= 	dataIni.split("/");
			var dataIni 	= 	new Date(partesData[2], partesData[1] - 1, partesData[0]);
			console.log(dataFim);
			console.log(dataIni);
			if( dataFim < dataIni ){
				swal({
					title: "Ops!",
					text: "DATA FINAL menor que a DATA INICIAL",
					type: 'warning'
				}).then(function() {
					$('#dt_fim').val('');
				}); 
			}
		}
	});

	$('#calcular_peso').bind('click', function(){		
		
		if($('#due').val() == ''){
			swal({
				title: "Ops!",
				text: "Preencha uma DUE",
				type: 'warning'
			}).then(function() {}); 
		}else{

			$.ajax({
				method: "POST",
				url: base_url+"AreaAdministrador/retornaPesosDUE",
				async: true,
				data: { due 	: 	$('#due').val() }
			}).done(function( data ) {
				var dados = $.parseJSON(data);
				$('.mostrar_saldo').empty();						
				$('.mostrar_saldo').html(dados.retorno); 
				$('.mostrar_saldo').fadeIn('slow');
				$('.mostrar_saldo').attr('style','animation-play-state: running !important');			
				$(".peso").mask("999.999.999.999", { reverse : true});
			});
		}
	});

	$('#html_table').DataTable({
	    	"scrollX": true,
	    	"order": [[ 1, "desc" ]],
			"language": {
				"sEmptyTable": "Nenhum registro encontrado",
			    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
			    "sInfoPostFix": "",
			    "sInfoThousands": ".",
			    "sLengthMenu": "_MENU_    Resultados por página",
			    "sLoadingRecords": "Carregando...",
			    "sProcessing": "Processando...",
			    "sZeroRecords": "Nenhum registro encontrado",
			    "sSearch": "Pesquisar",
			    "oPaginate": {
			        "sNext": '<i class="la la-angle-double-right"></i>',
			        "sPrevious": '<i class="la la-angle-double-left"></i>',
			        "sFirst": "Primeiro",
			        "sLast": "Último"
			    },
			    "oAria": {
			        "sSortAscending": ": Ordenar colunas de forma ascendente",
			        "sSortDescending": ": Ordenar colunas de forma descendente"
			    }
			}
		});
	    
	 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
		$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
		$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
		$('.dataTables_filter').css('float','right');
		$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
		$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
		$('table thead').css('background-color', '#f4f3f8');		
		$('table tbody tr:odd').addClass('zebraUm');
		$('table tbody tr:even').addClass('zebraDois');

		

    $('#enviar').bind('click', function(){    	
    	
    	$('#rel_chamados').slideDown('slow');    	
    	var dt_ini 	= 	$('#dt_ini').val();
    	var dt_fim	= 	$('#dt_fim').val();    	
    	var placa 	= 	$('#placa').val();
    	var due 	= 	$('#due').val();
    	var cpf 	= 	$('#cpf').val();
		var status 	= 	$('#status').val();

	    $.ajax({
			method: "POST",
			url: base_url+"AreaAdministrador/relEntregaCargasDUEAjax",
			async: true,
			data: { dt_ini 	: 	dt_ini,
					dt_fim 	: 	dt_fim,					
					placa   : 	placa,
					due 	: 	due,
					cpf 	: 	cpf,
					status	: 	status }
		}).done(function( data ) {
			var dados = $.parseJSON(data);
			$('#relatorio').empty();
			$('#relatorio').append(dados.retorno);
			datatable();				
		});
		
	});	

});


function datatable(){

	$('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 1, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');	
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');
}
