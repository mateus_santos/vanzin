$(document).ready(function(){
	$.fn.datetimepicker.defaults.language = 'pt-BR';
	$('.form_datetime').mask('99/99/9999 00:00');
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('.datepicker').datepicker({
	    format: 'dd/mm/yyyy'
	    
	});
	$ ( ".form_datetime" ). datetimepicker ({
        format: 'dd/mm/yyyy hh:ii',
        autoclose : true , 
        todayBtn : true , 
        locale : 'pt-BR',
        language : 'pt-BR',
        minuteStep : 10 
    });

	//Verifica se a data de atracação não tem conflito com as outras datas
    $('#dthr_atracacao').change(function(e){
        if($('#dthr_desatracacao').val() != ''){
			var dthr_atracacao 		= retornaDataHoraInteira($('#dthr_atracacao').val());
			var dthr_desatracacao 	= retornaDataHoraInteira($('#dthr_desatracacao').val());
			
            if(dthr_atracacao >= dthr_desatracacao){
                swal({
                    title: "Ops!",
                    text: "A data da atracação não pode ser maior ou igual a data de desatracação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_atracacao').val('');
                }); 
            }
        }
        
    });
    
    //Verifica se a data de desatracação não tem conflito com as outras datas
    $('#dthr_desatracacao').change(function(e){ 
        if($('#dthr_atracacao').val() != ''){

			var dthr_atracacao 		= retornaDataHoraInteira($('#dthr_atracacao').val());
			var dthr_desatracacao 	= retornaDataHoraInteira($('#dthr_desatracacao').val());

            if(dthr_atracacao >= dthr_desatracacao){
                swal({
                    title: "Ops!",
                    text: "A data da desatracação não pode ser menor ou igual a data de atracação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_desatracacao').val('');
                }); 
            }
        }
        
    });

	$('#dt_fim').bind('focusout', function(){
		var dataFim = $('#dt_fim').val();
		if(dataFim != '' && (dataFim != undefined && dataFim != '') ){ 
			var partesData = dataFim.split("/");
			var dataFim = new Date(partesData[2], partesData[1] - 1, partesData[0]);
			var dataIni 	= 	$('#dt_ini').val();
			var partesData	= 	dataIni.split("/");
			var dataIni 	= 	new Date(partesData[2], partesData[1] - 1, partesData[0]);
			console.log(dataFim);
			console.log(dataIni);
			if( dataFim < dataIni ){
				swal({
					title: "Ops!",
					text: "DATA FINAL menor que a DATA INICIAL",
					type: 'warning'
				}).then(function() {
					$('#dt_fim').val('');
				}); 
			}
		}
	});

	$('#html_table').DataTable({
	    	"scrollX": true,
	    	"order": [[ 0, "desc" ]],
			"language": {
				"sEmptyTable": "Nenhum registro encontrado",
			    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
			    "sInfoPostFix": "",
			    "sInfoThousands": ".",
			    "sLengthMenu": "_MENU_    Resultados por página",
			    "sLoadingRecords": "Carregando...",
			    "sProcessing": "Processando...",
			    "sZeroRecords": "Nenhum registro encontrado",
			    "sSearch": "Pesquisar",
			    "oPaginate": {
			        "sNext": '<i class="la la-angle-double-right"></i>',
			        "sPrevious": '<i class="la la-angle-double-left"></i>',
			        "sFirst": "Primeiro",
			        "sLast": "Último"
			    },
			    "oAria": {
			        "sSortAscending": ": Ordenar colunas de forma ascendente",
			        "sSortDescending": ": Ordenar colunas de forma descendente"
			    }
			}
		});
	    
	 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
		$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
		$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
		$('.dataTables_filter').css('float','right');
		$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
		$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
		$('table thead').css('background-color', '#f4f3f8');
		//$('table thead').css('height', '150px');
		$('table tbody tr:odd').addClass('zebraUm');
		$('table tbody tr:even').addClass('zebraDois');

		

    $('#enviar').bind('click', function(){
    	
    	$('#rel_chamados').slideDown('slow');    	
    	var dt_ini 	= 	$('#dt_ini').val();
    	var dt_fim	= 	$('#dt_fim').val();
    	var placa 	= 	$('#placa').val();
    	var nr_nf 	= 	$('#nr_nf').val();
    	var empresa_id = '';
    	if( $('#empresa_id').val() != undefined && $('#empresa_id').val() != '' ){
    		empresa_id = $('#empresa_id').val();
    	} 
/*//Comentado em 24/05/2022 pois estava dando erro na tela de pesquisa de transito
	    $.ajax({
			method: "POST",
			url: base_url+"AreaClientes/retornaAgendamentosAjax",
			async: true,
			data: { dt_ini 		: 	dt_ini,
					dt_fim 		: 	dt_fim,
					placa  		: 	placa,
					nr_nf   	: 	nr_nf,
					empresa_id 	: 	empresa_id	}
		}).done(function( data ) {
			var dados = $.parseJSON(data);
			$('#relatorio').empty();
			$('#relatorio').append(dados.retorno);
			datatable();
				
		});*/
	});	

});


function datatable(){

	$('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');
}

function excluirTransito(id){ 
	
	swal({
        title: 'Exclusão de Trânsito Simplificado',
        text: 'Tem certeza que deseja excluir este Transito?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: "swal2-confirm btn btn-danger m-btn m-btn--custom",
        confirmButtonText: "Excluir",
        cancelButtonText: "Cancelar"
    }).then(function(isConfirm) {
        if (isConfirm.value) {
        	$.ajax({
				method: "POST",
				url: base_url+"AreaTransito/excluirTransitoAjax",
				async: true,
				data: { id 	: 	id 	}
			}).done(function( data ) {
				var dados = $.parseJSON(data);
				
				if(dados.retorno.existe_movimento[0].existe > 0){
					swal({
						title: 'Ops',
						text:  'Não é possível realizar a operação, existem movimentos neste Trânsito',
						type:  'warning'
					}).then(function() {
						if( dados.retorno){
							window.location.href=base_url+"AreaTransito/pesquisaTransito";
						}
					}); 
				}else{
					if(dados.retorno.exclui_due && dados.retorno.exclui_transito){
						swal({
							title: "OK!",
							text: 'Trânsito Simplificado excluído com Sucesso!',
							type: 'success'
						}).then(function() {
							if( dados.retorno){
								window.location.href=base_url+"AreaTransito/pesquisaTransito";
							}
						}); 
					}
				}
			
				
				
			});
        }
    });
}
;(function($){
	$.fn.datetimepicker.dates['pt-BR'] = {
        format: 'dd/mm/yyyy',
		days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo"],
		daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
		daysMin: ["Do", "Se", "Te", "Qu", "Qu", "Se", "Sa", "Do"],
		months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
		monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
		today: "Hoje",
		suffix: [],
		meridiem: []
	};
}(jQuery));
//Função feita para retornar a data em ordem para comparação
function retornaDataHoraInteira(vl_data) {

	var divide = vl_data.split(" ");
	var dv_data = divide[0].split("/");
	if(vl_data.length > 10){
		var dv_hora = divide[1].split(":");
		var datahora = dv_data[2]+dv_data[1]+dv_data[0]+dv_hora[0]+dv_hora[1];
	}else{
		var datahora = dv_data[2]+dv_data[1]+dv_data[0];
	}
	//var datahora = dv_data[2]+dv_data[1]+dv_data[0]+dv_hora[0]+dv_hora[1];

	return datahora;
}