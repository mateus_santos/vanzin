$(document).ready(function(){
    $.fn.datetimepicker.defaults.language = 'pt-BR'; 
    $('.form_datetime').mask('99/99/9999 00:00');
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    $(".peso").mask("###.###.###", { reverse : true});
    
    $ ( ".form_datetime" ). datetimepicker ({
        format: 'dd/mm/yyyy hh:ii',
        autoclose : true , 
        todayBtn : true , 
        locale : 'pt-BR',
        language : 'pt-BR',
        minuteStep : 10 
    });

    //Verifica se a data de atracação não tem conflito com as outras datas
    $('#dthr_atracacao').change(function(e){

        var dt_atrac    = retornaDataHoraInteira($('#dthr_atracacao').val());
        var dt_desatrac = retornaDataHoraInteira($('#dthr_desatracacao').val());
        var dt_ini_oper = retornaDataHoraInteira($('#dthr_ini_operacao').val());
        var dt_fim_oper = retornaDataHoraInteira($('#dthr_fim_operacao').val());

        if($('#dthr_desatracacao').val() != ''){
            if(dt_atrac > dt_desatrac){
                swal({
                    title: "Ops!",
                    text: "A data da atracação não pode ser maior que a data de desatracação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_atracacao').val('');
                }); 
            }
        }
        if($('#dthr_ini_operacao').val() != ''){
            if(dt_atrac > dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da atracação não pode ser maior que a data de Início de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_atracacao').val('');
                }); 
            }
        }
        if($('#dthr_fim_operacao').val() != ''){
            if(dt_atrac >= dt_fim_oper){
                swal({
                    title: "Ops!",
                    text: "A data da atracação não pode ser maior ou igual que a data de fim de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_atracacao').val('');
                }); 
            }
        }
        
    });
    
    //Verifica se a data de desatracação não tem conflito com as outras datas
    $('#dthr_desatracacao').change(function(e){

        var dt_atrac    = retornaDataHoraInteira($('#dthr_atracacao').val());
        var dt_desatrac = retornaDataHoraInteira($('#dthr_desatracacao').val());
        var dt_ini_oper = retornaDataHoraInteira($('#dthr_ini_operacao').val());
        var dt_fim_oper = retornaDataHoraInteira($('#dthr_fim_operacao').val());

        if($('#dthr_atracacao').val() != ''){
            if(dt_atrac > dt_desatrac){
                swal({
                    title: "Ops!",
                    text: "A data da desatracação não pode ser menor que a data de atracação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_desatracacao').val('');
                }); 
            }
        }
        if($('#dthr_ini_operacao').val() != ''){
            if(dt_desatrac <= dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da desatracação não pode ser menor ou igual a data de Início de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_desatracacao').val('');
                }); 
            }
        }
        if($('#dthr_fim_operacao').val() != ''){
            if(dt_desatrac < dt_fim_oper){
                swal({
                    title: "Ops!",
                    text: "A data da desatracação não pode ser menor que a data de fim de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_desatracacao').val('');
                }); 
            }
        }
        
    });

    //Verifica se a data de Início de operação não tem conflito com as outras datas
    $('#dthr_ini_operacao').change(function(e){ 

        var dt_atrac    = retornaDataHoraInteira($('#dthr_atracacao').val());
        var dt_desatrac = retornaDataHoraInteira($('#dthr_desatracacao').val());
        var dt_ini_oper = retornaDataHoraInteira($('#dthr_ini_operacao').val());
        var dt_fim_oper = retornaDataHoraInteira($('#dthr_fim_operacao').val());

        if($('#dthr_atracacao').val() != ''){
            if(dt_atrac > dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da atracação não pode ser maior que a data de início de operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_ini_operacao').val('');
                }); 
            }
        }
        if($('#dthr_desatracacao').val() != ''){
            if(dt_desatrac <= dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da desatracação não pode ser menor ou igual a data de Início de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_ini_operacao').val('');
                }); 
            }
        }
        if($('#dthr_fim_operacao').val() != ''){
            if(dt_fim_oper < dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da final de operação não pode ser menor que a data de início de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_ini_operacao').val('');
                }); 
            }
        }
        
    });

    //Verifica se a data de Fim de operação não tem conflito com as outras datas
    $('#dthr_fim_operacao').change(function(e){ 

        var dt_atrac    = retornaDataHoraInteira($('#dthr_atracacao').val());
        var dt_desatrac = retornaDataHoraInteira($('#dthr_desatracacao').val());
        var dt_ini_oper = retornaDataHoraInteira($('#dthr_ini_operacao').val());
        var dt_fim_oper = retornaDataHoraInteira($('#dthr_fim_operacao').val());

        if($('#dthr_atracacao').val() != ''){
            if(dt_atrac > dt_fim_oper){
                swal({
                    title: "Ops!",
                    text: "A data da atracação não pode ser maior que a data de fim de operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_fim_operacao').val('');
                }); 
            }
        }
        if($('#dthr_desatracacao').val() != ''){
            if(dt_desatrac < dt_fim_oper){
                swal({
                    title: "Ops!",
                    text: "A data da desatracação não pode ser menor que a data de fim de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_fim_operacao').val('');
                }); 
            }
        }
        if($('#dthr_ini_operacao').val() != ''){
            if(dt_fim_oper < dt_ini_oper){
                swal({
                    title: "Ops!",
                    text: "A data da final de operação não pode ser menor que a data de início de Operação",
                    type: 'warning'
                }).then(function() {
                    $('#dthr_fim_operacao').val('');
                }); 
            }
        }
        
    });

    var total_lin = $("#tabela_due tr").length - 1;
    for(var x=0; x < total_lin; x++){
        $("#btn_adiciona_cliente_"+x).click(function(e){
            var myWindow = window.open("../../AreaAdministrador/cadastrarEmpresa", "Cadastro de Empresa", "width=600,height=500");
            
        });

        $("#exportador_"+x).keyup(function(e){
            var valor = this.value;
            var id = this.id;
            var divide = id.split('_');

            var num_empresa = divide[1];

            if(valor.length > 2){
                $.ajax({
                    method: "POST",
                    url: base_url+'areaTransito/buscaExportadorAjax/',
                    async: true,
                    data: { exportador    :   valor }
                }).done(function(data){
                    $("#lista_"+num_empresa).html(''); 
                    var dados = $.parseJSON(data);
                    var total_linhas = dados.retorno.exportador.length;
                    if(total_linhas > 0){
                        
                        var codigo = dados.retorno.exportador[0].id;
                        var nome    = dados.retorno.exportador[0].label;

                        var div = "<div style='background-color:white; float:left;'>";
                        $.each(dados.retorno.exportador, function(chave, valor){
                            div += "<div id='codigo_exp_"+chave+"' onclick='seleciona_exp("+num_empresa+","+valor.id+",\""+valor.label+"\")'>"+valor.label+"</div>";
                        });
                         div += "</div>";

                        
                        $("#lista_"+num_empresa).append(div);
                       
                    }
                     
                });
            }else{
                $("#lista_"+num_empresa).html('');
            }
            
        });

        //Verifica se esta aberta alguma DUE pra o mesmo exportador
        $('#status_'+x).change(function(e){
            var vl_selecionado = this.value;
            var id_selecionado = this.id;

            var div_linha = id_selecionado.split('_');

            var vl_empresa_linha = $('#id_empresa_'+div_linha[1]).val();
            var aberta = false;
            if(vl_selecionado == 'S'){
                aberta = true;
            }else{
                aberta = false;
            }

            for(var w=0; w < total_lin; w++){
                var vl_empresa  = $('#id_empresa_'+w).val();
                var vl_status   = $('#status_'+w).val();
                var id_status   = $('#status_'+w).attr('id');

                if(id_selecionado != id_status){
                    //alert(vl_empresa+' - '+vl_status+' - '+vl_empresa_linha+' - '+vl_selecionado);
                    if(vl_empresa_linha == vl_empresa){
                        if(vl_status == 1 && aberta){
                            
                            swal({
                                title: "Ops!",
                                text: "Somente pode haver uma DUE em Aberto para o mesmo Exportador",
                                type: 'warning'
                            }).then(function() {
                               $('#'+this.id).val('');
                            }); 
                            $(this).val('0');
                        }
                    }
                }
                
            }
        })
    }
   

	$('#adiciona').click(function(e){        
     
        var total_linhas = $("#tabela_due tr").length - 1;

        var div_linha = "<tr id='linha_"+total_linhas+"'>";
            div_linha += "<td style='padding-left:1px; padding-right:1px;'>";
            div_linha += "<input type='text' name='nr_due[]' id='nr_due_"+total_linhas+"' value='' class='form-control m-input campos_menor' required maxlength='20'/>";
            div_linha += "</td>";
            div_linha += "<td style='padding-left:1px; padding-right:1px;'>";
            div_linha += "<div><input type='text' style='width:85%; float:left;' name='exportador[]' id='exportador_"+total_linhas+"' autocomplete='off' value='' class='form-control m-input campos_menor' required/>";
            div_linha += "<input type='hidden'  name='id_empresa[]' id='id_empresa_"+total_linhas+"'  />";
            div_linha += "<input type='button'  style='width:10%;  float:left;' id='btn_adiciona_cliente_"+total_linhas+"' value='+' class='form-control m-input campos_menor' /><div id='lista_"+total_linhas+"'></div></div>";
            div_linha += "</td>";
            div_linha += "<td style='padding-left:1px; padding-right:1px;'>"; 
            div_linha += "<input type='text' name='ncm[]' id='ncm_"+total_linhas+"' value='' class='form-control m-input campos_menor' required maxlength='8'/>";
            div_linha += "</td>";
            div_linha += "<td style='padding-left:1px; padding-right:1px;'>"; 
            div_linha += "<input type='text' name='ce_mercante[]' id='ce_mercante_"+total_linhas+"' value='' class='form-control m-input campos_menor'  maxlength='15'/>";
            div_linha += "</td>";
            div_linha += "<td style='padding-left:1px; padding-right:1px;'>"; 
            div_linha += "<input type='text' name='peso[]' id='peso_"+total_linhas+"' value='' class='form-control m-input peso campos_menor' required maxlength='15'/>";
            div_linha += "</td>";
            div_linha += "<td style='padding-left:1px; padding-right:1px;'>"; 
            div_linha += "<select name='tipo[]' id='tipo_"+total_linhas+"' class='form-control m-select campos_menor' />";
            div_linha += "<option value = ''></option>";
            div_linha += "</select>";
            div_linha += "</td>";
            div_linha += "<td style='padding-left:1px; padding-right:1px;'>"; 
            div_linha += "<input type='text' name='ordem[]' id='ordem_"+total_linhas+"' value='' class='form-control m-input ordem campos_menor' required/>";
            div_linha += "</td>"; 
            div_linha += "<td style='padding-left:1px; padding-right:1px;'>"; 
            div_linha += "<select name='status[]' id='status_"+total_linhas+"' class='form-control m-input campos_menor' required >";
            div_linha += "<option value=''>	Selecione o tipo 	</option>";
            div_linha += "<option value='S'>ABERTA</option>";
            div_linha += "<option value='N'>ENCERRADA</option>";
            div_linha += "</select>";
            div_linha += "</td>";
            div_linha += "<td style='width:2%;'><img src='"+base_url+"/img_perfil/exc_mini.png' id='exclui_"+total_linhas+"'></td>";
            div_linha += "</tr>";

            $("#itens_due").append(div_linha);

            $(".peso").mask("###.###.###,##", { reverse : true});

            $(".ordem").mask("9", { reverse : true});

            $("#btn_adiciona_cliente_"+total_linhas).click(function(e){
                var myWindow = window.open("../../AreaAdministrador/cadastrarEmpresa", "Cadastro de Empresa", "width=600,height=500");
                
            });

            $("#exclui_"+total_linhas).click(function(e){
                $("#linha_"+total_linhas).remove();
                
            });

            $("#nr_due_"+total_linhas).blur(function(e){
                verifica_due(this.value, this.id);
                
            });

            $("#peso_"+total_linhas).blur(function(e){
                var peso = this.value;
                if(peso < 1){
                    swal({
                        title: "Ops!",
                        text: "Peso inválido. Verifique!",
                        type: 'warning'
                    }).then(function() {
                       $('#'+this.id).val('');
                    }); 
                }
                
            });
            
            $.ajax({
                method: "POST",
                url: base_url+'areaTransito/buscaTipoAjax/',
                async: true,
                data: { }
            }).done(function(data){
                
                var dados = $.parseJSON(data);
    
                var total_tipos = dados.retorno.tipos.length;
                if(total_tipos > 0){

                    $("#tipo_"+total_linhas).append("<option value=''>Selecione o Tipo</option>");
                    for (var i = 0; i < total_tipos; i++) {
                        
                        var codigo = dados.retorno.tipos[i].id;
                        var no_tipo = dados.retorno.tipos[i].no_tipo;

                        $("#tipo_"+total_linhas).append("<option value='"+codigo+"'>" + no_tipo + "</option>");
                    }
    
                }
    
            });
    

            $("#ordem_"+total_linhas).blur(function(e){ 
                verificaOrdem(this.id);
                
            });

            $("#exportador_"+total_linhas).keyup(function(e){
                var valor = this.value;
                var id = this.id;
                var divide = id.split('_');

                var num_empresa = divide[1];

                if(valor.length > 2){
                    $.ajax({
                        method: "POST",
                        url: base_url+'areaTransito/buscaExportadorAjax/',
                        async: true,
                        data: { exportador    :   valor }
                    }).done(function(data){
                        $("#lista_"+num_empresa).html(''); 
                        var dados = $.parseJSON(data);
                        var total_linhas = dados.retorno.exportador.length;
                        if(total_linhas > 0){
                            
                            var codigo = dados.retorno.exportador[0].id;
                            var nome    = dados.retorno.exportador[0].label;

                            var div = "<div style='background-color:white; float:left;'>";
                            $.each(dados.retorno.exportador, function(chave, valor){
                                div += "<div id='codigo_exp_"+chave+"' onclick='seleciona_exp("+num_empresa+","+valor.id+",\""+valor.label+"\")'>"+valor.label+"</div>";
                            });
                             div += "</div>";

                            
                            $("#lista_"+num_empresa).append(div);
                           
                        }
                         
                    });
                }else{
                    $("#lista_"+num_empresa).html('');
                }
                
            });

            //Verifica se esta aberta alguma DUE pra o mesmo exportador
        $('#status_'+total_linhas).change(function(e){
            var vl_selecionado = this.value;
            var id_selecionado = this.id;

            var div_linha = id_selecionado.split('_');

            var vl_empresa_linha = $('#id_empresa_'+div_linha[1]).val();
            var aberta = false;
            if(vl_selecionado == 1){
                aberta = true;
            }else{
                aberta = false;
            }

            for(var w=0; w < total_lin; w++){
                var vl_empresa  = $('#id_empresa_'+w).val();
                var vl_status   = $('#status_'+w).val();
                var id_status   = $('#status_'+w).attr('id');

                if(id_selecionado != id_status){
                    //alert(vl_empresa+' - '+vl_status+' - '+vl_empresa_linha+' - '+vl_selecionado);
                    if(vl_empresa_linha == vl_empresa){
                        if(vl_status == 'S' && aberta){
                            
                            swal({
                                title: "Ops!",
                                text: "Somente pode haver uma DUE em Aberto para o mesmo Exportador",
                                type: 'warning'
                            }).then(function() {
                               $('#'+this.id).val('');
                            }); 
                            $(this).val('0');
                        }
                    }
                }
                
            }
        })
    });
   
	
});

//Seleciona registro do ajax e joga o valor no id
function seleciona_exp(num, codigo, nome){
    $("#id_empresa_"+num).val(codigo);
    $("#exportador_"+num).val(nome);
    $("#lista_"+num).html('');
 //   alert(id+' aqui '+valor);
}

function exclui_linha(num){
    $("#linha_"+num).remove();
}

function verifica_due(valor, id){
    
    $.ajax({
        method: "POST",
        url: base_url+'areaTransito/verificaDueAjax',
        async: true,
        data: { nr_due    :   valor }
    }).done(function(data){
        
        var dados = $.parseJSON(data);
        
        var total = dados.retorno.existe[0].total;
       
        if(total > 0){
            
            swal({
                title: "Ops!",
                text: "Due já existe no Sistema. Verifique",
                type: 'warning'
            }).then(function() {
                $('#'+id).val('');
                $('#'+id).focus(); 
            }); 
           
        }
         
    });
}


//Verifica a Ordem da DUE e exportador
function verificaOrdem(id){
    var total_linhas = $("#tabela_due tr").length;

    if($('#'+id).val() > (total_linhas-1)){
        swal({
            title: "Ops!",
            text: "Número de ordem inválido, está maior que o número de Dues lançadas. Verifique!",
            type: 'warning'
        }).then(function() {
           $('#'+id).val('');
           $('#'+id).focus();
        }); 
    }
    var due_array = [];
    var linha_array = [];

    for(var i=0; i < total_linhas; i++){
        if(typeof $('#nr_due_'+i).val() !== 'undefined'){
            
            due_array.push($('#nr_due_'+i).val());
            due_array.push($('#id_empresa_'+i).val());
            due_array.push($('#ordem_'+i).val());

        }
        
    }
    var existe = 0;
    var x = 1;
    var y = 1;
    var linha_x = '';
    var linha_y = '';
    var d = 0;//contador de duplicidades
    for(var w=0; w < due_array.length; w++){
       
        if(x == 3 ){
            linha_x += '_'+due_array[w];
           // alert('Nr Ordem: '+linha_x);
            for(var z=0; z < due_array.length; z++){
                if(y == 3 ){
                    linha_y += '_'+due_array[z];

                    if(linha_x == linha_y){
                        d++;
                        if(d > 1){
                            //alert('existe: '+ linha_y);
                            swal({
                                title: "Ops!",
                                text: "Due, Exportador e Número de ordem já existem no Sistema. Verifique!",
                                type: 'warning'
                            }).then(function() {
                               $('#'+id).val('');
                               $('#'+id).focus();
                            }); 
                        } 

                    }

                    linha_y = '';
                    y = 1;
                }else{
                    linha_y += due_array[z];
                    y++;
                }

            }
            d = 0;
            linha_x = '';
            x = 1;
        }else{
            linha_x += due_array[w];
            x++;
        }
    }
}

;(function($){ 
	$.fn.datetimepicker.dates['pt-BR'] = {
        format: 'dd/mm/yyyy',
		days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo"],
		daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
		daysMin: ["Do", "Se", "Te", "Qu", "Qu", "Se", "Sa", "Do"],
		months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
		monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
		today: "Hoje",
		suffix: [],
		meridiem: []
	};
}(jQuery));

//Função feita para retornar a data em ordem para comparação
function retornaDataHoraInteira(vl_data) {

    var datahora = '';    

    if (vl_data != ''){

        var divide = vl_data.split(" ");
        var dv_data = divide[0].split("/");
        var dv_hora = divide[1].split(":");

        var datahora = dv_data[2]+dv_data[1]+dv_data[0]+dv_hora[0]+dv_hora[1];
    }
	return datahora;
}