$(document).ready(function(){

	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$(".peso").mask("999.999.999.999", { reverse : true});
    $("#placa").inputmask({mask: ['AAA-9999','AAA-9A99']});
	
	$('#placa').keypress(function(e){ 
		var k = e.which || e.keyCode; 
		if(k==13){
			e.preventDefault(); 
		}
	});

    $("#placa").blur(function(e){
        
        if($("#placa").val() != ""){
            $.ajax({
                method: "POST",
                url: base_url+'areaTransito/verificaPesagemAjax/',
                async: true,
                data: { placa : $("#placa").val() }
            }).done(function(data){
               
                var dados = $.parseJSON(data);
                
                if (dados.retorno.existe == null){
                    var existe =  0;
                }else{
                    var existe = dados.retorno.existe[0].total;
                }
                
               
                if(existe < 1){
                    swal({
                        title: "Ops!",
                        text: "Veículo não possui registro de pesagem/saída. Verifique",
                        type: 'warning'
                    }).then(function() {
                        $("#placa").val('');
                    }); 
                }else{

                    $.ajax({
                        method: "POST",
                        url: base_url+'areaTransito/verificaSiscomexAjax/',
                        async: true,
                        data: { placa : $("#placa").val() }
                    }).done(function(data){
                       
                        var dados = $.parseJSON(data);
 
                        if(dados.retorno.existe[0].total < 1){
                            swal({
                                title: "Ops!",
                                text: "Entrega de Carga deste veículo não foi efetuada no Siscomex. Verifique",
                                type: 'warning'
                            }).then(function() {
                                $("#placa").val('');
                            }); 
                        }
                    
                    });

                }
            
            });
        }


    }); 

//Função que executa ao clicar no botão emitir
    $("#btn_emite").click(function(e){
        
        if($("#placa").val() == ""){

            swal({
                title: "Ops!",
                text: "Por favor informe a placa para emissão do extrato.",
                type: 'warning'
            }).then(function() {
                $("#placa").val('');
            }); 
        }
    });
	
});

