$(document).ready(function(){
			$('#cnpj').mask('00.000.000/0000-00');
			$('#cpf').mask('000.000.000-00');
			$('#cep').mask('00000-000');
			$('#bicos').mask('00000');				
			$('.celular').mask('(00) 00000 - 0000');

			$('#formulario').submit(function(){
					 
				$('#enviar').attr('disabled', 'disabled');
					 
			});
			
			$('#cnpj').bind('focusout', function(){
				var cnpj = $(this).val(); 
				if(isCNPJValid(cnpj)){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_cnpj');?>",
						async: true,
						data: { cnpj 	:	cnpj }
						}).success(function( data ) {
							var dados = $.parseJSON(data);		 

							if(dados.length > 0){

								swal({
									title: 'CNPJ já cadastrado no sistema!',
									text: 'Por favor, entre em contato com o representante legal da sua empresa para ele realizar seu cadastro no sistema da WERTCO',
									type:'warning'
								}).then(function(isConfirm) {
									$('#cnpj').val('');								
									$('#razao_social').val('');									
									$('#fantasia').val('');									
									$('#telefone').val('');									
									$('#endereco').val('');										
									$('input[name="posto_rede"]').val('');									
									$('#nr_bombas').val('');												
									$('#bicos').val('');									
									$('input[type="checkbox"]').val('');									
									$('input[name="software_utilizado"]').val('');
									$('input[name="mecanico_atende"]').val('');									
									
								});
								

							}							
					});	
				} else {
					swal({
						title: 'CNPJ inválido!',
						text: 'Favor verificar o CNPJ informado',
						type:'error'
					}).then(function(isConfirm) {
						$('#cnpj').val('');
						$('#cnpj').focus();
					})
				}
			});
			
			function isCNPJValid(cnpj) {  
			
				var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
				if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
					return false;
				for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
				if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
					return false; 
				for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
				if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
					return false; 
				return true; 
			};

			$('#cpf').bind('focusout', function(){
				var cpf = $(this).val();
				if(cpf != ""){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_cpf');?>",
						async: true,
						data: { cpf 	: 	cpf }
					}).success(function( data ) {
						var dados = $.parseJSON(data);
							if(dados.status == 'erro'){
								swal(
							  		'Ops!',
							  		dados.mensagem,
							  		'error'
								)
								$('#cpf').val('');	
							} 
						
					});
				}
			});

			$('#email_pessoal').bind('focusout', function(){
				var email = $(this).val();
				if(email != ""){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_email');?>",
						async: true,
						data: { email 	: 	email }
					}).success(function( data ) {
						var dados = $.parseJSON(data);
							if(dados.status == 'erro'){
								swal(
							  		'Ops!',
							  		dados.mensagem,
							  		'error'
								)
								$('#email_pessoal').val('');	
							}
						
					});
				}
			});

			$('#senha2').bind('focusout', function(){		
				var senha 	= 	$('#senha').val();
				var senha2 	= 	$('#senha2').val();
				if( senha != senha2 ){
					swal(
				  		'Ops!',
				  		'Senha não confere, tente novamente',
				  		'error'
					)
					$('#senha').val('');
					$('#senha2').val('');
				}	
			});
		});