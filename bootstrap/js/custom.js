(function($) {
	
    "use strict";
		
	$(window).on("load", function() { 	

		// PRELOADER
		var d = new Date();
		var n = d.getTime();;
		(function($, window, document, undefined) {
			var s = document.body || document.documentElement,
				s = s.style,
				prefixTransition = "";

			if (s.WebkitTransition === "") prefixTransition = "-webkit-";
			if (s.MozTransition === "") prefixTransition = "-moz-";
			if (s.OTransition === "") prefixTransition = "-o-";

			$.fn.extend({
				onCSSTransitionEnd: function(callback) {
					var $this = $(this).eq(0);
					$this.one("webkitTransitionEnd mozTransitionEnd oTransitionEnd otransitionend transitionend", callback);
					if ((prefixTransition == "" && !("transition" in s)) || $this.css(prefixTransition + "transition-duration") == "0s") {
						callback();
					} else {
						var arr_1 = $this.css(prefixTransition + "transition-duration").split(", ");
						var arr_2 = $this.css(prefixTransition + "transition-delay").split(", ");
						length = 0;
						for (var i = 0; i < arr_1.length; i++) {
							length = parseFloat(arr_1[i]) + parseFloat(arr_2[i]) > length ? parseFloat(arr_1[i]) + parseFloat(arr_2[i]) : length;
						};
						var d = new Date();
						var l = d.getTime();
						if ((l - n) > length * 1000) {
							callback();
						}
					}
					return this;
				}
			});
		})(jQuery, window, document);
		$("#preloader").addClass("loading");
		$("#preloader #loader").onCSSTransitionEnd(function() {
			$("#preloader").addClass("ended");
		});
		
		//FILTERABLE PORTFOLIO
		$(".simplefilter li").on("click", function() {
			$(".simplefilter li").removeClass("active");
			$(this).addClass("active");
		});
		var options = {
			animationDuration: 0.6,
			filter: "all",
			callbacks: {
				onFilteringStart: function() {},
				onFilteringEnd: function() {}
			},
			delay: 0,
			delayMode: "alternate",
			easing: "ease-out",
			layout: "sameSize",
			selector: ".filtr-container",
			setupControls: true
		}
		/*var filterizd = $(".filtr-container").filterizr(options);
		filterizd.filterizr("setOptions", options);*/

	});	
			
	$(document).ready(function () {

		$('#telefone').mask('(99) 9999-99999');
			
		$("#bombas_ico").bind('click', function(){
			$('.bombas_ico').attr('src',base_url+'bootstrap/img/bombaamarelaM.png');	
		});		
		$(".bombas_menu").bind('click', function(){
			$('.bombas_ico').attr('src',base_url+'bootstrap/img/bombabranca.png');
		});
			
		// REMOVE # FROM URL
		$("a[href='#']").on("click", (function(e) {
			e.preventDefault();
		}));
		
		// LOGOS SLIDER
		$("#bxslider").bxSlider({
			minSlides: 1,
			maxSlides: 6,
			slideWidth: 200,
			slideMargin: 30,
			ticker: true,
			speed: 30000
		});	

		// INITIALIZING MAGNIFIC POPUP
		jQuery(".magnific-popup-gallery").each(function() {
			magnific_popup_init(jQuery(this))
		});
		
		
			
		jQuery(".mfp-youtube").magnificPopup({
			type: "iframe",
			mainClass: "mfp-fade",
			removalDelay: 0,
			preloader: false,
			fixedContentPos: false,
			iframe: {
				patterns: {
					youtube: {
						src: "https://youtube.com/embed/%id%?autoplay=1&rel=0"
					},
				}
			}
		});
		jQuery(".mfp-vimeo").magnificPopup({
			type: "iframe",
			mainClass: "mfp-fade",
			removalDelay: 0,
			preloader: false,
			fixedContentPos: false,
			iframe: {
				patterns: {
					vimeo: {
						src: "https://player.vimeo.com/video/%id%?autoplay=1"
					}
				}
			}
		});
		function magnific_popup_init(item) {
			item.magnificPopup({
				delegate: "a[data-gal^='magnific-pop-up']",
				type: "image",
				removalDelay: 500,
				mainClass: "mfp-zoom-in",
				fixedContentPos: false,
				callbacks: {
					beforeOpen: function() {
						// just a hack that adds mfp-anim class to markup 
						this.st.image.markup = this.st.image.markup.replace("mfp-figure", "mfp-figure mfp-with-anim");
					}
				},
				gallery: {
					enabled: true
				}
			});
					
		}
		
		// BACK TO TOP
		$("#back-top a").on("click", function() {
			$("body,html").stop(false, false).animate({
				scrollTop: 0
			}, 800);
			return false;
		});

		// TESTIMONIAL CAROUSEL
		$("#quote-carousel").carousel({
			wrap: true,
			interval: 6000
		});
		
		// TESTIMONIAL CAROUSEL TOUCH OPTIMIZED
		var cr = $("#quote-carousel");
		cr.on("touchstart", function(event){
			var xClick = event.originalEvent.touches[0].pageX;
			$(this).one("touchmove", function(event){
				var xMove = event.originalEvent.touches[0].pageX;
				if( Math.floor(xClick - xMove) > 5 ){
					cr.carousel('next');
				}
				else if( Math.floor(xClick - xMove) < -5 ){
					cr.carousel('prev');
				}
			});
			cr.on("touchend", function(){
					$(this).off("touchmove");
			});
		});

		// BUTTON ICON ANIMATION
		var btn_hover = "";
		$(".custom-button").each(function() {
			var btn_text = $(this).text();
			$(this).addClass(btn_hover).empty().append("<span data-hover='" + btn_text + "'>" + btn_text + "</span>");
		});

		// SINGLE PAGE SCROLL
		$("#singlepage-nav").singlePageNav({
			offset: 0,
			filter: ":not(.nav-external)",
			updateHash: 0,
			currentClass: "current",
			easing: "swing",
			speed: 750
		});
		
		// LOCAL SCROLL
		$(".scroll-to-target[href^='#']").on("click", function(scroll_to_target) {
			scroll_to_target.preventDefault();
			var a = this.hash,
				i = $(a);
			$("html, body").stop().animate({
				scrollTop: i.offset().top
			}, 900, "swing", function() {})
		}	)
		
		// HAMBURGER ICON ANIMATION
		$(".link-menu").on("click", function(){
			if( $(this).hasClass('link-externo') ){
				if( $(this).attr('link') == 'area_restrita' ){
					window.open(base_url+'home/area_restrita');
				}else if( $(this).attr('link') == 'orcamento' ){
					window.open(base_url+'home/orcamento');
				}else if( $(this).attr('link') == 'capacitacao' ){
					window.open(base_url+'tecnicos/cadastro/1');
				}
			} 
			$("#icon-toggler").removeClass("open");
		});

		$("#icon-toggler").on("click", function(){
			$(this).toggleClass("open");
		});
		
		
		
		/********************************************************************************
		*********************************************************************************
		**************************	Início Cadastro de Newletter	*********************
		*********************************************************************************
		*********************************************************************************/
		
		$("#enviar_newsletter").bind('click', function(){
			var email = $("#email_newsletter").val();
			
			if(email != "" && valida_email(email) == true){
				$.ajax({
					method: "POST",
					url:  base_url+"newsletter/cadastra",
					async: true,
					data: { email	:	email }
					}).success(function( data ) {
						var dados = $.parseJSON(data);		
						if(dados.retorno == "sucesso"){
							$("#output_message_newsletter").text('Obrigado pelo interesse. Em breve você receberá as novidades da Wertco!');
							$("#output_message_newsletter").css('color','#6cd015');
						}else if(dados.retorno == "E-mail já cadastrado"){
							$("#output_message_newsletter").text(dados.retorno);
							$("#output_message_newsletter").css('color','#f14343');
						}else{
							$("#output_message_newsletter").text("Erro, entre em contato com a Wertco");
							$("#output_message_newsletter").css('color','#6cd015');
						}
					});	
			}else{
				$("#output_message_newsletter").text('Insira um e-mail corretamente');
				$("#output_message_newsletter").css('color','#f14343');
			}
		});
				
		/********************************************************************************
		*********************************************************************************
		**************************	Fim Cadastro de Newletter	*************************
		*********************************************************************************
		*********************************************************************************/
		
		/********************************************************************************
		*********************************************************************************
		**************************	Início Fale Conosco		*****************************
		*********************************************************************************
		*********************************************************************************/
		
		$("#form-submit").bind('click', function(){
			var email 		= 	$("#email_contact").val();
			var firstname 	= 	$("#firstname").val();
			var lastname	= 	$("#lastname").val();
			var message 	= 	$("#message").val();
			var phone 		= 	$("#telefone").val();
						
			if((email != "" && valida_email(email) == true) && message != "" ){
				$(this).after('<img src="'+base_url+'bootstrap/img/loading.gif" id="loading" />');
				$(this).unbind('click');
				$.ajax({
					method: "POST",
					url: base_url+"contato/cadastra",
					async: true,
					data: { email	 	:	email,
							firstname 	: 	firstname,
							lastname	:	lastname,
							message		:	message,
							phone 		: 	phone}
					}).success(function( data ) {
						var dados = $.parseJSON(data);		 
						if(dados.retorno == "sucesso"){
							$("#contato").text('Obrigado pelo interesse. Em breve você receberá as novidades da Wertco!');
							$("#contato").css('color','#6cd015');
						}else if(dados.retorno == "Mensagem já enviada."){
							$("#contato").text(dados.retorno);
							$("#contato").css('color','#f14343');
						}else{
							$("#contato").text("Erro, entre em contato com a Wertco");
							$("#contato").css('color','#6cd015');
						}
						
						$("#loading").remove();
						$(this).bind('click');
					});	
			}else{
				$("#contato").text('Preencha todos os campos corretamente');
				$("#contato").css('color','#f14343');
			}
		});
				
		/********************************************************************************
		*********************************************************************************
		**************************	Fim Cadastro de Newletter	*************************
		*********************************************************************************
		*********************************************************************************/
		
		/********************************************************************************
		*********************************************************************************
		**************************	Início Trabalhe Conosco		*************************
		*********************************************************************************
		*********************************************************************************/
		$('#form_curriculo').bind('submit', function(){
						
			// Captura os dados do formulário
			var formulario = document.getElementById('form_curriculo');
			// Instância o FormData passando como parâmetro o formulário
			var formData = new FormData(formulario);
		    $(this).after('<img src="'+base_url+'bootstrap/img/loading.gif"  id="loading" width="100" style="margin: 0 43%;" />');
			
		    $.ajax({
		        url: base_url+'Home/trabalheConosco',
		        type: 'POST',
		        data: formData,
		        cache: false,
		        contentType: false,
		        processData: false	
		    }).success(function( data ) {

	        	var dados = $.parseJSON(data);		        	
	    		if(dados.retorno == "sucesso"){
					$("#trabalhe").text('Obrigado, suas informações estão salvas em nossa base de dados!');
					$("#trabalhe").css('color','#6cd015');
				}else{
					$("#trabalhe").text("Erro, entre em contato com a Wertco");
					$("#trabalhe").css('color','#6cd015');
				}
					
				$("#loading").remove();
				$(this).bind('click');	        
			});

			

		    $(this).unbind('submit');
		    return false;
		});
		


		/********************************************************************************
		*********************************************************************************
		**************************	Fim Trabalhe Conosco		*************************
		*********************************************************************************
		*********************************************************************************/


		
		/********************************************************************************
		*********************************************************************************
		**************************	INÍCIO BOMBA DIFERENCIAIS   *************************
		*********************************************************************************
		*********************************************************************************/
		
		$(".portico").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');

			if( expanded === 'false' ){
				$('.portico_i').removeClass('fa-plus-circle').addClass('fa-minus-circle'); 
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/portico.png'); 
			
			}else{
				$('.portico_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');

			}
		});
		
		$(".bicos").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.bicos_i').removeClass('fa-plus-circle').addClass('fa-minus-circle'); 
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/bicos.png');
			}else{
				$('.bicos_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});
		
		$(".base").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.base_i').removeClass('fa-plus-circle').addClass('fa-minus-circle'); 
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/baserobusta.png');
			}else{
				$('.base_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});
		
		$(".mostrador").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.mostrador_i').removeClass('fa-plus-circle').addClass('fa-minus-circle'); 
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/painelfrontal.png');
			}else{
				$('.mostrador_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});
		
		$(".multimidia").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.multimidia_i').removeClass('fa-plus-circle').addClass('fa-minus-circle'); 
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/multimidia.png');
			}else{
				$('.multimidia_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});
		
		$(".indicadores").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.indicadores_i').removeClass('fa-plus-circle').addClass('fa-minus-circle'); 
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/indicadordeprodutos.png');
			}else{
				$('.indicadores_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});
		$(".teclado").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.teclado_i').removeClass('fa-plus-circle').addClass('fa-minus-circle'); 
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/teclado.png');
			}else{
				$('.teclado_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});		
		
		$(".display").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.display_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/mostrador.png');
			}else{
				$('.display_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});
		
		$(".smc").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.smc_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/caracteristica2.png');
			}else{
				$('.smc_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});
		
		$(".revestimentos").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.revestimentos_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/caracteristica2.png');
			}else{
				$('.revestimentos_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});

		$(".teclado").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.teclado_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/caracteristica2.png');
			}else{
				$('.teclado_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});


		$(".certificacoes").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.certificacoes_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/caracteristica2.png');
			}else{
				$('.certificacoes_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});

		$(".manutencao").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.manutencao_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/caracteristica2.png');
			}else{
				$('.manutencao_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});
		
		$(".eletronica").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.eletronica_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/painelfrontalcompanyteccinza.png');
			}else{
				$('.eletronica_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});
		
		$(".conectividade").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.conectividade_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/painelfrontalblueethercinza.png');
			}else{
				$('.conectividade_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$("#altera_img").attr('src','');
				$('#altera_img').hide();
			}
		});
		
		$(".antifraude").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.antifraude_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/baserobustaokpulser.png');
			}else{
				$('.antifraude_i').removeClass('fa-minus-circle').addClass('fa-plus-circle'); 
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});
		
		$(".totalizador").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.totalizador_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/totalizador.png');
			}else{
				$('.totalizador_i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});

		$(".segura").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.segura_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/segura.png');
			}else{
				$('.segura_i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});


		$(".identfid").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.identfid_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/identfid.png');
			}else{
				$('.identfid_i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
				$('#altera_img').hide();
				$("#altera_img").attr('src','');
			}
		});

		$(".armazenamento").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$('.armazenamento_i').removeClass('fa-plus-circle').addClass('fa-minus-circle');	
				$("#altera_img").attr('src',base_url+'bootstrap/img/bombas/armazenamento.png');
			}else{
				$('.armazenamento_i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
				$('#altera_img').hide();				
				$("#altera_img").attr('src','');
			}
		});
		
		
		
		/*$("#portico").bind('click', function(){
			$("#altera_img").attr('src','portico.png');
		});*/
		
		/********************************************************************************
		*********************************************************************************
		**************************	FIM BOMBA DIFERENCIAIS   ****************************
		*********************************************************************************
		*********************************************************************************/
	});

	function uploadCurriculo(){
		var formData 	= 	new FormData(this);
		var email 		= 	$("#email_curriculo").val();
		var nome 		= 	$("#nome").val();
		var sobrenome	= 	$("#sobrenome").val();
		var message 	= 	$("#message_curriculo").val();
		var phone 		= 	$("#telefone_curriculo").val();
		var curriculo	= 	$("#curriculo").val();

		
					
		/*if((email != "" && valida_email(email) == true) && message != "" && curriculo != "" ){
			$(this).after('<img src="'+base_url+'bootstrap/img/loading.gif" id="loading" />');
			$(this).unbind('click');*/
			$.ajax({
				method: "POST",
				url: base_url+"Home/trabalheConosco",				
				data: { email	 	:	email,
						nome 		: 	nome,
						sobrenome	:	sobrenome,
						message		:	message,
						phone 		: 	phone,
						curriculo 	: 	curriculo,
						dados 		: 	formData}
				}).success(function( data ) {
					var dados = $.parseJSON(data);		 
					
					
					$("#loading").remove();
					$(this).bind('click');
				});	
			
		/*}else{
			return false;
			$("#contato").text('Preencha todos os campos corretamente');
			$("#contato").css('color','#f14343');
		}*/
		return false;			
	}	
	
	function valida_email(email)
	{
		var x=email;
		var atpos=x.indexOf("@");
		var dotpos=x.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
		{		
		return false;
		}
		return true;
	}
	$('[data-toggle="tooltip"]').tooltip();

	$(window).on("scroll", function() {
		
		// FIX HEADER ON SCROLL
		var e = $(window).scrollTop();
		$(window).height();
		e > 1 ? $(".header").addClass("header-fixed") : $(".header").removeClass("header-fixed");
		
		// BACK TO TOP
		if ($(this).scrollTop() > 100) {
			$("#back-top").fadeIn();
		} else {
			$("#back-top").fadeOut();
		}
		
	});
	
	

})(jQuery);