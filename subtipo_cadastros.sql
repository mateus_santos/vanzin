-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 15-Ago-2020 às 20:31
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `wertco`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `subtipo_cadastros`
--

CREATE TABLE `subtipo_cadastros` (
  `id` int(11) NOT NULL,
  `tipo_cadastro_pai` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `nvl` int(11) NOT NULL,
  `obs` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `subtipo_cadastros`
--

INSERT INTO `subtipo_cadastros` (`id`, `tipo_cadastro_pai`, `descricao`, `nvl`, `obs`) VALUES
(1, 4, 'Responsável Legal', 1, 'Possui acesso ao contrato de serviço, realiza a gestão dos funcionários que possuirão acesso ao sistema da wertco, acesso a solicitação e atualização do cartão técnico, downloads e gerador json.'),
(2, 4, 'Gerente Técnico', 2, 'Realiza a gestão dos funcionários que possuirão acesso ao sistema da wertco, acesso a solicitação e atualização do cartão técnico,  downloads e gerador json'),
(3, 4, 'Funcionário', 3, 'Possuirá acesso a solicitação e atualização do cartão técnico,  downloads e gerador json.');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `subtipo_cadastros`
--
ALTER TABLE `subtipo_cadastros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subtipo_cadastro_x_tipo_cadastro` (`tipo_cadastro_pai`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `subtipo_cadastros`
--
ALTER TABLE `subtipo_cadastros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `subtipo_cadastros`
--
ALTER TABLE `subtipo_cadastros`
  ADD CONSTRAINT `fk_subtipo_cadastro_x_tipo_cadastro` FOREIGN KEY (`tipo_cadastro_pai`) REFERENCES `tipo_cadastros` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
