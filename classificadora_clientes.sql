-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 19-Set-2020 às 01:16
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `vanzin`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `classificadora_clientes`
--

CREATE TABLE `classificadora_clientes` (
  `id` int(11) NOT NULL,
  `classificadora_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `classificadora_clientes`
--

INSERT INTO `classificadora_clientes` (`id`, `classificadora_id`, `cliente_id`) VALUES
(13, 3465, 3461),
(14, 3465, 2);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `classificadora_clientes`
--
ALTER TABLE `classificadora_clientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_classificadora_empresas` (`classificadora_id`),
  ADD KEY `fk_classificadora_cliente_empresas` (`cliente_id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `classificadora_clientes`
--
ALTER TABLE `classificadora_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `classificadora_clientes`
--
ALTER TABLE `classificadora_clientes`
  ADD CONSTRAINT `fk_classificadora_cliente_empresas` FOREIGN KEY (`cliente_id`) REFERENCES `empresas` (`id`),
  ADD CONSTRAINT `fk_classificadora_empresas` FOREIGN KEY (`classificadora_id`) REFERENCES `empresas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
